
@the-same-user-in-several-domains

Feature: Read permissions scenarios when the user with the same public key is added to several domains
  The tests assume the default set of domain read permissions: "admin", "manager", "trader", "director", "aml" for all endpoints

  @smoke
  @A
  @same-user-in-two-domains-different-roles-not-in-subdomain-read-permissions
  Scenario: User can read data of subdomain when it is added to top and subdomain with different roles. Subdomain permissions do not
    include any of the user's roles.
    Given a root domain admin user is logged in
    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And root domain admin user is added to "subdomain1" domain as "supervisor"

    And "subdomain1" domain read permissions are updated with the following
      | users        | manager, trader, director, aml |
      | domains      | manager, trader, director, aml |
      | endpoints    | manager, trader, director, aml |
      | policies     | manager, trader, director, aml |
      | accounts     | manager, trader, director, aml |
      | transactions | manager, trader, director, aml |
      | requests     | manager, trader, director, aml |
      | events       | manager, trader, director, aml |

    Given "subdomain admin user" is logged in
    When request to read accounts list is made
    Then get requests fail with error "PermissionDeniedError"

    Given "supervisor" is logged in
    When request to read accounts list is made
    Then requests are succeeded

    When request to read transactions list is made
    Then requests are succeeded

    When request to read users list is made
    Then requests are succeeded

    When request to read domains list is made
    Then requests are succeeded

    When request to read policies list is made
    Then requests are succeeded

    When request to read endpoints list is made
    Then requests are succeeded

    When request to read events list is made
    Then requests are succeeded

    When a root domain admin user is logged in
    And the current domain is a root domain
    When request to read accounts list is made
    Then requests are succeeded

  @B
  @same-user-in-two-domains-different-roles-not-in-top-domain-read-permissions
  Scenario: User can read data of subdomain when it is added to top and subdomain with different roles, but top domain permissions do not include the role of user.
    Given a root domain admin user is logged in
    And the "supervisor" user is created

    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the current domain is a root domain
    And "root domain" domain "supervisor" user is added to "subdomain1" domain as "trader"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When request to read users list is made
    Then requests are succeeded

    Given "trader" is logged in
    When request to read users list is made
    Then requests are succeeded

    When the current domain is a root domain
    And "supervisor" is logged in
    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

  @E
  @same-user-in-two-domains-with-the-same-roles-not-in-top-domain-read-permissions
  Scenario: User can read data of subdomain when it is added to top and subdomain with different roles, but top domain permissions do not include the role of user.
    Given a root domain admin user is logged in
    And the "supervisor" user is created

    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the current domain is a root domain
    And "subdomain1" domain read permissions are updated with the following
      | users        | supervisor, admin, manager, trader, director, aml |
      | domains      | supervisor, admin, manager, trader, director, aml |
      | endpoints    | supervisor, admin, manager, trader, director, aml |
      | policies     | supervisor, admin, manager, trader, director, aml |
      | accounts     | supervisor, admin, manager, trader, director, aml |
      | transactions | supervisor, admin, manager, trader, director, aml |
      | requests     | supervisor, admin, manager, trader, director, aml |
      | events       | supervisor, admin, manager, trader, director, aml |

    And "root domain" domain "supervisor" user is added to "subdomain1" domain as "supervisor"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When request to read users list is made
    Then requests are succeeded

    Given "supervisor" is logged in
    When request to read users list is made
    Then requests are succeeded

  @D
  @same-user-in-two-domains-with-same-roles-not-in-subdomain-read-permissions
  Scenario: User can read data of subdomain when it is added to top and subdomain with the same roles, subdomain permissions do not include the role of user.
    Given a root domain admin user is logged in
    And the "trader" user is created

    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the current domain is a root domain
    And "root domain" domain "trader" user is added to "subdomain1" domain as "supervisor"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When request to read users list is made
    Then requests are succeeded

    Given "supervisor" is logged in
    When request to read users list is made
    Then requests are succeeded

  @F
  @same-user-in-two-domains-with-same-role-not-in-top-or-subdomain-read-permissions
  Scenario: User cannot read data of subdomain when it is added to top and subdomain with the same role, when neither top nor subdomain permissions include the role of user.
    Given a root domain admin user is logged in
    And the "supervisor" user is created

    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the current domain is a root domain
    And "root domain" domain "supervisor" user is added to "subdomain1" domain as "supervisor"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When request to read users list is made
    Then requests are succeeded

    Given "supervisor" is logged in
    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

  @C
  @same-user-in-two-domains-with-different-roles-not-in-top-or-subdomain-read-permissions
  Scenario: User cannot read data of subdomain when it is added to top and subdomain with different roles, when neither top nor subdomain permissions include the role of user.
    Given a root domain admin user is logged in
    And the "controller" user is created

    And the subdomain is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the current domain is a root domain
    And "root domain" domain "controller" user is added to "subdomain1" domain as "supervisor"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When request to read users list is made
    Then requests are succeeded

    Given "supervisor" is logged in
    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"