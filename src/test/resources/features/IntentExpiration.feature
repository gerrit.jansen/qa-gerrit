#https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
# https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/22
@ignore
@intent-expiration-tests

Feature: Intent expiration tests

  @smoke
  @intent-expires-before-approval
  Scenario: Intent expires before approval
    Given a root domain admin user is logged in
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager  |
      | simpleCondition2 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |

    And the policy is created

    And the "trader" user is created
    And the "manager" user is created

    Given intent expiration time is set to 1 minutes from now
    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When expiration timeout for intent is reached
    When "manager" user approves the intent
    Then the intent status is "Expired"

  @decision-expiration-before-next-decision
  Scenario: Intent expires before approval
    Given a root domain admin user is logged in
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager  |
      | simpleCondition2 quorum | 2        |

      | simpleCondition3 role   | director  |
      | simpleCondition3 quorum | 1         |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
      | 3 | simpleCondition3 |
    And the policy is created

    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "director" user number "1" is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given decision expiration time is set to 1 minute

    When "manager1" user approves the intent
    And decision intent succeeds

    When expiration timeout for decision is reached

    When "manager2" user approves the intent
    And decision intent succeeds

    When "director1" user approves the intent
    And decision intent succeeds
    
    Then the intent status is "Open"

