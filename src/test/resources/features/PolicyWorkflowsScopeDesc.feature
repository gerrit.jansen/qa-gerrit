@policy-workflow-tests
@scope-descendants
@scope-self-and-descendants
@candidate

Feature: Policies workflows. Scope Descendants, SelfAndDescendants
  Tests in the current set are focused on the policy workflow testing using the same condition expression. Testing scenarios are designed using the following testing criteria:

  1) number of applicable policies. Tag example @1-applicable-policy
  2) policy scope. Tag example @scope-self
  3) positive or negative actions in respect to the requirement of progress steps to be sequential. Tag example @negative-sequence
  4) positive or negative actions in respect to the requirement of user to be eligible to submit actions which are controlled by the policy. Tag example @negative-eligibility
  5) logical structures used in the workflow condition. Tag example @and-logical-path
  6) recursive complexity of the workflow condition structure. Tag example @2-workflow-recursive-complexity
  7) number of progress steps in the progress per policy. Tag example @2-workflow-progress-steps
  8) number of decisions in the progress step (quorum). Tag example @2-progress-step-decisions

  Workflow condition recursive complexity explanation:

  SimpleWorkflowCondition as a progress step - 0
  AND/OR condition with SimpleWorkflowCondition in the left/right - 1
  AND/OR condition with AND/OR condition (consisting of simple conditions) in the left/right - 2
  AND/OR condition with AND/OR condition (consisting of other AND/OR conditions ) in the left/right - 3 etc.

  Background:
    Given a root domain admin user is logged in
    And the subdomain is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in

  @smoke
  @2-applicable-policy
  @and-logical-path
  @1-workflow-recursive-complexity
  @2-workflow-progress-steps
  @2-progress-step-decisions

  @parallel-approvals-in-workflow-with-2-applicable-policies
  Scenario: Parallel approvals in workflow with 2 applicable policies.
    Given the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "AND" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | AND              |
    And the policy is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created

    And the policy has the following properties
      | scope               | Self                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the progress per policy from domain "subdomain1" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"

    Given the current domain is "subdomain1" domain
    And "trader" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    And "manager1" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    And "manager2" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    Given the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And "manager" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    And "admin" user approves the "subdomain2" intent
    And decision intent succeeds
    Then the intent status is "Executed"

    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

  @smoke
  @rejection-in-workflow-with-2-applicable-policies-top-domain-rejection
  Scenario: Rejection in workflow with 2 applicable policies. Reject from top domain
    Given the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "OR" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | OR              |
    And the policy is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created

    And the policy has the following properties
      | scope               | Self                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the progress per policy from domain "subdomain1" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"

    Given the current domain is "subdomain1" domain
    And "trader" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    And "manager1" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    And "manager2" user rejects the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Rejected"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Rejected"
    And intent submitter action is recorded in "Reject" decision of progress step "2"

    When "admin" user approves the "subdomain2" intent
    Then an error with "cannot be signed anymore" is returned

    Given the current domain is "subdomain2" domain
    When "manager" user approves the "subdomain2" intent
    Then an error with "cannot be signed anymore" is returned

  @rejection-in-workflow-with-2-applicable-policies-subdomain-rejection
  Scenario: Rejection in workflow with 2 applicable policies. Reject from subdomain
    Given the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "OR" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | OR              |
    And the policy is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created

    And the policy has the following properties
      | scope               | Self                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the progress per policy from domain "subdomain1" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"

    Given the current domain is "subdomain1" domain
    And "trader" user approves the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the current domain is "subdomain2" domain
    And "manager" user rejects the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Rejected"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Rejected"
    And intent submitter action is recorded in "Reject" decision of progress step "2"

    Given the current domain is "subdomain1" domain
    When "manager1" user approves the "subdomain2" intent
    Then an error with "cannot be signed anymore" is returned

  @rejection-in-workflow-with-2-applicable-policies-first-step-rejection-from-top-domain
  Scenario: Rejection in workflow with 2 applicable policies. First step rejection from top domain
    Given the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition1 |
      | 3 | simpleCondition3 |
    And the policy is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created
    And the "admin" user is created

    And the policy has the following properties
      | scope               | Self                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
      | 3 | simpleCondition3 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the progress per policy from domain "subdomain1" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"

    Given the current domain is "subdomain1" domain
    And "trader" user rejects the "subdomain2" intent
    And decision intent succeeds

    Then the intent status is "Rejected"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Rejected"
    And intent submitter action is recorded in "Reject" decision of progress step "1"

    Given the current domain is "subdomain2" domain
    
    And "manager" user approves the "subdomain2" intent
    Then an error with "cannot be signed anymore" is returned

    Given the current domain is "subdomain1" domain
    When "manager1" user approves the "subdomain2" intent
    Then an error with "cannot be signed anymore" is returned

  @smoke
  @workflow-with-2-applicable-policies-premature-decision
  Scenario: Workflow with 2 applicable policies. Premature decision
    Given the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition1 |
      | 3 | simpleCondition3 |
    And the policy is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created
    And the "admin" user is created

    And the policy has the following properties
      | scope               | Self                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
      | 3 | simpleCondition3 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the progress per policy from domain "subdomain1" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"

    Given the current domain is "subdomain1" domain
    And "manager1" user rejects the "subdomain2" intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"
    And the intent status is "Open"

    And "manager2" user rejects the "subdomain2" intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"
    And the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Open"

    When "trader" user approves the "subdomain2" intent
    And decision intent succeeds
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the current domain is "subdomain2" domain
    And "admin" user approves the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"
    And the intent status is "Open"

  @failed-workflow-and-policy-condition-update
  Scenario: Failed workflow and policy condition update
    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                              |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression |context.request.payload.roles.includes('trader') && !context.references['users'][context.request.author.id].roles.includes('admin') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader |
      | simpleCondition1 quorum | 1      |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "OR" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | OR              |
    And the policy is created
    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created
    And the "aml" user is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created
    And the "aml" user is created

    And the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | aml  |
      | simpleCondition1 quorum | 1    |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Failed"
    And intent details error "code" contains "NoApplicablePoliciesForDomain"
    And intent details error "message" contains "Author is not authorized to propose"

    When createUser intent is proposed by "manager" user
    Then the intent status is "Failed"
    And intent details error "code" contains "NoApplicablePoliciesForDomain"
    And intent details error "message" contains "No applicable policies for domain"

    When createUser intent is proposed by "aml" user
    Then the intent status is "Failed"
    And intent details error "code" contains "NoApplicablePoliciesForDomain"
    And intent details error "message" contains "No applicable policies for domain"

    Given "subdomain admin user" is logged in
    And policy is updated to have null condition
    When createUser intent is proposed by "aml" user
    Then the intent status is "Open"
    And the number of applicable policies is "2"

    Given the current domain is "subdomain1" domain
    And createUser intent is proposed by "aml" user
    Then the intent status is "Failed"
    And intent details error "code" contains "NoApplicablePoliciesForDomain"
    And intent details error "message" contains "Author is not authorized to propose"

  @3-applicable-policies
  @or-logical-path
  @1-workflow-recursive-complexity
  @3-workflow-progress-steps
  @2-progress-step-decisions
  @approval-workflow-in-third-level-domain
  Scenario: Approval workflow in third level domain
    Given the policy has the following properties
      | scope               | Descendants                                                                     |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader |
      | simpleCondition1 quorum | 1      |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2       |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "OR" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | OR |

    And the policy is created
    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "trader" user is created
    And the "manager" user is created

    And the policy has the following properties
      | scope               | SelfAndDescendants                                                              |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader  |
      | simpleCondition1 quorum | 1    |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    And subdomain number "3" is created
    And the current domain is "subdomain3" domain
    And "subdomain admin user" is logged in
    And the "manager" user is created
    And the "trader" user is created

    And the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader  |
      | simpleCondition1 quorum | 1    |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "3"

    Given the progress per policy from current domain is being verified

    Then the progress per policy contains correct policy reference
    And the progress per policy has "1" progress steps
    And the progress step "1" has state "Approved"

    Given the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    Given the progress per policy from domain "subdomain2" is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Open"

    When "trader" user approves the "subdomain3" intent
    And decision intent succeeds
    Then the intent status is "Open"

    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Given the current domain is "subdomain1" domain
    When "trader" user approves the "subdomain3" intent
    And decision intent succeeds

    Given the progress per policy from domain "subdomain1" is being verified
    And the progress per policy has "2" progress steps
    And the progress per policy contains correct policy reference
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager1" user approves the "subdomain3" intent
    And decision intent succeeds

    Given the progress per policy from domain "subdomain1" is being verified
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "manager2" user approves the "subdomain3" intent
    And decision intent succeeds

    Given the progress per policy from domain "subdomain1" is being verified
    And the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    Given the current domain is "subdomain2" domain
    When "manager" user approves the "subdomain3" intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    And the intent status is "Executed"