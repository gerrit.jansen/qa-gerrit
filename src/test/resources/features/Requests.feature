@basic-operations

Feature: User Requests

  Scenario: Users in a domain can list requests for that domain
    Given a root domain admin user is logged in
    And the "admin" user number "1" is created
    And the "admin" user number "2" is created

    When "admin1" is logged in
    Then the number of requests are > 0
    When "admin2" is logged in
    Then the number of requests are > 0

  Scenario: A User in a subdomain can list requests for that domain
    Given a root domain admin user is logged in
    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    Then the number of requests are zero
    When the "admin" user number "2" is created
    Then the number of requests are 1
    And the "admin" user number "3" is created
    Then the number of requests are 2


  Scenario: Two users in the same domain see only their me requests
    Given a root domain admin user is logged in
    And the "admin" user number "1" is created
    And "admin1" is logged in
    And subdomain number "1" is created, request as "subdomain1_request"
    And subdomain number "2" is created, request as "subdomain2_request"

    Given a root domain admin user is logged in
    And the "admin" user number "2" is created
    And "admin2" is logged in
    And subdomain number "3" is created, request as "subdomain3_request"

    When "admin1" is logged in
    Then the me requests are
      | subdomain1_request |
      | subdomain2_request |

    When "admin2" is logged in
    Then the me requests are
      | subdomain3_request |


  Scenario: A user in two domains can see me requests from both
    Given a root domain admin user is logged in
    And the "supervisor" user is created

    Given subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And the policy has the following properties
      | scope               | Self                                                                             |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('manager') |

    And all policy workflow structures use the following simple conditions
      | simpleCondition1 role | manager |

    And finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And "root domain" domain "supervisor" user is added to "subdomain1" domain as "manager"

    And subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in

    And the policy has the following properties
      | scope               | Self                                                                             |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('manager') |

    And all policy workflow structures use the following simple conditions
      | simpleCondition1 role | manager |

    And finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created


    And "root domain" domain "supervisor" user is added to "subdomain2" domain as "manager"

    And the current domain is "subdomain1" domain
    And "manager" is logged in


    Then the me requests count is 0

    Given subdomain number "3" is created, request as "subdomain3_request"
    And subdomain number "4" is created, request as "subdomain4_request"

    Then the me requests are
      | subdomain3_request |
      | subdomain4_request |

    Given the current domain is "subdomain2" domain
    And "manager" is logged in

    And subdomain number "5" is created, request as "subdomain5_request"
    And subdomain number "6" is created, request as "subdomain6_request"
    And subdomain number "7" is created, request as "subdomain7_request"

    Then the me requests are
      | subdomain3_request |
      | subdomain4_request |
      | subdomain5_request |
      | subdomain6_request |
      | subdomain7_request |

    Given the current domain is "root domain" domain
    And "supervisor" is logged in

    Then the me requests are
      | subdomain3_request |
      | subdomain4_request |
      | subdomain5_request |
      | subdomain6_request |
      | subdomain7_request |

    Given the current domain is "subdomain1" domain
    And "manager" is logged in

    Given subdomain number "8" is created, request as "subdomain8_request"

    Then the me requests are
      | subdomain3_request |
      | subdomain4_request |
      | subdomain5_request |
      | subdomain6_request |
      | subdomain7_request |
      | subdomain8_request |
