@policy-workflow-tests
@scope-self
@candidate

Feature: Policies workflows. Scope Self
  Tests in the current set are focused on the policy workflow testing using the same condition expression. Testing scenarios are designed using the following testing criteria:

  1) number of applicable policies. Tag example @1-applicable-policy
  2) policy scope. Tag example @scope-self
  3) positive or negative actions in respect to the requirement of progress steps to be sequential. Tag example @negative-sequence
  4) positive or negative actions in respect to the requirement of user to be eligible to submit actions which are controlled by the policy. Tag example @negative-eligibility
  5) logical structures used in the workflow condition. Tag example @and-logical-path
  6) recursive complexity of the workflow condition structure. Tag example @2-workflow-recursive-complexity
  7) number of progress steps in the progress per policy. Tag example @2-workflow-progress-steps
  8) number of decisions in the progress step (quorum). Tag example @2-progress-step-decisions

  Workflow condition recursive complexity explanation:

  SimpleWorkflowCondition as a progress step - 0
  AND/OR condition with SimpleWorkflowCondition in the left/right - 1
  AND/OR condition with AND/OR condition (consisting of simple conditions) in the left/right - 2
  AND/OR condition with AND/OR condition (consisting of other AND/OR conditions ) in the left/right - 3 etc.

  Background:
    Given a root domain admin user is logged in
    And the subdomain is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in


  @smoke
  @1-applicable-policy
  @and-logical-path
  @1-workflow-recursive-complexity
  @2-workflow-progress-steps
  @2-progress-step-decisions
  @approval-in-workflow-with-and-condition-simple-and
  Scenario: Approval in workflow with AND condition. SIMPLE->AND
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2        |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "AND" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | AND              |
    And the policy is created

    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager1" user approves the intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "admin" user approves the intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "manager2" user approves the intent
    And decision intent succeeds

    Then the intent status is "Executed"
    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

#    ignore till https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/580 is fixed

  @ignore
  @allow-user-to-reject-proposed-intent
  Scenario: User is allowed to reject proposed intent
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader  |
      | simpleCondition1 quorum | 1       |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
    And the policy is created

    And the "trader" user is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"

    When "trader" user rejects the intent
    Then decision intent succeeds

  @smoke
  @1-applicable-policy
  @or-logical-path
  @1-workflow-recursive-complexity
  @2-workflow-progress-steps
  @2-progress-step-decisions
  @approval-in-workflow-with-or-condition-simple-or
  Scenario: Approval in workflow with OR condition. SIMPLE->OR
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2        |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "OR" condition with the following data
      | left  | simpleCondition2 |
      | right | simpleCondition3 |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | OR               |

    And the policy is created

    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    When createUser intent is proposed by "trader" user
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager1" user approves the intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "admin" user approves the intent
    And decision intent succeeds

    Then the intent status is "Executed"
    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

  @1-applicable-policy
  @simple-logical-path
  @0-workflow-recursive-complexity
  @3-workflow-progress-steps
  @2-progress-step-decisions
  @approval-in-workflow-with-simple-conditions
  Scenario: Approval in workflow with simple conditions
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 2        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2        |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
      | 3 | simpleCondition3 |

    And the policy is created

    And the "trader" user number "1" is created
    And the "trader" user number "2" is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "admin" user is created

    When createUser intent is proposed by "trader1" user
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "trader2" user approves the intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager1" user approves the intent
    And decision intent succeeds
    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "manager2" user approves the intent
    And decision intent succeeds
    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "admin" user approves the intent
    And decision intent succeeds
    Then the intent status is "Executed"
    Given the progress per policy from current domain is being verified
    Then the progress step "3" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "3"

  @smoke
  @1-applicable-policy
  @simple-logical-path
  @0-workflow-recursive-complexity
  @2-workflow-progress-steps
  @2-progress-step-decisions
  @reject-an-intent-with-workflow-of-simple-conditions
  Scenario: Reject the intent with workflow of simple conditions
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |

    And the policy is created

    And the "trader" user is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And createUser intent is proposed by "trader" user

    When "manager1" user rejects the intent
    And decision intent succeeds

    Then the intent status is "Rejected"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "2" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Rejected"
    And intent submitter action is recorded in "Reject" decision of progress step "2"

  @smoke
  @negative-sequence
  @1-applicable-policy
  @simple-logical-path
  @0-workflow-recursive-complexity
  @3-workflow-progress-steps
  @2-progress-step-decisions
  @negative-test-of-approval-order-in-workflow-with-simple-conditions
  Scenario: Negative test of approval order in workflow with simple conditions
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 2        |

      | simpleCondition2 role   | admin    |
      | simpleCondition2 quorum | 1        |

      | simpleCondition3 role   | manager  |
      | simpleCondition3 quorum | 1        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |
      | 3 | simpleCondition3 |

    And the policy is created

    And the "trader" user number "1" is created
    And the "trader" user number "2" is created
    And the "manager" user is created
    And the "admin" user is created
    And createUser intent is proposed by "trader1" user

    When "admin" user approves the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"

    Given  "trader2" user approves the intent
    And decision intent succeeds
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Approved"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager" user approves the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"

    When "manager" user rejects the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"

    Given "admin" user approves the intent
    And decision intent succeeds
    Given the progress per policy from current domain is being verified
    And intent submitter action is recorded in "Approve" decision of progress step "2"
    And the progress step "2" has state "Approved"
    And the intent status is "Open"

    When "manager" user approves the intent
    And decision intent succeeds
    Given the progress per policy from current domain is being verified
    Then intent submitter action is recorded in "Approve" decision of progress step "3"
    Then the intent status is "Executed"

  @smoke
  @negative-eligibility
  @1-applicable-policy
  @simple-logical-path
  @0-workflow-recursive-complexity
  @2-workflow-progress-steps
  @2-progress-step-decisions
  @user-which-is-not-in-the-workflow-cannot-approve-or-reject-the-intent-simple-conditions
  Scenario: User which is not in the workflow cannot approve or reject the intent. Simple conditions
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 2        |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |

    And the policy is created
    And the "trader" user is created
    And the "director" user is created
    And createUser intent is proposed by "trader" user

    When "director" user approves the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"

    When "director" user rejects the intent
    Then decision intent request fails with correct details and hint includes "This user is not allowed to submit for the current workflow step"

  @1-applicable-policy
  @and-or-logical-path
  @2-workflow-recursive-complexity
  @3-workflow-progress-steps
  @3-progress-step-decisions
  @approval-in-workflow-with-or-condition-of-recursive-level-2-and-or-simple
  Scenario: Approval in workflow with OR condition of recursive level 2. AND->OR->SIMPLE
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | CreateUser                                                                      |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 2        |

      | simpleCondition2 role   | manager  |
      | simpleCondition2 quorum | 3        |

      | simpleCondition3 role   | admin    |
      | simpleCondition3 quorum | 1        |

    And policy workflow has "AND1" condition with the following data
      | left  | simpleCondition1 |
      | right | simpleCondition2 |

    And policy workflow has "AND2" condition with the following data
      | left  | simpleCondition1 |
      | right | simpleCondition3 |

    And policy workflow has "OR" condition with the following data
      | left  | AND1 |
      | right | AND2 |

    Then finally the policy workflow condition structure is
      | 1 | AND1             |
      | 2 | OR               |
      | 3 | simpleCondition3 |

    And the policy is created

    And the "trader" user number "1" is created
    And the "trader" user number "2" is created
    And the "trader" user number "3" is created
    And the "trader" user number "4" is created
    And the "manager" user number "1" is created
    And the "manager" user number "2" is created
    And the "manager" user number "3" is created
    And the "admin" user number "1" is created
    And the "admin" user number "2" is created

    When createUser intent is proposed by "trader1" user
    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "3" progress steps
    And the progress step "1" has state "Open"
    And the progress step "2" has state "Open"
    And the progress step "3" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "trader2" user approves the intent
    And decision intent succeeds

    Then the intent status is "Open"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager1" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "1" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager2" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "1" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager3" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"
    And the intent status is "Open"

    When "trader3" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "admin1" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "trader4" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "2" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "2"

    When "admin2" user approves the intent
    And decision intent succeeds

    Given the progress per policy from current domain is being verified
    Then the progress step "3" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "3"
    And the intent status is "Executed"


  @1-applicable-policy
  @and-logical-path
  @1-workflow-recursive-complexity
  @1-workflow-progress-steps
  @1-progress-step-decisions
  @one-step-approval-flow-with-and-condition
  Scenario: One step approval flow with AND condition
    Given the policy has the following properties
      | scope               | Self                                                                        |
      | intentTypes         | CreateUser                                                                  |
      | conditionExpression | context.request.payload.roles.includes('trader') && !context.references['users'][context.request.author.id].roles.includes('admin')|

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | trader   |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | manager |
      | simpleCondition2 quorum | 1        |

    And policy workflow has "AND" condition with the following data
      | left  | simpleCondition1 |
      | right | simpleCondition2 |

    Then finally the policy workflow condition structure is
      | 1 | AND |

    And the policy is created

    And the "trader" user is created
    And the "manager" user is created

    When createUser intent is proposed by "manager" user

    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "1" progress steps
    And the progress step "1" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "trader" user approves the intent
    And decision intent succeeds

    Then the intent status is "Executed"
    Given the progress per policy from current domain is being verified
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"