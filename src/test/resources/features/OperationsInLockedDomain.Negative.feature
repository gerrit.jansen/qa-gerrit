@basic-operations
@locked-domain-cases

Feature: Negative. Operations in a locked domain

  @candidate
  @smoke
  @modify-read-permissions-of-locked-domain
  Scenario: Attempts to modify locked domain from locked domain and from top domain, unlock domain and modify
    Given a root domain admin user is logged in
    And subdomain number "1" is created

    And subdomain lock intent is executed
    And subdomain lock state is "locked"

    Given created subdomain is a current domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When intent for read permissions update of "subdomain1" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|
    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "root domain"

    When subdomain unlock intent is is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When subdomain unlock intent is is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned

    Given the current domain is a root domain
    And a root domain admin user is logged in

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "subdomain1"

    When intent for read permissions update of "subdomain1" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|

    Then an error with "InvalidIntentError" is returned

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When "subdomain1" domain unlock intent is is submitted using the author and target domain specified above
    And request succeeds and submitted intent status is "Executed"

    When intent for read permissions update of "subdomain1" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|
    And request succeeds and submitted intent status is "Executed"

  @smoke
  @create-account-and-read-data-in-a-locked-domain
  Scenario: Create account in a locked domain
    Given the vault is configured
    And a root domain admin user is logged in
    And the subdomain is created

    And subdomain lock intent is executed
    And subdomain lock state is "locked"

    Given created subdomain is a current domain
    And "subdomain admin user" is logged in

    When create "bitcoin-testnet" account request is submitted
    Then an error with "PermissionDeniedError" is returned

    When request to read accounts list is made
    Then get requests fail with error "PermissionDeniedError"

  @create-account-and-read-data-in-a-coercive-locked-domain
  Scenario: Create account and read data in a coercive locked domain
    Given the vault is configured
    And a root domain admin user is logged in
    And coercive subdomain is created
    And the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 200                                                                       |

    And policy condition expression sets request author domain to "coercive subdomain 1"
    And all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    And finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And a root domain admin user is logged in
    And the "coercive subdomain 1" domain is locked by intent from "root domain"

    Given the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

    When create "bitcoin-testnet" account request is submitted
    Then an error with "PermissionDeniedError" is returned

    When request to read accounts list is made
    Then get requests fail with error "PermissionDeniedError"

    Given a root domain admin user is logged in
    And the current domain is "coercive subdomain 1" domain

    When request to read accounts list is made
    Then requests are succeeded