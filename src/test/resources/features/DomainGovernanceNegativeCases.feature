@basic-operations
@create-update-lock
@negative-domain-governance-cases

Feature: Domain governance negative cases for update, update permissions and lock

  Background:
    Given a root domain admin user is logged in
    And all policies except default are locked
    And the subdomain is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in

  @smoke
  @update-permissions-of-subdomain-from-top-domain-without-matching-policies
  Scenario: Negative. Top domain is not allowed to update permissions of subdomain without matching policies
    Given subdomain number "2" is created
    And the current domain is "subdomain1" domain

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain2"

    When intent for read permissions update of "subdomain2" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|

    And intent request succeeds
    Then intent is created in "subdomain2" domain and has status "Failed"

    Given the current domain is "subdomain1" domain
    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When intent for read permissions update of "subdomain2" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|

    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain2"
    And target domain is set to "subdomain2"

    When intent for subdomain read permissions update is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|

    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain2"
    And target domain is set to "subdomain1"

    When intent for subdomain read permissions update is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|

    Then an error with "PermissionDeniedError" is returned

  @update-subdomain-from-subdomain
  Scenario: Negative. Subdomain admin is not allowed to update subdomain details
    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When intent for subdomain alias update is submitted using the author and target domain specified above
    Then an error with "InvalidIntentError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "root domain"

    When intent for subdomain alias update is submitted using the author and target domain specified above
    Then an error with "InvalidIntentError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When intent for subdomain alias update is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When intent for subdomain alias update is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned

  @negative-lock-subdomain-from-subdomain
  Scenario: Negative. Subdomain admin is not allowed to lock domain
    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When "subdomain1" domain unlock intent is is submitted using the author and target domain specified above
    Then an error with "InvalidIntentError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "root domain"

    When "subdomain1" domain unlock intent is is submitted using the author and target domain specified above
    Then an error with "InvalidIntentError" is returned

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When "subdomain1" domain unlock intent is is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When "subdomain1" domain unlock intent is is submitted using the author and target domain specified above
    Then an error with "PermissionDeniedError" is returned
    
    @lock-top-domain-from-subdomain
    Scenario: Negative. Subdomain admin is not allowed to lock domain
      Given request author object is created and contains id of admin user from "subdomain1"
      And request author object contains domainId of "subdomain1"
      And target domain is set to "subdomain1"

      When "root domain" domain lock intent is is submitted using the author and target domain specified above
      Then an error with "PermissionDeniedError" is returned

      Given request author object is created and contains id of admin user from "subdomain1"
      And request author object contains domainId of "subdomain1"
      And target domain is set to "root domain"

      When "root domain" domain lock intent is is submitted using the author and target domain specified above
      Then an error with "PermissionDeniedError" is returned