@basic-operations

Feature: Create Tickers for Ethereum Tokens

  Scenario: Create a custom ticker for an ethereum token
    Given a root domain admin user is logged in
    Given we create an ethereum "ethereum-testnet-rinkeby" contract ticker "test_ticker" with "METACO TEST TOKEN 1" token
