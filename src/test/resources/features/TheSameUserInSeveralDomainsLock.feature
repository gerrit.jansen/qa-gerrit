@the-same-user-in-several-domains

Feature: The same user is several domains scenarios


  @same-user-in-several-domains-locked-in-the-middle-domain
  Scenario: User which is added to root and descendants can read domain endpoints when domain in the middle is locked or when the user is locked in one of the domains
    Given a root domain admin user is logged in
    And the "supervisor" user is created

    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    And "root domain" domain "supervisor" user is added to "subdomain1" domain as "manager"

    And subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And "root domain" domain "supervisor" user is added to "subdomain2" domain as "manager"

    And subdomain number "3" is created
    And the current domain is "subdomain3" domain
    And "subdomain admin user" is logged in
    And "root domain" domain "supervisor" user is added to "subdomain3" domain as "manager"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    Given the "subdomain2" domain is locked by intent from "subdomain1"

    And "manager" is logged in
    And the current domain is "subdomain2" domain

    When request to read users list is made
    Then requests are succeeded

    Given the current domain is "subdomain3" domain
    When request to read users list is made
    Then requests are succeeded

    Given the current domain is "subdomain1" domain
    When request to read users list is made
    Then requests are succeeded

    Given the current domain is "root domain" domain
    And a root domain admin user is logged in
    Given the "subdomain2" domain is unlocked by intent from "root domain"
    Then "subdomain2" domain is "unlocked"

    Given the "manager" user from "subdomain2" is locked

    When the current domain is "subdomain1" domain
    And "manager" is logged in

    And request to read users list is made
    Then requests are succeeded

    When the current domain is "subdomain2" domain
    And request to read users list is made
    Then requests are succeeded

    When the current domain is "subdomain3" domain
    And request to read users list is made
    Then requests are succeeded

  @smoke
  @same-user-in-several-domains-user-locked-in-middle-domain
  Scenario: User which is added to root and descendants and can unlock subdomain
    Given a root domain admin user is logged in
    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    And root domain admin user is added to "subdomain1" domain as "supervisor"

    And subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And root domain admin user is added to "subdomain2" domain as "admin"
    And the "admin" user from "subdomain2" is locked

    And subdomain number "3" is created

    Given the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "subdomain3" domain is locked by intent from "subdomain2"

    Given a root domain admin user is logged in
    And the current domain is a root domain

    Given request author object is created and contains id of "default" user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "subdomain3"

    When createUser intent is proposed using the author and target domain specified above
    Then an error with "DomainLockedError" is returned

    When the "subdomain3" domain is unlocked by intent from "root domain"
    Then "subdomain3" domain is "unlocked"

    Given target domain is set to "subdomain3"
    When createUser intent is proposed using the author and target domain specified above
    Then intent is created in "subdomain3" domain and has status "Executed"
