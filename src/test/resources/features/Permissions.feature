@basic-operations
@permissions
@smoke
@candidate

Feature: Tests of read access permissions for given user role

  @permission-based-user-access-to-all-endpoints-except-domain
  Scenario Outline: User can read only endpoints allowed in readAccess domain property (all endpoints except domain test)
    Given a root domain admin user is logged in
    And "root domain" domain read permissions are updated with the following
      |users        |users-reader-role, admin, manager, trader, director, aml        |
      |domains      |domains-reader-role, admin, manager, trader, director, aml      |
      |endpoints    |endpoints-reader-role, admin, manager, trader, director, aml    |
      |policies     |policies-reader-role, admin, manager, trader, director, aml     |
      |accounts     |accounts-reader-role, admin, manager, trader, director, aml     |
      |transactions |transactions-reader-role, admin, manager, trader, director, aml |
      |requests     |requests-reader-role, admin, manager, trader, director, aml     |
      |events       |events-reader-role, admin, manager, trader, director, aml       |

    And the "<UserRole>" user is created

    Given "<UserRole>" is logged in
    When request to read <List1> list is made
    Then requests are succeeded

    When request to read <List2> list is made
    Then get requests fail with error "PermissionDeniedError"
    When request to read <List3> list is made
    Then get requests fail with error "PermissionDeniedError"
    When request to read <List4> list is made
    Then get requests fail with error "PermissionDeniedError"
    When request to read <List5> list is made
    Then get requests fail with error "PermissionDeniedError"
    When request to read <List6> list is made
    Then get requests fail with error "PermissionDeniedError"


    Examples:
      | UserRole                 | List1        | List2        | List3        | List4        | List5        | List6        |
      | users-reader-role        | users        | endpoints    | policies     | accounts     | transactions | events       |
      | endpoints-reader-role    | endpoints    | accounts     | transactions | events       | users        | policies     |
      | policies-reader-role     | policies     | transactions | events       | users        | accounts     | endpoints    |
      | accounts-reader-role     | accounts     | events       | users        | transactions | endpoints    | policies     |
      | transactions-reader-role | transactions | users        | events       | endpoints    | policies     | accounts     |
      | events-reader-role       | events       | users        | endpoints    | policies     | accounts     | transactions |

#    to do for requests
  @permission-based-user-assess-to-domains-endpoint
  Scenario: User can read only endpoints allowed in readAccess domain property. Domain endpoint test
    Given a root domain admin user is logged in
    And "root domain" domain read permissions are updated with the following
      |users        |users-reader-role, admin, manager, trader, director, aml        |
      |domains      |domains-reader-role, admin, manager, trader, director, aml      |
      |endpoints    |endpoints-reader-role, admin, manager, trader, director, aml    |
      |policies     |policies-reader-role, admin, manager, trader, director, aml     |
      |accounts     |accounts-reader-role, admin, manager, trader, director, aml     |
      |transactions |transactions-reader-role, admin, manager, trader, director, aml |
      |requests     |requests-reader-role, admin, manager, trader, director, aml     |
      |events       |events-reader-role, admin, manager, trader, director, aml       |

    And the "domains-reader-role" user is created
    And the "users-reader-role" user is created
    And the "policies-reader-role" user is created
    And the "accounts-reader-role" user is created
    And the "transactions-reader-role" user is created
    And the "events-reader-role" user is created

    Given "domains-reader-role" is logged in
    When request to read domains list is made
    Then requests are succeeded

    When request to read endpoints list is made
    Then get requests fail with error "PermissionDeniedError"

    When request to read policies list is made
    Then get requests fail with error "PermissionDeniedError"

    When request to read accounts list is made
    Then get requests fail with error "PermissionDeniedError"

    When request to read transactions list is made
    Then get requests fail with error "PermissionDeniedError"

    When request to read events list is made
    Then get requests fail with error "PermissionDeniedError"

    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

    Given "users-reader-role" is logged in
    When request to read domains list is made
    Then get requests fail with error "RequesterLockedError"

    When request to read details of an item from domains list is made
    Then get requests fail with error "PermissionDeniedError"

    Given "policies-reader-role" is logged in
    When request to read domains list is made
    Then get requests fail with error "RequesterLockedError"

    When request to read details of an item from domains list is made
    Then get requests fail with error "PermissionDeniedError"

    Given "accounts-reader-role" is logged in
    When request to read domains list is made
    Then get requests fail with error "RequesterLockedError"

    When request to read details of an item from domains list is made
    Then get requests fail with error "PermissionDeniedError"

    Given "transactions-reader-role" is logged in
    When request to read domains list is made
    Then get requests fail with error "RequesterLockedError"

    Given "events-reader-role" is logged in
    When request to read domains list is made
    Then get requests fail with error "RequesterLockedError"

    When request to read details of an item from domains list is made
    Then get requests fail with error "PermissionDeniedError"

  @top-domain-user-can-read-data-of-subdomains
  Scenario: Top domain user can read data of subdomains
    Given a root domain admin user is logged in
    And "root domain" domain read permissions are updated with the following
      |users        |topdomain-admin-role, users-reader-role, admin, manager, trader, director, aml        |
      |domains      |domains-reader-role, admin, manager, trader, director, aml      |
      |endpoints    |endpoints-reader-role, admin, manager, trader, director, aml    |
      |policies     |policies-reader-role, admin, manager, trader, director, aml     |
      |accounts     |accounts-reader-role, admin, manager, trader, director, aml     |
      |transactions |transactions-reader-role, admin, manager, trader, director, aml |
      |requests     |requests-reader-role, admin, manager, trader, director, aml     |
      |events       |events-reader-role, admin, manager, trader, director, aml       |

    And subdomain number "1" is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in
    And "subdomain1" domain read permissions are updated with the following

      | users        | subdomain-users-reader-role, admin, manager, trader, director, aml                                  |
      | domains      | subdomain-domains-reader-role, domains-reader-role, admin, manager, trader, director, aml           |
      | endpoints    | subdomain-endpoints-reader-role, endpoints-reader-role, admin, manager, trader, director, aml       |
      | policies     | subdomain-policies-reader-role, policies-reader-role, admin, manager, trader, director, aml         |
      | accounts     | subdomain-accounts-reader-role, accounts-reader-role, admin, manager, trader, director, aml         |
      | transactions | subdomain-transactions-reader-role, transactions-reader-role, admin, manager, trader, director, aml |
      | requests     | subdomain-requests-reader-role, requests-reader-role, admin, manager, trader, director, aml         |
      | events       | subdomain-events-reader-role, events-reader-role, admin, manager, trader, director, aml             |

    And the "topdomain-admin-role" user is created
    And "topdomain-admin-role" is logged in
    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

    Given the current domain is a root domain
    And a root domain admin user is logged in
    And the "users-reader-role" user is created
    And the "user-without-permission" user is created
    And the "subdomain-users-reader-role" user is created

    Given "users-reader-role" is logged in
    Given the current domain is "subdomain1" domain

    When request to read users list is made
    Then requests are succeeded

    Given the current domain is a root domain
    Given "user-without-permission" is logged in
    Given the current domain is "subdomain1" domain

    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

    Given the current domain is a root domain
    Given "subdomain-users-reader-role" is logged in
    Given the current domain is "subdomain1" domain

    When request to read users list is made
    Then get requests fail with error "PermissionDeniedError"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" user from "subdomain1" domain is logged in
    And the "subdomain-users-reader-role" user is created
    Given "subdomain-users-reader-role" is logged in
    When request to read users list is made
    Then requests are succeeded

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    Given subdomain number "2" is created
    Given the current domain is "subdomain2" domain
    And "subdomain admin user" user from "subdomain1" domain is logged in
    When request to read users list is made
    Then requests are succeeded

    Given the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    Given subdomain number "3" is created
    Given the current domain is "subdomain3" domain
    And "subdomain admin user" user from "subdomain2" domain is logged in
    When request to read users list is made
    Then requests are succeeded

    Given "subdomain admin user" user from "subdomain1" domain is logged in
    And the current domain is "subdomain3" domain
    When request to read users list is made
    Then requests are succeeded
