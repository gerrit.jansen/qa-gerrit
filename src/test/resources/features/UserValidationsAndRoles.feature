@user-data-related

Feature: Users
  The file contains tests for verification of user related data
  - user public keys validations
  - list of users with the same public key
  - aggregation of all users roles per domain
  - me requests

  @user-roles
  Scenario: Listing Known User Roles are ordered and only those assigned to users
    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    And the "test" user number "1" with roles "admin,testc,testb" is created
    Then the known user roles is ordered
    And the known user roles only contain assigned user roles


    @two-users-with-the-same-public-key-cannot-be-added-to-the-same-domain-with-createUser-request
    Scenario: Two users with the same public keys cannot be added to the same domain with createUser request
      Given subdomain number "1" is created
      And the current domain is "subdomain1" domain
      And the "manager" user is created
      When intent to add "subdomain1" domain "manager" user to "subdomain1" domain as "director" is submitted
      Then an error with "already in use" is returned

  @two-users-with-the-same-public-key-cannot-be-added-to-the-same-domain-with-createDomain-request
  Scenario: Two users with the same public keys cannot be added to the same domain with createDomain request
    When create subdomain with two users having the same public key intent is submitted
    Then an error with "users do not contain unique public keys" is returned