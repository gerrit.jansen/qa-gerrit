@basic-operations
@create-update-lock-positive
@positive-domain-governance-cases
@smoke

Feature: Domain governance. Update, update permissions, lock domain positive cases

  Background:
    Given a root domain admin user is logged in
    And all policies except default are locked
    And the subdomain is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in


  @create-and-update-domain
  Scenario: Create and update domain
    Given subdomain number "2" is created
    And "subdomain admin user" user from "subdomain1" domain is logged in

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When intent for "subdomain2" domain alias update is submitted using the author and target domain specified above
    Then request succeeds and submitted intent status is "Executed"
    Then "subdomain2" domain revision after update is correct
    And updated "subdomain2" domain details are correct

    When "subdomain2" alias is updated
    Then "subdomain2" domain revision after update is correct

  @update-root-domain-alias-from-root-domain
  Scenario: Update root domain alias from root domain
    Given a root domain admin user is logged in

    When "root domain" alias is updated
    And "root domain" domain revision after update is correct
    And updated "root domain" domain details are correct

    When "root domain" alias is updated
    And "root domain" domain revision after update is correct

  @update-permissions-of-root-domain-from-root-domain
  Scenario: Update permissions of root domain from root domain
    Given a root domain admin user is logged in

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And target domain is set to "root domain"

    When intent for read permissions update of "root domain" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|
      |domains|admin, x,y,z                             |

    Then request succeeds and submitted intent status is "Executed"
    Then "root domain" permissions update details and revision are correct

    When intent for read permissions update of "root domain" is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one, aml   |
      |domains|admin, manager, trader, director, test, aml |

    Then request succeeds and submitted intent status is "Executed"
    Then "root domain" permissions update details and revision are correct

  @update-read-permissions-of-subdomain-as-a-subdomain-admin
  Scenario: Update read permissions of subdomain as a subdomain admin
    Given request author object is created and contains id of admin user from subdomain
    And request author object contains domainId of subdomain
    And target domain is set to subdomain id

    When intent for subdomain read permissions update is submitted using the author and target domain specified above
      |users|admin, manager, trader, director, test, one|
      |domains|admin, x,y,z                             |

    Then request succeeds and submitted intent status is "Executed"
    Then subdomain permissions update details and revision are correct

    When intent for subdomain read permissions update is submitted using the author and target domain specified above
      |users|admin, x, trader, y, test, one             |
      |domains|admin, x,y,manager                       |

    Then request succeeds and submitted intent status is "Executed"
    And subdomain permissions update details and revision are correct

  @update-subdomain-from-top-domain
  Scenario: Update subdomain of subdomain
    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" user from "subdomain2" domain is logged in
    And subdomain number "3" is created

    Given request author object is created and contains id of admin user from "subdomain2"
    And request author object contains domainId of "subdomain2"
    And target domain is set to "subdomain2"

    When intent for "subdomain3" domain alias update is submitted using the author and target domain specified above
    Then request succeeds and submitted intent status is "Executed"
    And "subdomain3" domain revision after update is correct
    Then updated "subdomain3" domain details are correct

    When intent for "subdomain3" domain alias update is submitted using the author and target domain specified above
    Then request succeeds and submitted intent status is "Executed"
    And "subdomain3" domain revision after update is correct

    @subdomain-should-be-locked-if-top-domain-was-locked
    Scenario: Subdomain should be locked if top domain was locked
      Given subdomain number "2" is created

      When the current domain is "subdomain1" domain
      And "subdomain admin user" user from "subdomain1" domain is logged in

      Given request author object is created and contains id of admin user from "subdomain1"
      And request author object contains domainId of "subdomain1"
      And target domain is set to "subdomain1"

      When subdomain lock intent is submitted using the author and target domain specified above
      When "subdomain2" domain lock intent is is submitted using the author and target domain specified above
      And request succeeds and submitted intent status is "Executed"
      Then "subdomain2" domain is "locked"
      And "subdomain2" domain revision after update is correct

      Given a root domain admin user is logged in
      And the current domain is a root domain
      Given request author object is created and contains id of admin user from "root domain"
      And request author object contains domainId of "root domain"
      Given target domain is set to "subdomain2"

      When createUser intent is proposed using the author and target domain specified above
      Then an error with "DomainLockedError" is returned


  @lock-and-unlock-subdomain-from-top-domain
  Scenario: Lock and unlock subdomain from top domain
    Given subdomain number "2" is created
    And the current domain is "subdomain1" domain

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain1"

    When "subdomain2" domain lock intent is is submitted using the author and target domain specified above
    And request succeeds and submitted intent status is "Executed"
    Then "subdomain2" domain is "locked"
    And "subdomain2" domain revision after update is correct

    When "subdomain2" domain unlock intent is is submitted using the author and target domain specified above
    And request succeeds and submitted intent status is "Executed"
    Then "subdomain2" domain is "unlocked"
    And "subdomain2" domain revision after update is correct

    When "subdomain2" domain lock intent is is submitted using the author and target domain specified above
    And request succeeds and submitted intent status is "Executed"
    Then "subdomain2" domain is "locked"
    And "subdomain2" domain revision after update is correct

    When "subdomain2" domain unlock intent is is submitted using the author and target domain specified above
    And request succeeds and submitted intent status is "Executed"
    Then "subdomain2" domain is "unlocked"

#    Providing the default policy of root domain has workflow which is not null
  @update-permissions-of-subdomain-from-top-domain-with-matching-policy
  Scenario: Update subdomain permissions from top domain with matching policies are provided
    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                                            |
      | intentTypes         | v0_UpdateDomainPermissions                                                                    |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('admin') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And the subdomain number "2" is created with Yes policy including update rights for "subdomain1" admin
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "subdomain1"
    And request author object contains domainId of "subdomain1"
    And target domain is set to "subdomain2"

    When intent for read permissions update of "subdomain2" is submitted using the author and target domain specified above
      |users|admin, manager, mm, director, test, one|

    Then intent request succeeds
    And intent is created in "subdomain2" domain and has status "Open"

    When "subdomain admin user" user approves the intent
    And decision intent succeeds

    Then the intent status is "Executed"
    And "subdomain2" permissions update details and revision are correct

    @lock-subdomain-from-top-domain-with-matching-policies
    Scenario: Lock subdomain of subdomain with matching policies
      Given the policy has the following properties
        | scope | Descendants |
        | rank  | 200                |
      And policy condition expression sets request author domain to "subdomain1"
      Given all policy workflow structures use the following simple conditions
        | simpleCondition1 role   | admin |
        | simpleCondition1 quorum | 1     |

      Then finally the policy workflow condition structure is
        | 1 | simpleCondition1 |

      And the policy is created

      Given subdomain number "2" is created

      Given the current domain is "subdomain2" domain
      And "subdomain admin user" user from "subdomain2" domain is logged in

      And the policy has the following properties
        | scope | SelfAndDescendants |
        | rank  | 200                |
      And policy condition expression sets request author domain to "subdomain1"
      Given all policy workflow structures use the following simple conditions
        | simpleCondition1 role   | admin |
        | simpleCondition1 quorum | 1     |

      Then finally the policy workflow condition structure is
        | 1 | simpleCondition1 |

      And the policy is created

      And subdomain number "3" is created

      Given "subdomain admin user" user from "subdomain1" domain is logged in
      Given request author object is created and contains id of admin user from "subdomain1"
      And request author object contains domainId of "subdomain1"
      And target domain is set to "subdomain2"

      When "subdomain3" domain lock intent is is submitted using the author and target domain specified above

      Given "subdomain admin user" user from "subdomain2" domain is logged in
      And the current domain is "subdomain2" domain
      And "subdomain admin user" user approves the intent
      And decision intent succeeds

      And request succeeds and submitted intent status is "Executed"
      Then "subdomain3" domain is "locked"
      And "subdomain3" domain revision after update is correct