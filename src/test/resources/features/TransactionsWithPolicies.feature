@transactions-with-policies

Feature: Policies workflows applied to release quarantined transfers and transactions intents

  Background:
    Given a root domain admin user is logged in
    And the vault is configured
#     creating accounts if none, asserting there are accounts with needed derivation path if there are accounts
    And pre-conditions for accounts balances recovery are met
    And all recovery transfers are released to accounts
    And all policies except default are locked


  @release-quarantined-transfers-and-workflow-null-condition
  Scenario: AND workflow, null condition for release quarantined transfers intent
    Given a root domain admin user is logged in
    And the "manager" user is created
    And the "trader" user is created
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | v0_ReleaseQuarantinedTransfers                                                  |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | manager  |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | trader  |
      | simpleCondition2 quorum | 1       |

    And policy workflow has "AND" condition with the following data
      | left  | simpleCondition1 |
      | right | simpleCondition2 |

    Then finally the policy workflow condition structure is
      | 1 | AND |

    And the policy is created

    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin transaction intent from "account1" to "account2" is executed
    And transaction ledger status is "Detected"

    When release "Bitcoin testnet" quarantined transfers to "account2" intent is proposed by "trader" user

    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress per policy has "1" progress steps
    And the progress step "1" has state "Open"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    When "manager" user approves the intent
    And decision intent succeeds

    Then the intent status is "Executed"
    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the progress step "1" has state "Approved"
    And intent submitter action is recorded in "Approve" decision of progress step "1"

    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    And a root domain admin user is logged in
    And the policy is locked
    And the bitcoin funds are sent back to account1

  @release-quarantined-transfers-and-workflow
  Scenario: Release quarantined transfers. AND workflow
    Given a root domain admin user is logged in
    And the "manager" user is created
    And the "trader" user is created
    Given the policy has the following properties
      | scope               | Self                                                                            |
      | intentTypes         | v0_ReleaseQuarantinedTransfers                                                  |
      | conditionExpression | context.references['users'][context.request.author.id].roles.includes('trader') |

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | manager  |
      | simpleCondition1 quorum | 1        |

      | simpleCondition2 role   | trader  |
      | simpleCondition2 quorum | 1       |

    And policy workflow has "AND" condition with the following data
      | left  | simpleCondition1 |
      | right | simpleCondition2 |

    Then finally the policy workflow condition structure is
      | 1 | AND |

    And the policy is created

    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin transaction intent from "account1" to "account2" is executed
    And transaction ledger status is "Detected"

    When release "Bitcoin testnet" quarantined transfers to "account2" intent is proposed by "trader" user

    Then the intent status is "Open"
    And the number of applicable policies is "1"

    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference

    When "manager" user approves the intent
    And decision intent succeeds
    Then the intent status is "Executed"

    And "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    And "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    And a root domain admin user is logged in
    And the policy is locked
    And the bitcoin funds are sent back to account1