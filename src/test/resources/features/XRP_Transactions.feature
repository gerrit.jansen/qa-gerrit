@transactions
@xrp-transactions

Feature: XRPL transactions

  Background:
    Given a root domain admin user is logged in
    And the vault is configured
#     creating accounts if none, asserting there are accounts with needed derivation path if there are accounts
    And pre-conditions for accounts balances recovery are met
    And all recovery transfers are released to accounts
    And all policies except default are locked

  @smoke
  @xrp-transaction-to-address-destination
  Scenario: XRPL transaction to address destination
    Given we have two recovered "xrpl-testnet" accounts
    When the xrp transaction intent from "account1" to address of "account2" is executed
    And transaction ledger status is "Confirmed"

    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    When release "XRPL testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    And the xrp funds are sent back to account1

  @xrp-transaction-to-account-destination
  Scenario: XRPL transaction to account destination
    Given we have two recovered "xrpl-testnet" accounts
    When the xrp transaction intent from "account1" to "account2" is executed
    And transaction ledger status is "Confirmed"

    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    When release "XRPL testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    And the xrp funds are sent back to account1

  @xrp-transaction-to-endpoint-destination
  Scenario: XRPL transaction to endpoint destination
    Given we have two recovered "xrpl-testnet" accounts
    And endpoint using "account2" address is created

    When the xrp transaction intent from "account1" to endpoint with "account2" address is executed
    And transaction ledger status is "Confirmed"

    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    When release "XRPL testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "XRPL testnet" after "outgoing" transaction
    Then "account2" has correct balances of "XRPL testnet" after "incoming" transaction

    And the xrp funds are sent back to account1

  @xrp-transaction-to-external-address-destination
  Scenario: XRPL transaction to external address destination
    Given we have two recovered "xrpl-testnet" accounts
    When the xrp transaction intent from "account1" to external address is executed
    And transaction ledger status is "Confirmed"

    Then corresponding transfer and fee are present
    And "account1" has correct balances of "XRPL testnet" after "outgoing" transaction