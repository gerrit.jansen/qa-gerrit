@policy-workflow-tests
@scope-descendants
@coercive-actions
@candidate

Feature: Coercive domain scenarios

  Background:
#    Given the vault is configured
    And coercive subdomain is created
    And the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

  @smoke
  @coercive-domain-action-when-subdomain-doesn't-have-policies-matching-intent
  Scenario: Coercive domain action when subdomain doesn't have policies matching to intent

    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                              |
    And policy condition expression sets request author domain to "coercive subdomain 1"

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And the subdomain is created

    And created subdomain is a current domain
    And "subdomain admin user" user from "subdomain1" domain is logged in

    When subdomain yes policy is locked
    And the user attempts to unlock the yes policy
    Then the intent status is "Failed"

    Given the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "coercive subdomain 1"
    And request author object contains domainId of "coercive subdomain 1"
    And target domain is set to subdomain id
    And reference is set to id of yes policy from subdomain

    When the user submits the unlock policy intent for "subdomain1" domain using the author and reference specified above
    Then the number of applicable policies is "1"
    And the intent status is "Executed"

    Given the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    When createUser intent is proposed by "subdomain admin user" user
    Then the intent status is "Executed"

  @coercive-domain-action-when-subdomain-has-policies-matching-to-intent
  Scenario: Coercive domain action when subdomain has policies matching to intent

    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 200                                                                       |

    And policy condition expression sets request author domain to "coercive subdomain 1"
    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    Given the subdomain is created
    And created subdomain is a current domain
    And "subdomain admin user" is logged in

    And the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | intentTypes         |  v0_UnlockPolicy                                                                         |
      | rank                | 300                                                                       |

    And policy condition expression sets request author domain to "coercive subdomain 1"
    And all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 2       |

      | simpleCondition2 role   | manager   |
      | simpleCondition2 quorum | 1         |

    And finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
      | 2 | simpleCondition2 |

    And the policy is created

    When subdomain yes policy is locked
    And the user attempts to unlock the yes policy
    Then the intent status is "Failed"

    Given the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

    And request author object is created and contains id of admin user from "coercive subdomain 1"
    And request author object contains domainId of "coercive subdomain 1"
    And reference is set to id of yes policy from subdomain

    When the user submits the unlock policy intent for "subdomain1" domain using the author and reference specified above
    Then the number of applicable policies is "1"
    Given the progress per policy from current domain is being verified
    Then the progress per policy contains correct policy reference
    And the intent status is "Executed"

    Given  the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    When createUser intent is proposed by "subdomain admin user" user
    Then the intent status is "Executed"

  @coercive-domain-action-to-grand-child-subdomain
  Scenario: Coercive domain action to grand-child subdomain
    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 200                                                                       |

    And policy condition expression sets request author domain to "coercive subdomain 1"

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And subdomain number "1" is created

    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    And subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in

    When subdomain yes policy is locked
    And the user attempts to unlock the yes policy
    Then the intent status is "Failed"

    Given the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "coercive subdomain 1"
    And request author object contains domainId of "coercive subdomain 1"
    And reference is set to id of yes policy from subdomain

    When the user submits the unlock policy intent for "subdomain2" domain using the author and reference specified above
    And the current domain is "coercive subdomain 1" domain
    Then the number of applicable policies is "1"
    And the intent status is "Executed"

    And the current domain is "subdomain2" domain
    And "subdomain admin user" is logged in
    When createUser intent is proposed by "subdomain admin user" user
    Then the intent status is "Executed"

  @coercive-domain-action-to-grand-child-subdomain-when-other-coercive-domain-in-between
  Scenario: Coercive domain action to grand-child subdomain when there is another coercive domain with matching policy in-between
    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 200                                                                       |
    And policy condition expression sets request author domain to "coercive subdomain 1"
    And all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    And finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And coercive subdomain number "2" is created

    And the current domain is "coercive subdomain 2" domain
    And "subdomain admin user" is logged in

    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When subdomain yes policy is locked
    And the user attempts to unlock the yes policy
    Then the intent status is "Failed"

    Given the current domain is "coercive subdomain 1" domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "coercive subdomain 1"
    And request author object contains domainId of "coercive subdomain 1"
    And reference is set to id of yes policy from subdomain

    When the user submits the unlock policy intent for "subdomain1" domain using the author and reference specified above
    And the current domain is "coercive subdomain 1" domain
    Then the number of applicable policies is "1"
    And the intent status is "Executed"

    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    When createUser intent is proposed by "subdomain admin user" user
    Then the intent status is "Executed"

  @unlock-subdomain-policy-from-coercive-root-domain
    Scenario: Unlock subdomain policy from coercive root domain
    Given a root domain admin user is logged in
    And root domain is a coercive domain
    And the current domain is a root domain
    And all policies except default are locked

    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 300                                                                       |

    And policy condition expression sets request author domain to "root domain"

    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |

    And the policy is created

    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in

    When subdomain yes policy is locked
    And the user attempts to unlock the yes policy
    Then the intent status is "Failed"

    And the current domain is a root domain
    And a root domain admin user is logged in

    Given request author object is created and contains id of admin user from "root domain"
    And request author object contains domainId of "root domain"
    And reference is set to id of yes policy from subdomain

    When the user submits the unlock policy intent for "subdomain1" domain using the author and reference specified above
    Then the number of applicable policies is "1"
    And the intent status is "Executed"

    And the current domain is "subdomain1" domain
    And "subdomain admin user" is logged in
    When createUser intent is proposed by "subdomain admin user" user
    Then the intent status is "Executed"

  @ignore
  @coercive-actions-against-accounts-in-subdomains
  Scenario: Coercive actions against accounts in subdomains: lock, release transfers, perform transaction
    Given a root domain admin user is logged in
    And root domain is a coercive domain
    And all policies except default are locked
    Given the vault is configured
    And pre-conditions for accounts balances recovery are met
    And we have two recovered "ethereum-testnet-rinkeby" accounts

    Given the policy has the following properties
      | scope               | SelfAndDescendants                                                        |
      | rank                | 300                                                                       |
    And policy condition expression sets request author domain to "root domain"
    Given all policy workflow structures use the following simple conditions
      | simpleCondition1 role   | admin   |
      | simpleCondition1 quorum | 1       |

    Then finally the policy workflow condition structure is
      | 1 | simpleCondition1 |
    And the policy is created

    And subdomain number "1" is created
    And the current domain is "subdomain1" domain
    And "subdomain admin user" user from "subdomain1" domain is logged in
    And "ethereum-testnet-rinkeby" account number "3" is created

    Given subdomain number "2" is created
    And the current domain is "subdomain2" domain
    And "subdomain admin user" user from "subdomain2" domain is logged in
    And "ethereum-testnet-rinkeby" account number "4" is created

    Given the current domain is a root domain
    And a root domain admin user is logged in
    When account number "4" lock intent is executed
    Then account number "4" lock state is "Locked"

    When ethereum transaction is sent from recovered root domain account to account number "4"
    And release "ethereum-testnet-rinkeby" quarantined transfers to "account4" intent is proposed by "default" user
    Then an error with "InvalidIntentError" is returned

    When account number "4" unlock intent is executed
    Then account number "4" lock state is "Unlocked"
    And release "ethereum-testnet-rinkeby" quarantined transfers to "account4" intent is proposed by "default" user
    Then intent is created in "subdomain2" domain and has status "Executed"

    Given the current domain is a root domain
    And a root domain admin user is logged in
    Given Metaco Test Token is minted to account number "4"
    When the transaction intent from "account4" to "account3" is executed using a call of smart contract transfer function
      |amountInData| 10000000000000000000 |
      |amount      | 0                   |

    And transaction ledger status is "Detected"
    Then "account4" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account3" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

    When release "METACO TEST TOKEN" quarantined transfers to "account3" intent is executed
    Then "account4" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account3" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

  @negative-submit-intent-to-sibling-domain
  Scenario: Negative. Submit intent to sibling domain
    And subdomain number "1" is created
    And coercive subdomain number "2" is created

    And the current domain is "coercive subdomain 2" domain
    And "subdomain admin user" is logged in

    Given request author object is created and contains id of admin user from "coercive subdomain 2"
    And request author object contains domainId of "coercive subdomain 2"
    And target domain is set to "subdomain1"

    When createUser intent is proposed using the author and target domain specified above
    Then request fails with error "IntentDecisionNotApplicable"