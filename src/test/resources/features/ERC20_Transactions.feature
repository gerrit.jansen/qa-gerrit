@transactions
@erc20-transactions
Feature: ERC20 transactions

  Background:
    Given a root domain admin user is logged in
    And the vault is configured
#     creating accounts if none, asserting there are accounts with needed derivation path if there are accounts
    And pre-conditions for accounts balances recovery are met
    And all recovery transfers are released to accounts
    And all policies except default are locked

  @smoke
  @erc-20-transaction-with-address-destination
  Scenario: ERC20 transaction with address destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    And "account1" has token "METACO TEST TOKEN" in its balances

    When the transaction intent from "account1" to "account2" is executed using a call of smart contract transfer function
      |amountInData| 10000000000000000000 |
      |amount      | 0                   |

    And transaction ledger status is "Detected"
    Then "account1" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account2" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

    When release "METACO TEST TOKEN" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account2" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

    And token funds are sent back to account1
      |amountInData| 10000000000000000000 |
      |amount      | 0                   |


  @erc20-transaction-to-abi-endpoint
  Scenario: ERC20 transaction to ABI endpoint
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    And "account1" has token "METACO TEST TOKEN" in its balances
    And endpoint with ABI is created

    When the transaction intent from "account1" to "account2" is executed using transaction order to endpoint with ABI
      |amountInData| 10000000000000000000 |
      |amount      | 0                   |

    And transaction ledger status is "Detected"

    Then "account1" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account2" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

    When release "METACO TEST TOKEN" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
    Then "account2" has correct balances of "METACO TEST TOKEN" after "incoming" transaction

    And token funds are sent back to account1
      |amountInData| 10000000000000000000 |
      |amount      | 0                    |

  @erc20-transaction-to-external-address-destination
  Scenario: ERC20 transaction to external address destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    And "account1" has token "METACO TEST TOKEN" in its balances

    When the transaction intent from "account1" to external address is executed using a call of smart contract transfer function
      |amountInData| 10000000000000000000 |
      |amount      | 0                    |

    And transaction ledger status is "Detected"

    Then corresponding transfer and fee are present
    And "account1" has correct balances of "METACO TEST TOKEN" after "outgoing" transaction
