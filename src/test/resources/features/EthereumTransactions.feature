@transactions
@ethereum-transactions

Feature: Ethereum transactions

  Background:
    Given a root domain admin user is logged in
    And the vault is configured
#     creating accounts if none, asserting there are accounts with needed derivation path if there are accounts
    And pre-conditions for accounts balances recovery are met
    And all recovery transfers are released to accounts
    And all policies except default are locked

  @smoke
  @ethereum-transaction-to-account-destination
  Scenario: Ethereum transaction to account destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts

    When ethereum transaction from "account1" to "account2" is executed
    And transaction ledger status is "Detected"
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    When release "Ethereum testnet rinkeby" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    And ethereum funds are sent back to account1

  @ethereum-transaction-to-account-destination-dry-run
  Scenario: Ethereum transaction to account destination dry run
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    When ethereum dry run transaction from "account1" to "account2" is executed
    Then we have a valid dry run result

  @ethereum-transaction-to-address-destination
  Scenario: Ethereum transaction to address destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts

    When ethereum transaction from "account1" to address of "account2" is executed
    And transaction ledger status is "Detected"
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    When release "Ethereum testnet rinkeby" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction
    And ethereum funds are sent back to account1

  @ignore
#    https://gitlab.internal.m3t4c0.com/silo/platform/eth-indexer/-/issues/18
  @ethereum-transaction-to-address-destination-with-data
  Scenario: Ethereum transaction to address destination with data in raw transaction
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    And "ethereum-testnet-rinkeby" account number "3" is created
    And "ethereum-testnet-rinkeby" account number "4" is created

    And ethereum transaction from "account1" to "account3" is executed
    And transaction ledger status is "Detected"
    And release "Ethereum testnet rinkeby" quarantined transfers to "account3" intent is executed

    When ethereum transaction from "account3" to address of "account4" with encoded text in data field is executed
    And transaction ledger status is "Detected"
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    When release "Ethereum testnet rinkeby" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

  @ethereum-transaction-to-endpoint-destination
  Scenario: Ethereum transaction to endpoint destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    And endpoint using "account2" address is created

    When the ethereum transaction intent from "account1" to endpoint with "account2" address is executed
    And transaction ledger status is "Detected"

    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    When release "Ethereum testnet rinkeby" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction
    Then "account2" has correct balances of "Ethereum testnet rinkeby" after "incoming" transaction

    And ethereum funds are sent back to account1

  @eth-transaction-to-external-address-destination
  Scenario: Ethereum transaction to external address destination
    Given we have two recovered "ethereum-testnet-rinkeby" accounts
    When the ethereum transaction intent from "account1" to external address is executed
    And transaction ledger status is "Detected"

    Then corresponding transfer and fee are present
    And "account1" has correct balances of "Ethereum testnet rinkeby" after "outgoing" transaction