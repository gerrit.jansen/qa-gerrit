@transactions
@bitcoin-transactions

Feature: Bitcoin transactions

  Background:
    Given a root domain admin user is logged in
    And the vault is configured
#     creating accounts if none, asserting there are accounts with needed derivation path if there are accounts
    And pre-conditions for accounts balances recovery are met
    And all recovery transfers are released to accounts
    And all policies except default are locked

  @smoke
  @btc-transaction-to-address-destination
  Scenario: Bitcoin transaction to address destination
    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin transaction intent from "account1" to address of "account2" is executed
    And transaction ledger status is "Detected"

    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    When release "Bitcoin testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    And the bitcoin funds are sent back to account1

  @btc-transaction-to-address-destination-dry-run
  Scenario: Bitcoin transaction to address destination dry run
    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin dry run transaction intent from "account1" to address of "account2" is executed
    Then we have a valid dry run result

  @btc-transaction-to-account-destination
  Scenario: Bitcoin transaction to account destination
    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin transaction intent from "account1" to "account2" is executed
    And transaction ledger status is "Detected"

    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    When release "Bitcoin testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    And the bitcoin funds are sent back to account1

  @btc-transaction-to-endpoint-destination
  Scenario: Bitcoin transaction to endpoint destination
    Given we have two recovered "bitcoin-testnet" accounts
    And endpoint using "account2" address is created

    When the bitcoin transaction intent from "account1" to endpoint with "account2" address is executed
    And transaction ledger status is "Detected"

    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction

    When release "Bitcoin testnet" quarantined transfers to "account2" intent is executed
    Then "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction
    Then "account2" has correct balances of "Bitcoin testnet" after "incoming" transaction
    And the bitcoin funds are sent back to account1

  @btc-transaction-to-external-address-destination
  Scenario: Bitcoin transaction to external address destination
    Given we have two recovered "bitcoin-testnet" accounts
    When the bitcoin transaction intent from "account1" to external address is executed
    And transaction ledger status is "Detected"

    Then corresponding transfer and fee are present
    And "account1" has correct balances of "Bitcoin testnet" after "outgoing" transaction

#  https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/496
#  @btc-redirect-transaction
#  Scenario: Redirect of bitcoin quarantined transfer
#    Given we have two recovered "bitcoin-testnet" accounts
#    And the bitcoin transaction intent from "account1" to address of "account2" is executed
#    And transaction ledger status is "Detected"
#    And transfers are quarantined
#
#    When bitcoin redirect transaction order from "account2" to an external address is executed
#    Then "account2" balances are not affected by redirect transaction

