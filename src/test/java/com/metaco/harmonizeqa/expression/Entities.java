package com.metaco.harmonizeqa.expression;

import com.metaco.harmonize.api.om.Lock;
import com.metaco.harmonize.api.om.MetaData;
import com.metaco.harmonize.api.om.Users;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonize.openapi.Component;
import com.metaco.harmonize.openapi.PropType;
import com.metaco.harmonize.openapi.Property;

import java.util.*;
import java.util.stream.Collectors;

public class Entities {

    public <T> List<Expression> generatateExpression(T entity, Map<String, Object> propertyValues, Set<String> excludedProperties) {

        var component = (Component) entity.getClass().getAnnotation(Component.class);

        return Arrays.stream(component.properties())
                .filter(p -> !excludedProperties.contains(p.name()) && propertyValues.containsKey(p.name()))
                .map(p -> createExpression(p, propertyValues.get(p.name())))
                .collect(Collectors.toList());
    }

    private Expression createExpression(Property p, Object o) {

        if (p.type() == PropType.ARRAY) {
            var v = ((Collection) o).stream().map(item -> quote(item)).collect(Collectors.joining(", ")).toString();
            return new FunctionExpression("includes", p.name(), v, true);
        }

        if (o instanceof List && ((List) o).size() == 2 && p.type() != PropType.ARRAY) {
            var list = ((List) o);
            var a = (Number) list.get(0);
            var b = (Number) list.get(1);
            if (a.longValue() > b.longValue())
                b = a;

            return new CompareExpression("<", p.name(), a.toString(), b.toString(), true);
        }

        var componentAnnotation = (Component) o.getClass().getAnnotation(Component.class);
        if (componentAnnotation != null) {
            // we generate an equal statement for each property of the component

            var expressions = generatateExpression(o, ((JSON) o).asMap(), Set.of());
            return new AndExpression(p.name(), expressions, true);

        }

        return new Expression("==", p.name(), quote(o), true);
    }

    private String quote(Object o) {
        if (o == null)
            return "null";

        if (o instanceof String)
            return "'" + o + "'";

        var componentAnnotation = (Component) o.getClass().getAnnotation(Component.class);
        if (componentAnnotation != null) {
            // we have an entity object as the value
            throw new RuntimeException("Entities are not allowed here");
        }

        return o.toString();
    }


    /**
     * Holds the parts of a simple left op right expression
     */
    public static class Expression {
        final String operator;
        final String left;
        final String right;
        final boolean truth;

        public Expression(String operator, String left, String right, boolean truth) {
            this.operator = operator;
            this.left = left;
            this.right = right;
            this.truth = truth;
        }

        public String toString() {
            return toString("");
        }

        public String toString(String prefix) {
            return prefix + left + operator + right;
        }
    }

    public static class FunctionExpression extends Expression {

        public FunctionExpression(String operator, String left, String right, boolean truth) {
            super(operator, left, right, truth);
        }

        public String toString() {
            return toString("");
        }

        public String toString(String prefix) {
            return prefix + left + "." + operator + "(" + right + ")";
        }
    }

    public static class CompareExpression extends Expression {

        final String a;
        final String b;
        final String name;

        public CompareExpression(String operator, String name, String a, String b, boolean truth) {
            super(operator, name, null, truth);
            this.a = a;
            this.b = b;
            this.name = name;
        }

        public String toString() {
            return toString("");
        }

        public String toString(String prefix) {
            return a + " " + operator + " " + prefix + name + " " + operator + " " + b;
        }
    }


    public static class AndExpression extends Expression {

        final String name;
        final List<Expression> expressions;

        public AndExpression(String name, List<Expression> expressions, boolean truth) {
            super("&&", name, null, truth);
            this.name = name;
            this.expressions = expressions;
        }

        public String toString() {
            return toString("");
        }

        public String toString(String prefix) {
            return expressions.stream().map(v -> v.toString(name + ".")).collect(Collectors.joining(" " + operator + " "));
        }
    }

    public static void main(String args[]) {

        var expressions = new Entities().generatateExpression(
                Users.User.builder().alias("admin").id("1").lock(Lock.LockStatus.Unlocked).publicKey("bla").roles(List.of("test")).build(),
                Map.of("admin", "admin", "id", "1", "lock", "unlocked", "publicKey", "bla", "roles", List.of("test"),
                        "metadata", new MetaData(Map.of("description", "bla", "revision", List.of(1, 10000),
                                "createdAt", "2022-01-01"))),
                Set.of());

        expressions.forEach(System.out::println);
    }
}
