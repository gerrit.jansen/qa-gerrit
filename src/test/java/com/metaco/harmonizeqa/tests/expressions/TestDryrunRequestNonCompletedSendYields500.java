package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.SystemUtil;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class TestDryrunRequestNonCompletedSendYields500 extends BaseSteps {

    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestDryrunRequestNonCompletedSendYields5002");

    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws InterruptedException {

        harmonize().ledgers().list().forEach(System.out::println);
        var ledger = harmonize().ledgers().list().filter(l -> l.getId().toLowerCase().contains("tezos")).findFirst().get();

        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var domainMemId = "8";
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());

        var accountId = memoizedData.onceOnly(domainMemId + "-account_id", u -> true, () -> generateId());


        var m = memoizedData.onceOnly(domainMemId + "_domain",
                (domain) -> {
                    var id = ((Intents.Intent) domain).getPayloadId().get();
                    return harmonize().domains().get(id).isPresent();
                },
                () -> createDomain("true",
                        List.of(Policies.IntentType.v0_CreateAccount, Policies.IntentType.v0_CreateUser, Policies.IntentType.v0_CreateTransactionOrder),
                        domainId, admin1));

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));


        print("Ledger Id: " + ledger.getId());
        print("DomainId: " + domainId);
        print("AccountId: " + accountId);

        var accAddress = admin1Ctx.harmonize.accounts(domainId).getLatestAddress("09a5c94b-dd84-4c81-8fb4-5e0814db406c");
        print(accAddress);
        admin1Ctx.harmonize.accounts(domainId).list()
                .forEach(a ->{

            var status = a.getAccountAdditionalDetails().map(d -> d.getStatus()).orElse(null);
            var address = status.equalsIgnoreCase("Completed") ? admin1Ctx.harmonize.accounts(domainId).getLatestAddress(a.getId()) : null;


            print("Account: " + a.getId() + " status: " + status + " address: " + address);
            admin1Ctx.harmonize.accounts(domainId).balances(a.getId()).forEach(b -> {
                print(b);
            });

        });

        harmonize().accounts(harmonize().domains().getRootDomain().get().getId()).list()
                .filter(a -> a.getLedgerId().toLowerCase().contains("tezos"))
                .forEach(a -> {
                    var status = a.getAccountAdditionalDetails().map(d -> d.getStatus()).orElse(null);
                    var address = status.equalsIgnoreCase("Completed") ? admin1Ctx.harmonize.accounts(domainId).getLatestAddress(a.getId()) : null;

                    if(address == null || address.isEmpty()) {
                        print("Generating address");
                        address = admin1Ctx.harmonize.accounts(domainId).generateAddress(a.getId());
                    }

                    print("Root Account: " + a.getId() + " status: " + status + " address: " + address);
                    print("\t" + a.getLedgerId());
                });

//        var account = memoizedData.onceOnly(System.currentTimeMillis() + "_account_1",
//                (ac) -> admin1Ctx.harmonize.accounts(domainId).get(accountId).isPresent(),
//                () -> {
//                    var accountCreate = Accounts.CreateAccount.builder()
//                            .ledgerId(ledger.getId())
//                            .vaultId("00000000-0000-0000-0000-000000000000")
//                            .alias(accountId)
//                            .targetDomainId(domainId)
//                            .build();
//
//                    var intent = admin1Ctx.harmonize
//                            .accounts(domainId).create(accountCreate, Expire.hours(24)).waitForIntentExecuted(Timeout.minutes(1));
//                    print("Intent create account: " + intent);
//                    print(admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get());
//
//                    return accountCreate;
//                }
//        );
//
//

        if(true)
            return;

        admin1Ctx.harmonize.accounts(domainId).list().forEach(System.out::println);

        var nonCompletedAccount = admin1Ctx.harmonize.accounts(domainId)
                .list()
                .filter(acc -> {
                    var pDetails = acc.getAccountAdditionalDetails();
                    if (pDetails.isEmpty())
                        return true;
                    if (!pDetails.get().getProcessing().getStatus().equalsIgnoreCase("complete"))
                        return true;
                    return false;
                })
                .findFirst();


        if (nonCompletedAccount.isEmpty())
            throw new RuntimeException("No non completed account was found");


        var parameters = Transactions.OrderParameters.xrpl()
                .toDestination(TransactionDestination.AddressDestination.builder()
                        .address(nonCompletedAccount.get().getId())
                        .build())
                .memos(List.of())
                .maximumFee("10000")
                .amount("10000")
                .priorityMedium()
                .build();

        print(parameters.asJson());
        print("Using account: " + nonCompletedAccount.get());

        var intent = admin1Ctx.harmonize.transactions(domainId).dryRun()
                .accountId("0bca7444-cb8c-4eb1-a106-2c2f6662c8bc")
                .parameters(
                        parameters
                ).submit();

        print(intent);

    }

    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestDryrunRequestNonCompletedSendYields500().run();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }

    }
}
