package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonize.utils.CollectionUtils;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestDryrunRequestOverload extends BaseSteps {

    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestDryrunRequestOverload1");

    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws InterruptedException {

        print("Harmonize: " + harmonize().getContext().getConfig().getGatewayURL());

        harmonize().ledgers().list().forEach(System.out::println);
        var ledger = harmonize().ledgers().list().filter(l -> l.getId().equals("bitcoin-testnet")).findFirst().get();

        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var domainMemId = "200";
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());

        var accountId = memoizedData.onceOnly(domainMemId + "-account_id", u -> true, () -> generateId());


        var m = memoizedData.onceOnly(domainMemId + "_domain",
                (domain) -> {
                    var id = ((Intents.Intent) domain).getPayloadId().get();
                    return harmonize().domains().get(id).isPresent();
                },
                () -> createDomain("true",
                        List.of(
                                Policies.IntentType.v0_ReleaseQuarantinedTransfers,
                                Policies.IntentType.v0_CreatePolicy, Policies.IntentType.v0_UpdatePolicy,
                                Policies.IntentType.v0_CreateAccount, Policies.IntentType.v0_CreateUser, Policies.IntentType.v0_CreateTransactionOrder),
                        domainId, admin1));

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        admin1Ctx.harmonize.accounts(domainId).list().forEach(System.out::println);

        print("Ledger Id: " + ledger.getId());
        print("DomainId: " + domainId);
        print("AccountId: " + accountId);


        var account = memoizedData.onceOnly(accountId + "_account_1",
                (ac) -> {
                    var acc = admin1Ctx.harmonize.accounts(domainId).list().filter(a -> a.getId().equals(ac.getId())).findFirst();

                    print(">>>>>>>>>> Found Account ID: " + acc);
                    return acc.isPresent();
                },
                () -> {
                    var accountCreate = Accounts.CreateAccount.builder()
                            .id(accountId)
                            .ledgerId(ledger.getId())
                            .vaultId("00000000-0000-0000-0000-000000000000")
                            .alias(accountId)
                            .targetDomainId(domainId)
                            .build();

                    var intent = admin1Ctx.harmonize
                            .accounts(domainId).create(accountCreate, Expire.hours(24)).waitForIntentExecuted(Timeout.minutes(1));
                    print("Intent create account: " + intent);
                    print(admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get());

                    return accountCreate;
                }
        );

        assert account.getId().equals(accountId);

        var address = admin1Ctx.harmonize.accounts(domainId).getLatestAddress(account.getId());
        print(address);

        admin1Ctx.harmonize.transactions(domainId).listTransfers().forEach(transfer -> {
            if (transfer.isQuarantined()) {
                print("releasing quarntined transfer " + transfer.getId());
                admin1Ctx.harmonize.accounts(domainId).releaseQuarantinedTransfers()
                        .accountId(accountId)
                        .transferIds(transfer.getId())
                        .submit().waitForIntentExecuted(Timeout.minutes(10));

            }
        });

        Thread.sleep(1000);
        admin1Ctx.harmonize.accounts(domainId).balances(accountId).forEach(System.out::println);

        Callable<String> sendDryRun = () -> {

            StringBuilder buff = new StringBuilder();

            var parameters = Transactions.OrderParameters.btc()
                    .toDestination(TransactionDestination.AddressDestination.builder()
                            .address("tb1ql7w62elx9ucw4pj5lgw4l028hmuw80sndtntxt")
                            .build())
                    .maximumFee("1000")
                    .amount("1000")
                    .priorityMedium()
                    .build();

            buff.append("Using account: " + account + " \n");

            var intent = admin1Ctx.harmonize.transactions(domainId).dryRun()
                    .accountId(accountId)
                    .parameters(
                            parameters
                    ).submit();

            buff.append("intent = " + intent + "\n");
            var a = admin1Ctx.harmonize.accounts(domainId).get(accountId).get();
            buff.append("Status = " + a.getAccountAdditionalDetails().get().getStatus() + "\n");
            return buff.toString();
        };

        List<Callable<String>> callers = IntStream.rangeClosed(0, 1000).mapToObj(a -> sendDryRun).collect(Collectors.toList());


        var waiters2 = Parallel.run(callers.toArray(new Callable[0]));
        var responses = Parallel.waitAll(waiters2, 1, TimeUnit.HOURS);

        AtomicInteger completed = new AtomicInteger(0);
        List<String> failed = new ArrayList<>(0);


        responses.forEach(resp -> {
            var c = completed.getAndIncrement();
            if (!resp.toString().contains("Completed")) {
                failed.add(resp.toString());
            }
            if (c % 10 == 0)
                print("Resp: " + c + " > " + resp);

        });

        print("Completed: " + completed.get());
        print("Failed: " + failed.size());

        failed.forEach(System.out::println);
    }


    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestDryrunRequestOverload().run();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }
    }
}
