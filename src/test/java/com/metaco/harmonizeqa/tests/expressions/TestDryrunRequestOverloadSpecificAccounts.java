package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.TransactionDestination;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestDryrunRequestOverloadSpecificAccounts extends BaseSteps {

   public void runTezos() {

        print("Harmonize: " + harmonize().getContext().getConfig().getGatewayURL());

        harmonize().ledgers().list().forEach(System.out::println);

        var fromAccountId = "2ff1a8ab-4fb9-4d43-8a3b-1eb2c1d20c86";
        var fromAccountLatestAddress = "5GpbA4tSvcLp2y4xrz7QzSWpSc97b2CtRn5cT99faStex3SV";

       var toAccountId = "ddb1be2c-2560-4720-91cb-1118842bf089";
       var toAccountLatestAddress = "5Dd3TKigh2yBJ75b3KCipJqbmZJRAn1mtzW79PaaYKd16zGN";

       var domainId = harmonize().domains().getRootDomain().get().getId();

       // ensure the accounts exist
       for(var accountId : List.of(fromAccountId, toAccountId)) {
           print("=====================================================");
           print(harmonize().accounts(domainId).get(accountId).get());
           print(harmonize().accounts(domainId).balances(accountId));
           print("=====================================================");
       }

       Callable<String> sendDryRun = () -> {

            StringBuilder buff = new StringBuilder();
            var id = UUID.randomUUID();

            var parameters = Transactions.OrderParameters.substrate()
                    .toDestination(TransactionDestination.AddressDestination.builder()
                            .address(toAccountLatestAddress)
                            .build())
                    .maximumFee("18600001546")
                    .amount("3975399")
                    .priorityMedium()
                    .build();


            var intent = harmonize().transactions(domainId).dryRun()
                    .accountId(fromAccountId)
                    .parameters(
                            parameters
                    ).submit();

            buff.append("thread_id" + id + ", intent = " + intent + "\n");
            return buff.toString();
        };

        List<Callable<String>> callers = IntStream.rangeClosed(0, 1000).mapToObj(a -> sendDryRun).collect(Collectors.toList());


        var waiters2 = Parallel.run(callers.toArray(new Callable[0]));
        var responses = Parallel.waitAll(waiters2, 1, TimeUnit.HOURS, (e) -> e.toString());

        AtomicInteger completed = new AtomicInteger(0);
        List<String> failed = new ArrayList<>(0);


        responses.forEach(resp -> {
            var c = completed.getAndIncrement();
            if (!(resp.toString().contains("Completed") || resp.toString().contains("Success"))) {
                failed.add(resp.toString());
                print(resp.toString());
            }
//            if (c % 10 == 0)
//                print("Resp: " + c + " > " + resp);

        });

        print("Completed: " + completed.get());
        print("Failed: " + failed.size());

        failed.forEach(System.out::println);
    }


    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestDryrunRequestOverloadSpecificAccounts().runTezos();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }
    }
}
