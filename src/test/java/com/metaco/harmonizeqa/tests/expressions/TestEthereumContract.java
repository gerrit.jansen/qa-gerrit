package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.Domains;
import com.metaco.harmonize.api.om.Policies;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.harmonize.api.om.Users;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TestEthereumContract extends BaseSteps {

    private static MemoizedData memoizedData = new MemoizedData("/tmp", "TestEthereumContract");

    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws InterruptedException, IOException {

//        print(harmonize().accounts(rootDomainId()).list().filter(a -> a.getLedgerId().toLowerCase().contains("eth")));

        var accountId = memoizedData.onceOnly(
                "accountID", (v) -> true, () -> UUID.randomUUID().toString());

        print("AccountID", accountId);

        var accountOpt = harmonize().accounts(rootDomainId()).get(accountId);

        if (accountOpt.isEmpty())
            harmonize().accounts(rootDomainId()).create()
                    .id(accountId)
                    .alias(accountId)
                    .vaultId("00000000-0000-0000-0000-000000000000")
                    .ledgerId("ethereum-testnet-rinkeby")
                    .expire(Expire.hours(1))
                    .submit().waitForIntentExecuted(Timeout.minutes(1));

        // {"alias":"44ee53aa-7a71-4fb9-9423-997fea5fce30","domainId":"5cd224fe-193e-8bce-c94c-c6c05245e2d1","id":"44ee53aa-7a71-4fb9-9423-997fea5fce30","ledgerId":"ethereum-testnet-rinkeby","lock":"Unlocked","metadata":{"createdAt":"2022-05-03T17:58:39.665Z","createdBy":{"domainId":"5cd224fe-193e-8bce-c94c-c6c05245e2d1","id":"6ac20654-450e-29e4-65e2-1bdecb7db7c4"},"customProperties":{},"lastModifiedAt":"2022-05-03T17:58:39.665Z","lastModifiedBy":{"domainId":"5cd224fe-193e-8bce-c94c-c6c05245e2d1","id":"6ac20654-450e-29e4-65e2-1bdecb7db7c4"},"revision":1},"providerDetails":{"keyInformation":{"derivationPath":"1'/65'","type":"VaultDerived"},"keyStrategy":"VaultHard","type":"Vault","vaultId":"00000000-0000-0000-0000-000000000000"}}
        var account = harmonize().accounts(rootDomainId()).get(accountId).get();

        print(account);

        String contractByteCode = Files.readString(Path.of("src", "test", "resources", "eth_contract.dat"));

        // 0xdcd27fab014ef9f09932b5546bf4de67c81344dd
//        var address =harmonize().accounts(rootDomainId()).getLatestAddress(accountId);
//        print(address);

        print("Transactions");
        harmonize().transactions(rootDomainId()).list(TransactionActions.ListCriteria
        .builder().accountId(accountId).build()
        ).forEach(t ->{

            print("Transaction: ", t);


        });


        harmonize().transactions(rootDomainId()).listOrders(TransactionActions.ListCriteria
                .builder().accountId(accountId).build()
        ).forEach(o ->{

            print("Order: ", o);


        });

        harmonize().transactions(rootDomainId()).listTransfers(TransactionActions.ListCriteria
                .builder().accountId(accountId).build()
        ).forEach(tr ->{

            print("Transfer: ", tr);
            if(tr.isQuarantined()) {
                harmonize().accounts(rootDomainId()).releaseQuarantinedTransfers()
                        .accountId(accountId)
                        .transferIds(tr.getId())
                        .submit().waitForIntentExecuted(Timeout.minutes(1));
            }

        });


//        print("Balance:", harmonize().accounts(rootDomainId()).balances(accountId).collect(Collectors.toList()));

        print(harmonize().requests(rootDomainId()).get("49f6b62b-bb13-4d21-8895-6d894add3e02"));

//        var intent = harmonize().transactions(rootDomainId())
//                .create()
//                .fromAccountId(accountId)
//                .parameters(Transactions.OrderParameters.eth()
//                        .toAccount(accountId)
//                        .amount(0)
//                        .maximumFee("1000")
//                        .priorityMedium()
//                        .data(contractByteCode)
//                .build())
//                .submit();
//        print(intent);

//        print("Intent:");
//        print("IntentID: ",intent.getIntentId());
//        print("RequestId: ",intent.getRequestId());

    }


    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestEthereumContract().run();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
            memoizedData.close();
        }
    }
}
