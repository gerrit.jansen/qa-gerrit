package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.TransactionDestination;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestDryrunRequestOverloadSpecificAccountsBTC extends BaseSteps {

   public void runTezos() {

        print("Harmonize: " + harmonize().getContext().getConfig().getGatewayURL());

        harmonize().ledgers().list().forEach(System.out::println);

        var fromAccountId = "f1c0d667-2240-4eb8-8e4e-9c4833ba4a19";
        var fromAccountLatestAddress = "2N1GN2ZagieMSDdc2tnGQz8xouxjEckFU5A";

       var toAccountId = "272c6245-34cf-4c06-810c-d2163e3fb778";
       var toAccountLatestAddress = "2MwbeEvjfkeqNonN5REkBP8DouJQKQNn9Fb";

       var domainId = harmonize().domains().getRootDomain().get().getId();

       // ensure the accounts exist
       for(var accountId : List.of(fromAccountId, toAccountId)) {
           print("=====================================================");
           print(harmonize().accounts(domainId).get(accountId).get());
           print(harmonize().accounts(domainId).balances(accountId));
           print("=====================================================");
       }

       Callable<String> sendDryRun = () -> {

            StringBuilder buff = new StringBuilder();
            var id = UUID.randomUUID();

            var parameters = Transactions.OrderParameters.btc()
                    .toDestination(TransactionDestination.AddressDestination.builder()
                            .address(toAccountLatestAddress)
                            .build())
                    .maximumFee("10000")
                    .amount("257")
                    .priorityMedium()
                    .build();


            var intent = harmonize().transactions(domainId).dryRun()
                    .accountId(fromAccountId)
                    .parameters(
                            parameters
                    ).submit();

            buff.append("thread_id" + id + ", intent = " + intent + "\n");
            return buff.toString();
        };

        List<Callable<String>> callers = IntStream.rangeClosed(0, 1000).mapToObj(a -> sendDryRun).collect(Collectors.toList());


        var waiters2 = Parallel.run(callers.toArray(new Callable[0]));
        var responses = Parallel.waitAll(waiters2, 1, TimeUnit.HOURS);

        AtomicInteger completed = new AtomicInteger(0);
        List<String> failed = new ArrayList<>(0);


        responses.forEach(resp -> {
            var c = completed.getAndIncrement();
            if (!(resp.toString().contains("Completed") || resp.toString().contains("Success"))) {
                failed.add(resp.toString());
            }
            if (c % 10 == 0)
                print("Resp: " + c + " > " + resp);

        });

        print("Completed: " + completed.get());
        print("Failed: " + failed.size());

        failed.forEach(System.out::println);
    }


    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestDryrunRequestOverloadSpecificAccountsBTC().runTezos();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }
    }
}
