package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.api.LocalHarmonizeContext;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestPolicyExpressionsTransactionOrderBadExpressionsRootDomain extends BaseExpressions {
    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestPolicyExpressionsTransactionOrder");

    public Tuple<IntentData<Policies.CreatePolicy>, Policies.CreatePolicy> createPolicy(
            String domainId,
            String authorId,
            String expression,
            List<Policies.IntentType> intentTypes, int quorum) {

        var policy =
                Policies.CreatePolicy.builder()
                        .alias("TestPolicy-" + System.currentTimeMillis())
                        .rank(1000)
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role("admin")
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.SelfAndDescendants)
                        .build();

        print("Creating domain policy");

        return Tuple.n(harmonize().policies(domainId)
                .create(policy, Expire.hours(1))
                .waitForIntentExecuted(Timeout.minutes(1)), policy);

    }


    public void checkTransactionOrderPolicies() throws Exception {


        var authorId = harmonize().getContext().getSigner().getAuthor().getId();
        var authorRoles = harmonize().users(rootDomainId()).get(authorId).get().getRoles();

        print("Author: ", authorId, " roles: ", authorRoles);

        var domainMemId = System.currentTimeMillis();
        var account1Id = memoizedData.onceOnly(domainMemId + "-account1_id", u -> true, () -> generateId());
        var account2Id = memoizedData.onceOnly(domainMemId + "-account2_id", u -> true, () -> generateId());
        var amount = "5432";
        var vaultId = "00000000-0000-0000-0000-000000000000";

        var ledgerId = "bitcoin-testnet";

        var badExpression = List.of(
                "context.request.author.id == null.this"
        ).stream().collect(Collectors.joining(" && "));


        var tuple = createPolicy(rootDomainId(), authorId, badExpression,
                List.of(Policies.IntentType.v0_CreateAccount), 1);
        var badPolicyIntent = tuple.a;
        var badPolicyCreate = tuple.b;

        print("Bad policy created: ", harmonize().policies(rootDomainId()).get(badPolicyCreate.getId()).get());
        print("Bad policy intent state: ", harmonize().intents(rootDomainId()).get(badPolicyIntent.getIntentId()).get());

        print("Creating account 1 to trigger bad policy");
        // create an account that will cause the bad policy to be executed
        var account1 = harmonize().accounts(rootDomainId()).create()
                .id(account1Id)
                .alias("Testaccount-" + System.currentTimeMillis())
                .vaultId(vaultId)
                .ledgerId(ledgerId)
                .submit().waitForSucceed(Timeout.minutes(1));

        Thread.sleep(1000);

        print("Bad policy id : " + badPolicyCreate.getId());
        var account1IntentState = harmonize().intents(rootDomainId()).get(account1.getIntent().getIntentId()).get().getState();
        print("Bad account intent state: ", account1IntentState);

        assertEquals("PolicyScriptingExecutionFailed", account1IntentState.getError().getCode());

        // Ensure that the bad policy was executed
//        assertTrue("The bad policy must have been executed against the account create", progress.isPresent());

        // fix the bad policy
        var badPolicyGoodUpdate = harmonize().policies(rootDomainId()).get(badPolicyCreate.getId()).get().asUpdate();
        badPolicyGoodUpdate.setCondition(Policies.PolicyCondition.simpleBuilder().expression("context.request.author.id.length > 0").build());

        harmonize().policies(rootDomainId()).update(badPolicyGoodUpdate, Expire.hours(1))
                .waitForIntentExecuted(Timeout.minutes(1));

        Thread.sleep(1000);

        // try to create an account again and wait for executed
        // if a timeout here then the policy was either not properly updated or some other error
        var account2 = harmonize().accounts(rootDomainId()).create()
                .id(account2Id)
                .alias("Testaccount-" + System.currentTimeMillis())
                .vaultId(vaultId)
                .ledgerId(ledgerId)
                .submit().waitForIntentExecuted(Timeout.minutes(1));

        Thread.sleep(1000);

        // check that the fixed policy was used
        var account2IntentState = harmonize().intents(rootDomainId()).get(account2.getIntent().getIntentId()).get().getState();
        var progress2 = account2IntentState.getProgress().stream().filter(p -> p.getPolicyReference().getId().equals(badPolicyGoodUpdate.getReference().getId())).findFirst();
        print("Good account intent state: ", account2IntentState);

        // Ensure that the good bad policy was executed
        assertTrue("The good bad policy must have been executed against the account create", progress2.isPresent());

    }

    private <T> T waitForValue(long timeout, Supplier<T> supplier) {
        var start = System.currentTimeMillis();
        while (true) {
            if ((System.currentTimeMillis() - start) > timeout)
                throw new RuntimeException("Timeout while waiting for object " + supplier);
            var v = supplier.get();
            if (v != null)
                return v;
        }

    }

    private Accounts.CreateAccount approveAccount(LocalHarmonizeContext ctx, IntentData.AccountIntentData<Accounts.CreateAccount> accountIntent) {
        ctx.harmonize.intents(accountIntent.getIntent().getTargetDomainId())
                .approve(accountIntent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return accountIntent.getIntent().getPayload();
    }


    public static void main(String args[]) throws Throwable {
        new TestPolicyExpressionsTransactionOrderBadExpressionsRootDomain().checkTransactionOrderPolicies();
//
//        ScriptEngineManager manager = new ScriptEngineManager();
//        ScriptEngine engine = manager.getEngineByName("javascript");
//        engine.eval("console.log(1+1  + \"hi\")");
    }

}
