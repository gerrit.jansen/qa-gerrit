package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.api.LocalHarmonizeContext;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestPolicyExpressionsTransactionOrder extends BaseExpressions {
    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestPolicyExpressionsTransactionOrder");

    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build(),
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression("true")
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentType(Policies.IntentType.v0_CreateAccount)
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return Tuple.n(harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policies.get(0));

    }


    public void checkTransactionOrderPolicies() throws Exception {


        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var admin2 = memoizedData.onceOnly("admin2.2", u -> true, () -> NewUser.create("admin2", this));

        var domainMemId = System.currentTimeMillis();
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());
        var account1Id = memoizedData.onceOnly(domainMemId + "-account1_id", u -> true, () -> generateId());
        var account2Id = memoizedData.onceOnly(domainMemId + "-account2_id", u -> true, () -> generateId());
        var amount = "5432";
        var vaultId = "00000000-0000-0000-0000-000000000000";

        var ledgerId = "bitcoin-testnet";

        var vault = harmonize().vaults(rootDomainId()).get(vaultId).get();

        var expression = List.of(

//                 test current domain properties
//                "context.references['domains']['" + domainId + "'].id == '" + domainId + "'", //works
//                "context.references['domains']['" + domainId + "'].parentId=='" + rootDomainId() + "'", // works
//                "context.references['domains']['" + domainId + "'].governingStrategy=='CoerceDescendants'", // works
//                "context.references['domains']['" + domainId + "'].permissions.readAccess.domains.includes('admin')", // works
//                "context.references['domains']['" + domainId + "'].permissions.readAccess.domains.includes('admin')", // works

                // test root domain properties
                // author subdomain.user
                "context.request.author.domainId != context.references['domains']['" + domainId + "'].parentId"
//                "context.request.author.id == '6ac20654-450e-29e4-65e2-1bdecb7db7c4' || context.request.author.id == '96baf724-5830-4c67-8d20-9ac670cfdf3f' || context.request.author.id == '7b1641e1-b28a-4e93-91fd-07d9bd3086d7"

                // https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/670
//                "context.references['domains']['" + rootDomainId() + "'].parentId==null", // works
//                "context.references['domains']['" + rootDomainId() + "'].governingStrategy=='CoerceDescendants'", // works
//                "context.references['domains']['" + rootDomainId() + "'].permissions.readAccess.domains.includes('admin')" // works
//
//
//
//                "context.references['users'][context.request.author.id].roles.includes('admin')",

//                "context.request.targetDomainId=='" + domainId + "'", // works
//                "context.request.payload.accountId=='" + account1Id + "'", // works
//                "context.request.payload.parameters.outputs[0].destination.accountId=='" + account2Id + "'", // works
//
//                "context.request.payload.parameters.outputs[0].amount=='" + amount + "'", // works
//                "context.request.payload.parameters.outputs[0].destination.type=='Account'", // works
//                "context.request.payload.parameters.feeStrategy.priority=='Medium'", // works
//                "context.request.payload.parameters.feeStrategy.type=='Priority'", // works
//                "context.request.payload.parameters.maximumFee=='" + amount + "'", // works
//                "context.request.payload.parameters.type=='Bitcoin'", // works
//
//                // test accounts details for sending account
//                "context.references['accounts'][context.request.payload.accountId].id ==context.request.payload.accountId",
//                "context.references['accounts'][context.request.payload.accountId].ledgerId == '" + ledgerId + "'",
//                "context.references['accounts'][context.request.payload.accountId].providerDetails.vaultId == '" + vaultId + "'",
//                "context.references['accounts'][context.request.payload.accountId].providerDetails.keyInformation.publicKey.value.length > 0",
//
//                // test accounts details for output account
//
//                "context.references['accounts'][context.request.payload.parameters.outputs[0].destination.accountId].id ==context.request.payload.parameters.outputs[0].destination.accountId",
//                "context.references['accounts'][context.request.payload.parameters.outputs[0].destination.accountId].ledgerId == '" + ledgerId + "'",
//                "context.references['accounts'][context.request.payload.parameters.outputs[0].destination.accountId].providerDetails.vaultId == '" + vaultId + "'",
//                "context.references['accounts'][context.request.payload.parameters.outputs[0].destination.accountId].providerDetails.keyInformation.publicKey.value.length > 0"

                // test outgoing endpoint

        ).stream().collect(Collectors.joining(" && "));

        var m = memoizedData.onceOnly(domainMemId + "_domain",
                t -> !harmonize().domains().get(t.get("domainId").toString()).isEmpty(),
                () -> {
                    var t = createDomainMultipleApproval(expression,
                            List.of(Policies.IntentType.v0_CreateTransactionOrder), domainId, admin1, admin2, 2);
                    return Map.of("intentId", t.a.getIntentId(), "domainId", t.a.getIntent().getPayloadId().get(), "policyId", t.b.getId());
                });

        var domainPolicyId = m.get("policyId").toString();

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));


        var account1 = memoizedData.onceOnly(account1Id, acc -> admin1Ctx.harmonize.accounts(domainId).get(acc.getId()).isPresent(),
                () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                        .id(account1Id)
                        .alias("Testaccount_" + System.currentTimeMillis())
                        .vaultId(vaultId)
                        .ledgerId(ledgerId)
                        .submit().waitForSucceed(Timeout.minutes(1)))
        );

        var account2 = memoizedData.onceOnly(account2Id, acc -> admin1Ctx.harmonize.accounts(domainId).get(acc.getId()).isPresent(),
                () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                        .id(account2Id)
                        .alias("Testaccount-" + System.currentTimeMillis())
                        .vaultId(vaultId)
                        .ledgerId(ledgerId)
                        .submit().waitForSucceed(Timeout.minutes(1)))
        );


        var account1Address = memoizedData.onceOnly(account1.getId() + "_accountAddress1",
                a -> true,
                () -> waitForValue(60 * 5, () -> admin1Ctx.harmonize.accounts(domainId).getLatestAddress(account1.getId()).get()));

        print(account1Address);

        var account2Address = memoizedData.onceOnly(account2.getId() + "_accountAddress2",
                a -> true,
                () -> waitForValue(60 * 5, () -> admin1Ctx.harmonize.accounts(domainId).getLatestAddress(account1.getId()).get()));

        print(account2Address);

        var txId = generateId();
        var txIntentData = admin1Ctx.harmonize.transactions(domainId)
                .create()
                .id(txId)
                .fromAccount(account1)
                .parameters(Transactions.OrderParameters.btc()
                        .toAccount(account2.getId())
                        .amount(amount)
                        .priorityMedium()
                        .maximumFee(amount)
                        .build()
                )
                .submit()
                .waitForSucceed(Timeout.minutes(5));

        var txIntent = admin1Ctx.harmonize.intents(domainId).get(txIntentData.getIntentId()).get();
        print(txIntent);
        var policyReference = txIntent.getState().getProgress().stream().map(p -> p.getPolicyReference()).findFirst().get();


        assertEquals("Ensure that the policy we created with the domain was used for this intent", domainPolicyId, policyReference.getId());
        print("Policy from domain: ", domainPolicyId, " policy executed: ", policyReference.getId());

//

    }

    private <T> T waitForValue(long timeout, Supplier<T> supplier) {
        var start = System.currentTimeMillis();
        while (true) {
            if ((System.currentTimeMillis() - start) > timeout)
                throw new RuntimeException("Timeout while waiting for object " + supplier);
            var v = supplier.get();
            if (v != null)
                return v;
        }

    }

    private Accounts.CreateAccount approveAccount(LocalHarmonizeContext ctx, IntentData.AccountIntentData<Accounts.CreateAccount> accountIntent) {
        ctx.harmonize.intents(accountIntent.getIntent().getTargetDomainId())
                .approve(accountIntent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return accountIntent.getIntent().getPayload();
    }


    public static void main(String args[]) throws Throwable {
        new TestPolicyExpressionsTransactionOrder().checkTransactionOrderPolicies();
//
//        ScriptEngineManager manager = new ScriptEngineManager();
//        ScriptEngine engine = manager.getEngineByName("javascript");
//        engine.eval("console.log(1+1  + \"hi\")");
    }

}
