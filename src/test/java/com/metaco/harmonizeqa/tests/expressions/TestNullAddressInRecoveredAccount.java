package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestNullAddressInRecoveredAccount extends BaseSteps {


    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws InterruptedException {

        print("Harmonize: " + harmonize().getContext().getConfig().getGatewayURL());

        var domainId = harmonize().domains().getRootDomain().get().getId();
//        harmonize().accounts(domainId).list().forEach(a ->{
//            print("------ Account --------");
//            print(a.asJson());
//            print("-----------------------");
//        });

//        harmonize().transactions(domainId).listOrders().forEach(order -> {
//            print("--------- Order --------");
//            print(order.asJson());
//            print("------------------------");
//        });

        AtomicInteger transfers = new AtomicInteger();
        AtomicInteger nullRecipientAddress = new AtomicInteger();

        harmonize().transactions(domainId).listTransfers().forEach(transfer -> {
            transfers.incrementAndGet();
            var recipient = transfer.getRecipient();
            if(recipient instanceof Transactions.RecipientTransferPartyAccount) {
                if( ((Transactions.RecipientTransferPartyAccount) recipient).getAddress() == null){
                    print("Null address for " + transfer);
                    nullRecipientAddress.incrementAndGet();
                }
            }else if(recipient instanceof Transactions.RecipientTransferPartyAddress) {
                if(((Transactions.RecipientTransferPartyAddress) recipient).getAddress() == null){
                    print("Null address for address recipient: " + transfer);
                    nullRecipientAddress.incrementAndGet();
                }
            }
        });

        print("Transfers: " + transfers.get() + ", Null Recipient Transfers: " + nullRecipientAddress.get());
    }


    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestNullAddressInRecoveredAccount().run();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }
    }
}
