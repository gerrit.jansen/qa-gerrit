package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Domains;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Policies;
import com.metaco.harmonize.api.om.Users;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps;

import java.util.List;
import java.util.stream.Collectors;

public class BaseExpressions extends BaseSteps {

    /**
     * Creates a multi approve domain with two users.
     */
    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policy = Policies.CreateDomainGenesisPolicy.builder()
                .condition(Policies.PolicyCondition.simpleBuilder()
                        .expression(expression)
                        .build())
                .workflow(Policies.WorkflowCondition.simpleBuilder()
                        .role(adminRole)
                        .quorum(quorum)
                        .build())
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                .scope(Policies.Scope.Self)
                .build();

        return Tuple.n(harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(policy)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policy);
    }

}
