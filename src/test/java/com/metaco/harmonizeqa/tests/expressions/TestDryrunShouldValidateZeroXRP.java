package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.api.om.ledgers.xrpl.XrplMemo;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.List;
import java.util.stream.Collectors;


public class TestDryrunShouldValidateZeroXRP extends BaseSteps {

    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestDryrunShouldValidateZeroXRP_03");


    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }

    public void run() {
        var domainId = "b43ec668-f37a-4cae-b4dd-2bc7b62e8a7";

    }

    public void runDryRun() throws InterruptedException {

        harmonize().ledgers().list().forEach(System.out::println);


        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var domainMemId = "2";
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());

        var accountId = memoizedData.onceOnly(domainMemId + "-account_id", u -> true, () -> generateId());


        var m = memoizedData.onceOnly(domainMemId + "_domain",
                (domain) -> {
                    var id = ((Intents.Intent) domain).getPayloadId().get();
                    return harmonize().domains().get(id).isPresent();
                },
                () -> createDomain("true",
                        List.of(Policies.IntentType.v0_CreateAccount, Policies.IntentType.v0_CreateUser, Policies.IntentType.v0_CreateTransactionOrder, Policies.IntentType.v0_UpdatePolicy,
                                Policies.IntentType.v0_UpdateAccount, Policies.IntentType.v0_UpdatePolicy, Policies.IntentType.v0_UpdateUser, Policies.IntentType.v0_UpdateDomain, Policies.IntentType.v0_UpdateDomainPermissions,
                                Policies.IntentType.v0_ReleaseQuarantinedTransfers),
                        domainId, admin1));


        var xrplLedger = harmonize().ledgers().get("xrpl-testnet").get();
        // xrpl-testnet

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        var account = memoizedData.onceOnly(domainMemId + "_account_1",
                (ac) -> admin1Ctx.harmonize.accounts(domainId).get(accountId).isPresent(),
                () -> {
                    var accountCreate = Accounts.CreateAccount.builder()
                            .id(accountId)
                            .ledgerId("xrpl-testnet")
                            .vaultId("00000000-0000-0000-0000-000000000000")
                            .alias(accountId)
                            .targetDomainId(domainId)
                            .build();

                    var intent = admin1Ctx.harmonize
                            .accounts(domainId).create(accountCreate, Expire.hours(24)).waitForIntentExecuted(Timeout.minutes(1));
                    print("Intent create account: " + intent);
                    print(admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get());

                    return accountCreate;
                }
        );

        var address = admin1Ctx.harmonize.accounts(domainId).getLatestAddress(accountId);
        while (address.isEmpty()) {
            address = admin1Ctx.harmonize.accounts(domainId).generateAddress(accountId);
            if (address.isPresent())
                break;
            address = admin1Ctx.harmonize.accounts(domainId).getLatestAddress(accountId);
            print("Waiting for account to get an address");
            Thread.sleep(1000);
        }
        // address: rHrMUPdimRGh4G4GFmacz9nQe2tshdyax5
        // send to address rUagWW4TkxPHpTgetGv4YcjtPscViftaG3
        print(account);
        print("Address:" + address);

        admin1Ctx.harmonize.transactions(domainId)
                .listTransfers(TransactionActions.ListCriteria.builder().accountId(accountId).build()
        )
                .filter(t -> t.isQuarantined()).forEach(t -> {

            admin1Ctx.harmonize.accounts(domainId).releaseQuarantinedTransfers()
                    .accountId(accountId)
                    .transferIds(t.getId()).submit().waitForIntentExecuted(Timeout.minutes(1));

        });

        var balance = admin1Ctx.harmonize.accounts(domainId).balances(accountId).collect(Collectors.toList());
        print("Balance: ", balance);

        print("Orders");
        admin1Ctx.harmonize.transactions(domainId).listOrders(TransactionActions.ListCriteria.builder().accountId(accountId).build()).forEach(o -> {
            print(o);
        });

        print("Transactions");
        admin1Ctx.harmonize.transactions(domainId).list(TransactionActions.ListCriteria.builder().accountId(accountId).build()).forEach(o -> {
            print(o);
        });

//         Transaction with zero xrp to valid address in testnet
//        var response = admin1Ctx.harmonize.transactions(domainId).create().parameters(
//                Transactions.OrderParameters.xrpl()
//                        .memos(XrplMemo.builder()
//                                .memoType("687474703a2f2f6578616d706c652e636f6d2f6d656d6f2f67656e65726963")
//                                .memoData("72656e74").build())
//                        .priorityMedium()
//                        .maximumFee("1000")
//                        .amount("0")
//                        .toLedger("rUagWW4TkxPHpTgetGv4YcjtPscViftaG3")
//                        .build())
//                .fromAccountId(accountId)
//                .submit().waitForIntentExecuted(Timeout.minutes(10));
//
//        print("Intent data");
//        print(response);


        // Send transaction to same account
//        var response = admin1Ctx.harmonize.transactions(domainId).create().parameters(
//                Transactions.OrderParameters.xrpl()
//                        .memos(XrplMemo.builder()
//                                .memoType("687474703a2f2f6578616d706c652e636f6d2f6d656d6f2f67656e65726963")
//                                .memoData("72656e74").build())
//                        .priorityMedium()
//                        .maximumFee("1000")
//                        .amount("100")
//                        .toLedger(address.get().getAddress())
//                        .build())
//                .fromAccountId(accountId)
//                .submit().waitForIntentExecuted(Timeout.minutes(10));
//
//        print(response);
//        print("Executed intent");
//        print(admin1Ctx.harmonize.intents(domainId).get(response.getIntentId()).get());

        // Send transaction to same account
//        var response = admin1Ctx.harmonize.transactions(domainId).dryRun().parameters(
//                Transactions.OrderParameters.xrpl()
//                        .memos(XrplMemo.builder()
//                                .memoType("687474703a2f2f6578616d706c652e636f6d2f6d656d6f2f67656e65726963")
//                                .memoData("72656e74").build())
//                        .priorityMedium()
//                        .maximumFee("1000")
//                        .amount("0")
//                        .toLedger("rUagWW4TkxPHpTgetGv4YcjtPscViftaG3")
//                        .build())
//                .accountId(accountId).submit();
//
//        print(response);

    }

    public static void main(String arg[]) throws Throwable {
        AuditManager.enable();

        var r = new TestDryrunShouldValidateZeroXRP();
        try {
            r.runDryRun();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            r.memoizedData.close();
        }

    }
}
