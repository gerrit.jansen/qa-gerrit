package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestDryrunRequestNonCompletedSendYields500XRPL extends BaseSteps {

    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestDryrunRequestNonCompletedSendYields5002");

    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws Exception {

        harmonize().ledgers().list().forEach(System.out::println);
        var ledger = harmonize().ledgers().list().filter(l -> l.getId().toLowerCase().contains("xrpl")).findFirst().get();

        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var domainMemId = "10";
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());

        var accountId = memoizedData.onceOnly(domainMemId + "-account_id", u -> true, () -> generateId());

        var m = memoizedData.onceOnly(domainMemId + "_domain",
                (domain) -> {
                    var id = ((Intents.Intent) domain).getPayloadId().get();
                    return harmonize().domains().get(id).isPresent();
                },
                () -> createDomain("true",
                        List.of(Policies.IntentType.v0_CreateAccount, Policies.IntentType.v0_CreateUser, Policies.IntentType.v0_CreateTransactionOrder,
                                Policies.IntentType.v0_ReleaseQuarantinedTransfers, Policies.IntentType.v0_UpdatePolicy),
                        domainId, admin1));

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        print("Ledger Id: " + ledger.getId());
        print("DomainId: " + domainId);
        print("AccountId: " + accountId);

        admin1Ctx.harmonize.accounts(domainId).list()
//                .filter(a -> a.getId().equals("0bca7444-cb8c-4eb1-a106-2c2f6662c8bc"))
                .forEach(a -> {

                    var status = a.getAccountAdditionalDetails().map(d -> d.getStatus()).orElse(null);
                    var address = status.equalsIgnoreCase("Completed") ? admin1Ctx.harmonize.accounts(domainId).getLatestAddress(a.getId()) : null;


                    print("Account: " + a.getId() + " status: " + status + " address: " + address);
                    admin1Ctx.harmonize.accounts(domainId).balances(a.getId()).forEach(b -> {
                        if (b.getQuarantinedAmount().length() > 1) {
                            var transferIds = admin1Ctx.harmonize.transactions(domainId).listTransfers()
                                    .filter(t -> t.isQuarantined()).map(t -> t.getId()).collect(Collectors.toList());
                            if (transferIds.size() > 0) {
                                try {
                                    admin1Ctx.harmonize.accounts(domainId).releaseQuarantinedTransfers().accountId(
                                            a.getId()
                                    ).transferIds(transferIds)
                                            .submit().waitForIntentExecuted(Timeout.minutes(1));
                                } catch (Exception e) {
                                    print(e);
                                    print("Failed to release quarantined transfers for + " + a.getId());
                                }
                            }
                        }
                        print(b);
                    });

//            print(a.getId() + " status: " + status + " address: " + address);

                });

        Callable<Accounts.CreateAccount> createAccount = () -> {
            var accountCreate = Accounts.CreateAccount.builder()
                    .ledgerId(ledger.getId())
                    .vaultId("00000000-0000-0000-0000-000000000000")
                    .alias(accountId)
                    .targetDomainId(domainId)
                    .build();

            var intent = admin1Ctx.harmonize
                    .accounts(domainId).create(accountCreate, Expire.hours(24)).waitForIntentExecuted(Timeout.minutes(1));
            print("Intent create account: " + intent);
            print(admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get());

            return accountCreate;
        };

        var waits = Parallel.run(createAccount, createAccount, createAccount, createAccount, createAccount,
                createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount,
                createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount,
                createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount, createAccount);
        Parallel.waitAll(waits, 5, TimeUnit.MINUTES);


//        admin1Ctx.harmonize.accounts(domainId).list().forEach(System.out::println);


        var fromAddress = "rKF7Z6dpdvMx1xsbxVD33H7BoDMYZJC8VG";
        var fromAccountId = "e04a727d-b685-45aa-a34a-8e01863020a6";

        Function<Accounts.Account, String> sendDryRun = (Accounts.Account account) -> {

            StringBuilder buff = new StringBuilder();

            var parameters = Transactions.OrderParameters.xrpl()
                    .toDestination(TransactionDestination.AccountDestination.builder()
                            .accountId(account.getId())
                            .build())
                    .memos(List.of())
                    .maximumFee("1000")
                    .amount("1000")
                    .priorityMedium()
                    .build();

            print(parameters.asJson());
            buff.append("Using account: " + account + " \n");

            var intent = admin1Ctx.harmonize.transactions(domainId).dryRun()
                    .accountId(fromAccountId)
                    .parameters(
                            parameters
                    ).submit();

            buff.append("intent = " + intent + "\n");
            var a = admin1Ctx.harmonize.accounts(domainId).get(account.getId()).get();
            buff.append("Status = " + a.getAccountAdditionalDetails().get().getStatus() + "\n");
            return buff.toString();
        };

        List<Callable<String>> callers = admin1Ctx.harmonize.accounts(domainId)
                .list().map( a -> asCallable(a, sendDryRun)).collect(Collectors.toList());

        Collections.shuffle(callers);

        var waiters2 = Parallel.run(callers.toArray(new Callable[0]));
        var responses = Parallel.waitAll(waiters2, 10, TimeUnit.MINUTES);

        responses.forEach(System.out::println);
    }

    public static Callable<String> asCallable(Accounts.Account account, Function<Accounts.Account, String> sendDryRun) {
        return () -> sendDryRun.apply(account);
    }

    public static void main(String arg[]) throws Throwable {
        try {
            AuditManager.enable();
            new TestDryrunRequestNonCompletedSendYields500XRPL().run();
        } finally {
            try (FileWriter writer = new FileWriter("audit.json")) {
                AuditManager.write(writer);
            }
            Thread.sleep(1000);
        }

    }
}
