package com.metaco.harmonizeqa.tests.expressions;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.api.LocalHarmonizeContext;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestCreateDomainSameUserAlias extends BaseExpressions {


    public void run() throws Throwable {

        var adminRole = "admin";
        var user1 = NewUser.create("admin1", this);
        var user2 = NewUser.create("admin2", this);
        var user3 = NewUser.create("admin1", this);


        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression("true")
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(List.of(Policies.IntentType.v0_CreateAccount.name(),
                                Policies.IntentType.v0_CreatePolicy.name(),
                                Policies.IntentType.v0_UpdatePolicy.name(),
                                Policies.IntentType.v0_CreateUser.name(),
                                Policies.IntentType.v0_UpdateUser.name()
                        ))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        var domainId = UUID.randomUUID().toString();
        try {
            var intent = harmonize().domains().create()
                    .targetDomainId(currentDomain())
                    .alias(domainId)
                    .id(domainId)
                    .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                    .user(Users.CreateDomainGenesisUser.builder()
                            .alias(user1.alias)
                            .id(user1.id)
                            .publicKey(user1.generatedKeys.getPublicKey().asBase64Str())
                            .role(adminRole)
                            .build())
                    .user(Users.CreateDomainGenesisUser.builder()
                            .alias(user2.alias)
                            .id(user2.id)
                            .publicKey(user2.generatedKeys.getPublicKey().asBase64Str())
                            .id(user2.id)
                            .role(adminRole)
                            .build())
                    .user(Users.CreateDomainGenesisUser.builder()
                            .alias(user3.alias)
                            .id(user3.id)
                            .publicKey(user3.generatedKeys.getPublicKey().asBase64Str())
                            .id(user3.id)
                            .role(adminRole)
                            .build())
                    .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                    .submit().waitForIntentExecuted(Timeout.minutes(5));

            assertTrue(false); // we do not expect an exception here
        } catch (Exception e) {
            print(e);
            assertTrue(true); // we expect an exception
        }
    }


    public static void main(String args[]) throws Throwable {
        new TestCreateDomainSameUserAlias().run();
    }

}
