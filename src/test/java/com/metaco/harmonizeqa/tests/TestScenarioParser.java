package com.metaco.harmonizeqa.tests;

import com.metaco.harmonizeqa.testrails.features.Scenario;
import com.metaco.harmonizeqa.testrails.features.ScenarioParser;
import com.metaco.harmonizeqa.testrails.features.TestResult;
import com.metaco.harmonizeqa.testrails.features.TestResultsParser;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TestScenarioParser {

//    @Test
    public void testResultsParser() throws Throwable {
        Path path = Path.of("src/test/resources/features");
        List<Scenario> scenarios = ScenarioParser.parseFeatureFiles(path).collect(Collectors.toList());

        List<TestResult> results = TestResultsParser.parseResults("src/test/resources/tests/TEST-harmonizeqa.RunCucumberTest.xml", scenarios)
                .stream().filter(r -> r.getScenario() != null).collect(Collectors.toList());

        //assertTrue(results.size() > 0);
        for (TestResult result : results) {
            assertNotNull(result.getScenario());
            assertNotNull(result.getMessage());
            assertFalse(result.isOk());
        }
    }

//    @Test
    public void testScenarioParser() throws IOException {

        Path path = Path.of("src/test/resources/features");

        List<Scenario> scenarios = ScenarioParser.parseFeatureFiles(path).collect(Collectors.toList());


        assertTrue(scenarios.size() > 1);
        Scenario scenario = scenarios.get(0);
        assertNotNull(scenario.getFile());
        assertNotNull(scenario.getFeatureName());
        assertNotNull(scenario.getName());
    }

}


