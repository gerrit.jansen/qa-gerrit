package com.metaco.harmonizeqa.tests;

import java.io.Serializable;
import java.util.Objects;

public class Tuple<A, B> implements Serializable {
    public final A a;

    public Tuple(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public final B b;

    public static <A, B> Tuple<A, B> n(A a, B b) {
        return new Tuple(a, b);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(a, tuple.a) && Objects.equals(b, tuple.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    @Override
    public String toString() {
        return "Tuple{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
