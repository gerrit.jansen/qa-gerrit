package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.api.LocalHarmonizeContext;
import harmonizeqa.steps.BaseSteps;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestIntentTransactionOrderSupervisorCleanup extends MemoizedData {

    public TestIntentTransactionOrderSupervisorCleanup() {
        super("./target/test_data", "TestIntentTransactionOrderSupervisorCleanup");
    }

    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policy = Policies.CreateDomainGenesisPolicy.builder()
                .condition(Policies.PolicyCondition.simpleBuilder()
                        .expression("context.references['users'][context.request.author.id].roles.includes('" + adminRole + "')")
                        .build())
                .workflow(Policies.WorkflowCondition.simpleBuilder()
                        .role(adminRole)
                        .quorum(quorum)
                        .build())
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .intentType(Policies.IntentType.v0_CreateUser)
                .intentType(Policies.IntentType.v0_UpdatePolicy)
                .intentType(Policies.IntentType.v0_UpdateUser)
                .intentType(Policies.IntentType.v0_CreateAccount)
                .intentType(Policies.IntentType.v0_UpdateAccount)
                .intentType(Policies.IntentType.v0_CreateTransactionOrder)
                .intentType(Policies.IntentType.v0_ReleaseQuarantinedTransfers)
                .intentType(Policies.IntentType.v0_UnlockAccount)
                .intentType(Policies.IntentType.v0_UpdateEndpoint)

                .scope(Policies.Scope.Self)
                .build();

        return Tuple.n(harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(policy)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policy);
    }


    public void runTestIntentShouldExpireAfterPolicyUpdate() throws Exception {

        print("Vaults");
        print(harmonize().vaults().list());
        print("Ledgers");
        print(harmonize().ledgers().list());


        var ids = onceOnly("domainId", d -> true, () -> JSON.of("domainId", generateId()));
        print("IDS: " + ids);

        var domainId = ids.getStr("domainId");

        ids = onceOnly("accountId", a -> true, () -> JSON.of("accountId", generateId()));
        var accountId = ids.getStr("accountId");
        ids = onceOnly("accountId2", a -> true, () -> JSON.of("accountId", generateId()));
        var accountId2 = ids.getStr("accountId");

        NewUser admin1 = onceOnly("admin1", (user) -> harmonize().users(domainId).get(user.id).isPresent(), () -> NewUser.create("admin1", this));
        NewUser admin2 = onceOnly("admin2", (user) -> harmonize().users(domainId).get(user.id).isPresent(), () -> NewUser.create("admin2", this));

        print("Users: ", admin1.id, ", ", admin2.id);
        Domains.CreateDomain domain = onceOnly(domainId,
                d -> harmonize().domains().get(d.getStr("id")).isPresent(),
                () -> (Domains.CreateDomain) createDomainMultipleApproval(domainId, admin1, admin2, 2).a.getIntent().getPayload());

//
        final var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        final var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        print(domain);

        var account = onceOnly("account1", acc -> admin1Ctx.harmonize.accounts(domainId).get(acc.getId()).isPresent(),
                () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                        .id(accountId)
                        .alias(accountId)
                        .vaultId("00000000-0000-0000-0000-000000000000")
                        .ledgerId("bitcoin-testnet")
                        .submit().waitForSucceed(Timeout.minutes(1)))
        );

        print("Account1", account);

        var addresses = onceOnly(accountId + "_address",
                a -> true,
                () -> admin1Ctx.harmonize.accounts(domainId).getLatestAddress(accountId).get());


        print(addresses);

        var address = addresses.getAddress();
        if (addresses.getAddress().equals("2N6xdVnGB2wp48EWaPJt3bNsHHkLA6YCupP"))
            address = "2Mu57CXWXBb8fiSyKoMZe9dhrtRaQcXZMf1";

        print("Using source account address: ", address);

        print(harmonize().accounts(domainId).balances(accountId));
        harmonize().accounts(domainId).balances(accountId).forEach(b -> {

            if (!(b.getQuarantinedAmount().isBlank() || b.getQuarantinedAmount().equals("0"))) {
                releaseQuarantined(admin1Ctx, admin2Ctx, domainId, account);
            }

        });

        var account2 = onceOnly("account2", acc -> admin1Ctx.harmonize.accounts(domainId).get(acc.getId()).isPresent(),
                () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                        .id(accountId2)
                        .alias(accountId)
                        .vaultId("00000000-0000-0000-0000-000000000000")
                        .ledgerId("bitcoin-testnet")
                        .submit().waitForSucceed(Timeout.minutes(1)))
        );

        print("Account2", account2);

        addresses = onceOnly(accountId2 + "_address2",
                a -> true,
                () -> admin1Ctx.harmonize.accounts(domainId).getLatestAddress(accountId2).get());


        var account2Get = onceOnly(accountId2 + "_account",
                a -> true,
                () -> admin1Ctx.harmonize.accounts(domainId).get(accountId2).get());


        print(addresses);

        var address2 = addresses.getAddress();
        print("Using destination address:", address2);


        print("Source Balance:", admin1Ctx.harmonize.accounts(domainId).balances(accountId).collect(Collectors.toList()));
        print("Destination Balance:", admin1Ctx.harmonize.accounts(domainId).balances(accountId2).collect(Collectors.toList()));


        var transactionCreateIntent = onceOnly("transaction1_1_" + System.currentTimeMillis(),
                (t) -> true,
                () -> doTransfer(domainId, admin1Ctx, account, account2Get));


        var transactionIntent = admin1Ctx.harmonize.intents(domainId).get(transactionCreateIntent.getId()).get();
        assertEquals("OPEN", transactionIntent.getState().getStatus().toUpperCase());

        //get the policy used with the create transaction

        // update the policy
        var policyId = "65f42e31-740f-4ee5-8cdd-e4684154629d";

        var policyUpdate = admin1Ctx.harmonize.policies(domainId).get(policyId).get().asUpdate();
        policyUpdate.setRank(10);
        policyUpdate.setAlias("UpdatedPolicy123");

        var policyIntent = admin1Ctx.harmonize.policies(domainId).update(policyUpdate, Expire.hours(1)).waitForSucceed(Timeout.minutes(1));
        admin2Ctx.harmonize.intents(domainId).approve(policyIntent.getIntentId())
                .waitForSucceed(Timeout.minutes(1));

        Thread.sleep(5000);

        transactionIntent = admin1Ctx.harmonize.intents(domainId).get(transactionCreateIntent.getId()).get();
        print(transactionIntent);
        assertEquals("FAILED", transactionIntent.getState().getStatus().toUpperCase());

        print("Account1: ", admin1Ctx.harmonize.accounts(domainId).get(account.getId()).get());

        print("Account1.balances: ", admin1Ctx.harmonize.accounts(domainId).balances(account.getId()).collect(Collectors.toList()));

        print("Account2: ", admin1Ctx.harmonize.accounts(domainId).get(account2.getId()).get());

        print("Account2.balances: ", admin1Ctx.harmonize.accounts(domainId).balances(account2.getId()).collect(Collectors.toList()));

    }

    private Intents.Propose<?> doTransfer(String domainId, LocalHarmonizeContext admin1Ctx, Accounts.CreateAccount account, Accounts.Account account2Get) {
        return admin1Ctx.harmonize.transactions(domainId).create()
                .fromAccount(account)
                .parameters(
                        Transactions.OrderParameters.btc()
                                .toAccount(account2Get.getId())
                                .priorityMedium()
                                .maximumFee("1")
                                .amount("1")
                                .build())
                .submit()
                .waitForSucceed(Timeout.minutes(1))
                .getIntent();
    }

    private void releaseQuarantined(LocalHarmonizeContext admin1Ctx, LocalHarmonizeContext admin2Ctx, String domainId, Accounts.CreateAccount account) {

        var transferIds = getQuarantinedTransferIds(admin1Ctx, domainId, account.getId());
        if (transferIds.size() > 0) {
            print("Releasing transfers for ", transferIds);

            approveQuaranteedRelease(admin2Ctx,
                    admin1Ctx.harmonize.accounts(domainId).releaseQuarantinedTransfers()
                            .transferIds(transferIds)
                            .accountId(account.getId())
                            .submit().waitForSucceed(Timeout.minutes(1)));
        } else {
            print("No quarantined transfers found");
        }

    }

    private List<String> getQuarantinedTransferIds(LocalHarmonizeContext ctx, String domainId, String accountId) {
        return ctx.harmonize.transactions(domainId).listTransfers()
                .filter(transfer -> transfer.isQuarantined())
                .map(Transactions.Transfer::getId)
                .collect(Collectors.toList());
    }

    private Accounts.ReleaseQuarantinedTransfers approveQuaranteedRelease(LocalHarmonizeContext ctx, IntentData<Accounts.ReleaseQuarantinedTransfers> intent) {
        ctx.harmonize.intents(intent.getIntent().getTargetDomainId())
                .approve(intent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return intent.getIntent().getPayload();
    }

    private Accounts.CreateAccount approveAccount(LocalHarmonizeContext ctx, IntentData.AccountIntentData<Accounts.CreateAccount> accountIntent) {
        ctx.harmonize.intents(accountIntent.getIntent().getTargetDomainId())
                .approve(accountIntent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return accountIntent.getIntent().getPayload();
    }

    public static void main(String args[]) throws Throwable {
        new TestIntentTransactionOrderSupervisorCleanup().runTestIntentShouldExpireAfterPolicyUpdate();
    }

}
