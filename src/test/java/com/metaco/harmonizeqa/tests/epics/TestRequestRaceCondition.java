package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.BaseRequests;
import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.conf.Config;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonize.net.Requests;
import com.metaco.harmonize.sig.Signature;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import com.metaco.qasdk.Parallel;
import harmonizeqa.steps.BaseSteps;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class TestRequestRaceCondition extends BaseSteps {

    private MemoizedData memoizedData = new MemoizedData("/tmp", "TestPolicyExpressionsTransactionOrder");

    public JSON createDomain(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(1)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1))
                .getIntent();
    }


    public void run() throws Throwable {

        var admin1 = memoizedData.onceOnly("admin1.2", u -> true, () -> NewUser.create("admin1", this));
        var domainMemId = System.currentTimeMillis();
        var domainId = memoizedData.onceOnly(domainMemId + "-domain_id", u -> true, () -> generateId());

        var m = memoizedData.onceOnly(domainMemId + "_domain",
                (domain) -> {
                    var id = ((Intents.Intent) domain).getIntentId();
                    return harmonize().domains().get(id).isPresent();
                },
                () -> createDomain("true",
                        List.of(Policies.IntentType.v0_CreateAccount, Policies.IntentType.v0_CreateUser),
                        domainId, admin1));


        var intentId = UUID.randomUUID().toString();
        var testUser = NewUser.create("test_" + intentId, this);

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        var propose = Intents.Propose.builder()
                .targetDomainId(domainId)
                .author(Author.builder().domainId(domainId).id(admin1.id).build())
                .customProperties(Map.of())
                .description("Test Propose Racecondition")
                .expiry(Expire.hours(1).fromNow())
                .intentId(intentId)
                .payload(Users.CreateUser.builder()
                        .role("admin")
                        .targetDomainId(domainId)
                        .description("Test user for race condition")
                        .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                        .id(testUser.id)
                        .alias(testUser.id)
                        .customProperties(Map.of())
                        .build())
                .build();


        Signature signature = admin1Ctx.harmonize.getContext().getSigner().signPayload(
                admin1Ctx.harmonize.getContext(),
                propose
        );

        Intents.ProposeIntentBody request = Intents.ProposeIntentBody.builder()
                .propose(propose)
                .signature(signature)
                .build();

        //preload the token
        admin1Ctx.harmonize.getContext().getAuthenticator().getToken(
                admin1Ctx.harmonize.getContext()
        );

        Callable<Map<String, Object>> responseSubmit = () -> {
            var response = admin1Ctx.harmonize.users(domainId)
                    .postJson(
                            admin1Ctx.harmonize.getContext(),
                            admin1Ctx.harmonize.getContext().getConfig().getGatewayURL(),
                            "/v1/intents",
                            request,
                            "requestId", intentId);
            return Map.of("timestamp: ", System.currentTimeMillis(), "response", response);
        };

        // wake up threads
        for(int i = 0; i < 100; i++) {
            Parallel.waitAll(Parallel.run(
                    () -> 1000 * 10,
                    () -> 1000 * 10
            ), 1, TimeUnit.MINUTES);
        }
        var responseValues = Parallel.waitAll(Parallel.run(
                responseSubmit,
                responseSubmit,
                responseSubmit,
                responseSubmit,
                responseSubmit,
                responseSubmit,
                responseSubmit,
                responseSubmit
        ), 10, TimeUnit.MINUTES);

        for(var v: responseValues) {
            print(v);
        }
        Thread.sleep(5000);
        var state  = admin1Ctx.harmonize.intents(domainId).get(intentId).get().getState();
        print(state);

        state.getProgress().forEach(w -> print(w));

        assertTrue(state.getStatus().equals("Executed"));
    }

    public static void main(String arg[]) throws Throwable {

        new TestRequestRaceCondition().run();

    }
}
