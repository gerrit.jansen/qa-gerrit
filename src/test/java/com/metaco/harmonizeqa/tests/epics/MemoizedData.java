package com.metaco.harmonizeqa.tests.epics;

import harmonizeqa.steps.BaseSteps;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Only create's each data piece once, and saves a copy to disk.
 */
public class MemoizedData extends BaseSteps {

    final DB db;
    final Map<String, Object> MAP;


    final Path baseDir;
    final String url;
    final String dbName;

    public MemoizedData(String dir, String dbName) {
        this.baseDir = Path.of(dir);
        ensureCreated(baseDir);

        url = dir + "/" + dbName + ".db";
        this.dbName = dbName;
        db = DBMaker.fileDB(url).make();
        MAP = (Map<String, Object>) db.treeMap(dbName).createOrOpen();
    }

    public synchronized <T> T onceOnly(String id, Function<T, Boolean> existRemoteFn, Supplier<T> fn) {
        return withMap(m -> {
            Object obj = m.get(id);

            if (obj != null) {
                T v = (T) obj;
                if (existRemoteFn.apply(v)) {
                    return v;
                }
                // the object exist but not in remote, so we create a new instance and store it.
                v = fn.get();
                m.put(id, v);

                return v;
            } else {
                // the object doesnt exist so we create and store it
                T v = fn.get();
                m.put(id, v);
                return v;
            }
        });
    }


    public <R> R withMap(Function<Map<String, Object>, R> fn) {
        try {
            return fn.apply(MAP);
        } finally {
            db.commit();
        }
    }

    private static void ensureCreated(Path baseDir) {
        try {
            if(!baseDir.toFile().exists())
                Files.createDirectories(baseDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        db.close();
    }
}
