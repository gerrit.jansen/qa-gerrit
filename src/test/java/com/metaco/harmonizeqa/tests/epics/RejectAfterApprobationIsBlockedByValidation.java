package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import harmonizeqa.steps.BaseSteps;
import org.junit.Assert;

import java.util.UUID;
import java.util.function.Supplier;

/**
 * https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/580
 * QA: https://gitlab.internal.m3t4c0.com/silo/platform/qa-v4/-/issues/12
 */
public class RejectAfterApprobationIsBlockedByValidation extends BaseSteps {


    public IntentData createDomainMultipleApproval(String alias, NewUser admin1, NewUser admin2, int quorum) {

        String id = UUID.randomUUID().toString();

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(alias)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all("admin").build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role("admin")
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role("admin")
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(
                        Policies.CreateDomainGenesisPolicy.builder()
                                .workflow(Policies.WorkflowCondition.simpleBuilder()
                                        .role("admin")
                                        .quorum(quorum)
                                        .build())
                                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                                .intentType(Policies.IntentType.v0_CreateUser)
                                .scope(Policies.Scope.Self)
                                .build())
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));
    }

    public void run1() throws Exception {
        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);
        String domainAlias = "domain_" + System.currentTimeMillis();

        var domainIntent = createDomainMultipleApproval(domainAlias, admin1, admin2, 2);
        var domainId = domainIntent.getIntent().getPayloadId().get().toString();

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        //1. Test same user create and reject
        // admin1 creates a test user to start the multi user approval policy

        var testUser = NewUser.create("testuser1", this);

        var intent = admin1Ctx.harmonize.users(domainId).create()
                .id(testUser.id)
                .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                .alias("testuser1")
                .role("admin")
                .submit()
                .waitForProcessingOrSucceeded(Timeout.minutes(1));

        Supplier<Intents.State> intentStateGetter = () -> admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get().getState();

        var state = intentStateGetter.get();

        // This step ensures we are in a multi user approval policy
        print("State: ", state);
        Assert.assertTrue(state.isStatus(Intents.IntentStatus.Open));

        // admin1 should be allowed to reject the proposal
        admin1Ctx.harmonize.intents(domainId).reject(intent.getIntentId(),
                "Reject after approbation is blocked by validation test",
                Expire.hours(1)
        ).waitForIntentStatus(Intents.IntentStatus.Rejected, Timeout.minutes(1));

        var state2 = intentStateGetter.get();
        print("State2: ", state2);

        Assert.assertTrue(state2.isStatus(Intents.IntentStatus.Rejected));

        // Test 2 admin1 create and admin2 rejects

        var intent2 = admin1Ctx.harmonize.users(domainId).create()
                .id(testUser.id)
                .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                .alias("testuser1")
                .role("admin")
                .submit()
                .waitForProcessingOrSucceeded(Timeout.minutes(1));

        Supplier<Intents.State> intentStateGetter2 = () -> admin1Ctx.harmonize.intents(domainId).get(intent2.getIntentId()).get().getState();

        var state3 = intentStateGetter2.get();

        // This step ensures we are in a multi user approval policy
        print("State3: ", state3);
        assert state3.isStatus(Intents.IntentStatus.Open);

        // admin2 should be allowed to reject the proposal
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        Assert.assertFalse(admin2Ctx.signer.getAuthor().getId().equalsIgnoreCase(admin1Ctx.signer.getAuthor().getId()));

        admin2Ctx.harmonize.intents(domainId).reject(intent2.getIntentId(),
                "Reject after approbation is blocked by validation test",
                Expire.hours(1)
        ).waitForIntentStatus(Intents.IntentStatus.Rejected, Timeout.minutes(1));


        var state4 = intentStateGetter2.get();
        print("State4: ", state4);

        Assert.assertTrue(state4.isStatus(Intents.IntentStatus.Rejected));
    }

    public void run2() throws Exception {
        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);
        String domainAlias = "domain_" + System.currentTimeMillis();

        var domainIntent = createDomainMultipleApproval(domainAlias, admin1, admin2, 3);
        var domainId = domainIntent.getIntent().getPayloadId().get().toString();

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        //1. Test admin1 create, admin2 approve, then admin1 rejects
        // admin1 creates a test user to start the multi user approval policy

        var testUser = NewUser.create("testuser1", this);

        var intent = admin1Ctx.harmonize.users(domainId).create()
                .id(testUser.id)
                .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                .alias("testuser1")
                .role("admin")
                .submit()
                .waitForProcessingOrSucceeded(Timeout.minutes(1));

        Supplier<Intents.State> intentStateGetter = () -> admin1Ctx.harmonize.intents(domainId).get(intent.getIntentId()).get().getState();

        var state = intentStateGetter.get();

        // This step ensures we are in a multi user approval policy
        print("State: ", state);
        assert state.isStatus(Intents.IntentStatus.Open);

        // admin2 should be allowed to approve the proposal
        admin2Ctx.harmonize.intents(domainId).approve(intent.getIntentId(), Expire.hours(1)
        ).waitForSucceed(Timeout.minutes(1));

        Thread.sleep(2000);

        var state2 = intentStateGetter.get();
        print("State2: ", state2);

        Assert.assertTrue(state2.isStatus(Intents.IntentStatus.Open));

        // admin2 should be allowed to reject the proposal
        admin2Ctx.harmonize.intents(domainId).reject(intent.getIntentId(), "Reject test", Expire.hours(1)
        ).waitForIntentStatus(Intents.IntentStatus.Rejected, Timeout.minutes(1));

        var state3 = intentStateGetter.get();
        print("State3: ", state3);

        Assert.assertTrue(state3.isStatus(Intents.IntentStatus.Rejected));


    }

    public static void main(String args[]) throws Exception {
        new RejectAfterApprobationIsBlockedByValidation().run1();
//        new RejectAfterApprobationIsBlockedByValidation().run2();

    }

}
