package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.LedgerActions;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.harmonize.api.om.ledgers.btc.BitcoinRedirectTransactionOrderParameters;
import harmonizeqa.steps.BaseSteps;
import io.cucumber.java.an.E;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * See: https://metacocloud-my.sharepoint.com/:w:/r/personal/gerrit_jansen_metaco_com/_layouts/15/Doc.aspx?sourcedoc=%7BB3AE0006-0CD2-48F4-9270-8D2082684567%7D&file=BitcoinRedirect%20Removal%20Tests.docx&action=default&mobileredirect=true&DefaultItemOpen=1&ct=1644918615684&wdOrigin=OFFICECOM-WEB.START.OTHER&cid=55d131c6-9a8a-4d7a-be25-86a96408526c
 * See: https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/592
 */
public class BitcoinRedirectRemoved extends BaseSteps {

    public void run() throws Throwable {

        var ledgers = harmonize().ledgers().list();

        var account1 = harmonize().accounts(rootDomainId()).get("03be1a52-ffa2-4ada-8bad-08c3a3b307c8").get();

        var account2 = harmonize().accounts(rootDomainId()).get("ee8eb6bd-8635-4025-91d0-5d3eb1d0eb04").get();

        print(account1);

        var data = harmonize().transactions(rootDomainId())
                .create()
                .fromAccount(account1)
                .parameters(BitcoinRedirectTransactionOrderParameters.builder()
                        .amount("1")
                        .redirectedTransfers(Arrays.asList(account1.getId()))
                        .priorityHigh()
                        .maximumFee("100")
                        .toAccount(account2.getId())
                .build())
                .build()
        .waitForIntentExecuted(Timeout.minutes(1));

        print(data);
    }

    public static void main(String args[]) throws Throwable {
        new BitcoinRedirectRemoved().run();
    }
}
