package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestUserGet extends BaseSteps {



    public static void main(String args[]) throws Throwable {
        new TestUserGet().run();
    }

    private void run() {
        print(harmonize().users("5cd224fe-193e-8bce-c94c-c6c05245e2d1")
                .get("6ac20654-450e-29e4-65e2-1bdecb7db7c4").get());
    }

}
