package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.RequestActions;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import harmonizeqa.steps.BaseSteps;
import org.junit.Assert;

import java.util.UUID;

import static org.junit.Assert.assertTrue;

/**
 * Add validation to not allow posting of intent with the same id
 * <p>
 * https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/235
 * https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/merge_requests/765#77dff551296251290157332d4ad53d71c9aa2192
 * <p>
 * We only expect the validation for intent that are not approval and rejection intents.
 */
public class ValidationWithSameId extends BaseSteps {


    public IntentData createDomain(Harmonize harmonize, String alias, NewUser newUser1, NewUser newUser2) {

        String id = UUID.randomUUID().toString();

        return harmonize.domains().create()
                .targetDomainId(currentDomain())
                .alias(alias)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all("admin").build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(newUser1.alias)
                        .id(newUser1.id)
                        .publicKey(newUser1.generatedKeys.getPublicKey().asBase64Str())
                        .role("admin")
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(newUser2.alias)
                        .id(newUser2.id)
                        .publicKey(newUser2.generatedKeys.getPublicKey().asBase64Str())
                        .role("admin")
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(
                        Policies.CreateDomainGenesisPolicy.builder()
                                .workflow(Policies.WorkflowCondition.simpleBuilder()
                                        .role("admin")
                                        .quorum(2)
                                        .build())
                                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                                .intentType(Policies.IntentType.v0_CreateUser)
                                .scope(Policies.Scope.Self)
                                .build())

                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));

    }

    private void run() throws Throwable {

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);
        var admin3 = NewUser.create("admin3", this);

        var intentData = createDomain(harmonize(), "domain_" + System.currentTimeMillis(), admin1, admin2);
        intentData.waitForIntentExecuted(Timeout.minutes(1));

        var domainId = intentData.getIntent().getPayloadId().get().toString();

        var ctxUser1 = context().switchUser(admin1.userCredentials(domainId));

        // create user, we want an intentData instance here
        intentData = ctxUser1.harmonize.users(domainId)
                .create()
                .role("admin")
                .publicKey(admin3.generatedKeys.getPublicKey().asBase64Str())
                .alias(admin3.alias)
                .id(admin3.id)
                .expire(Expire.hours(24))
                .submit()
                .waitForSucceed(Timeout.minutes(1));

        print("Waiting 10 seconds before sending second intent");
        Thread.sleep(10000);

        // force double intent post
        try {
            ctxUser1.harmonize.intents(domainId).postIntent(
                    ctxUser1.harmonize.getContext(),
                    intentData.getIntent(),
                    (req, status, t) -> ctxUser1.harmonize.requests(domainId).waitForStatus(req.ensureRequestId(), status, t),
                    (req, status, t) -> ctxUser1.harmonize.intents(domainId).waitForStatus(req.getIntentId(), status, t)
            ).waitForSucceed(Timeout.minutes(1));

            assertTrue(false);
        } catch (IntentException error) {

            assertTrue(error.getIntentData().getResponse().status == 409);
            assertTrue(error.getIntentData().getResponse().body.contains("IntentAlreadyExists"));

        } catch (RequestActions.WaitForRequestFailed e) {
            print(e.getRequest());
            print(e);
            // assert that the response history contains intent already exists
            assertTrue(
                    e.getRequest().getHistory().stream()
                            .filter(h -> nullSafeString(h.getHint()).equalsIgnoreCase("[IntentAlreadyExists]"))
                            .findAny().isPresent());

            assertTrue("We received an IntentAlreadyExists but we expected a 409 http code, that should cause a different exception to be thrown", false);
        }


    }

    private static String nullSafeString(String hint) {
        return hint == null ? "" : hint;
    }

    public static void main(String args[]) throws Throwable {
        new ValidationWithSameId().run();
    }

}
