package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.Domains;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Policies;
import com.metaco.harmonize.api.om.Users;
import com.metaco.harmonizeqa.tests.NewUser;
import com.mifmif.common.regex.Generex;
import harmonizeqa.api.UserCredentials;
import harmonizeqa.steps.BaseSteps;

import java.io.FileWriter;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
public class FuzzProfileNames extends BaseSteps {


    private static final Generex generex = new Generex("[a-z0-9\\-]+");

    public void run() throws Exception {

        List<String> roles = randomRoles(5);

        var admin1 = NewUser.create("admin1", this);
        var domainId = generateId();

//        var domainIntentData = createDomainSingleApproval(roles, domainId, admin1);

        Thread.sleep(5000);


        var userHarmonize = context().switchUser(UserCredentials.from(domainId, admin1.id, admin1.generatedKeys));

        var admin2 = NewUser.create("admin2", this);
        var userCreateIntentData = userHarmonize.harmonize.users(domainId).create()
                .targetDomainId(domainId)
                .id(admin2.id)
                .alias(admin2.alias)
                .role("admin")
                .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                .description("Test role user")
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));

        print(userCreateIntentData);

    }


    public static List<String> randomRoles(int n) {
        Set<String> s = new HashSet<>();
        while (s.size() < n) {
            s.add(randomRole());
        }

        return new ArrayList<>(s);
    }

    public static final String randomRole() {
        return generex.random();
    }


    public IntentData createDomainMultipleApproval(String alias, List<String> roles, NewUser admin1, NewUser admin2, int quorum) {

        String id = UUID.randomUUID().toString();


        var adminRole = roles.get(1);

        print("Using admin role: " + adminRole);
        // Admin1 role=Admin1
        // Admin2 role=Admin2

        // create user with Admin2 match Policy role==Admin2??
        // Policy1   role Admin1, condition=null
        // Policy2   role Admin2, condition=null, role is only for permission but is never used in the policy matching
        //
        var policies = roles.stream().map(
                role -> Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression("context.references['users'][context.request.author.id].roles.includes('" + role + "')")
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(role)
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentType(Policies.IntentType.v0_CreateUser)
                        .scope(Policies.Scope.Self)
                        .build()

        ).collect(Collectors.toList());

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(alias)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(roles).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));
    }



    public void run3RoleLength() throws Exception {

        List<String> roles = randomRoles(5);
//
//        StringBuilder longRoleBuilder = new StringBuilder();
//
//        while(longRoleBuilder.length() < 50)
//            longRoleBuilder.append(roles.stream().collect(Collectors.joining("-")));
//
//        String longRole = longRoleBuilder.subSequence(0, 50) + "a";
//
//        roles.add(longRole);

//        print(roles);

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);
        String domainAlias = "domain_" + System.currentTimeMillis();


        var domainIntent = createDomainMultipleApproval(domainAlias, roles, admin1, admin2, 1);

        print(domainIntent);

        var domainId = domainIntent.getIntent().getPayloadId().get().toString();


        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        var testUser = NewUser.create("testuser1", this);

        var intent = admin1Ctx.harmonize.users(domainId).create()
                .id(testUser.id)
                .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                .alias("testuser1")
                .roles(roles)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));


        print(admin1Ctx.harmonize.users(domainId).listKnownUserRoles());

    }

    public void run2() throws Exception {

        List<String> roles = randomRoles(5);

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);
        String domainAlias = "domain_" + System.currentTimeMillis();


        var domainIntent = createDomainMultipleApproval(domainAlias, roles, admin1, admin2, 1);

        print(domainIntent);

        var domainId = domainIntent.getIntent().getPayloadId().get().toString();


        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));

        var testUser = NewUser.create("testuser1", this);

        var intent = admin1Ctx.harmonize.users(domainId).create()
                .id(testUser.id)
                .publicKey(testUser.generatedKeys.getPublicKey().asBase64Str())
                .alias("testuser1")
                .roles(roles)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));


        print(admin1Ctx.harmonize.users(domainId).listKnownUserRoles());

    }

    public static void main(String args[]) throws Exception {
        AuditManager.enable();
        try {
            new FuzzProfileNames().run();
        } finally {
            try (FileWriter writer = new FileWriter("./audit.json")) {
                AuditManager.write(writer);
            }
        }
    }
}
