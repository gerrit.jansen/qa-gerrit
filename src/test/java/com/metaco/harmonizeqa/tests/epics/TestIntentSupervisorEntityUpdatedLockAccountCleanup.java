package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Lock;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.api.LocalHarmonizeContext;
import harmonizeqa.steps.BaseSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestIntentSupervisorEntityUpdatedLockAccountCleanup extends BaseSteps {

    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policy = Policies.CreateDomainGenesisPolicy.builder()
                .condition(Policies.PolicyCondition.simpleBuilder()
                        .expression("context.references['users'][context.request.author.id].roles.includes('" + adminRole + "')")
                        .build())
                .workflow(Policies.WorkflowCondition.simpleBuilder()
                        .role(adminRole)
                        .quorum(quorum)
                        .build())
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .intentType(Policies.IntentType.v0_CreateUser)
                .intentType(Policies.IntentType.v0_UpdatePolicy)
                .intentType(Policies.IntentType.v0_CreatePolicy)
                .intentType(Policies.IntentType.v0_UpdateUser)
                .intentType(Policies.IntentType.v0_CreateAccount)
                .intentType(Policies.IntentType.v0_UpdateAccount)
                .intentType(Policies.IntentType.v0_LockAccount)

                .scope(Policies.Scope.Self)
                .build();

        return Tuple.n(harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(policy)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policy);
    }


    public void runTestIntentShouldExpireAfterPolicyUpdate() throws Exception {

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);

        var domainId = generateId();

        var tuple = createDomainMultipleApproval(domainId, admin1, admin2, 2);

        var policy = tuple.b;

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        var testUser = NewUser.create("testuser1", this);

        print("Create user in domain ", domainId, " policy: ", policy.getId());

        var accountToLock = approveAccountCreateIntent(admin2Ctx, domainId, admin1Ctx.harmonize.accounts(domainId).create()
                .id(testUser.id)
                .alias("test_account123")
                .vaultId("00000000-0000-0000-0000-000000000000")
                .ledgerId("bitcoin-testnet")
                .submit()
                .waitForSucceed(Timeout.minutes(1)));

        var lockAccountIntent = admin1Ctx.harmonize.accounts(domainId).lock(accountToLock, Lock.Locked, Expire.hours(1)).waitForSucceed(Timeout.minutes(1));

        var lockAccountIntent2 = admin1Ctx.harmonize.intents(domainId).get(lockAccountIntent.getIntentId());
        print("Create lock account intent: ", lockAccountIntent2);

        // assert that the policy was used
        var policyReferenceOpt = lockAccountIntent2.get().getState().getProgress().stream().filter(sp -> sp.getPolicyReference().getId().equalsIgnoreCase(policy.getId())).findFirst();
        assertTrue(policyReferenceOpt.isPresent());
        var policyReference = policyReferenceOpt.get();

        policyReference.getStepProgress().stream().filter(wp -> wp.getState().equals("OPEN"));


        print("Request history");
        print(admin1Ctx.harmonize.requests(domainId).get(lockAccountIntent.getRequestId()).get().getHistory());

        print("Updating account");
        Thread.sleep(1000);

        // Update Account again
        approveAccountCreateIntent(admin2Ctx, domainId,
                admin1Ctx.harmonize.accounts(domainId)
                        .update(accountToLock.asUpdate().setDescription("Test user updated"), Expire.hours(1))
                        .waitForSucceed(Timeout.minutes(1))
        );


        print("Waiting 5 seconds");
        Thread.sleep(5000);
        print("After account update we expect the previous intent ", lockAccountIntent2.get().getId(), " to fail");

        //here we expect a "No workflow exists in this domain requiring a signature"
        try {
            print(admin2Ctx.harmonize.intents(domainId).approve()
                    .intentId(lockAccountIntent2.get().getId())
                    .submit()
                    .waitForIntentExecuted(Timeout.minutes(1)));
            assertTrue("Expecting an error here", false);
        } catch (IntentException intentException) {
            print("Got expected intent exception when aproving intent: ", lockAccountIntent2.get().getId());
            assertTrue(true);
        }

        print("Request history");
        print(admin1Ctx.harmonize.requests(domainId).get(lockAccountIntent.getRequestId()).get().getHistory());
        print("Intent after policy was updated");
        print(admin1Ctx.harmonize.intents(domainId).get(lockAccountIntent.getIntentId()));

        lockAccountIntent2 = admin1Ctx.harmonize.intents(domainId).get(lockAccountIntent.getIntentId());
        print(lockAccountIntent2);

        assertEquals(lockAccountIntent2.get().getState().getStatus().toUpperCase(), "FAILED");
        assertTrue(lockAccountIntent2.get().getState().getError().getMessage().contains("is referring to old revision"));

        print("Events:");
        print(admin1Ctx.harmonize.events(domainId).list());
    }

    private Accounts.Account approveAccountCreateIntent(LocalHarmonizeContext ctx, String domainId, IntentData intentData) {
        print("Approving Intent data", ctx.harmonize.intents(domainId).get(intentData.getIntentId()));

        ctx.harmonize.intents(domainId).approve(intentData.getIntentId()).waitForSucceed(Timeout.minutes(1));
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return ctx.harmonize.accounts(domainId).get(intentData.getIntent().getPayloadId().get().toString()).orElse(null);
    }

    public static void main(String args[]) throws Throwable {
        new TestIntentSupervisorEntityUpdatedLockAccountCleanup().runTestIntentShouldExpireAfterPolicyUpdate();
    }

}
