package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.LedgerActions;
import harmonizeqa.steps.BaseSteps;

import static org.junit.Assert.assertTrue;

public class ReturnsInternalError500LedgerFees extends BaseSteps {

    public void run() throws Throwable {

        var ledgers = harmonize().ledgers().list();

        ledgers.forEach(l -> {
            print(l.getId());
            try {
                var fees = harmonize().ledgers().fees(l.getId()).get();
                print(fees);
                assertTrue(true);
            } catch (LedgerActions.LedgerError e) {
                assertTrue(e.getMessage().contains("Error getting ledger fees: 503"));
            }
        });

    }

    public static void main(String args[]) throws Throwable {
        new ReturnsInternalError500LedgerFees().run();
    }
}
