package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.msg.JSON;
import harmonizeqa.steps.BaseSteps;

import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class TezosExplore extends BaseSteps {

    public void run() throws Throwable {
        var testAccountId = "04a6db92-2498-40db-afcb-12811c04ca15";
        var ledgerOpt = harmonize().ledgers().list().filter(l -> l.getId().contains("tezos")).findAny();

        assertTrue("Tests expect a Tezos ledger", ledgerOpt.isPresent());

        var ledger = ledgerOpt.get();

        print(ledger);

        print(harmonize().ledgers().list().map(JSON::asJson));

        harmonize().accounts(rootDomainId()).list().forEach(account -> {

            print("Account: ", account);
            print("balances: ", harmonize().accounts(account.getDomainId()).balances(account.getId()).map(JSON::asJson).collect(Collectors.joining(",")));
            print("addresses: ", harmonize().accounts(account.getDomainId()).listAddresses(account.getId()).map(JSON::asJson).collect(Collectors.joining(",")));
            print("latestAddress: ", harmonize().accounts(account.getDomainId()).getLatestAddress(account.getId()).map(JSON::asJson));
            print("-------");
        });


    }

    public static void main(String args[]) throws Throwable {
        new TezosExplore().run();
    }
}
