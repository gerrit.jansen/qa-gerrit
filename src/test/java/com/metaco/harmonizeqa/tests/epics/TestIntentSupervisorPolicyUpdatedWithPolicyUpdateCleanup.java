package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestIntentSupervisorPolicyUpdatedWithPolicyUpdateCleanup extends BaseSteps {

    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policy = Policies.CreateDomainGenesisPolicy.builder()
                .condition(Policies.PolicyCondition.simpleBuilder()
                        .expression("context.references['users'][context.request.author.id].roles.includes('" + adminRole + "')")
                        .build())
                .workflow(Policies.WorkflowCondition.simpleBuilder()
                        .role(adminRole)
                        .quorum(quorum)
                        .build())
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .intentType(Policies.IntentType.v0_CreateUser)
                .intentType(Policies.IntentType.v0_UpdatePolicy)
                .intentType(Policies.IntentType.v0_CreatePolicy)
                .intentType(Policies.IntentType.v0_UpdateUser)
                .scope(Policies.Scope.Self)
                .build();

        return Tuple.n(harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(policy)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policy);
    }


    public void runTestIntentShouldExpireAfterPolicyUpdate() throws Exception {

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);

        var domainId = generateId();

        var tuple = createDomainMultipleApproval(domainId, admin1, admin2, 2);

        var policy = tuple.b;

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        var testPolicyId = generateId();

        print("Create user in domain ", domainId, " policy: ", policy.getId());

        var initialPolicyUpdate = admin1Ctx.harmonize.policies(domainId).list().findFirst().get().asUpdate()
                .setAlias("New policy Alias");

        var updatePolicyIntent = admin1Ctx.harmonize.policies(domainId)
                .update(initialPolicyUpdate, Expire.hours(1))
                .waitForSucceed(Timeout.hours(1));

        var updatePolicyIntent2 = admin1Ctx.harmonize.intents(domainId).get(updatePolicyIntent.getIntentId());
        print("Create user intent: ", updatePolicyIntent2);

        Thread.sleep(1000);

        // assert that the policy was used
        var policyReferenceOpt = updatePolicyIntent2.get().getState().getProgress().stream().filter(sp -> sp.getPolicyReference().getId().equalsIgnoreCase(policy.getId())).findFirst();
        assertTrue(policyReferenceOpt.isPresent());
        var policyReference = policyReferenceOpt.get();

        policyReference.getStepProgress().stream().filter(wp -> wp.getState().equals("OPEN"));


        print("Request history");
        print(admin1Ctx.harmonize.requests(domainId).get(updatePolicyIntent.getRequestId()).get().getHistory());

        print("Updating policy");
        Thread.sleep(1000);
        // Update policy
        var policyUpdate = admin1Ctx.harmonize.policies(domainId).get(policy.getId()).get().asUpdate()
                .setRank(10);
        var updatePolicyIntentData = admin1Ctx.harmonize.policies(domainId).update(policyUpdate, Expire.hours(24)).waitForSucceed(Timeout.minutes(1));
        // we need to approve the policy update
        print("Approving the policy update", updatePolicyIntentData);
        admin2Ctx.harmonize.intents(domainId)
                .approve()
                .intentId(updatePolicyIntentData.getIntentId()).submit()
                .waitForIntentExecuted(Timeout.minutes(1));

        print("Waiting 5 seconds");
        Thread.sleep(5000);
        print("After policy update we expect the previous intent ", updatePolicyIntent2.get().getId(), " to fail");

        //here we expect a "No workflow exists in this domain requiring a signature"
        try {
            print(admin2Ctx.harmonize.intents(domainId).approve()
                    .intentId(updatePolicyIntent2.get().getId())
                    .submit()
                    .waitForIntentExecuted(Timeout.minutes(1)));
            assertTrue("Expecting an error here", false);
        } catch (IntentException intentException) {
            print("Got expected intent exception when aproving intent: ", updatePolicyIntent2.get().getId());
            assertTrue(true);
        }

        print("Request history");
        print(admin1Ctx.harmonize.requests(domainId).get(updatePolicyIntent.getRequestId()).get().getHistory());
        print("Intent after policy was updated");
        print(admin1Ctx.harmonize.intents(domainId).get(updatePolicyIntent.getIntentId()));

        updatePolicyIntent2 = admin1Ctx.harmonize.intents(domainId).get(updatePolicyIntent.getIntentId());
        print(updatePolicyIntent2);

        assertEquals(updatePolicyIntent2.get().getState().getStatus().toUpperCase(), "FAILED");
        assertTrue(updatePolicyIntent2.get().getState().getError().getMessage().contains("is referring to old revision"));
    }

    public static void main(String args[]) throws Throwable {
        new TestIntentSupervisorPolicyUpdatedWithPolicyUpdateCleanup().runTestIntentShouldExpireAfterPolicyUpdate();
    }

}
