package com.metaco.harmonizeqa.tests.epics;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.api.LocalHarmonizeContext;
import harmonizeqa.steps.BaseSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * https://gitlab.internal.m3t4c0.com/groups/silo/platform/-/epics/18
 */
public class TestIntentSupervisorDomainUpdatedWithUpdateDomain extends BaseSteps {

    public Tuple<IntentData, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(LocalHarmonizeContext ctx, String parentDomainId, String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policy = Policies.CreateDomainGenesisPolicy.builder()
                .condition(Policies.PolicyCondition.simpleBuilder()
                        .expression("context.references['users'][context.request.author.id].roles.includes('" + adminRole + "')")
                        .build())
                .workflow(Policies.WorkflowCondition.simpleBuilder()
                        .role(adminRole)
                        .quorum(quorum)
                        .build())
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .intentType(Policies.IntentType.v0_CreateUser)
                .intentType(Policies.IntentType.v0_CreateDomain)
                .intentType(Policies.IntentType.v0_CreatePolicy)
                .intentType(Policies.IntentType.v0_CreateUser)
                .intentType(Policies.IntentType.v0_UpdateDomain)
                .intentType(Policies.IntentType.v0_UpdatePolicy)
                .intentType(Policies.IntentType.v0_UpdateDomainPermissions)
                .intentType(Policies.IntentType.v0_UpdateUser)
                .scope(Policies.Scope.Self)
                .build();

        return Tuple.n(ctx.harmonize.domains().create()
                .parentId(parentDomainId)
                .targetDomainId(parentDomainId == null ? currentDomain() : parentDomainId)
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(policy)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1)), policy);
    }


    public void runTestIntentShouldExpireAfterPolicyUpdate() throws Exception {

        var admin1 = NewUser.create("admin1", this);
        var admin2 = NewUser.create("admin2", this);

        var parentDomainId = generateId();
        var domainId = generateId();

        // Create the parent domain
        createDomainMultipleApproval(context(), null, parentDomainId, admin1, admin2, 2);


        var parentAdmin1Ctx = context().switchUser(admin1.userCredentials(parentDomainId));
        var parentAdmin2Ctx = context().switchUser(admin2.userCredentials(parentDomainId));

        // Create the sub domain over which we'll perform the domain specific updates which are only visible in the parent domain
        var tuple = createDomainMultipleApproval(parentAdmin1Ctx, parentDomainId, domainId, admin1, admin2, 2);
        var policy = tuple.b;

        var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
        var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

        print("Create user in domain ", domainId, " policy: ", policy.getId());


        var updateDomain = parentAdmin1Ctx.harmonize.domains().get(domainId).get().asUpdate()
                .setDescription("Domain update test123");

        var updateDomainIntent = parentAdmin1Ctx.harmonize.domains().update(updateDomain, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(1));


        Thread.sleep(1000);

        var updateDomainIntent2 = parentAdmin1Ctx.harmonize.intents(parentDomainId).get(updateDomainIntent.getIntentId());
        print("Update domain intent: ", updateDomainIntent2);

        Thread.sleep(1000);

        // assert that the policy was used
        var policyReferenceOpt = updateDomainIntent2.get().getState().getProgress().stream().filter(sp -> sp.getPolicyReference().getId().equalsIgnoreCase(policy.getId())).findFirst();
        assertTrue(policyReferenceOpt.isPresent());
        var policyReference = policyReferenceOpt.get();

        policyReference.getStepProgress().stream().filter(wp -> wp.getState().equals("OPEN"));


        print("Request history");
        print(parentAdmin1Ctx.harmonize.requests(parentDomainId).get(updateDomainIntent.getRequestId()).get().getHistory());

        print("Updating domain");
        Thread.sleep(1000);
        // Update domain
        var domainUpdate = parentAdmin1Ctx.harmonize.domains().get(domainId).get().asUpdate()
                .setDescription("New description for domain");

        var updateDomainIntentData = parentAdmin1Ctx.harmonize.domains().update(domainUpdate, Expire.hours(24)).waitForSucceed(Timeout.minutes(1));
        // we need to approve the policy update
        print("Approving the domain update", updateDomainIntentData);
        parentAdmin2Ctx.harmonize.intents(parentDomainId)
                .approve()
                .intentId(updateDomainIntentData.getIntentId()).submit()
                .waitForIntentExecuted(Timeout.minutes(1));

        print("Waiting 5 seconds");
        Thread.sleep(5000);
        print("After domain update we expect the previous intent ", updateDomainIntent2.get().getId(), " to fail");

        //here we expect a "No workflow exists in this domain requiring a signature"
        try {
            print(parentAdmin1Ctx.harmonize.intents(parentDomainId).approve()
                    .intentId(updateDomainIntent2.get().getId())
                    .submit()
                    .waitForIntentExecuted(Timeout.minutes(1)));
            assertTrue("Expecting an error here", false);
        } catch (IntentException intentException) {
            print("Got expected intent exception when approving intent: ", updateDomainIntent2.get().getId());
            assertTrue(true);
        }

        print("Request history");
        print(parentAdmin1Ctx.harmonize.requests(parentDomainId).get(updateDomainIntent.getRequestId()).get().getHistory());
        print("Intent after domain was updated");
        print(parentAdmin1Ctx.harmonize.intents(parentDomainId).get(updateDomainIntent.getIntentId()));

        updateDomainIntent2 = parentAdmin1Ctx.harmonize.intents(parentDomainId).get(updateDomainIntent.getIntentId());
        print(updateDomainIntent2);

        assertEquals(updateDomainIntent2.get().getState().getStatus().toUpperCase(), "FAILED");
        assertTrue(updateDomainIntent2.get().getState().getError().getMessage().contains("is referring to old revision"));
    }

    public static void main(String args[]) throws Throwable {
        new TestIntentSupervisorDomainUpdatedWithUpdateDomain().runTestIntentShouldExpireAfterPolicyUpdate();
    }

}
