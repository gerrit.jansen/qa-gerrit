package com.metaco.harmonizeqa.tests;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.api.GeneratedKeys;
import harmonizeqa.steps.BaseSteps;
import org.junit.Assert;

import java.util.UUID;

/**
 * https://metacocloud-my.sharepoint.com/personal/gerrit_jansen_metaco_com/_layouts/15/doc.aspx?sourcedoc={fb543c03-7095-4f5a-a12e-93cf886ac69c}&action=edit
 * https://gitlab.internal.m3t4c0.com/silo/platform/silo-core/-/issues/254
 *
 * Harmonize spins in infinite error loop when creating an account for a ledger that does not exist.
 */
public class TestAccountCreateUnknownLedger extends BaseSteps {

    public void run() {
        var accountId = UUID.randomUUID().toString();
        var domainId = harmonize().domains().getRootDomain().get().getId();

        harmonize().ledgers().list().forEach( l -> print(l.getId()));

        var accountCreate = Accounts.CreateAccount.builder()
                .id(accountId)
                .ledgerId("bitcoin-testnet1")
                .vaultId("00000000-0000-0000-0000-000000000000")
                .alias(accountId)
                .targetDomainId(domainId)
                .build();

        var intent = harmonize()
                .accounts(domainId).create(accountCreate, Expire.hours(24)).waitForIntentExecuted(Timeout.minutes(1));

        print(intent);
    }


    public static void main(String arg[]) throws Throwable {
        AuditManager.enable();

        new TestAccountCreateUnknownLedger().run();

        AuditManager.write(new java.io.File(".").getAbsolutePath() + "/audit.json");
    }
}
