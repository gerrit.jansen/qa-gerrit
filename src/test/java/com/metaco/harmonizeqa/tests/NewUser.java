package com.metaco.harmonizeqa.tests;

import com.metaco.harmonize.msg.JSON;
import harmonizeqa.api.GeneratedKeys;
import harmonizeqa.api.UserCredentials;
import harmonizeqa.steps.BaseSteps;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.UUID;

public class NewUser extends JSON.MapJSON {

    public final String id;
    public final String alias;
    public final GeneratedKeys generatedKeys;

    public NewUser(String id, String alias, GeneratedKeys generatedKeys) {
        super(Map.of("id", id, "alias", alias, "generatedKeys", generatedKeys));
        this.id = id;
        this.alias = alias;
        this.generatedKeys = generatedKeys;
    }

    public UserCredentials userCredentials(String domainId) {
        return UserCredentials.from(domainId, id, generatedKeys);
    }

    public static NewUser create(String alias, BaseSteps steps) {
        String id = UUID.randomUUID().toString();
        GeneratedKeys userKeys = steps.requestKeys();

        return new NewUser(id, alias, userKeys);
    }

}
