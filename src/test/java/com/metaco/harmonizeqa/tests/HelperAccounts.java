package com.metaco.harmonizeqa.tests;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Tickers;
import harmonizeqa.steps.BaseSteps;

import java.util.Locale;
import java.util.stream.Collectors;

public class HelperAccounts extends BaseSteps {

    public void printTickers() {
        print(harmonize()
                .tickers()
                .list()
                .map(Object::toString)
                .collect(Collectors.joining("\n")));

    }

    public void printAccountBalance(String domainId, String accountId) {
        print(harmonize().accounts(domainId).balances(accountId));

    }


    public void forceBalanceUpdate(String domainId, String accountId) {
        print(harmonize().accounts(domainId).get(accountId).get());
        harmonize().accounts(domainId).forceUpdateBalance(accountId);
    }

    public void printAccountAddress(String domainId, String accountId) {

        var account = harmonize().accounts(domainId).get(accountId).get();
        print(account);
        var address = harmonize().accounts(domainId).listAddresses(accountId).collect(Collectors.toList());
        if (address.size() == 0) {
            print("Address:", harmonize().accounts(domainId).generateAddress(accountId).get());
        } else {
            print("Address: ", address.stream().map(Object::toString).collect(Collectors.joining(" ")));
        }
    }

    public void listDomains() {
        var domains = harmonize().domains().list().collect(Collectors.toList());
        System.out.println("Domains: " + domains.size());

        domains.stream().limit(10).forEach(System.out::println);
    }

    public void createTicker() {

        var createTicker = harmonize().tickers().create()
                .ledgerId("ethereum-testnet-rinkeby")
                .name("TestTokenToken")
                .kind(Tickers.TickerKind.Contract)
                .decimals(18)
                .symbol("TTT")
                .ledgerDetails(Tickers.TickerLedgerDetails.eth().ERC20("0xcf0bb3fca8aad4e149ebd09125ac3c88e0040ac3").build())
                .expire(Expire.hours(24))
                .submit()
                .waitForSucceed(Timeout.minutes(1))
                .waitForIntentExecuted(Timeout.minutes(1));
        print(createTicker);


    }

    public void createBadTicker() {

        var ctx = harmonize().getContext();
        var token = harmonize().getContext().getAuthenticator().getToken(ctx);


        var createTicker = harmonize().tickers().create()
                .ledgerId("bitcoin-testnet")
                .name("Token12")
                .kind(Tickers.TickerKind.Native)
                .decimals(18)
                .symbol("TTT")
                .ledgerDetails(Tickers.TickerLedgerDetails.btc().Native().build())
                .expire(Expire.hours(24))
                .submit()
                .waitForSucceed(Timeout.minutes(1))
                .waitForIntentExecuted(Timeout.minutes(1));
        print(createTicker);

    }


    public void run() throws InterruptedException {
        listDomains();
//        harmonize().accounts(currentDomain()).list()
//                .forEach(a -> forceBalanceUpdate(currentDomain(), a.getId()));

//        Thread.sleep(10000);

//        harmonize().accounts(currentDomain()).list()
//                .filter(a -> a.getLedgerId().equalsIgnoreCase("ethereum-testnet-rinkeby"))
//                .forEach(a -> printAccountBalance(currentDomain(), a.getId()));


        createTicker();
        createTicker();
        createTicker();

//        createBadTicker();
        printTickers();

    }


    public static void main(String args[]) throws InterruptedException {
        new HelperAccounts().run();

    }
}
