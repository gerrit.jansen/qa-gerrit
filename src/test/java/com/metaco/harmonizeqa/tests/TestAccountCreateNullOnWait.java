package com.metaco.harmonizeqa.tests;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.audit.AuditManager;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.api.GeneratedKeys;
import harmonizeqa.steps.BaseSteps;
import org.junit.Assert;

import java.util.UUID;

/**
 * String id = UUID.randomUUID().toString();
 * String vaultId = memory().get("vault_id");
 * <p>
 * Accounts.CreateAccountProviderDetails createAccountProviderDetails = Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
 * .accountKeyStrategy(Accounts.AccountKeyStrategy.VaultHard)
 * .vaultId(vaultId)
 * .build();
 * <p>
 * <p>
 * Accounts.CreateAccount createAccount = Accounts.CreateAccount.builder()
 * .ledgerId(ledgerId)
 * .vaultId(vaultId)
 * .providerDetails(createAccountProviderDetails)
 * .alias(String.format("%s account %s", ledgerId, id))
 * .id(id)
 * .build();
 * <p>
 * you can authorise as genesis user and
 * 6ac20654-450e-29e4-65e2-1bdecb7db7c4  for user id
 * 5cd224fe-193e-8bce-c94c-c6c05245e2d1 -  user domain
 * <p>
 * put harmonize().accounts("5cd224fe-193e-8bce-c94c-c6c05245e2d1")
 * .create(author.build(), "18a86c5d-663d-49e0-b249-88bc9e16d508", createAccount, Expire.hours(1));
 */
public class TestAccountCreateNullOnWait extends BaseSteps {

    public IntentData createDomain(String alias, NewUser newUser1) {

        String id = UUID.randomUUID().toString();

        return harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(alias)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all("admin").build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(newUser1.alias)
                        .id(newUser1.id)
                        .publicKey(newUser1.generatedKeys.getPublicKey().asBase64Str())
                        .role("admin")
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policy(
                        Policies.CreateDomainGenesisPolicy.builder()
                                .workflow(Policies.WorkflowCondition.simpleBuilder()
                                        .role("admin")
                                        .quorum(1)
                                        .build())
                                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                                .intentType(Policies.IntentType.v0_CreateAccount)
                                .intentType(Policies.IntentType.v0_CreateEndpoint)
                                .intentType(Policies.IntentType.v0_UpdateVault)
                                .intentType(Policies.IntentType.v0_CreateUser)

                                .scope(Policies.Scope.SelfAndDescendants)
                                .build())

                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));

    }

    public void runWithAuthorOnMethodCall() {
        String id = UUID.randomUUID().toString();
        var user1 = NewUser.create("admin1", this);

        String vaultId = "00000000-0000-0000-0000-000000000000";

        var rootDomainVault = harmonize().vaults(currentDomain()).list().findFirst().get();

        IntentData domainIntent = createDomain("domain_" + System.currentTimeMillis(),
                new NewUser(
                        context().userCredentials.getId(),
                        "admin",
                        new GeneratedKeys(context().userCredentials.getPrivateKey(), context().userCredentials.getPublicKey())
                ));

        String domainId = domainIntent.getIntent().getPayloadId().get().toString();


        Accounts.CreateAccountProviderDetails createAccountProviderDetails = Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
                .accountKeyStrategy(Accounts.AccountKeyStrategy.VaultHard)
                .vaultId(vaultId)
                .build();

        Ledgers.Ledger ledger = harmonize().ledgers().list().findFirst().get();


        Accounts.CreateAccount createAccount = Accounts.CreateAccount.builder()
                .ledgerId(ledger.getId())
                .vaultId(vaultId)
                .providerDetails(createAccountProviderDetails)
                .alias(String.format("%s account %s", ledger.getId(), id))
                .id(id)
                .build();

        String targetDomainId = domainId;
        var harmonizeCtxUser1 = context().switchUser(user1.userCredentials(targetDomainId));

        var accountIntentData = harmonize().accounts(currentDomain())
                .create(
                        Author.builder()
                                .id(user1.id)
                                .domainId(targetDomainId)
                                .build(),
                        targetDomainId,
                        createAccount, Expire.hours(1));

        accountIntentData.waitForSucceed(Timeout.minutes(1));
        accountIntentData.waitForIntentExecuted(Timeout.minutes(1));

        var createAccountIntentState = harmonizeCtxUser1.harmonize.intents(targetDomainId).get(accountIntentData.getIntentId()).get().getState();
        Assert.assertTrue(createAccountIntentState.isStatus(Intents.IntentStatus.Executed));

    }

    public void runWithSwitchUser() {
        String id = UUID.randomUUID().toString();
        var user1 = NewUser.create("admin1", this);

        String vaultId = "00000000-0000-0000-0000-000000000000";

        var rootDomainVault = harmonize().vaults(currentDomain()).list().findFirst().get();

        IntentData domainIntent = createDomain("domain_" + System.currentTimeMillis(), user1);

        String domainId = domainIntent.getIntent().getPayloadId().get().toString();


        Accounts.CreateAccountProviderDetails createAccountProviderDetails = Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
                .accountKeyStrategy(Accounts.AccountKeyStrategy.VaultHard)
                .vaultId(vaultId)
                .build();

        Ledgers.Ledger ledger = harmonize().ledgers().list().findFirst().get();


        Accounts.CreateAccount createAccount = Accounts.CreateAccount.builder()
                .ledgerId(ledger.getId())
                .vaultId(vaultId)
                .providerDetails(createAccountProviderDetails)
                .alias(String.format("%s account %s", ledger.getId(), id))
                .id(id)
                .build();

        String targetDomainId = domainId;
        var harmonizeCtxUser1 = context().switchUser(user1.userCredentials(targetDomainId));

        var accountIntentData = harmonizeCtxUser1.harmonize.accounts(targetDomainId).create(createAccount, Expire.hours(1));

        accountIntentData.waitForSucceed(Timeout.minutes(1));
        accountIntentData.waitForIntentExecuted(Timeout.minutes(1));

        var createAccountIntentState = harmonizeCtxUser1.harmonize.intents(targetDomainId).get(accountIntentData.getIntentId()).get().getState();
        Assert.assertTrue(createAccountIntentState.isStatus(Intents.IntentStatus.Executed));

    }

    public static void main(String arg[]) throws Throwable {
        AuditManager.enable();

        new TestAccountCreateNullOnWait().runWithSwitchUser();

        new TestAccountCreateNullOnWait().runWithAuthorOnMethodCall();

        AuditManager.write(new java.io.File(".").getAbsolutePath() + "/audit.json");
    }
}
