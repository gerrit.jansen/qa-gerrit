package com.metaco.harmonizeqa.transfers;

import com.google.common.collect.Lists;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps2;
import org.junit.Assert;

import java.util.*;
import java.util.stream.Collectors;

public class TransactionFilterTests extends BaseSteps2 {

    public void run() throws Throwable {

        var domainTransactions = harmonize().domains().list()
                .map(domain ->
                        Tuple.n(domain.getId(),
                                harmonize().transactions(domain.getId()).list().collect(Collectors.toList())))
                .collect(Collectors.groupingBy(v -> v.a,
                        Collectors.flatMapping(t -> t.b.stream(), Collectors.toList())));

        domainTransactions.forEach((domainId, transactions) ->
                testPaging(domainId, new ArrayList<>(transactions)));
    }

    public void testPaging(String domainId, List<Transactions.Transaction> transactions) {
        if (transactions.size() == 0)
            return;

        if (!domainId.equals("b317336c-8b4b-47ce-b230-01a5c0838138"))
            return;

        int partitionSize = 5;
        String ledgerId = null; //transactions.stream().map(t -> t.getLedgerId()).findFirst().orElse(null);
        Transactions.LedgerTransactionStatus ledgerStatus = null;  //Transactions.LedgerTransactionStatus.Confirmed;//Transactions.LedgerTransactionStatus.Confirmed;

        String accountId = null; /*transactions.stream()
                .filter(t -> t.getRelatedAccounts() != null && t.getRelatedAccounts().size() > 0)
                .flatMap(t -> t.getRelatedAccounts().stream())
                .filter(account -> account.getId() != null)
                .map(account -> account.getId()).findFirst().orElse(null); */

        String ledgerTransactionId = null; /* transactions.stream()
                .filter(t -> t.getLedgerTransactionData() != null && t.getLedgerTransactionData().getLedgerTransactionId() != null)
                .map(t -> t.getLedgerTransactionData().getLedgerTransactionId()).findFirst().orElse(null); */

        String orderReferenceDomainId = null; /* transactions.stream()
                .filter(t -> t.getOrderReference() != null)
                .map(t -> t.getOrderReference().getDomainId()).findFirst().orElse(null); */

        String orderReferenceId = null; /* transactions.stream()
                .filter(t -> t.getOrderReference() != null)
                .map(t -> t.getOrderReference().getId()).findFirst().orElse(null); */

        Transactions.ProcessingStatus processingStatus = null; // Transactions.ProcessingStatus.Completed;

        // print transactions
        if (false) {
            print(transactions);
        }

        var listCriteria = TransactionActions.ListCriteria.builder()
                .ledgerId(ledgerId)
                .ledgerStatus(ledgerStatus)
                .accountId(accountId)
                .ledgerTransactionId(ledgerTransactionId)
                .orderReferenceDomainId(orderReferenceDomainId)
                .orderReferenceId(orderReferenceId)
                .processingStatus(processingStatus)
                .sortBy("id").sortOrder(SortOrder.DESC).limit(5).build();


        print("Test Paging for domain: " + domainId);

        var filteredTx = transactions.stream().filter(listCriteria::filter).collect(Collectors.toList());

        if (partitionSize > 0) {

            // sort in desc by transaction
            Collections.sort(filteredTx, (t1, t2) -> t2.getId().compareTo(t1.getId()));

            var partitionsCalculated = Lists.partition(filteredTx, partitionSize);

            String startingAfter = null;

            if (partitionsCalculated.size() == 0) {
                var txPage = harmonize().transactions(domainId).listTransactionsCollection(listCriteria);
                System.out.println("Expect zero transactions, found: " + txPage.getCount());
                Assert.assertEquals(0, txPage.getCount());
            } else {

                for (int i = 0; i < partitionsCalculated.size(); i++) {

                    var calculatedPage = partitionsCalculated.get(i);

                    System.out.println(calculatedPage.size());

                    listCriteria = (TransactionActions.ListCriteria) listCriteria.updateStartingAfter(startingAfter);

                    var txPage = harmonize().transactions(domainId).listTransactionsCollection(listCriteria);
                    print(listCriteria.toFilterString() + " " + "expected: " + calculatedPage.size() + ", got: " + txPage.getCount());
                    for (int a = 0; a < calculatedPage.size(); a++) {
                        var expectedTx = calculatedPage.get(a);
                        if (a < txPage.getCount()) {
                            var tx = txPage.items().get(a);
                            System.out.println("expectedTx: " + expectedTx.getId() + " = " + tx.getId());
                            Assert.assertEquals(expectedTx.getId(), tx.getId());
                            if (ledgerId != null) {
                                System.out.println("expectedLedgerId: " + ledgerId + " = " + tx.getLedgerId());
                                Assert.assertEquals(ledgerId, tx.getLedgerId());
                            }
                            if (ledgerStatus != null) {
                                System.out.println("expectedLedgerStatus: " + ledgerStatus + " = " + Transactions.LedgerTransactionStatus.valueOf(tx.getLedgerStatus()));
                                Assert.assertEquals(ledgerStatus, Transactions.LedgerTransactionStatus.valueOf(tx.getLedgerStatus()));
                            }

                            if (accountId != null) {
                                print("expectedAccountID: " + accountId + " found: " + tx.getRelatedAccounts().stream().map(account -> account.getId()).collect(Collectors.toList()));
                                Assert.assertTrue(tx.getRelatedAccounts().stream().anyMatch(account -> account.getId().equals(accountId)));
                            }

                            if (ledgerTransactionId != null) {
                                var ledgerTransactionData = tx.getLedgerTransactionData();
                                var ledgerTransactionDataId = ledgerTransactionData == null ? null : ledgerTransactionData.getLedgerTransactionId();
                                print("expectedLedgerTransactionId: " + ledgerTransactionId + " found: " + ledgerTransactionDataId);
                                Assert.assertEquals(ledgerTransactionId, ledgerTransactionDataId);
                            }


                            if (orderReferenceDomainId != null) {
                                var orderReference = tx.getOrderReference();
                                var foundOrderReferenceDomainId = orderReference == null ? null : orderReference.getDomainId();
                                print("expectedOrderReferenceDomainId: " + orderReferenceDomainId + " found: " + foundOrderReferenceDomainId);
                                Assert.assertEquals(orderReferenceDomainId , foundOrderReferenceDomainId);
                            }

                            if (orderReferenceId != null) {
                                var orderReference = tx.getOrderReference();
                                var foundOrderReferenceId = orderReference == null ? null : orderReference.getId();
                                print("expectedOrderReferenceId: " + orderReferenceId + " found: " + foundOrderReferenceId);
                                Assert.assertEquals(orderReferenceId , foundOrderReferenceId);
                            }

                            if (processingStatus != null) {
                                var foundProcessingStatus = tx.getProcessingStatus();
                                print("expectedFoundProcessingStatus: " + processingStatus + " found: " + foundProcessingStatus);
                                Assert.assertEquals(processingStatus.name() , foundProcessingStatus);
                            }
                        } else {
                            System.out.println("expectedTx: " + expectedTx.getId() + " = Non found");
                            Assert.assertTrue(false);
                        }
                    }


                    if (txPage.getCount() > 0) {
                        startingAfter = txPage.items().get(txPage.getCount() - 1).getId();
                    }
                }
            }
        }

        print("==================================================================");


    }


    public static void main(String args[]) throws Throwable {
        new TransactionFilterTests().run();
    }
}
