package com.metaco.harmonizeqa.transfers;

import com.google.common.collect.Lists;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps2;
import org.junit.Assert;

import java.util.*;
import java.util.stream.Collectors;

public class OrderFilterTests extends BaseSteps2 {

    public void run() throws Throwable {

        var domainTransactions = harmonize().domains().list()
                .map(domain ->
                        Tuple.n(domain.getId(),
                                harmonize().transactions(domain.getId()).list().collect(Collectors.toList())))
                .collect(Collectors.groupingBy(v -> v.a,
                        Collectors.flatMapping(t -> t.b.stream(), Collectors.toList())));

        var domainOrders = harmonize().domains().list()
                .map(domain ->
                        Tuple.n(domain.getId(),
                                harmonize().transactions(domain.getId()).listOrders().collect(Collectors.toList())))
                .collect(Collectors.groupingBy(v -> v.a,
                        Collectors.flatMapping(t -> t.b.stream(), Collectors.toList())));

        domainOrders.forEach((domainId, orders) ->
                testPaging(domainId, domainTransactions.get(domainId), new ArrayList<>(orders)));
    }

    private static final Optional<Transactions.Transaction> findOrderTransaction(List<Transactions.Transaction> transactions, Transactions.TransactionOrder order) {
        return transactions.stream().filter(t -> t.getOrderReference() != null && t.getOrderReference().getId().equals(order.getId())).findFirst();
    }

    public void testPaging(String domainId, List<Transactions.Transaction> transactions, List<Transactions.TransactionOrder> orders) {
        if (orders.size() == 0)
            return;

        if (!domainId.equals("b317336c-8b4b-47ce-b230-01a5c0838138"))
            return;

        int partitionSize = 5;
        String ledgerId = null; //transactions.stream().map(t -> t.getLedgerId()).findFirst().orElse(null);

        String parametersType = null; //"Ethereum";
        String metadataCreatedBy = null; /* orders.stream().filter(o -> o.getMetadata() != null && o.getMetadata().getCreatedBy() != null)
                .map(o -> o.getMetadata().getCreatedBy().getId()).findFirst().orElse(null); */

        String metadataLastModifiedBy = null;/* orders.stream().filter(o -> o.getMetadata() != null && o.getMetadata().getLastModifiedBy() != null)
                .map(o -> o.getMetadata().getLastModifiedBy().getId()).findFirst().orElse(null); */

        String metadataDescription = null; /*orders.stream().filter(o -> o.getMetadata() != null && o.getMetadata().getDescription() != null)
                .map(o -> o.getMetadata().getDescription().split(" ")[0]).findFirst().orElse(null);*/

        var metadataCustomPropertiesItem = (orders.stream().filter(o -> o.getMetadata() != null && o.getMetadata().getCustomProperties() != null && o.getMetadata().getCustomProperties().size() > 0)
                .map(o -> o.getMetadata().getCustomProperties().keySet().toArray().toString()).findFirst().orElse(null));
        List<String> metadataCustomProperties = metadataCustomPropertiesItem != null ? List.of(metadataCustomPropertiesItem) : null;


        String accountId = null; /* orders.stream()
                .filter(t -> t.getAccountId() != null)
                .map(t -> t.getAccountId()).findFirst().orElse(null); */


        // print orders
        if (false) {
            print(orders);
        }

        final var listCriteria = TransactionActions.ListOrdersCriteria.builder()
                .ledgerId(ledgerId)
                .accountId(accountId)
                .parametersType(parametersType)
                .metadataCreatedBy(metadataCreatedBy)
                .metadataLastModifiedBy(metadataLastModifiedBy)
                .metadataDescription(metadataDescription)
                .metadataCustomProperties(metadataCustomProperties)
                .sortBy("id").sortOrder(SortOrder.DESC).limit(5).build();


        print("Test Paging for domain: " + domainId);

        var filteredTx = orders.stream()
                .map(o -> Tuple.n(findOrderTransaction(transactions, o), o))
                .filter(t -> listCriteria.filter(t.a.orElse(null), t.b)).collect(Collectors.toList());

        var iteratorListCriteria = listCriteria;
        if (partitionSize > 0) {

            // sort in desc by transaction
            Collections.sort(filteredTx, (t1, t2) -> t2.b.getId().compareTo(t1.b.getId()));

            var partitionsCalculated = Lists.partition(filteredTx, partitionSize);

            String startingAfter = null;

            if (partitionsCalculated.size() == 0) {
                var txPage = harmonize().transactions(domainId).listOrdersCollection(iteratorListCriteria);
                System.out.println("Expect zero transactions, found: " + txPage.getCount());
                Assert.assertEquals(0, txPage.getCount());
            } else {

                for (int i = 0; i < partitionsCalculated.size(); i++) {

                    var calculatedPage = partitionsCalculated.get(i);

                    System.out.println(calculatedPage.size());

                    iteratorListCriteria = (TransactionActions.ListOrdersCriteria) iteratorListCriteria.updateStartingAfter(startingAfter);

                    var txPage = harmonize().transactions(domainId).listOrdersCollection(iteratorListCriteria);

                    print(listCriteria.toFilterString() + " " + "expected: " + calculatedPage.size() + ", got: " + txPage.getCount());

                    for (int a = 0; a < calculatedPage.size(); a++) {
                        var expectedOrderTuple = calculatedPage.get(a);
                        if (a < txPage.getCount()) {
                            var tx = txPage.items().get(a);
                            System.out.println("expectedOrder: " + expectedOrderTuple.b.getId() + " = " + tx.getData().getId());
                            Assert.assertEquals(expectedOrderTuple.b.getId(), tx.getData().getId());
                            var foundOrderTX = findOrderTransaction(transactions, tx.getData());

                            if (ledgerId != null) {
                                System.out.println("expectedLedgerId: " + ledgerId + " = " + foundOrderTX.map(t -> t.getLedgerId()));
                                Assert.assertEquals(ledgerId, foundOrderTX.map(t -> t.getLedgerId()).orElse(null));
                            }

                            if (accountId != null) {
                                System.out.println("expectedAccountId: " + accountId + " = " + tx.getData().getAccountId());
                                Assert.assertEquals(accountId, tx.getData().getAccountId());
                            }

                            if (parametersType != null) {
                                System.out.println("parametersType: " + parametersType + " = " + tx.getData().getParameters().getType());
                                Assert.assertEquals(parametersType, tx.getData().getParameters().getType());
                            }

                            if (metadataCreatedBy != null) {
                                System.out.println("metadataCreatedBy: " + metadataCreatedBy + " = " + tx.getData().getMetadata().getCreatedBy());
                                Assert.assertEquals(metadataCreatedBy, tx.getData().getMetadata().getCreatedBy().getId());
                            }

                            if (metadataLastModifiedBy != null) {
                                System.out.println("metadataLastModifiedBy: " + metadataLastModifiedBy + " = " + tx.getData().getMetadata().getLastModifiedBy());
                                Assert.assertEquals(metadataLastModifiedBy, tx.getData().getMetadata().getLastModifiedBy().getId());
                            }
                            if (metadataDescription != null) {
                                System.out.println("metadataDescription: " + metadataDescription + " = " + tx.getData().getMetadata().getDescription());
                                Assert.assertTrue(tx.getData().getMetadata().getDescription().contains(metadataDescription));
                            }

                            if (metadataCustomProperties != null) {
                                System.out.println("metadataCustomProperties: " + Arrays.toString(metadataCustomProperties.toArray(new String[0])) + " = " + JSON.of(tx.getData().getMetadata().getCustomProperties()));
                                Assert.assertTrue(tx.getData().getMetadata().getCustomProperties().keySet().stream().allMatch(p -> metadataCustomProperties.contains(p)));
                            }
                        } else {
                            System.out.println("expectedOrder: " + expectedOrderTuple.b.getId() + " = Non found");
                            Assert.assertTrue(false);
                        }
                    }


                    if (txPage.getCount() > 0) {
                        startingAfter = txPage.items().get(txPage.getCount() - 1).getData().getId();
                    }
                }
            }
        }

        print("==================================================================");


    }


    public static void main(String args[]) throws Throwable {
        new OrderFilterTests().run();
    }
}
