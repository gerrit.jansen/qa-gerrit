package com.metaco.harmonizeqa.transfers;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.Transactions;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonizeqa.tests.Tuple;
import harmonizeqa.steps.BaseSteps2;
import org.junit.Assert;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransferFilterTests extends BaseSteps2 {

    public void run() throws Throwable {

        var domainTransactions = harmonize().domains().list()
                .map(domain ->
                        Tuple.n(domain.getId(),
                                harmonize().transactions(domain.getId()).list().collect(Collectors.toList())))
                .collect(Collectors.groupingBy(v -> v.a,
                        Collectors.flatMapping(t -> t.b.stream(), Collectors.toList())));

        var domainTransfers = harmonize().domains().list()
                .map(domain ->
                        Tuple.n(domain.getId(),
                                harmonize().transactions(domain.getId()).listTransfers().collect(Collectors.toList())))
                .collect(Collectors.groupingBy(v -> v.a,
                        Collectors.flatMapping(t -> t.b.stream(), Collectors.toList())));

        domainTransfers.forEach((domainId, transfers) ->
                testPaging(domainId, domainTransactions.get(domainId), new ArrayList<>(transfers)));
    }

    private static Stream<String> accountIds(Transactions.Transfer transfer) {
        var sender = transfer.getSender();
        var recipient = transfer.getRecipient();
        var senders = transfer.getSenders();


        Set<String> accountIds = new HashSet<>();
        if (sender != null && sender.isAccount())
            accountIds.add(sender.asAccount().getAccountId());
        if (recipient != null && recipient.isAccount())
            accountIds.add(recipient.asAccount().getAccountId());

        if (senders != null && senders.size() > 0) {
            var senderAccountIds = senders.stream().filter(s -> s.isAccount()).map(s -> sender.asAccount().getAccountId()).collect(Collectors.toList());
            accountIds.addAll(senderAccountIds);
        }

        return accountIds.stream();
    }

    public void testPaging(String domainId, List<Transactions.Transaction> transactions, List<Transactions.Transfer> transfers) {
        if (transfers.size() == 0)
            return;

        if (!domainId.equals("b317336c-8b4b-47ce-b230-01a5c0838138"))
            return;

        int partitionSize = 5;
        String transactionId = null; //transfers.stream().filter(t -> t.getTransactionId() != null).map(t -> t.getTransactionId()).findFirst().orElse(null);
        String tickerId = null; //transfers.stream().filter(t-> t.getTickerId() != null).map(t -> t.getTickerId()).findFirst().orElse(null);
        Boolean quarantined = null;
        Transactions.TransferKind kind = null; //Transactions.TransferKind.Transfer;
        String accountId = null;/* transfers.stream()
                .filter(t -> t != null)
                .flatMap(t -> accountIds(t)).findFirst().orElse(null); */

        // print orders
        if (false) {
            print(transfers);
        }

        final var listCriteria = TransactionActions.ListTransfersCriteria.builder()
                .transactionId(transactionId)
                .tickerId(tickerId)
                .quarantined(quarantined)
                .transferKind(kind)
                .accountId(accountId)
                .sortBy("id").sortOrder(SortOrder.DESC).limit(5).build();


        print("Test Paging for domain: " + domainId);

        var filteredTx = transfers.stream()
                .filter(listCriteria::filter).collect(Collectors.toList());

        var iteratorListCriteria = listCriteria;
        if (partitionSize > 0) {

            // sort in desc by transaction
            Collections.sort(filteredTx, (t1, t2) -> t2.getId().compareTo(t1.getId()));

            var partitionsCalculated = Lists.partition(filteredTx, partitionSize);

            String startingAfter = null;

            if (partitionsCalculated.size() == 0) {
                var txPage = harmonize().transactions(domainId).listTransfersCollection(listCriteria);
                System.out.println("Expect zero transactions, found: " + txPage.getCount());
                Assert.assertEquals(0, txPage.getCount());
            } else {

                for (int i = 0; i < partitionsCalculated.size(); i++) {

                    var calculatedPage = partitionsCalculated.get(i);

                    System.out.println(calculatedPage.size());

                    iteratorListCriteria = (TransactionActions.ListTransfersCriteria) iteratorListCriteria.updateStartingAfter(startingAfter);

                    var txPage = harmonize().transactions(domainId).listTransfersCollection(iteratorListCriteria);

                    print(listCriteria.toFilterString() + " " + "expected: " + calculatedPage.size() + ", got: " + txPage.getCount());

                    for (int a = 0; a < calculatedPage.size(); a++) {
                        var expectedTransfer = calculatedPage.get(a);
                        if (a < txPage.getCount()) {
                            var tx = txPage.items().get(a);
                            System.out.println("expectedTransfer: " + expectedTransfer.getId() + " = " + tx.getId());
                            System.out.println("txAccountIds: " + accountIds(tx).collect(Collectors.joining(", ")));
                            Assert.assertEquals(expectedTransfer.getId(), tx.getId());


                            if (accountId != null) {
                                var hasAccount = tx.hasSendersAccountId(accountId) || tx.hasRecipientAccountId(accountId);
                                System.out.println("expectedAccountId: " + accountId + " = " + hasAccount);
                                Assert.assertTrue(hasAccount);
                            }

                            if (transactionId != null) {
                                System.out.println("expectedTransactionId " + transactionId + " = " + tx.getTransactionId());
                                Assert.assertEquals(transactionId, tx.getTransactionId());
                            }

                            if (tickerId != null) {
                                System.out.println("expectedTickerId " + tickerId + " = " + tx.getTickerId());
                                Assert.assertEquals(tickerId, tx.getTickerId());
                            }

                            if(quarantined != null){
                                System.out.println("expectedQuarantined " + quarantined + " = " + tx.isQuarantined());
                                Assert.assertEquals(quarantined, tx.isQuarantined());
                            }

                            if(kind != null){
                                System.out.println("expectedTransferKind " + kind + " = " + tx.getKind());
                                Assert.assertEquals(kind.name(), tx.getKind());
                            }

                        } else {
                            System.out.println("expectedTransfer: " + expectedTransfer.getId() + " = Non found");
                            Assert.assertTrue(false);
                        }
                    }


                    if (txPage.getCount() > 0) {
                        startingAfter = txPage.items().get(txPage.getCount() - 1).getId();
                    }
                }
            }
        }

        print("==================================================================");


    }

    public static void main(String args[]) throws Throwable {
        new TransferFilterTests().run();
    }
}
