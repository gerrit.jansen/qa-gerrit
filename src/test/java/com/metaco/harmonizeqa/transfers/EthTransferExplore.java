package com.metaco.harmonizeqa.transfers;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Transactions;
import harmonizeqa.steps.BaseSteps2;

import java.util.List;
import java.util.stream.Collectors;

public class EthTransferExplore extends BaseSteps2 {


    public void run() throws InterruptedException {

        harmonize().ledgers().list().forEach(System.out::println);
        var testConfig = new DomainTestConfig();
        testConfig.ledgerId = "ethereum-testnet-rinkeby";


        var testContext = getTestContext("5", testConfig);

        var domainId = testContext.domain.getId();
        var account1Id = testContext.account1.getId();
        var account2Id = testContext.account2.getId();

        var account1 = testContext.getAdmin1Context().harmonize.accounts(domainId).get(account1Id).get();
        var account2 = testContext.getAdmin1Context().harmonize.accounts(domainId).get(account2Id).get();

        var account1Address = testContext.getAdmin1Context().harmonize.accounts(domainId).getLatestAddress(account1Id).get();

        print("Account1: ", account1Id);
        print("Account1 details", account1);
        print("Address1 Address: " + account1Address);
        print("Account2: ", account2Id);
        print("Account2 details", account2);


        var balances = testContext.getAdmin1Context().harmonize.accounts(domainId).balances(account1Id).collect(Collectors.toList());
        print("Balances: ");
        print(balances);

        // Check and release funds
        if (true) {
            //Explore sending from one account to the other
            for (var accountId : List.of(account1Id, account2Id)) {
                var balances2 = testContext.getAdmin1Context().harmonize.accounts(domainId).balances(accountId).collect(Collectors.toList());
                balances2.forEach(balance -> {

                    if (!(balance.getQuarantinedAmount().isBlank() || balance.getQuarantinedAmount().equals("0"))) {
                        print("Balance has quarantined funds:", balance.getQuarantinedAmount());

                        releaseQuarantined(testContext.getAdmin1Context(), testContext.getAdmin1Context(), domainId, accountId);
                    }

                });
            }

        } else {
            print("release quarantined deactivated");
        }

        // make a transfer
        if (false) {

//            transferFromAccount1ToExternalAddress(testContext, domainId, "0x05AbFB1f00c94a94CF035E27E4B859dDeE9259eB", account1);
            transferFromAccount1ToAccount2(testContext, domainId, account1Id, account2Id, account1);
            return;
        }

        // explore balances
        if (true) {
            print("Transfers");
            testContext.getAdmin1Context().harmonize.transactions(domainId).listTransfers()
                    .forEach(System.out::println);
            print("Orders");
            testContext.getAdmin1Context().harmonize.transactions(domainId).listOrders()
                    .forEach(System.out::println);

            print("transactions");
            testContext.getAdmin1Context().harmonize.transactions(domainId).list()
                    .forEach(System.out::println);

        }
    }

    private void transferFromAccount1ToAccount2(DomainTestInitContext testContext, String domainId, String account1Id, String account2Id, com.metaco.harmonize.api.om.Accounts.Account account1) throws InterruptedException {
        //balance is 1000000000000000
        var ethParams = Transactions.OrderParameters.eth()
                .amount("100")
                .priorityHigh()
                .toAccount(account2Id)
                .maximumFee("1000000000000000")
                .build();


        Transactions.TransactionDryRun dryRun = testContext.getAdmin1Context().harmonize.transactions(domainId).dryRun()
                .accountId(account1.getId())
                .parameters(
                        ethParams
                )
                .submit();

        print(dryRun);

        if (false) {
            print("Check that the transfer would run with a dry run intent, if fine remove the switch here");
            return;
        }
        var transferIntent = testContext.getAdmin1Context().harmonize.transactions(domainId).create()
                .fromAccountId(account1Id)
                .parameters(ethParams).submit()
                .waitForIntentExecuted(Timeout.minutes(5));

        var transactionOrderId = transferIntent.getIntent().getPayloadId().get();
        print("Transfer made: " + transferIntent);
        print("TransactionOder Id " + transactionOrderId);

        var transactionOrder = testContext.getAdmin1Context().harmonize.transactions(domainId).getTransactionOrder(transactionOrderId).get();

        print("TransactionOrder: ", transactionOrder);

        while (true) {
            var tx = testContext.getAdmin1Context().harmonize.transactions(domainId).list()
                    .filter(t -> t.getOrderReference() != null && t.getOrderReference().getId().equals(transactionOrderId)).findFirst().orElse(null);

            if (tx != null) {
                print("Transaction: ", tx.getProcessingDetails(), " ledger:", tx.getLedgerStatus(), " tx: data", tx.getLedgerTransactionData());
                print(tx);
                if (tx.getProcessingDetails().getStatus().equals("Completed") && tx.getLedgerStatus() != null
                        && tx.getLedgerStatus().equals("Detected")) {
                    print("Transaction completed and on ledger");

                    Thread.sleep(2000);
                    print("Transfers for transaction: ");
                    testContext.getAdmin1Context().harmonize.transactions(domainId).listTransfers().filter(f -> f.getTransactionId().equals(tx.getId())).forEach(
                            System.out::println
                    );

                    return;
                }
            } else {
                print("No transaction for order reference found yet: ", transactionOrderId);
            }

            Thread.sleep(10000);
        }
    }

    private void transferFromAccount1ToExternalAddress(DomainTestInitContext testContext, String domainId, String toAccountAddress, com.metaco.harmonize.api.om.Accounts.Account account1) throws InterruptedException {
        //balance is 1000000000000000
        var ethParams = Transactions.OrderParameters.eth()
                .amount("10")
                .priorityLow()
                .toLedgerAddress(toAccountAddress)
                .maximumFee("3000000000000000")
                .build();


        Transactions.TransactionDryRun dryRun = testContext.getAdmin1Context().harmonize.transactions(domainId).dryRun()
                .accountId(account1.getId())
                .parameters(
                        ethParams
                )
                .submit();

        print(dryRun);

        if (false) {
            print("Check that the transfer would run with a dry run intent, if fine remove the switch here");
            return;
        }
        var transferIntent = testContext.getAdmin1Context().harmonize.transactions(domainId).create()
                .fromAccountId(account1.getId())
                .parameters(ethParams).submit()
                .waitForIntentExecuted(Timeout.minutes(5));

        var transactionOrderId = transferIntent.getIntent().getPayloadId().get();
        print("Transfer made: " + transferIntent);
        print("TransactionOder Id " + transactionOrderId);

        var transactionOrder = testContext.getAdmin1Context().harmonize.transactions(domainId).getTransactionOrder(transactionOrderId).get();

        print("TransactionOrder: ", transactionOrder);

        while (true) {
            var tx = testContext.getAdmin1Context().harmonize.transactions(domainId).list()
                    .filter(t -> t.getOrderReference() != null && t.getOrderReference().getId().equals(transactionOrderId)).findFirst().orElse(null);

            if (tx != null) {
                print("Transaction: ", tx.getProcessingDetails(), " ledger:", tx.getLedgerStatus(), " tx: data", tx.getLedgerTransactionData());
                print(tx);
                if (tx.getProcessingDetails().getStatus().equals("Completed") && tx.getLedgerStatus() != null
                        && tx.getLedgerStatus().equals("Detected")) {
                    print("Transaction completed and on ledger");

                    Thread.sleep(2000);
                    print("Transfers for transaction: ");
                    testContext.getAdmin1Context().harmonize.transactions(domainId).listTransfers().filter(f -> f.getTransactionId().equals(tx.getId())).forEach(
                            System.out::println
                    );

                    return;
                }
            } else {
                print("No transaction for order reference found yet: ", transactionOrderId);
            }

            Thread.sleep(10000);
        }
    }

    public static void main(String args[]) throws Throwable {
        new EthTransferExplore().run();
    }
}
