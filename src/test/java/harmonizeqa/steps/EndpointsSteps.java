package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Accounts;
import com.metaco.harmonize.api.om.Endpoints;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Intents;
import harmonizeqa.TransactionsEnums;
import harmonizeqa.api.conf.JsonHelper;
import io.cucumber.java.en.And;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Permissions;
import java.util.Set;
import java.util.UUID;

public class EndpointsSteps extends BaseSteps{

    @And("endpoint using {string} address is created")
    public void endpointUsingAddressIsCreated(String account) throws Exception {
        String accountId = memory().get(account).toString();
        Accounts.Account acc = harmonize().accounts(currentDomain())
                .get(accountId)
                .orElseThrow(() -> new Exception(String.format("Account with id %s is not found in HMZ", accountId)));

        String address = harmonize().accounts(currentDomain()).getLatestAddress(acc.getId()).get().getAddress();
        createEndpoint(UUID.randomUUID().toString(), address, null, acc.getLedgerId());
    }

    @And("endpoint with ABI is created")
    public void endpointWithABIIsCreated() throws IOException {
        // Metaco test token contract address
        String contractAddress = "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc";

        Path filePath = Paths.get(JsonHelper.getNormalizedPath(System.getProperty("abi.file.path")));
        Endpoints.EthereumEndpointLedgerParameters parameters = Endpoints.EthereumEndpointLedgerParameters.builder()
                .abi(Files.readString(filePath, StandardCharsets.UTF_8))
                .build();
        createEndpoint(UUID.randomUUID().toString(), contractAddress, parameters, TransactionsEnums.Tickers.ETHER.getLedgerId());
    }

    public void createEndpoint(String endpointId, String address, Endpoints.EthereumEndpointLedgerParameters parameters, String ledgerId){
        Endpoints.CreateEndpoint createEndpoint = Endpoints.CreateEndpoint.builder()
                .id(endpointId)
                .address(address)
                .parameters(parameters)
                .trustScore(50)
                .alias(endpointId)
                .ledgerId(ledgerId)
                .locked(false)
                .submit();

        IntentData intentData = harmonize().endpoints(currentDomain()).create(createEndpoint, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));

        endpointsHelper.storeCreatedEndpoint(currentDomain(), createEndpoint, intentData);
        memory().set("endpointId", endpointId);
    }
}
