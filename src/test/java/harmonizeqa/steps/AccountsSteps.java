package harmonizeqa.steps;

import com.google.gson.JsonObject;
import com.metaco.harmonize.api.*;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.TransactionsEnums;
import harmonizeqa.api.AccountsHelper;
import harmonizeqa.api.EntityWrapper;
import harmonizeqa.api.conf.JsonHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.*;
import java.util.stream.Collectors;

import static harmonizeqa.steps.DomainSteps.theCurrentDomainIsARootDomain;

public class AccountsSteps extends BaseSteps {

    @And("pre-conditions for accounts balances recovery are met")
    public void preConditionsForAccountsBalancesRecoveryAreMet() throws RuntimeException{

        List<JsonObject> accountsData = JsonHelper.getJsonObjects(System.getProperty("accounts.recovery.config"));
        if(harmonize().accounts(currentDomain())
                .list().count() == 0){

            accountsData.forEach(item -> accountIsCreated(item.get("ledgerId").getAsString()));
        }
        List<Accounts.VaultAccountKeyInformation> keyStrategies = new ArrayList<>();

        accountsData.forEach(item -> keyStrategies.add(Accounts.AccountDerivedVaultAccountKeyInformation
                .create(JsonHelper.getJsonObjectAsMap(item.get("keyInformation").getAsJsonObject()))));

        keyStrategies.forEach(item -> {

            String ledgerId = accountsData.get(keyStrategies.indexOf(item)).getAsJsonObject().get("ledgerId").getAsString();
            AccountsActions.ListCriteria listCriteria = AccountsActions.ListCriteria.builder()
                    .domainId(currentDomain())
                    .sortBy("metadata.createdAt")
                    .sortOrder(SortOrder.ASC)
                    .ledgerId(ledgerId)
                    .build();

            Accounts.Account acc = harmonize().accounts(currentDomain())
                    .list(listCriteria).filter(account -> keyInfoEquals(account, item))
                    .findAny().orElseThrow(() -> new RuntimeException("Environment is not ready. Accounts which can be recovered are not present and cannot be created"));

            if(!accountsHelper.isAccountPresent(acc.getId())){
                accountsHelper.addAccount(currentDomain(), acc, "test");
                accountsHelper.setAccountAddresses(acc.getId(),
                        Objects.requireNonNull(harmonize().accounts(currentDomain()).getLatestAddress(acc.getId()).orElse(null)).getAddress());
            }
        });
    }

    private boolean keyInfoEquals(Accounts.Account acc, Accounts.VaultAccountKeyInformation item) {
        Accounts.VaultAccountKeyInformation keyInfo = ((Accounts.VaultAccountProviderInfo) acc.getProviderInfo()).getKeyInfo();
        return keyInfo.equals(item);
    }


    @And("we have two recovered {string} accounts")
    public void weHaveTwoRecoveredAccounts(String ledgerId) {
        List<EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>> accounts = accountsHelper.getAccountsByLedgerId(ledgerId);

        accounts.sort(Comparator.comparing(o -> o.getEntity().getMetaData().getCreatedAt()));

        accounts.forEach(account ->
        {
            String name = String.format("account%s", accounts.indexOf(account) + 1);
            memory().set(name, account.getId());
            harmonize().accounts(currentDomain()).waitForProcessingStatus(account.getId(), Set.of(Accounts.ProcessingStatus.Completed.name()), Timeout.minutes(2));
            accountsHelper.addPseudonymToAccount(account.getId(), name);
            accountsHelper.storeCurrentBalances(account, harmonize().accounts(account.getDomainId()).balances(account.getId()).collect(Collectors.toList()));
        });
    }

    @When("create {string} account request is submitted")
    public void createBitcoinAccountRequestIsSubmitted(String ledgerId) {
        String id = UUID.randomUUID().toString();
        String vaultId = memory().get("vault_id");

        Accounts.CreateAccountProviderDetails createAccountProviderDetails = Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
                .accountKeyStrategy(Accounts.AccountKeyStrategy.VaultHard)
                .vaultId(vaultId)
                .build();


        Accounts.CreateAccount createAccount = Accounts.CreateAccount.builder()
                .ledgerId(ledgerId)
                .vaultId(vaultId)
                .providerDetails(createAccountProviderDetails)
                .alias(id)
                .id(id)
                .build();

        try {
            IntentData<Accounts.CreateAccount> intentData = harmonize().accounts(currentDomain()).create(createAccount, Expire.hours(1))
                    .waitForSucceed(Timeout.minutes(3))
                    .waitForIntentStatus(
                            Set.of(Intents.IntentStatus.Approved,
                                    Intents.IntentStatus.Executed),
                            Timeout.minutes(3))
                    .waitForAccountProcessingStatus(Set.of(Accounts.ProcessingStatus.Completed), Timeout.minutes(5));
            memory().set("last_proposed_intent", intentData);
        }catch (RuntimeException ex){
            memory().set("intent_error", ex);
        }
    }

    @And("{string} account is created")
    public void accountIsCreated(String arg0) {
        accountNumberIsCreated(arg0, "1");
    }

    // the step sends funds from recovered account in root domain to account specified by pseudonym
    @And("ethereum transfers are sent from recovered root domain account to account number {string} and released")
    public void transfersAreSentFromRecoveredRootDomainAccountToAccountNumberAndReleased(String accountNumber) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> recoveredAccount = accountsHelper.getAccountByPseudonym("account1");

        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        String accountDomain = account.getDomainId();

        String accountAddress = harmonize().accounts(accountDomain).getLatestAddress(account.getId()).get().getAddress();

        // login as root admin user and send funds from recovered account, and release funds (coercive action from root domain)
        theCurrentDomainIsARootDomain();
        UserSteps.userIsLoggedIn("default");
        String txOrder =  transactionsHelper.sendETHFromAccountToAddress(recoveredAccount.getId(), recoveredAccount.getDomainId(), accountAddress);
        List<String> transferIds = transactionsHelper.fetchTransferIdsByTxOrder(txOrder, accountDomain);
        transactionsHelper.releaseQuarantinedTransfers(account.getId(), accountDomain, transferIds);
    }

    @And("ethereum transaction is sent from recovered root domain account to account number {string}")
    public void ethereumTransactionIsSentFromRecoveredRootDomainAccountToAccountNumber(String accountNumber) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> recoveredAccount = accountsHelper.getAccountByPseudonym("account1");

        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        String accountDomain = account.getDomainId();
        String accountAddress = harmonize().accounts(accountDomain).getLatestAddress(account.getId()).get().getAddress();

        // login as root admin user and send funds from recovered account, and release funds (coercive action from root domain)
        theCurrentDomainIsARootDomain();
        UserSteps.userIsLoggedIn("default");
        String txOrder =  transactionsHelper.sendETHFromAccountToAddress(recoveredAccount.getId(), recoveredAccount.getDomainId(), accountAddress);
        memory().set("transaction_order_id", txOrder);
    }

    @And("sent ethereum transfers are released to account number {string}")
    public void sentEthereumTransfersAreReleasedToAccountNumber(String accountNumber) throws Exception {
        String txOrder = memory().get("transaction_order_id");
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);

        List<String> transferIds = transactionsHelper.fetchTransferIdsByTxOrder(txOrder, account.getDomainId());
        transactionsHelper.releaseQuarantinedTransfers(account.getId(), account.getDomainId(), transferIds);
    }

    @And("Metaco Test Token is minted to account number {string}")
    public void metacoTestTokenIsMintedToAccountNumber(String accountNumber) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        transactionsHelper.mintMetacoTestTokenToAccount(account.getId(), account.getDomainId());
    }

    @When("account number {string} lock intent is executed")
    public void accountNumberLockIntentIsExecuted(String accountNumber) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        Accounts.Account entity = harmonize().accounts(account.getDomainId()).get(account.getId()).get();
        harmonize().accounts(account.getDomainId()).lock(entity, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));
    }

    @When("account number {string} unlock intent is executed")
    public void accountNumberUnlockIntentIsExecuted(String accountNumber) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        Accounts.Account entity = harmonize().accounts(account.getDomainId()).get(account.getId()).get();
        harmonize().accounts(account.getDomainId()).unlock(entity, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));
    }

    @Then("account number {string} lock state is {string}")
    public void accountNumberLockStateIs(String accountNumber, String lockState) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym("account" + accountNumber);
        Assert.assertEquals(harmonize().accounts(account.getDomainId()).get(account.getId()).get().getLock(), lockState);
    }

    @And("{string} account number {string} is created")
    public void accountNumberIsCreated(String arg0, String arg1) {
        String id = UUID.randomUUID().toString();
        String vaultId = memory().get("vault_id");

        Accounts.CreateAccountProviderDetails createAccountProviderDetails = Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
                .accountKeyStrategy(Accounts.AccountKeyStrategy.VaultHard)
                .vaultId(vaultId)
                .build();


        Accounts.CreateAccount createAccount = Accounts.CreateAccount.builder()
                .ledgerId(arg0)
                .vaultId(vaultId)
                .providerDetails(createAccountProviderDetails)
                .alias(id)
                .id(id)
                .build();

        IntentData<Accounts.CreateAccount> intentData = harmonize().accounts(currentDomain()).create(createAccount, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3))
                .waitForAccountProcessingStatus(Set.of(Accounts.ProcessingStatus.Completed), Timeout.minutes(5));

        harmonize().accounts(currentDomain()).waitForProcessingStatus(id, Set.of(Accounts.ProcessingStatus.Completed.name()), Timeout.minutes(2));
        AccountsHelper.waitForAccountAddress(harmonize(), currentDomain(), id, Timeout.minutes(2));

        accountsHelper.storeCreatedAccount(currentDomain(), createAccount, intentData, "account" + arg1);
        accountsHelper.setAccountAddresses(id, Objects.requireNonNull(harmonize().accounts(currentDomain()).getLatestAddress(createAccount.getId()).orElse(null)).getAddress());
        memory().set("account" + arg1, id);
    }
}

