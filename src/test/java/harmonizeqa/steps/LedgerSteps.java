package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.LedgerActions;
import com.metaco.harmonize.api.om.Ledgers;
import com.metaco.harmonize.api.om.ledgers.btc.BitcoinCurrentFees;
import com.metaco.harmonize.api.om.ledgers.btc.BitcoinFee;
import com.metaco.harmonize.api.om.ledgers.eth.EthereumCurrentFees;
import com.metaco.harmonize.api.om.ledgers.eth.EthereumFee;
import com.metaco.harmonize.api.om.ledgers.xrpl.XRPLCurrentFees;
import com.metaco.harmonize.api.om.ledgers.xrpl.XrplFee;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LedgerSteps extends BaseSteps {

    @Given("we have ledgers")
    public static void weHaveLedgers() {
        assert harmonize().ledgers().list().count() > 0;
    }

    @Then("all ledgers should return fees")
    public static void allLedgersShouldReturnFees() {

        List<Ledgers.Ledger> ledgers = harmonize().ledgers().list(LedgerActions.ListCriteria.builder().enabledOnly(true).build()).collect(Collectors.toList());
        assertTrue(ledgers.size() > 0);

        Exception error = null;
        String errorLedgers = "";
        for (Ledgers.Ledger ledger : ledgers) {
            try {
                System.out.println("Ledger: " + ledger.getId());
                Optional<Ledgers.CurrentFees> feesOpt = harmonize().ledgers().fees(ledger.getId());
                assertTrue(feesOpt.isPresent());

                Ledgers.CurrentFees fees = feesOpt.get();

                String ledgerId = ledger.getId().toLowerCase();

                if (ledgerId.contains("bitcoin")) {
                    assert fees instanceof BitcoinCurrentFees;
                    BitcoinFee high = fees.getHigh();
                    BitcoinFee medium = fees.getMedium();
                    BitcoinFee low = fees.getLow();
                    assertFees(high);
                    assertFees(medium);
                    assertFees(low);

                } else if (ledgerId.contains("eth")) {
                    assert fees instanceof EthereumCurrentFees;
                    EthereumFee high = fees.getHigh();
                    EthereumFee medium = fees.getMedium();
                    EthereumFee low = fees.getLow();
                    assertFees(high);
                    assertFees(medium);
                    assertFees(low);
                } else if (ledgerId.contains("xrpl")) {
                    assert fees instanceof XRPLCurrentFees;
                    XrplFee high = fees.getHigh();
                    XrplFee medium = fees.getMedium();
                    XrplFee low = fees.getLow();
                    assertFees(high);
                    assertFees(medium);
                    assertFees(low);
                }


            } catch (Exception e) {
                error = e;
                errorLedgers += " " + ledger.getId();
                System.out.println("Error listing fees");
            }
        }

        if (error != null) {
            throw new RuntimeException(error + " trying to list fees for " + errorLedgers);
        }

    }

    private static void assertFees(XrplFee fee) {
        assertNotNull(fee.getMultiplier());
        assertNotNull(fee.getFeeMap());
    }


    private static void assertFees(EthereumFee fee) {
        assertNotNull(fee.getGasPrice());
        assertNotNull(fee.getFeeMap());
    }

    private static void assertFees(BitcoinFee fee) {
        assertNotNull(fee.getSatoshiPerVbyte());
        assertNotNull(fee.getFeeMap());
    }
}
