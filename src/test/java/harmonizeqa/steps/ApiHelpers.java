package harmonizeqa.steps;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.UsersActions;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.conf.Context;
import com.metaco.harmonize.net.Requests;
import harmonizeqa.api.GeneratedKeys;
import harmonizeqa.api.Key;
import harmonizeqa.api.UserCredentials;

import java.net.URL;
import java.util.*;
import java.util.function.Supplier;

public class ApiHelpers {

    public static void waitForAccountProcessed(Harmonize harmonize, Author author, Domains.Domain domain, String accountId, Timeout timeout) {

        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {

            Optional<Accounts.Account> accountOpt = harmonize.accounts(domain.getId()).get(accountId);
            if (accountOpt.isPresent()) {
                Accounts.Account account = accountOpt.get();
                if (account.getAccountAdditionalDetails().isPresent() && account.getAccountAdditionalDetails().get().hasStatus(Set.of("Completed")))
                    return;
            }

        }

        throw new RuntimeException("Timed out while waiting for " + accountId + " account to become processed " + harmonize.accounts(domain.getId()).get(accountId));
    }

    public static <T> EntityData<T> acceptIntent(Harmonize harmonize, Author author, EntityData<T> entityData) {


        ApproveIntentData intentData = harmonize.intents(entityData.domainId)
                .approve(entityData.intentData.getIntentId(), Expire.hours(24));

        intentData.waitForProcessingOrSucceeded(Timeout.minutes(4));

        return new EntityData<>(
                entityData.domainId,
                entityData.entityGet,
                entityData.intentGet,
                intentData
        );
    }

    public static EntityData<Endpoints.Endpoint> createEndpoint(Harmonize harmonize, Author author, Domains.Domain domain, Accounts.Address address) {
        String id = UUID.randomUUID().toString();

        IntentData intentData = harmonize.endpoints(domain.getId())
                .create()
                .address(address)
                .trustScore(50)
                .ledgerId("bitcoin-testnet")
                .alias(id)
                .description("Test endpoint")
                .submit();

        intentData.waitForProcessingOrSucceeded(Timeout.minutes(4));

        return new EntityData<>(
                domain.getId(),
                () -> harmonize.endpoints(domain.getId()).get(id).orElseThrow(),
                () -> harmonize.intents(domain.getId()).get(intentData.getIntentId()).orElseThrow(),
                intentData
        );
    }

    public static EntityData<Policies.Policy> createPolicy(Harmonize harmonize, Author author, Domains.Domain domain) {
        String id = UUID.randomUUID().toString();

        int rank = harmonize.policies(domain.getId()).getHighestPolicyRank() + 1;

        IntentData intentData = harmonize.policies(domain.getId())
                .create()
                .id(id)
                .alias(id)
                .rank(rank)
                .intentType(Policies.IntentType.v0_CreateEndpoint)
                .scope(Policies.Scope.Self)
                .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                .condition(
                        Policies.PolicyCondition.simpleBuilder()
                                .expression("context.references['users'][context.request.author.id].roles.includes('trader')")
                                .build()
                )
                .workflow(
                        Policies.WorkflowCondition.simpleBuilder()
                                .role("trader")
                                .quorum(1)
                                .build(),
                        Policies.WorkflowCondition.simpleBuilder()
                                .role("director")
                                .quorum(2)
                                .build(),
                        Policies.WorkflowCondition.simpleBuilder()
                                .role("admin")
                                .quorum(1)
                                .build()
                )
                .description("Test policy")
                .submit();

        intentData.waitForProcessingOrSucceeded(Timeout.minutes(4));

        return new EntityData<Policies.Policy>(
                domain.getId(),
                () -> harmonize.policies(domain.getId()).get(id).orElseThrow(),
                () -> harmonize.intents(domain.getId()).get(intentData.getIntentId()).orElseThrow(),
                intentData);
    }

    public static Accounts.Address getAccountAddress(Harmonize harmonize, Author author, Domains.Domain domain, Accounts.Account account) {
        return generateAddress(harmonize, author, domain.getId(), account.getId(), Timeout.minutes(1)).orElseThrow();
    }


    public static Optional<Accounts.Address> generateAddress(Harmonize harmonize,
                                                             Author author,
                                                             String domainId, String accountId, Timeout timeout) {
        long start = System.currentTimeMillis();

        RuntimeException rte = null;

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
            try {
                return harmonize.accounts(domainId).generateAddress(accountId);
            } catch (Exception excp) {
                rte = new RuntimeException(excp);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (rte != null) {
            throw rte;
        }
        return Optional.empty();
    }

    public static EntityData<Accounts.Account> createAccount(Harmonize harmonize,
                                                             Author author,
                                                             Domains.Domain domain, Vaults.CreateVault vault) {
        String id = UUID.randomUUID().toString();

        IntentData intentData = harmonize.accounts(domain.getId())
                .create()
                .id(id)
                .alias(id)
                .description("Test Account")
                .ledgerId(harmonize.ledgers().list().filter(l -> l.getId().contains("bit")).findFirst().get().getId())
                .providerDetails(Accounts.CreateAccountProviderDetails.vaultAccountProviderDetails()
                        .vaultId(vault.getId())
                        .accountKeyStrategy(Accounts.AccountKeyStrategy.Random).build())
                .build();

        intentData.waitForSucceed(Timeout.minutes(4));

        return new EntityData<>(
                domain.getId(),
                () -> harmonize.accounts(domain.getId()).get(id).orElseThrow(),
                () -> harmonize.intents(domain.getId()).get(intentData.getIntentId()).orElseThrow(),
                intentData);

    }

    /*
     * If a KEY_REQUEST_URL service was specified, this method will make a POST to said service
     * and wait for the generated keys response:
     * <pre>
     *     {"publicKey": "", "privateKey": "", "base64PublicKey": ""}
     * </pre>
     */
    public static GeneratedKeys requestKeys(Context context) {
        URL url = context.getConfig().getKeyRequestURL().orElseThrow(() -> new RuntimeException("No KEY_REQUEST_URL was specified"));

        Requests.Response response = context.getRequests().post(
                context.getConfig(),
                url,
                "",
                "Content-Type", "application/json"
        );


        if (!response.isOk())
            throw new RuntimeException("Error listing users: " + response.status + ", " + response.body);

        Map<String, Object> m = response.json();

        Key.PrivateKey privateKey = Key.fromPemString(m.get("privateKey").toString());
        Key.PublicKey publicKey = Key.publicKey(m.get("publicKey").toString(), m.get("base64PublicKey").toString());

        return new GeneratedKeys(privateKey, publicKey);
    }

    public static class EntityData<T> {

        public final Supplier<T> entityGet;
        public final Supplier<Intents.IntentEntity> intentGet;

        public final BaseIntentData<?, ?> intentData;
        public final String domainId;

        public EntityData(String domainId,
                          Supplier<T> entityGet,
                          Supplier<Intents.IntentEntity> intentGet,
                          BaseIntentData<?, ?> intentData) {
            this.domainId = domainId;
            this.entityGet = entityGet;
            this.intentGet = intentGet;
            this.intentData = intentData;
        }

        public Intents.IntentEntity getIntent() {
            return intentGet.get();
        }

        public T get() {
            return entityGet.get();
        }

        public EntityData<T> waitForSucceed() {
            return waitForSucceed(Timeout.minutes(4));
        }

        public EntityData<T> waitForSucceed(Timeout timeout) {
            intentData.waitForSucceed(timeout);
            return this;
        }
    }
}
