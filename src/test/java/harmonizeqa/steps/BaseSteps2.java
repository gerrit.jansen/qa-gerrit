package harmonizeqa.steps;

import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonizeqa.tests.NewUser;
import com.metaco.harmonizeqa.tests.Tuple;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.api.LocalHarmonizeContext;
import io.vavr.Tuple3;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class BaseSteps2 extends BaseSteps {

    private AtomicReference<MemoizedData> memoizedData = new AtomicReference<>();


    public Tuple3<IntentData, Domains.CreateDomain, Policies.CreateDomainGenesisPolicy> createDomainMultipleApproval(
            String expression,
            List<Policies.IntentType> intentTypes,
            String id, NewUser admin1, NewUser admin2, int quorum) {

        var adminRole = "admin";

        var policies = List.of(
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression(expression)
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentTypes(intentTypes.stream().map(Object::toString).collect(Collectors.toList()))
                        .scope(Policies.Scope.Self)
                        .build(),
                Policies.CreateDomainGenesisPolicy.builder()
                        .condition(Policies.PolicyCondition.simpleBuilder()
                                .expression("true")
                                .build())
                        .workflow(Policies.WorkflowCondition.simpleBuilder()
                                .role(adminRole)
                                .quorum(quorum)
                                .build())
                        .scriptingEngine(Policies.ScriptingEngine.Javascript_v0)
                        .intentType(Policies.IntentType.v0_CreateAccount)
                        .intentType(Policies.IntentType.v0_CreateEndpoint)
                        .scope(Policies.Scope.Self)
                        .build()
        );

        print("Creating domain with id ", id);
        print("Creating domain policy");
        print(policies.stream().map(p -> p.getCondition().toString()));

        var intentData = harmonize().domains().create()
                .targetDomainId(currentDomain())
                .alias(id)
                .id(id)
                .permissions(Domains.Permissions.builder().readAccess(Domains.ReadAccess.builder().all(adminRole).build()).build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin1.alias)
                        .id(admin1.id)
                        .publicKey(admin1.generatedKeys.getPublicKey().asBase64Str())
                        .role(adminRole)
                        .build())
                .user(Users.CreateDomainGenesisUser.builder()
                        .alias(admin2.alias)
                        .id(admin2.id)
                        .publicKey(admin2.generatedKeys.getPublicKey().asBase64Str())
                        .id(admin2.id)
                        .role(adminRole)
                        .build())
                .governingStrategy(Domains.GoverningStrategy.CoerceDescendants)
                .policies(policies)
                .submit()
                .waitForIntentExecuted(Timeout.minutes(1));

        return new Tuple3(
                intentData, intentData.getIntent().getPayload(), policies.get(0));

    }


    /**
     * Initialises a test with 2 admin users and a domain.
     * The values are only created once per id specified.
     * <p>
     * Creates:
     * * Admin1, Admin2
     * * Domain
     * * 2 Accounts is specified in the config
     */
    public DomainTestInitContext getTestContext(String contextId, DomainTestConfig config) {

        var admin1 = onlyOnce(contextId + "_admin1", () -> NewUser.create("admin1", this));
        var admin2 = onlyOnce(contextId + "_admin2", () -> NewUser.create("admin2", this));

        var domainTuple = onlyOnce(contextId + "_domain", () -> {

            var domainId = generateId();
            print("Creating domain: ", domainId);
            var tuple = createDomainMultipleApproval(
                    config.domainPolicyExpression,
                    config.policyIntentTypes,
                    domainId, admin1, admin2, config.quorum
            );
            print("Created domain");

            return Tuple.n(domainId, tuple._3);
        });

        Accounts.CreateAccount account1 = null;
        Accounts.CreateAccount account2 = null;

        var domainId = domainTuple.a;

        //Create accounts if a ledger id is specified
        if (config.ledgerId != null) {

            print("Creating accounts for ledger:", config.ledgerId);

            var ledger = context().harmonize.ledgers().findLedger(config.ledgerId).orElseThrow();

            var admin1Ctx = context().switchUser(admin1.userCredentials(domainId));
            var admin2Ctx = context().switchUser(admin2.userCredentials(domainId));

            var account1Id = generateId();
            var account2Id = generateId();

            print("Creating account1:", account1Id);
            account1 = onlyOnce(contextId + "_account1", () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                    .id(account1Id)
                    .alias("Testaccount_" + System.currentTimeMillis())
                    .vaultId(vaultId)
                    .ledgerId(ledger.getId())
                    .submit().waitForSucceed(Timeout.minutes(1))));


            print("Creating account1:", account1Id);
            account2 = onlyOnce(contextId + "_account2", () -> approveAccount(admin2Ctx, admin1Ctx.harmonize.accounts(domainId).create()
                    .id(account2Id)
                    .alias("Testaccount_" + System.currentTimeMillis())
                    .vaultId(vaultId)
                    .ledgerId(ledger.getId())
                    .submit().waitForSucceed(Timeout.minutes(1))));

        }

        var domain = context().switchUser(admin1.userCredentials(domainId)).harmonize.domains().get(domainId).orElseThrow();

        // return the created values
        return new DomainTestInitContext(
                context(), domainTuple.b, domain, admin1, admin2,
                account1, account2);

    }


    public void releaseQuarantined(LocalHarmonizeContext admin1Ctx, LocalHarmonizeContext admin2Ctx, String domainId, String accountId) {

        var transferIds = getQuarantinedTransferIds(admin1Ctx, domainId, accountId);
        if (transferIds.size() > 0) {
            print("Releasing transfers for ", transferIds);

            approveQuaranteedRelease(admin2Ctx,
                    admin1Ctx.harmonize.accounts(domainId).releaseQuarantinedTransfers()
                            .transferIds(transferIds)
                            .accountId(accountId)
                            .submit().waitForSucceed(Timeout.minutes(1)));
        } else {
            print("No quarantined transfers found");
        }

    }

    public List<String> getQuarantinedTransferIds(LocalHarmonizeContext ctx, String domainId, String accountId) {
        return ctx.harmonize.transactions(domainId).listTransfers()
                .filter(transfer -> transfer.isQuarantined())
                .map(Transactions.Transfer::getId)
                .collect(Collectors.toList());
    }

    public Accounts.ReleaseQuarantinedTransfers approveQuaranteedRelease(LocalHarmonizeContext ctx, IntentData<Accounts.ReleaseQuarantinedTransfers> intent) {
        try {
            ctx.harmonize.intents(intent.getIntent().getTargetDomainId())
                    .approve(intent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));
        } catch (IntentException error) {
            if (!error.getIntentData().getResponse().body.contains("cannot be signed anymore")) {
                throw error;
            }

        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return intent.getIntent().getPayload();
    }

    /**
     * Get The MemoizedData instance for the current class
     *
     * @return
     */
    public MemoizedData getMemoizedData() {
        var data = memoizedData.get();
        if (data == null) {
            synchronized (this) {
                memoizedData.set(new MemoizedData("/tmp", this.getClass().getName() + "MemoizeData1"));
            }

            return memoizedData.get();
        }
        return data;
    }


    public Accounts.CreateAccount approveAccount(LocalHarmonizeContext ctx, IntentData.AccountIntentData<Accounts.CreateAccount> accountIntent) {
        try {
            ctx.harmonize.intents(accountIntent.getIntent().getTargetDomainId())
                    .approve(accountIntent.getIntentId()).waitForIntentExecuted(Timeout.minutes(1));
        } catch (IntentException error) {
            if (!error.getIntentData().getResponse().body.contains("cannot be signed anymore")) {
                throw error;
            }

        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return accountIntent.getIntent().getPayload();
    }


    public <T> T onlyOnce(String id, Supplier<T> fn) {
        return getMemoizedData().onceOnly(id, (v) -> true, fn);
    }

    public <T> T onlyOnce(String id, Function<T, Boolean> existRemoteFn, Supplier<T> fn) {
        return getMemoizedData().onceOnly(id, existRemoteFn, fn);
    }

    public class DomainTestConfig {
        public int quorum = 1;
        public String domainPolicyExpression = "true";
        public List<Policies.IntentType> policyIntentTypes = Arrays.asList(Policies.IntentType.values());

        /**
         * If specified 2 accounts are created
         */
        public String ledgerId = null;

    }

    public class DomainTestInitContext implements Serializable {

        public final LocalHarmonizeContext rootContext;
        public final Policies.CreateDomainGenesisPolicy domainGenesisPolicy;
        public final Domains.Domain domain;

        public final NewUser admin1;
        public final NewUser admin2;

        public final Accounts.CreateAccount account1;
        public final Accounts.CreateAccount account2;


        public DomainTestInitContext(LocalHarmonizeContext rootContext, Policies.CreateDomainGenesisPolicy domainGenesisPolicy, Domains.Domain domain, NewUser admin1, NewUser admin2, Accounts.CreateAccount account1, Accounts.CreateAccount account2) {
            this.rootContext = rootContext;
            this.domainGenesisPolicy = domainGenesisPolicy;
            this.domain = domain;
            this.admin1 = admin1;
            this.admin2 = admin2;
            this.account1 = account1;
            this.account2 = account2;
        }

        public LocalHarmonizeContext getAdmin1Context() {
            return rootContext.switchUser(admin1.userCredentials(domain.getId()));
        }

        public LocalHarmonizeContext getAdmin2Context() {
            return rootContext.switchUser(admin2.userCredentials(domain.getId()));
        }
    }

}
