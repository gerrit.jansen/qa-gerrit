package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Tickers;
import harmonizeqa.TransactionsEnums;
import io.cucumber.java.en.Given;

public class TickerSteps extends BaseSteps {

    @Given("we create an ethereum {string} contract ticker {string} with {string} token")
    public void weCreateAnEthereumContractTicker(String ledgerId, String tickerName, String tokenName) {
        var tickerEnum = TransactionsEnums.Tickers.byName(tokenName);

        var createTicker = harmonize().tickers().create()
                .ledgerId(ledgerId)
                .name(tickerName + "-" + System.currentTimeMillis())
                .kind(Tickers.TickerKind.Contract)
                .decimals(8)
                .symbol("USDC")
                .ledgerDetails(Tickers.TickerLedgerDetails.eth().ERC20(tickerEnum.getAddress()).build())
                .expire(Expire.hours(24))
                .submit()
                .waitForSucceed(Timeout.minutes(1))
                .waitForIntentExecuted(Timeout.minutes(1));

        memory().set("created_ticker", createTicker);
        memory().set(tickerName, createTicker);
    }
}