package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Lock;
import com.metaco.harmonize.api.PoliciesActions;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.api.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static harmonizeqa.steps.UserSteps.retrieveAndLoginUser;


public class PolicySteps extends BaseSteps {
    private static final Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());
    Scenario scenario;

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @And("the policy has the following properties")
    public void thePolicyHasTheFollowingProperties(DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, Object.class);
        UUID id = UUID.randomUUID();

        Policies.CreatePolicy.Builder<Policies.CreatePolicy> policyBuilder = PolicyHelper.setPropertiesToPolicyBuilder(map, Policies.CreatePolicy.builder())
                .id(id.toString());

        memory().set("policy_builder", policyBuilder);
        memory().set("policy_properties_map", map);
    }

    @Given("all policy workflow structures use the following simple conditions")
    public void allPolicyWorkflowStructuresUseTheFollowingSimpleConditions(DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, String.class);
        PolicyHelper.createSimpleConditionsMap(memory(), map);
    }

    @And("policy workflow has {string} condition with the following data")
    public void policyWorkflowHasANDConditionWithTheFollowingData(String orAnd, DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, String.class);
        PolicyHelper.createConditionsMap(memory(), map, orAnd);
    }

    @Then("finally the policy workflow condition structure is")
    public void finallyThePolicyWorkflowConditionStructureIs(DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, String.class);
        memory().set("policy_builder", PolicyHelper.buildPolicyWorkflow(memory(), map, memory().get("policy_builder")));
    }

    @And("the policy is created")
    public void thePolicyIsCreated() {
        if(scenario.getName().length() > 39){
            policyWithAliasIsCreated(scenario.getName().substring(0, 39));
        } else policyWithAliasIsCreated(scenario.getName());
    }

    @And("policy with alias {string} is created")
    public void policyWithAliasIsCreated(String alias) {
        Policies.CreatePolicy.Builder<Policies.CreatePolicy> policy = memory().get("policy_builder");
        String id = UUID.randomUUID().toString();
        String policyName = alias + id;

        if (policy == null)
            throw new RuntimeException("No policy_builder was set to memory");

        policy.alias(policyName);
        policy.id(id);
        policy.description("QA feature created workflow");

        Map<String, String> propertiesMap = memory().get("policy_properties_map");
        if(propertiesMap.get("rank") != null){
            policy.rank(Integer.parseInt(propertiesMap.get("rank")));
        }else {
            policy.rank(adjustPolicyRank(memory().get("policy_properties_map")));
        }

        Policies.CreatePolicy createPolicy = policy.submit();


        IntentData<Policies.CreatePolicy> intentData = harmonize().policies(currentDomain())

                .create(createPolicy, Expire.hours(1));

        intentData.waitForSucceed(Timeout.minutes(2));
        intentData.waitForIntentStatus(
                Set.of(Intents.IntentStatus.Approved,
                        Intents.IntentStatus.Executed),
                Timeout.minutes(1));

        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> policyWrapper = new EntityWrapper<>(currentDomain(), id, "type", context().userCredentials);
        policyWrapper.setCreateEntity(createPolicy, intentData);

        memory().set("policy_under_test", policyWrapper);
        PolicyHelper.storeCreatedPolicy(memory(), harmonize(), createPolicy, currentDomain(), intentData);
    }

    /**
     * The rank adjustment is done to be able to repeat the same policy test several times
     * or use the same intentTypes and condition expression in different tests on the same environment
     */
    public int adjustPolicyRank(Map<String, String> propertiesMap){
        PoliciesActions.ListCriteria listCriteria = PoliciesActions.ListCriteria.builder()
                .intentType(propertiesMap.get("intentTypes"))
                .sortBy("metadata.createdAt")
                .supervisionType(propertiesMap.get("scope"))
                .sortOrder(SortOrder.DESC)
                .limit(100)
                .build();

        List<Policies.Policy> policies = harmonize().policies(currentDomain())
                .list(listCriteria).collect(Collectors.toList());

        if(propertiesMap.get("conditionExpression") != null){

            if(harmonize().policies(currentDomain()).list(listCriteria).anyMatch(p -> p.getRank() == 1000 && p.getCondition().asMap().get("expression").equals(propertiesMap.get("conditionExpression")))) {
                LOGGER.info("The policy with matching intentTypes, scope and condition already exists and has rank 1000");
                return 1000;
            }
        }
        if(harmonize().policies(currentDomain()).list(listCriteria).anyMatch(p -> p.getRank() == 1000 && p.getCondition()== null)) {
            LOGGER.info("The policy with matching intentTypes, scope and condition already exists and has rank 1000");
            return 1000;
        }
        return policies
                .stream()
                .filter(policy -> policy.getRank() != 1000)
                .max(Comparator.comparing(Policies.Policy::getRank))
                .map(policy -> policy.getRank() + 1).orElse(400);
    }

    @And("the policy is locked")
    public void thePolicyIsLocked() throws Exception {
        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> storedPolicy = memory().get("policy_under_test");
        Policies.Policy policy = harmonize().policies(currentDomain())
                .get(storedPolicy.getId())
                .orElseThrow(() -> new Exception(String.format("Policy with id %s is not found in HMZ", storedPolicy.getId())));

        harmonize().policies(currentDomain()).lock(policy, Lock.Locked, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    private void lockAllPoliciesExceptDefault(){
        Map<String, Object> configMap = LocalHarmonizeContext.loadConfig();
        String defaultPolicyId = "00000000-0000-0000-0000-000000000001";
        if(currentDomain().equalsIgnoreCase(LocalHarmonizeContext.getRootDomainUserCredentials(configMap).getDomainId())){
            defaultPolicyId = "00000000-0000-0000-0000-000000000000";
        }
        String finalDefaultPolicyId = defaultPolicyId;
        harmonize().policies(currentDomain()).list().forEach(policy -> {
            if(!policy.getId().equalsIgnoreCase(finalDefaultPolicyId) && policy.getLock().equalsIgnoreCase(Lock.Unlocked.toString())){
                Policies.Policy p = harmonize().policies(currentDomain()).get(policy.getId()).orElse(null);
                harmonize().policies(currentDomain()).lock(p, Expire.hours(1))
                        .waitForSucceed(Timeout.minutes(2))
                        .waitForIntentStatus(
                                Set.of(Intents.IntentStatus.Approved,
                                        Intents.IntentStatus.Executed),
                                Timeout.minutes(2));;
            }
        });
    }

    @And("the policy is unlocked")
    public void thePolicyIsUnlocked() throws Exception {
        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> storedPolicy = memory().get("policy_under_test");
        Policies.Policy policy = harmonize().policies(currentDomain())
                .get(storedPolicy.getId())
                .orElseThrow(() -> new Exception(String.format("Policy with id %s is not found in HMZ", storedPolicy.getId())));

        harmonize().policies(currentDomain()).lock(policy, Lock.Unlocked, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    @When("policy condition expression is updated to {string}")
    public void policyConditionIsUpdatedTo(String arg0) {
        Policies.PolicyCondition newCondition = Policies.PolicySimpleCondition.builder().expression(arg0).build();
        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> storedPolicy = memory().get("policy_under_test");
        Policies.Policy policy = PolicyHelper.getPolicyById(memory(),currentDomain(), storedPolicy.getId()).getEntity();

        // to do
        long revision = policy.getMetaData().getRevision();

        Policies.UpdatePolicy updatePolicy = policy.asUpdate().setCondition(newCondition);

        IntentData<Policies.UpdatePolicy> intentData = harmonize().policies(currentDomain()).update(updatePolicy, Expire.hours(24));
        intentData.waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));

        PolicyHelper.storeUpdatedPolicy(memory(), harmonize(), updatePolicy, policy.getId(), currentDomain(), intentData);
    }

    @When("policy is updated to have null condition")
    public void policyIsUpdatedToHaveNullCondition() {
        Policies.UpdatePolicy policy = PolicyHelper.getPolicyByDomainId(memory(),currentDomain()).getEntity().asUpdate().setCondition(null);

        IntentData<Policies.UpdatePolicy> intentData = harmonize().policies(currentDomain()).update(policy, Expire.hours(24));
        intentData.waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    @And("all policies except default are locked")
    public void allPoliciesExceptDefaultAreLocked() {
        lockAllPoliciesExceptDefault();
    }

    @When("subdomain yes policy is locked")
    public void subdomainYesPolicyIsLocked() {
        Policies.Policy policy = harmonize().policies(currentDomain()).get("00000000-0000-0000-0000-000000000001").get();
        if(policy.getLock().equalsIgnoreCase(Lock.Unlocked.toString())){
            harmonize().policies(currentDomain()).lock(policy, Lock.Locked, Expire.hours(1))
                    .waitForIntentStatus(
                            Set.of(Intents.IntentStatus.Approved,
                                    Intents.IntentStatus.Executed),
                            Timeout.minutes(2));
        }
    }

    @And("the user attempts to unlock the yes policy")
    public void theUserAttemptsToUnlockThePolicy() {
        Policies.Policy policy = harmonize().policies(currentDomain()).get("00000000-0000-0000-0000-000000000001").get();
        IntentData intentData  = harmonize().policies(currentDomain()).unlock(policy, Expire.hours(1))
                .waitForSucceed(Timeout.seconds(20));
        memory().set("last_propose_intent", intentData);
    }

    @When("the user submits the unlock policy intent for {string} domain using the author and reference specified above")
    public void theUserSubmitsTheUnlockPolicyIntentForDomainUsingTheAuthorTargetAndReferenceSpecifiedAbove(String domainPseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = DomainSteps.getDomainByPseudonym(domainPseudonym);

        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = wrapper.getId();
        String reference = memory().get("reference");
        Policies.Policy policy = harmonize(null).policies(targetDomain).get(reference).get();

        retrieveAndLoginUser(getCurrentUser(), currentDomain());

        try{
            IntentData intentData = harmonize().policies(currentDomain()).unlock(author.build(), targetDomain, policy, Expire.hours(1));
            memory().set("propose_intent_request_id", intentData.getRequestId());
            intentData.waitForSucceed(Timeout.minutes(1));
            memory().set("last_propose_intent", intentData);

        }catch (RuntimeException ex){
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @Given("policy condition expression sets request author id to admin user of {string}")
    public void policyConditionExpressionSetsRequestAuthorIdToAdminUserOf(String domainPseudonym) {
        String domainId = DomainSteps.getDomainIdByPseudonym(domainPseudonym);
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> user = UserSteps.getUserByRole("admin", domainId);

        Policies.CreatePolicy.Builder<Policies.CreatePolicy> policy = memory().get("policy_builder");

        String expression = String.format("context.request.author.id == '%s'", user.getId());
        policy.condition(Policies.PolicyCondition.simpleBuilder()
                        .expression(expression)
                        .build());

        memory().set("policy_builder", policy);
    }

    @Given("policy condition expression sets request author domain to {string}")
    public void policyConditionExpressionSetsRequestAuthorDomainTo(String domainPseudonym) {
        String domainId = DomainSteps.getDomainIdByPseudonym(domainPseudonym);

        Policies.CreatePolicy.Builder<Policies.CreatePolicy> policy = memory().get("policy_builder");

        String expression = String.format("context.request.author.domainId == '%s'", domainId);
        policy.condition(Policies.PolicyCondition.simpleBuilder()
                .expression(expression)
                .build());

        memory().set("policy_builder", policy);
    }
}
