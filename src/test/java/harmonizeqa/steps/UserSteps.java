package harmonizeqa.steps;

import com.codepine.api.testrail.model.Run;
import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Lock;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.utils.TypeUtils;
import harmonizeqa.api.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static harmonizeqa.steps.DomainSteps.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserSteps extends BaseSteps {
    private static final Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    @Then("the known user roles is ordered")
    public void knownUserRolesIsOrdered() {
        String domainId = currentDomain();

        List<String> roles = harmonize().users(domainId).listKnownUserRoles().collect(Collectors.toList());

        List<String> sortedRoles = new ArrayList<>(roles);
        Collections.sort(sortedRoles);

        assertEquals(roles, sortedRoles);
    }

    @And("the known user roles only contain assigned user roles")
    public void theKnownUserRolesOnlyContainAssignedUserRoles() {
        String domainId = currentDomain();

        //get the known roles
        List<String> roles = harmonize().users(domainId)
                .listKnownUserRoles()
                .collect(Collectors.toList());

        // get all the assigned roles and deduplicate and sort
        List<String> userAssignedRoles = harmonize().users(domainId).list()
                .flatMap(u -> u.getRoles().stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        assertEquals(userAssignedRoles, roles);
    }

    @And("the user {string} is created with the following data")
    public void theUserIsCreatedWithTheFollowingData(String alias, DataTable dataTable) throws Exception {
        UUID id = UUID.randomUUID();

        String domainId = currentDomain();

        GeneratedKeys keys = requestKeys();
        memory().setCryptographicKeys(id.toString(), keys);

        Map<String, String> map = dataTable.asMap(String.class, Object.class);

        Users.CreateUser createUser = UserHelper.setUserProperties(map)
                .id(id.toString())
                .alias(alias)
                .locked(false)
                .publicKey(keys.getPublicKey().asBase64Str())
                .submit();

        IntentData intentData = harmonize().users(domainId).create(
                createUser, Expire.hours(1));
        intentData.waitForSucceed(Timeout.minutes(1)).waitForIntentStatus(
                Set.of(Intents.IntentStatus.Approved,
                        Intents.IntentStatus.Executed),
                Timeout.minutes(1));

        Users.User user = harmonize().users(domainId).get(createUser.getId())
                .orElseThrow(() -> new Exception(String.format("User with id %s is not found", createUser.getId())));

        storeCreatedUser(domainId, user, createUser, intentData, alias,
                UserCredentials.from(
                        domainId,
                        id.toString(),
                        keys.getPublicKey().asBase64Str(),
                        keys.getPrivateKey().asStr()
                ), map.get("roles ") + 1);
    }

    public void createUserWithKeys(Author author, String domainId, String pseudonym, String role, String alias, GeneratedKeys keys) throws Exception {
        UserCreateWrapper wrapper = proposeUserCreationWithKeys(author, domainId, pseudonym, role, alias, keys);
        if (wrapper == null)
            throw new RuntimeException("createUser intent could not be submitted");
        theUserWithRoleIsCreated(role, wrapper, domainId);
    }

    public void createUserWithRandomKeys(Author author, String targetDomain, String pseudonym, String roles) throws Exception {
        UserCreateWrapper wrapper = proposeUserCreationWithRandomKeys(author, targetDomain, pseudonym, roles);

        if (wrapper == null)
            throw new RuntimeException("createUser intent could not be submitted");
        theUserWithRoleIsCreated(roles, wrapper, targetDomain);
    }

    public void theUserWithRoleIsCreated(String roles, UserCreateWrapper wrapper, String domainId) throws Exception {
        Users.CreateUser createUser = wrapper.intentData.getIntent().getPayload();

        wrapper.intentData.waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));

        Users.User user = harmonize().users(domainId).get(createUser.getId())
                .orElseThrow(() -> new Exception(String.format("User with id %s is not found", createUser.getId())));

        storeCreatedUser(
                domainId, user, createUser, wrapper.intentData, roles,
                wrapper.userCredentials(domainId),
                wrapper.pseudonym
        );
    }

    @And("{string} domain {string} user is added to {string} domain as {string}")
    public void domainUserIsAddedToDomainAs(String userOriginDomain, String userPseudonym, String targetDomain, String roleInTargetDomain) throws Exception {
        String originDomainId = getDomainIdByPseudonym(userOriginDomain);

        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> user = getUserByPseudonym(userPseudonym, originDomainId);
        UserCredentials userCredentials = user.getUserCredentials();

        String domainId = DomainSteps.getDomainIdByPseudonym(targetDomain);
        GeneratedKeys keys = new GeneratedKeys(userCredentials.getPrivateKey(), userCredentials.getPublicKey());
        String alias = user.getCreateEntity().getAlias();

        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithKeys(author, domainId, roleInTargetDomain, roleInTargetDomain, alias, keys);
    }

    @When("intent to add {string} domain {string} user to {string} domain as {string} is submitted")
    public void intentToAddDomainUserToDomainAsIsSubmitted(String userOriginDomain, String userPseudonym, String targetDomain, String roleInTargetDomain) {
        String originDomainId = getDomainIdByPseudonym(userOriginDomain);

        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> user = getUserByPseudonym(userPseudonym, originDomainId);
        UserCredentials userCredentials = user.getUserCredentials();

        String domainId = DomainSteps.getDomainIdByPseudonym(targetDomain);
        GeneratedKeys keys = new GeneratedKeys(userCredentials.getPrivateKey(), userCredentials.getPublicKey());
        String alias = user.getCreateEntity().getAlias();

        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        proposeUserCreationWithKeys(author, domainId, userPseudonym, roleInTargetDomain, alias, keys);
    }

    @And("root domain admin user is added to {string} domain as {string}")
    public void rootDomainAdminUserIsAddedToSubdomainAs(String domainPseudonym, String role) throws Exception {
        Map<String, Object> configMap = LocalHarmonizeContext.loadConfig();
        UserCredentials userCredentials = LocalHarmonizeContext.getRootDomainUserCredentials(configMap);

        String domainId = DomainSteps.getDomainIdByPseudonym(domainPseudonym);
        GeneratedKeys keys = new GeneratedKeys(userCredentials.getPrivateKey(), userCredentials.getPublicKey());

        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithKeys(author, domainId, role, role, "siloadm@metaco.com", keys);
    }

    @And("coercive root domain admin user is added to {string} domain as {string}")
    public void coerciveDomainUserIsAddedToSubdomainAs(String domainPseudonym, String role) throws Exception {
        Map<String, Object> configMap = LocalHarmonizeContext.loadConfig();
        UserCredentials userCredentials = LocalHarmonizeContext.getRootDomainUserCredentials(configMap);

        String domainId = DomainSteps.getDomainIdByPseudonym(domainPseudonym);
        GeneratedKeys keys = new GeneratedKeys(userCredentials.getPrivateKey(), userCredentials.getPublicKey());

        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithKeys(author, domainId, role, role, "coercivesiloadm@metaco.com", keys);
    }

    @And("the {string} user is created")
    public void theUserIsCreated(String role) throws Exception {
        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithRandomKeys(author, currentDomain(), role, role);
    }

    @And("the {string} user number {string} is created")
    public void theUserNumberIsCreated(String arg0, String arg1) throws Exception {
        // pseudonym here is represented by combination of role and number to perform further search by pseudonym in the memory
        // Assuming all pseudonyms are unique within domain and represent role with numeric value: trader, trader1 etc
        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithRandomKeys(author, currentDomain(), arg0 + arg1, arg0);
    }

    @And("the {string} user number {string} with roles {string} is created")
    public void theUserNumberWithRolesIsCreated(String userPseudonym, String order, String roles) throws Exception {
        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        createUserWithRandomKeys(author, currentDomain(), userPseudonym, roles);
    }


    @Given("the {string} user from {string} is locked")
    public void theUserFromIsLocked(String userPseudonym, String domainPseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> domain = getDomainByPseudonym(domainPseudonym);
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> userToLock = getUserByPseudonym(userPseudonym, domain.getId());

        Users.User user = userToLock.getEntity();

        String targetDomain = domain.getId();
        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(currentDomain()).build();

        harmonize().users(domain.getId()).lock(author, targetDomain, user, Lock.Locked, Expire.hours(1))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    public UserCreateWrapper proposeUserCreationWithKeys(Author author, String domainId, String pseudonym, String role, String alias, GeneratedKeys keys) {
        String id = UUID.randomUUID().toString();
        return proposeUserCreation(author, domainId, pseudonym, role, id, alias, keys);
    }

    public UserCreateWrapper proposeUserCreationWithRandomKeys(Author author, String domainId, String pseudonym, String role) {
        String id = UUID.randomUUID().toString();
        String alias = "User " + id;
        GeneratedKeys keys = requestKeys();
        return proposeUserCreation(author, domainId, pseudonym, role, id, alias, keys);
    }

    public UserCreateWrapper proposeUserCreation(Author author, String domainId, String pseudonym, String roles, String id, String alias, GeneratedKeys keys) {
        memory().setCryptographicKeys(id, keys);

        Users.CreateUser createUser = Users.CreateUser.builder()
                .id(id)
                .alias(alias)
                .locked(false)
                .roles(Arrays.stream(roles.split(",")).map(s -> s.strip()).collect(Collectors.toList()))
                .publicKey(keys.getPublicKey().asBase64Str())
                .submit();

        Expire expire = Expire.hours(1);

        if (memory().get("intent_expiration_time") != null) {
            expire = memory().get("intent_expiration_time");
        }

        UserCreateWrapper userCreateWrapper = null;
        try {
            IntentData intentData = harmonize().users(domainId).create(author, domainId,
                    createUser, expire);

            memory().set("propose_intent_request_id", intentData.getRequestId());
            memory().set("last_propose_intent", intentData);

            userCreateWrapper = new UserCreateWrapper(intentData,
                    keys, pseudonym);

        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }

        return userCreateWrapper;
    }

    @When("createUser intent is proposed")
    public void createuserIntentIsProposed() {
        String domainId = currentDomain();
        Author author = Author.builder()
                .id(context().userCredentials.getId())
                .domainId(context().userCredentials.getDomainId())
                .build();

        UserCreateWrapper wrapper = proposeUserCreationWithRandomKeys(author, domainId, "test_user_creation", "trader");
        if (wrapper == null)
            throw new RuntimeException("createUser intent could not be submitted");

        wrapper.intentData.waitForSucceed(Timeout.minutes(2));

        memory().set("last_propose_intent", wrapper.intentData);
        memory().set("last_intent", wrapper.intentData);
    }

    @And("createUser intent is proposed by {string} user")
    public void createuserIntentIsProposedByUser(String pseudonym) {
        userIsLoggedIn(pseudonym);
        createuserIntentIsProposed();
    }

    @And("createUser intent is proposed using the author and target domain specified above")
    public void createuserIntentIsProposedUsingTheAuthorTargetAndReferenceSpecifiedAbove() throws Exception {
        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = memory().get("target_domain_id");
        proposeUserCreationWithRandomKeys(author.build(), targetDomain, "trader", "trader");
    }


    @Given("coercive root domain user is logged in")
    public void coerciveRootDomainUserIsLoggedIn() {
        loginCoerciveRootDomainUser();
        setCurrentDomainFromUserContext();
    }

    @Given("a root domain admin user is logged in")
    public void aRootDomainAdminUserIsLoggedIn() {
        theCurrentDomainIsARootDomain();
        userIsLoggedIn("default");
    }

    @Given("{string} is logged in")
    public static void userIsLoggedIn(String pseudonym) {
        retrieveAndLoginUser(pseudonym, currentDomain());
    }

    @Then("the number of requests are zero")
    public static void numberOfRequestsAreZero() {
        long count = harmonize().requests(currentDomain()).list().count();
        assertEquals(count, 0);
    }

    @Then("the number of requests are > 0")
    public static void numberOfRequestsAreBiggerThanOne() {
        long count = harmonize().requests(currentDomain()).list().count();
        assertTrue(count > 0);
    }

    @Then("the number of requests are {int}")
    public static void numberOfRequestsAreN(int numberOfRequests) {
        long count = harmonize().requests(currentDomain()).list().count();
        assertEquals(numberOfRequests, count);
    }

    @And("{string} user from {string} domain is logged in")
    public void userFromDomainIsLoggedIn(String userPseudonym, String domainPseudonym) {
        String domainId = getDomainIdByPseudonym(domainPseudonym);
        retrieveAndLoginUser(userPseudonym, domainId);
    }

    public static void retrieveAndLoginUser(String pseudonym, String userDomainId) {
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> wrapper = getUserByPseudonym(pseudonym, userDomainId);
        UserCredentials userCredentials = TypeUtils.assertNotNull(wrapper.getUserCredentials(), "UserCredentials");

        loginUser(pseudonym, userCredentials);
    }

    /**
     * Helper function that returns the EntityWrapper for a user by its role
     */
    public static EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> getUserByRole(String role, String domain) {
        return memory().<Users.User, Users.CreateUser, Users.UpdateUser>getEntityDataByRole("user", domain, role)
                .orElseThrow(() -> new RuntimeException("The user " + role + " was not set to memory"));
    }

    public static EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> getUserByPseudonym(String pseudonym, String domain) {
        return memory().<Users.User, Users.CreateUser, Users.UpdateUser>getEntityDataByPseudonym("user", domain, pseudonym)
                .orElseThrow(() -> new RuntimeException(String.format("The user %s in domain %s was not set to memory", pseudonym, domain)));
    }

    public static EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> getUserById(String id, String domain) {
        return memory().<Users.User, Users.CreateUser, Users.UpdateUser>getEntityDataById("user", domain, id)
                .orElseThrow(() -> new RuntimeException(String.format("The user with id %s in domain %s was not set to memory", id, domain)));
    }

    public static void loginUserFromDomain(String domainPseudonym, String userPseudonym){
        String domainId = getDomainIdByPseudonym(domainPseudonym);
        retrieveAndLoginUser(userPseudonym, domainId);
    }

    @Then("the me requests count is {int}")
    public void theMeRequestsAre(int count) {
        var requestIdCount = harmonize().me().requests()
                .map(Request::getId)
                .collect(Collectors.toSet()).size();

        assertEquals(count, requestIdCount);
    }

    @Then("the me requests are")
    public void theMeRequestsAre(DataTable dataTable) {

        var expectedRequestPseudonyms = dataTable.asList();

        var savedRequestIds = expectedRequestPseudonyms.stream()
                .map(v -> (IntentData) memory().get("request_" + v.strip()))
                .map(v -> v.getRequestId())
                .collect(Collectors.toSet());


        var intentObjects = expectedRequestPseudonyms.stream()
                .map(v -> (IntentData) memory().get("request_" + v.strip()))
                .collect(Collectors.toSet());

        var requestIds = harmonize().me().requests()
                .map(Request::getId)
                .collect(Collectors.toSet());


        assertEquals(savedRequestIds.size(), requestIds.size());
        assertEquals(savedRequestIds, requestIds);
    }

    public static class UserCreateWrapper {

        final IntentData<Users.CreateUser> intentData;
        final GeneratedKeys keys;
        final String pseudonym;

        public UserCreateWrapper(IntentData<Users.CreateUser> intentData, GeneratedKeys keys, String pseudonym) {
            this.intentData = intentData;
            this.keys = keys;
            this.pseudonym = pseudonym;
        }

        public UserCredentials userCredentials(String domainId) {
            return UserCredentials.from(
                    domainId,
                    intentData.getIntent().getPayload().getId(),
                    keys.getPublicKey().asBase64Str(),
                    keys.getPrivateKey().asStr()
            );
        }
    }

    public static String getDefaultUserOfDomain(String domainPseudonym) {
        String domainId = DomainSteps.getDomainIdByPseudonym(domainPseudonym);
        if (domainPseudonym.equalsIgnoreCase("root domain")) {
            return UserSteps.getUserByPseudonym("default", domainId).getId();
        } else {
            return UserSteps.getUserByPseudonym("subdomain admin user", domainId).getId();
        }
    }
}

