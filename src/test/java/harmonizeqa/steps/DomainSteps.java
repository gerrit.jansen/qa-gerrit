package harmonizeqa.steps;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.*;
import com.metaco.harmonize.api.Lock;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.utils.CollectionUtils;
import harmonizeqa.api.*;
import harmonizeqa.api.GeneratedKeys;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Implement all basic steps for Domain level operations
 */
public class DomainSteps extends BaseSteps {
    private static final Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    @Given("we have {int} test roles")
    public void createTestRoles(int roleCount) {
        List<String> testRoles =
                CollectionUtils.list("admin", "accountant");

        testRoles.addAll(IntStream.rangeClosed(0, roleCount)
                .mapToObj(i -> Long.toHexString(Double.doubleToLongBits(Math.random())))
                .map(s -> Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8)))
                .map(s -> s.replaceAll("[0-9\\=]", ""))
                .map(String::toLowerCase)
                .collect(Collectors.toSet()));

        Collections.shuffle(testRoles);

        memory().set("RANDOM_ROLES", testRoles);
    }

    public List<String> getRandomRoles() {
        return memory().get("RANDOM_ROLES", new ArrayList<>());
    }

    @Given("we have a root domain")
    public void we_have_a_root_domain() {
        // Write code here that turns the phrase above into concrete actions
        assertNotNull(rootDomainId());
    }

    @And("the subdomain is created")
    public void theSubdomainIsCreated() {
        subdomainNumberIsCreated("1");
    }

    public Domains.CreateDomain.Builder<Domains.CreateDomain> prepareStandardDomain() {
        String domainId = UUID.randomUUID().toString();

        String domainAlias = "domain" + domainId;

        String userId = UUID.randomUUID().toString();
        GeneratedKeys generatedKeys = requestKeys();

        memory().setCryptographicKeys(userId, generatedKeys);
        UserCredentials userCredentials = UserCredentials.from(domainId, userId, generatedKeys);
        memory().set("user_credentials_of_subdomain_user", userCredentials);

        List<String> roles = getRandomRoles();
        roles.addAll(Arrays.asList("admin", "manager", "trader", "director", "aml"));

        Policies.CreateDomainGenesisPolicy policy = prepareSubdomainPolicy(null, userId);

        return Domains.CreateDomain.builder()
                .id(domainId)
                .targetDomainId(currentDomain())
                .alias(domainAlias)
                .governingStrategy(Domains.GoverningStrategy.ConsiderDescendants)
                .permissions(Domains.Permissions.builder().readAccess(
                        Domains.ReadAccess.builder().all(roles).build()
                ).build())
                .policy(policy)
                .user(Users.CreateDomainGenesisUser.builder()
                        .id(userId)
                        .alias("admin")
                        .description("admin")
                        .customProperties(new HashMap<>())
                        .publicKey(userCredentials.getPublicKey().asBase64Str())
                        .role("admin")
                        .build());
    }

    @And("coercive subdomain is created")
    public void coerciveSubdomainIsCreated() {
        coerciveSubdomainNumberIsCreated("1");
    }

    @And("coercive subdomain number {string} is created")
    public void coerciveSubdomainNumberIsCreated(String arg0) {
        Domains.CreateDomain.Builder<Domains.CreateDomain> domainBuilder = prepareStandardDomain();
        domainBuilder.governingStrategy(Domains.GoverningStrategy.CoerceDescendants);

        Domains.CreateDomain createDomain = domainBuilder.build();
        submitDomain(createDomain, arg0, true);
    }

    @When("create subdomain with two users having the same public key intent is submitted")
    public void createSubdomainWithTwoUsersHavingTheSamePublicKeyIntentIsProposed() {
        Domains.CreateDomain.Builder<Domains.CreateDomain> domainBuilder = prepareStandardDomain();
        GeneratedKeys generatedKeys = requestKeys();

        List<Users.CreateDomainGenesisUser> users = new ArrayList<>();
        users.add(Users.CreateDomainGenesisUser.builder()
                .id(UUID.randomUUID().toString())
                .alias("admin")
                .description("admin")
                .customProperties(new HashMap<>())
                .publicKey(generatedKeys.getPublicKey().asBase64Str())
                .role("admin")
                .build());
        users.add(Users.CreateDomainGenesisUser.builder()
                .id(UUID.randomUUID().toString())
                .alias("admin")
                .description("admin")
                .customProperties(new HashMap<>())
                .publicKey(generatedKeys.getPublicKey().asBase64Str())
                .role("admin")
                .build());

        domainBuilder.users(users);

        Domains.CreateDomain createDomain = domainBuilder.build();

        try {
            harmonize().domains().create(createDomain, Expire.hours(1));
        }catch (RuntimeException exception){
            LOGGER.info(String.format("Intent could not be submitted %s", exception));
            memory().set("intent_error", exception);
        }
    }

    //requestPseudonym
    @And("the subdomain number {string} is created with default Yes policy for subdomain admin")
    public void theSubdomainNumberIsCreatedWithDefaultYesPolicyForSubdomainAdmin(String arg0) {
        theSubdomainNumberIsCreatedWithDefaultYesPolicyForSubdomainAdmin(arg0, null);
    }

    public void theSubdomainNumberIsCreatedWithDefaultYesPolicyForSubdomainAdmin(String arg0, String requestPseudonym) {
        Domains.CreateDomain domainBuilder = prepareStandardDomain().build();
        IntentData intentData = submitDomain(domainBuilder, arg0, false);

        if (requestPseudonym != null) {
            memory().set("request_" + requestPseudonym, intentData);
        }
    }

    private IntentData submitDomain(Domains.CreateDomain createDomain, String domainOrder, boolean isCoercive) {
        IntentData intentData = harmonize().domains().create(createDomain, Expire.hours(1));

        intentData.waitForSucceed(Timeout.minutes(4))
                .waitForIntentStatus(Set.of(Intents.IntentStatus.Executed), Timeout.minutes(1));
        Domains.CreateDomain domain = (Domains.CreateDomain) intentData.getIntent().getPayload();

        UserCredentials userCredentials = memory().get("user_credentials_of_subdomain_user");

        String domainPseudonym = "";
        if (isCoercive) {
            domainPseudonym = "coercive subdomain " + domainOrder;
        } else domainPseudonym = "subdomain" + domainOrder;

        storeNewDomain(domain, List.of(userCredentials), intentData, domainPseudonym);

        return intentData;
    }

    public Policies.CreateDomainGenesisPolicy prepareSubdomainPolicy(String topDomainUserId, String subdomainUserId) {
        Policies.PolicySimpleCondition policyCondition;

        if (topDomainUserId != null) {
            policyCondition = Policies.PolicySimpleCondition.builder().expression("context.request.author.id == " + "'" + subdomainUserId + "' || context.request.author.id == " + "'" + topDomainUserId + "'").build();

        } else {
            policyCondition = Policies.PolicySimpleCondition.builder().expression("context.request.author.id == " + "'" + subdomainUserId + "'").build();

        }
        Policies.WorkflowCondition workflowCondition = Policies.WorkflowCondition.simpleBuilder()
                .role("admin")
                .quorum(1)
                .build();

        String policyId = "00000000-0000-0000-0000-000000000001";
        memory().set("domain_yes_policy", policyId);

        return Policies.CreateDomainGenesisPolicy.builder()
                .id(policyId)
                .alias("Yes policy")
                .locked(false)
                .condition(policyCondition)
                .workflow(workflowCondition)
                .build();
    }

    @And("the subdomain number {string} is created with Yes policy including update rights for {string} admin")
    public void theSubdomainNumberIsCreatedWithYesPolicyIncludingUpdateRightsForAdmin(String arg0, String domainPseudonym) {
        String domainId = UUID.randomUUID().toString();

        String pseudonym = "subdomain" + arg0;
        String domainAlias = "domain" + domainId;
        Harmonize harmonize = harmonize();

        String userId = UUID.randomUUID().toString();
        GeneratedKeys generatedKeys = requestKeys();

        memory().setCryptographicKeys(userId, generatedKeys);
        UserCredentials userCredentials = UserCredentials.from(domainId, userId, generatedKeys);
        String topDomainUserId = UserSteps.getDefaultUserOfDomain(domainPseudonym);

        Policies.CreateDomainGenesisPolicy policy = prepareSubdomainPolicy(topDomainUserId, userId);

        IntentData intentData = harmonize.domains().create()
                .id(domainId)
                .alias(domainAlias)
                .targetDomainId(currentDomain())
                .permissions(Domains.Permissions.builder().readAccess(
                        Domains.ReadAccess.builder().all(Arrays.asList("admin", "manager", "trader", "director", "aml")).build()
                ).build())
                .policy(policy)
                .user(Users.CreateDomainGenesisUser.builder()
                        .id(userId)
                        .alias("admin")
                        .description("admin")
                        .customProperties(new HashMap<>())
                        .publicKey(userCredentials.getPublicKey().asBase64Str())
                        .role("admin")
                        .build())
                .submit();
        intentData.waitForSucceed(Timeout.minutes(4))
                .waitForIntentStatus(Set.of(Intents.IntentStatus.Executed), Timeout.minutes(1));
        Domains.CreateDomain domain = (Domains.CreateDomain) intentData.getIntent().getPayload();
        storeNewDomain(domain, List.of(userCredentials), intentData, pseudonym);
    }

    //, request as "subdomain3_request"
    @Given("subdomain number {string} is created, request as {string}")
    public void subdomainNumberIsCreated(String arg0, String requestPseudonym) {
        theSubdomainNumberIsCreatedWithDefaultYesPolicyForSubdomainAdmin(arg0, requestPseudonym);
    }

    @Given("subdomain number {string} is created")
    public void subdomainNumberIsCreated(String arg0) {
        theSubdomainNumberIsCreatedWithDefaultYesPolicyForSubdomainAdmin(arg0);
    }

    @When("{string} alias is updated")
    public void aliasIsUpdated(String subdomainPseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(subdomainPseudonym);
        Domains.UpdateDomain domainUpdate = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate();
        domainUpdate.setAlias("Test domain alias update " + UUID.randomUUID());

        long currentRevision = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getMetaData().getRevision();
        domainUpdate.getEntityIdReference().setRevision(currentRevision);

        Author author = Author.builder()
                .domainId(currentDomain())
                .id(context().userCredentials.getId())
                .build();

        // for update() subdomain parent domain should be specified as a target
        String targetDomain = wrapper.getDomainId();

        IntentData intentData = harmonize().domains().update(author, targetDomain, domainUpdate, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentExecuted(Timeout.minutes(3));

        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
        updateWrapper.setUpdate(domainUpdate);

        storeUpdatedDomain(wrapper, intentData, updateWrapper);
    }

    @When("subdomain alias is updated")
    public void subdomainIsUpdatedWithTheFollowingData() {
        aliasIsUpdated("subdomain1");
    }

    @When("subdomain lock intent is executed")
    public void subdomainLockIntentIsExecuted() throws Exception {
        domainLockIntentIsExecuted("subdomain1");
    }

    @When("{string} domain lock intent is executed")
    public void domainLockIntentIsExecuted(String domainPseudonym) throws Exception {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        //asUpdate is used here only for purpose of revision tracking
        Domains.UpdateDomain updateDomain = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate();

        long currentRevision = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getMetaData().getRevision();
        updateDomain.getEntityIdReference().setRevision(currentRevision);

        Author author = Author.builder()
                .domainId(currentDomain())
                .id(context().userCredentials.getId())
                .build();

        String targetDomain = wrapper.getDomainId();
        Domains.Domain domain = harmonize().domains().get(wrapper.getId()).orElseThrow(() -> new Exception(String.format("Domain with id %s is not returned", wrapper.getId())));

        IntentData intentData = harmonize().domains().lock(author, targetDomain, domain, Lock.Locked, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentExecuted(Timeout.minutes(3));

        wrapper.setEntity(domain);
        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
        updateWrapper.setUpdate(updateDomain);
        storeUpdatedDomain(wrapper, intentData, updateWrapper);
    }

    @When("subdomain unlock intent is executed")
    public void sudomainUnlockIntentIsExecuted() throws Exception {
        domainUnlockIntentIsExecuted("subdomain1");
    }

    @When("subdomain unlock intent is is submitted using the author and target domain specified above")
    public void subdomainUnlockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove() throws Exception {
        domainUnlockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove("subdomain1");
    }

    @When("{string} domain unlock intent is is submitted using the author and target domain specified above")
    public void domainUnlockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(String domainPseudonym) throws Exception {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        //asUpdate is used here only for purpose of revision tracking

        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = memory().get("target_domain_id");
        submitUnlockDomainIntent(author.build(), targetDomain, wrapper);
    }

    @When("subdomain lock intent is submitted using the author and target domain specified above")
    public void subdomainLockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove() throws Exception {
        domainLockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove("subdomain1");
    }

    @When("{string} domain lock intent is is submitted using the author and target domain specified above")
    public void domainLockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(String domainPseudonym) throws Exception {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        //asUpdate is used here only for purpose of revision tracking

        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = memory().get("target_domain_id");

        submitLockDomainIntent(author.build(), targetDomain, wrapper);
    }

    public void submitUnlockDomainIntent(Author author, String targetDomain, EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper) throws Exception {
        try {
            Domains.Domain domain = harmonize().domains().get(wrapper.getId()).orElseThrow(() -> new Exception(String.format("Domain with id %s is not returned", wrapper.getId())));

            IntentData intentData = harmonize().domains().lock(author, targetDomain, domain, Lock.Unlocked, Expire.hours(1));
            memory().set("intent", intentData);
            memory().set("last_propose_intent", intentData);

            UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
            updateWrapper.setUpdate(Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate());

            storeUpdatedDomain(wrapper, intentData, updateWrapper);

        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    public void submitLockDomainIntent(Author author, String targetDomain, EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper) throws Exception {
        try {
            Domains.Domain domain = harmonize().domains().get(wrapper.getId()).orElseThrow(() -> new Exception(String.format("Domain with id %s is not returned", wrapper.getId())));

            IntentData intentData = harmonize().domains().lock(author, targetDomain, domain, Lock.Locked, Expire.hours(1));
            memory().set("intent", intentData);
            memory().set("last_propose_intent", intentData);

            UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
            updateWrapper.setUpdate(Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate());

            storeUpdatedDomain(wrapper, intentData, updateWrapper);

        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @When("{string} domain unlock intent is executed")
    public void domainUnlockIntentIsExecuted(String domainPseudonym) throws Exception {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        //asUpdate is used here only for purpose of revision tracking
        Domains.UpdateDomain updateDomain = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate();

        long currentRevision = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getMetaData().getRevision();
        updateDomain.getEntityIdReference().setRevision(currentRevision);

        Author author = Author.builder()
                .domainId(currentDomain())
                .id(context().userCredentials.getId())
                .build();

        String targetDomain = wrapper.getDomainId();
        Domains.Domain domain = harmonize().domains().get(wrapper.getId()).orElseThrow(() -> new Exception(String.format("Domain with id %s is not returned", wrapper.getId())));

        IntentData intentData = harmonize().domains().lock(author, targetDomain, domain, Lock.Unlocked, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentExecuted(Timeout.minutes(3));

        wrapper.setEntity(domain);

        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
        updateWrapper.setUpdate(updateDomain);
        storeUpdatedDomain(wrapper, intentData, updateWrapper);
    }

    @Then("{string} domain is {string}")
    public void domainIs(String domainPseudonym, String lock) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        Assert.assertEquals(lock.toLowerCase(Locale.ROOT), Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getLock().toLowerCase(Locale.ROOT)
        );
    }

    @Then("subdomain lock state is {string}")
    public void subdomainIs(String lock) {
        domainIs("subdomain1", lock);
    }

    @When("root domain alias is updated")
    public void rootDomainAliasIsUpdated() {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = new EntityWrapper<>(rootDomainId(), rootDomainId(), "domain", null);
        Domains.UpdateDomain domainUpdate = Objects.requireNonNull(harmonize().domains().get(rootDomainId()).orElse(null)).asUpdate();

        domainUpdate.setAlias("New alias " + UUID.randomUUID());

        long currentRevision = Objects.requireNonNull(harmonize().domains().get(currentDomain()).orElse(null)).getMetaData().getRevision();
        domainUpdate.getEntityIdReference().setRevision(currentRevision);

        IntentData intentData = harmonize().domains().update(domainUpdate, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentExecuted(Timeout.minutes(3));

        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
        updateWrapper.setUpdate(domainUpdate);
        storeUpdatedDomain(wrapper, intentData, updateWrapper);
    }

    public Map<String, List<String>> buildReadAccessMap(DataTable dataTable) {
        Map<String, Object> map = dataTable.asMap(String.class, Object.class);
        Map<String, List<String>> readAccessMap = new HashMap<>();

        map.keySet().forEach(k -> {
            List<String> roles = Arrays.stream(map.get(k).toString().split(",")).collect(Collectors.toList());
            List<String> newList = new ArrayList<>();
            roles.forEach(r -> newList.add(r.trim()));
            readAccessMap.put(k, newList);
        });
        return readAccessMap;
    }

    @When("intent for read permissions update of {string} is submitted using the author and target domain specified above")
    public void intentForDomainReadPermissionsUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(String domainPseudonym, DataTable dataTable) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper =
                getDomainByPseudonym(domainPseudonym);

        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = memory().get("target_domain_id");
        String reference = wrapper.getId();

        Map<String, List<String>> readAccessMap = buildReadAccessMap(dataTable);

        try {
            Domains.UpdateDomainPermissions updateDomainPermissions = Objects.requireNonNull(harmonize().domains().get(reference).orElse(null)).asPermissionsUpdate();
            submitUpdateDomainPermissionsIntent(author.build(), targetDomain,
                    domainHelper.setUpdatePermissions(readAccessMap, updateDomainPermissions), wrapper);
            domainHelper.setUpdatePermissions(readAccessMap, updateDomainPermissions);
        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @And("{string} domain read permissions are updated with the following")
    public void domainReadPermissionsAreUpdatedWithTheFollowing(String domainPseudonym, DataTable dataTable) throws Exception {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper =
                getDomainByPseudonym(domainPseudonym);

        Author author = context().userCredentials.asAuthor();
        String targetDomain = wrapper.getId();
        String reference = wrapper.getId();

        Map<String, List<String>> readAccessMap = buildReadAccessMap(dataTable);

        try {
            Domains.UpdateDomainPermissions updateDomainPermissions = Objects.requireNonNull(harmonize().domains().get(reference).orElse(null)).asPermissionsUpdate();

            submitUpdateDomainPermissionsIntent(author, targetDomain,
                    domainHelper.setUpdatePermissions(readAccessMap, updateDomainPermissions), wrapper);
            IntentSteps.submittedIntentSucceeds("Executed");

        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
        //domainHelper.setUpdatePermissions(readAccessMap, updateDomainPermissions);


    }

    @When("intent for subdomain read permissions update is submitted using the author and target domain specified above")
    public void intentForSubdomainReadPermissionsUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(DataTable dataTable) {
        intentForDomainReadPermissionsUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove("subdomain1", dataTable);
    }

    @When("intent for {string} domain alias update is submitted using the author and target domain specified above")
    public void intentForDomainAliasUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(String domainPseudonym) {
        Author.Builder author = memory().get("author_object_builder");
        String targetDomain = memory().get("target_domain_id");
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper =
                getDomainByPseudonym(domainPseudonym);
        String reference = wrapper.getId();
        Domains.UpdateDomain domainUpdate = Objects.requireNonNull(harmonize().domains().get(reference).orElse(null)).asUpdate();
        domainUpdate.setAlias("Test domain alias update " + UUID.randomUUID());

        long currentRevision = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getMetaData().getRevision();
        domainUpdate.getEntityIdReference().setRevision(currentRevision);

        submitUpdateDomainIntent(author.build(), targetDomain, domainUpdate, wrapper);
    }

    public void submitUpdateDomainIntent(Author author, String targetDomain, Domains.UpdateDomain domainUpdate, EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper) {
        try {
            IntentData intentData = harmonize().domains().update(author, targetDomain, domainUpdate, Expire.hours(1));
            memory().set("intent", intentData);

            UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
            updateWrapper.setUpdate(domainUpdate);

            storeUpdatedDomain(wrapper, intentData, updateWrapper);

        } catch (RuntimeException ex) {
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    public void submitUpdateDomainPermissionsIntent(Author author, String targetDomain, Domains.UpdateDomainPermissions updatePermissions, EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper) {
        IntentData intentData = harmonize().domains().update(author, targetDomain, updatePermissions, Expire.hours(1));
        memory().set("intent", intentData);
        memory().set("last_propose_intent", intentData);

        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = new UpdateActionWrapper<>();
        updateWrapper.setUpdateDomainPermissions(updatePermissions);

        storeUpdatedDomain(wrapper, intentData, updateWrapper);
    }

    @When("intent for subdomain alias update is submitted using the author and target domain specified above")
    public void intentForSubdomainAliasUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove() {
        intentForDomainAliasUpdateIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove("subdomain1");
    }

    @And("{string} domain revision after update is correct")
    public void domainRevisionAfterUpdateIsCorrect(String domainPseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = getDomainByPseudonym(domainPseudonym);
        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> actionWrapper = wrapper.getUpdateActionWrapper();

        long expectedRevision = actionWrapper.getUpdate().getEntityIdReference().getRevision() + 1;
        waitForDomainRevisionUpdate(wrapper, expectedRevision, Timeout.seconds(5));

        updateDomainRevision(wrapper, expectedRevision);
    }

    public void waitForDomainRevisionUpdate(EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper, Long expectedRevision, Timeout timeout) {
        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
            long actualRevision = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).getMetaData().getRevision();

            if (actualRevision == expectedRevision) {
                return;
            }
        }
        throw new RuntimeException(String.format("Timed out while waiting for revision to be %s for domain %s", expectedRevision, wrapper.getId()));
    }

    @And("subdomain revision after update is correct")
    public void subdomainRevisionAfterUpdateIsCorrect() {
        domainRevisionAfterUpdateIsCorrect("subdomain1");
    }

    @Then("updated {string} domain details are correct")
    public void updatedDomainDetailsAreCorrect(String pseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> updateDomain = getDomainByPseudonym(pseudonym);
        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateActionWrapper = updateDomain.getUpdateActionWrapper();

        String data = Objects.requireNonNull(harmonize().domains().get(updateDomain.getId()).orElse(null)).asUpdate().asJson();
        Assert.assertEquals("Details updated are correct ", data, updateActionWrapper.getUpdate().asJson());
    }

    @Then("updated subdomain details are correct")
    public void updatedSubdomainDetailsAreCorrect() {
        updatedDomainDetailsAreCorrect("subdomain1");
    }

    @And("{string} permissions update details and revision are correct")
    public void permissionsUpdateDetailsAreCorrect(String domainPseudonym) {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> updateDomain = getDomainByPseudonym(domainPseudonym);
        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateActionWrapper = updateDomain.getUpdateActionWrapper();

        long expectedRevision = updateActionWrapper.getUpdateDomainPermissions().getEntityIdReference().getRevision() + 1;
        updateActionWrapper.getUpdateDomainPermissions().getEntityIdReference().setRevision(expectedRevision);

        String data = Objects.requireNonNull(harmonize().domains().get(updateDomain.getId()).orElse(null)).asPermissionsUpdate().asJson();
        Assert.assertEquals("Details updated are not correct ", data, updateActionWrapper.getUpdateDomainPermissions().asJson());
    }

    @And("subdomain permissions update details and revision are correct")
    public void subdomainPermissionsUpdateDetailsAreCorrect() {
        permissionsUpdateDetailsAreCorrect("subdomain1");
    }

    @Then("the subdomain should exist")
    public void the_subdomain_should_exist() {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain> wrapper = memory().get("last_created_subdomain");
        assertTrue(harmonize().domains().get(wrapper.getId()).isPresent());
    }

    @Given("created subdomain is a current domain")
    public void createdSubdomainIsACurrentDomain() {
        EntityWrapper<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain> wrapper = memory().get("last_created_subdomain");
        currentDomainIs(wrapper.getPseudonym());
    }

    @Given("the current domain is a root domain")
    public static void theCurrentDomainIsARootDomain() {
        setCurrentDomain("root domain");
    }

    @Given("the current domain is {string} domain")
    public void currentDomainIs(String domainPseudonym) {
        setCurrentDomain(domainPseudonym);
    }

    @Given("request author object is created and contains id of admin user from subdomain")
    public void requestAuthorObjectIsCreatedAndContainsIdOfAdminUserFromSubdomain() {
        requestAuthorObjectIsCreatedAndContainsIdOfAdminUserFrom("subdomain1");
    }

    @Given("request author object is created and contains id of admin user from {string}")
    public void requestAuthorObjectIsCreatedAndContainsIdOfAdminUserFrom(String domainPseudonym) {
        String domainId = getDomainIdByPseudonym(domainPseudonym);

        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> user = UserSteps.getUserByRole("admin", domainId);
        if (user.getPseudonym().equalsIgnoreCase("subdomain admin user") || user.getPseudonym().equalsIgnoreCase("default")) {
            user = UserSteps.getUserByPseudonym(user.getPseudonym(), domainId);
        }
        Author.Builder author = Author.builder()
                .id(user.getId());

        memory().set("author_object_builder", author);
    }

    @Given("request author object is created and contains id of {string} user from {string}")
    public void requestAuthorObjectIsCreatedAndContainsIdOfUserFrom(String userPseudonym, String domainPseudonym) {
        String originDomainId = getDomainIdByPseudonym(domainPseudonym);
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> user = UserSteps.getUserByPseudonym(userPseudonym, originDomainId);

        Author.Builder author = Author.builder()
                .id(user.getId());

        memory().set("author_object_builder", author);
    }

    @And("reference is set to id of yes policy from subdomain")
    public void referenceIsSetToIdOfYesPolicyFromSubdomain() {
        memory().set("reference", "00000000-0000-0000-0000-000000000001");
    }

    @And("request author object contains domainId of subdomain")
    public void requestAuthorObjectContainsDomainIdOfSubdomain() {
        requestAuthorObjectContainsDomainIdOf("subdomain1");
    }

    @And("request author object contains domainId of {string}")
    public void requestAuthorObjectContainsDomainIdOf(String domainPseudonym) {
        String domainId = getDomainIdByPseudonym(domainPseudonym);
        Author.Builder author = memory().get("author_object_builder");
        author.domainId(domainId);

        memory().set("author_object_builder", author);
    }

    @And("target domain is set to subdomain id")
    public void targetDomainIsSetToSubdomainId() {
        targetDomainIsSetTo("subdomain1");
    }

    @And("target domain is set to {string}")
    public void targetDomainIsSetTo(String domainPseudonym) {
        String domainId = getDomainIdByPseudonym(domainPseudonym);
        memory().set("target_domain_id", domainId);
    }

    /**
     * Helper function that will save the new CreateDomain
     * as an EntityWrapper
     */
    private void storeNewDomain(Domains.CreateDomain createDomain, List<UserCredentials> userCredentials, IntentData createIntentData, String domainPseudonym) {
        /*
            We do:
              * Create an EntityWrapper for the CreateDomain and store it in memory
              * For each user for the Domain we create a EntityWrapper and store it in memory
              * finall we switch the default domain and logged in user via the setCurrentDomain method
         */
        String entityType = "domain";
        String parentDomain = createIntentData.getIntent().getAuthor().getDomainId();

        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper = new EntityWrapper<>(parentDomain, createDomain.getId(), entityType, null);
        wrapper.setCreateEntity(createDomain, createIntentData);
        wrapper.setEntity(harmonize().domains().get(createDomain.getId()).orElseThrow());
        wrapper.setPseudonym(domainPseudonym);

        memory().setEntityData(wrapper);
        memory().set("last_created_subdomain", wrapper);

        Map<String, UserCredentials> userCredentialsMap = new HashMap<>();
        userCredentials.forEach(uc -> userCredentialsMap.put(uc.getId(), uc));

        createDomain.getUsers().forEach(u -> {

            if (!userCredentialsMap.containsKey(u.getId())) {
                throw new RuntimeException("Check the step creating the domain and users, there are no credentials for user " + u.getId() + " "
                        + u.getAlias());
            }

            storeCreatedUser(
                    createDomain.getId(),
                    u.asUser(),
                    null,
                    null,
                    u.getRoles().stream().collect(Collectors.joining(",")),
                    userCredentialsMap.get(u.getId()),
                    "subdomain admin user"
            );
        });
    }

    public static EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> getDomainByPseudonym(String pseudonym) {
        return memory().<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain, Domains.UpdateDomainPermissions>getEntityDataWithActionWrapperByType("domain")
                .filter(d -> d.getPseudonym().equalsIgnoreCase(pseudonym))
                .findFirst().orElseThrow(() -> new RuntimeException("The domain " + pseudonym + " was not set to memory"));
    }

    public static EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> getDomainById(String id) {
        return memory().<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain, Domains.UpdateDomainPermissions>getEntityDataWithActionWrapperByType("domain")
                .filter(d -> d.getId().equalsIgnoreCase(id))
                .findFirst().orElseThrow(() -> new RuntimeException("The domain with id " + id + " was not set to memory"));
    }

    public static String getDomainIdByPseudonym(String pseudonym) {
        if (pseudonym.equalsIgnoreCase("root domain"))
            return rootDomainId();

        else {
            return getDomainByPseudonym(pseudonym).getId();
        }
    }

    public void storeUpdatedDomain(EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper, IntentData intentData, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateDomain) {
        wrapper.setUpdateActionWrapper(updateDomain, intentData);
        memory().setEntityData(wrapper);
    }

    public EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> getDomainById(String domainId, String id) throws Exception {
        return memory().<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain, Domains.UpdateDomainPermissions>getEntityDataWithUpdateWrapperById("domain", domainId, id)
                .orElseThrow(() -> new Exception(String.format("Domain with id is not found %s", id)));
    }

    public void updateDomainRevision(EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> wrapper, Long revision) {
        // Setting revision to both Update and UpdateReadPermissions in case of any update (although it is not possible to update both with 1 request)
        Domains.UpdateDomainPermissions updatePermissions = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asPermissionsUpdate();
        updatePermissions.getEntityIdReference().setRevision(revision);

        Domains.UpdateDomain update = Objects.requireNonNull(harmonize().domains().get(wrapper.getId()).orElse(null)).asUpdate();
        update.getEntityIdReference().setRevision(revision);

        UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions> updateWrapper = wrapper.getUpdateActionWrapper();
        updateWrapper.setUpdateDomainPermissions(updatePermissions);
        updateWrapper.setUpdate(update);

        wrapper.setUpdateActionWrapper(updateWrapper);
        memory().setEntityData(wrapper);
    }

    @Given("the {string} domain is locked by intent from {string}")
    public void theDomainIsLockedByIntentFrom(String targetDomain, String currentDomain) throws Exception {
        currentDomainIs(currentDomain);
        requestAuthorObjectIsCreatedAndContainsIdOfAdminUserFrom(currentDomain);
        requestAuthorObjectContainsDomainIdOf(currentDomain);

        String domainId = getDomainIdByPseudonym(targetDomain);
        memory().set("target_domain_id", harmonize().domains().get(domainId).get().getParentId());
        domainLockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(targetDomain);

        IntentSteps.submittedIntentSucceeds("Executed");
    }

    @Given("the {string} domain is unlocked by intent from {string}")
    public void theDomainIsUnlockedByIntentFrom(String targetDomain, String currentDomain) throws Exception {
        currentDomainIs(currentDomain);
        requestAuthorObjectIsCreatedAndContainsIdOfAdminUserFrom(currentDomain);
        requestAuthorObjectContainsDomainIdOf(currentDomain);

        String domainId = getDomainIdByPseudonym(targetDomain);
        memory().set("target_domain_id", harmonize().domains().get(domainId).get().getParentId());
        domainUnlockIntentIsIsSubmittedUsingTheAuthorAndTargetDomainSpecifiedAbove(targetDomain);

        IntentSteps.submittedIntentSucceeds("Executed");
    }

    @And("root domain is a coercive domain")
    public void rootDomainIsACoerciveDomain() {
        Assert.assertTrue(harmonize().domains().get("5cd224fe-193e-8bce-c94c-c6c05245e2d1").get().getGoverningStrategy().equalsIgnoreCase("CoerceDescendants"));
    }
}
