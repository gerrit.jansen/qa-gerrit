package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.IntentsActions;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.api.DomainHelper;
import harmonizeqa.api.EntityWrapper;
import harmonizeqa.api.PolicyHelper;
import harmonizeqa.api.GlobalContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.time.temporal.TemporalUnit;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

import static harmonizeqa.steps.UserSteps.retrieveAndLoginUser;

public class IntentSteps extends BaseSteps {
    private static Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    @When("{string} user approves the intent")
    public void userApprovesTheIntent(String arg0) {
        retrieveAndLoginUser(arg0, currentDomain());
        IntentData proposedIntent = memory().get("last_propose_intent");
        ApproveIntentData acceptIntent;

        try{
            Expire expire = Expire.hours(1);
            if (memory().get("decision_expiration_time") != null){
                expire = memory().get("decision_expiration_time");
            }
            acceptIntent = harmonize().intents(currentDomain())
                    .approve(proposedIntent.getIntentId(), expire);
            memory().set("decision_intent", acceptIntent);
        }catch (RuntimeException ex){
            //IntentException
            LOGGER.info(String.format("Intent could not be approved %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @When("{string} user approves the {string} intent")
    public void userApprovesTheSubdomainIntent(String arg0, String arg1) {
        retrieveAndLoginUser(arg0, currentDomain());
        IntentData proposedIntent = memory().get("last_propose_intent");

        String subdomainId = DomainHelper.getDomainByPseudonym(memory(), arg1).getId();

        ApproveIntentData acceptIntent;
        try{
            acceptIntent = harmonize().intents(currentDomain())
                    .approveSubdomainIntent(subdomainId, proposedIntent.getIntent().getPayload().asMap().get("id").toString(),
                            proposedIntent.getIntentId(), Expire.hours(24));
            memory().set("decision_intent", acceptIntent);
        }catch (RuntimeException ex){
            LOGGER.info(String.format("Intent could not be approved %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @Then("intent request succeeds")
    public void intentRequestSucceeds() {
        IntentData intentData = memory().get("intent");
        intentData.waitForSucceed(Timeout.minutes(3));
    }

    @Then("an error with {string} is returned")
    public void anErrorWithIsReturned(String error) {
        if(memory().get("intent_error")!= null){
            Assert.assertTrue(String.format("Expected error %s, but was %s", error, memory().get("intent_error").toString()), memory().get("intent_error").toString().contains(error));
        }else{
            IntentData intentData = memory().get("intent");
            Assert.fail(String.format("Intent error was expected, but request succeeded. \nResponse: %s. \nSubmitted request: %s", intentData.getResponse(),
                    intentData.getIntent().asJson()));
        }
    }

    @When("{string} user rejects the intent")
    public void userRejectsTheIntent(String arg0) {
        retrieveAndLoginUser(arg0, currentDomain());

        IntentData proposedIntent = memory().get("last_propose_intent");
        RejectIntentData rejectIntent;

        try{
            rejectIntent= harmonize().intents(currentDomain()).reject(proposedIntent.getIntentId(), "AT test rejection", Expire.hours(24));
            memory().set("decision_intent", rejectIntent);

        }catch (RuntimeException ex){
            LOGGER.info(String.format("Intent could not be rejected %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @When("{string} user rejects the {string} intent")
    public void userRejectsTheSubdomainIntent(String arg0, String arg1) {
        retrieveAndLoginUser(arg0, currentDomain());

        String subdomainId = DomainHelper.getDomainByPseudonym(memory(), arg1).getId();
        IntentData proposedIntent = memory().get("last_propose_intent");
        RejectIntentData rejectIntent;

        try{
            rejectIntent =harmonize().intents(currentDomain())
                    .rejectSubdomainIntent(subdomainId, proposedIntent.getIntent().getPayload().asMap().get("id").toString(),
                            proposedIntent.getIntentId(), "Rejection test",  Expire.hours(24));
            memory().set("decision_intent", rejectIntent);

        }catch (RuntimeException ex){
            LOGGER.info(String.format("Intent could not be rejected %s", ex));
            memory().set("intent_error", ex);
        }
    }

    @And("decision intent succeeds")
    public void intentSucceeds(){
        BaseIntentData intentData = memory().get("decision_intent");
        intentData.waitForSucceed(Timeout.minutes(2));

        memory().set("last_intent", intentData);
    }

    @And("request succeeds and submitted intent status is {string}")
    public static void submittedIntentSucceeds(String status){
        IntentData intentData = memory().get("intent");
        Intents.IntentStatus intentStatus = Intents.IntentStatus.valueOf(status);

        intentData.waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(Set.of(intentStatus), Timeout.minutes(3));
    }

    @And("decision intent request fails with correct details and hint includes {string}")
    public void decisionIntentRequestFailsWithCorrectDetailsAndHintIncludes(String arg0) throws Exception {
        BaseIntentData intentData = memory().get("decision_intent");
        intentData.waitForStatus(Collections.singleton("Failed"), Timeout.seconds(10));

        Request request = harmonize().requests(currentDomain()).get(intentData.ensureRequestId())
                .orElseThrow(() -> new Exception(String.format("Request %s was not found", intentData.ensureRequestId())));

        List<Request.RequestStateHistory> historyList = request.getHistory();

        Request.RequestStateHistory item =  historyList.stream().filter(v ->
                v.getSource().equalsIgnoreCase("Notary") &&
                        v.getStatus().equalsIgnoreCase("Failed"))
                .findFirst().orElseThrow(() -> new Exception(String.format("Request %s was not failed in the notary source", request.getId())));

        String hint = item.getHint();
        Assert.assertTrue("Request failure error hint is not as expected", hint.toLowerCase().contains(arg0.toLowerCase()));
        Assert.assertEquals(intentData.getIntent().getAuthor().asJson(), request.getRequester().asJson());
    }

    @Then("intent is created in {string} domain and has status {string}")
    public void intentIsCreatedInDomainAndHasStatus(String domainPseudonym, String status) {
        IntentData intentData = memory().get("last_propose_intent");
        intentData.waitForProcessingOrSucceeded(Timeout.seconds(10));
        setCurrentDomain(domainPseudonym);
        UserSteps.userIsLoggedIn("subdomain admin user");

        String intentStatus = Objects.requireNonNull(harmonize().intents(currentDomain())
                .get(intentData.getIntentId()).orElse(null))
                .getState().getStatus();
        Assert.assertTrue(String.format("Expected intent status to be %s for intent %s in domain %s, but was %s",
                status, intentData.getIntentId(), currentDomain(), intentStatus), status.equalsIgnoreCase(intentStatus));
    }


    @Then("the intent status is {string}")
    public static void theIntentStatusIs(String arg0) throws Exception {
        Assert.assertEquals(arg0, getLastProposeIntentPerCurrentDomain().getState().getStatus());
    }

    @And("the number of applicable policies is {string}")
    public void theNumberOfApplicablePoliciesIs(String arg0) throws Exception {
        memory().set("number_of_applicable_policies", Integer.parseInt(arg0));

        Assert.assertEquals(arg0, Integer.toString(getLastProposeIntentPerCurrentDomain().getState().getProgress().size()));
    }

    @Given("the progress per policy from current domain is being verified")
    public void theProgressPerPolicyFromCurrentDomainIsBeingVerified() throws Exception {
        setCurrentProgressPerPolicy(currentDomain());
    }

    @Given("the progress per policy from domain {string} is being verified")
    public void theProgressPerPolicyFromDomainIsBeingVerified(String arg0) throws Exception {
        EntityWrapper domain = DomainHelper.getDomainByPseudonym(memory(), arg0);
        setCurrentProgressPerPolicy(domain.getId());
    }

    @Given("the progress per policy from root domain is being verified")
    public void theProgressPerPolicyFromRootDomainIsBeingVerified() throws Exception {
        setCurrentProgressPerPolicy(rootDomainId());
    }

    // Searching in the list of intents instead of getting intent details as with cross domain scenario
    // intent detail are not allowed to read to the user from the other domain
    public static Intents.IntentEntity getLastProposeIntentPerCurrentDomain() throws Exception {
        IntentData lastProposeIntent = memory().get("last_propose_intent");

        IntentsActions.ListCriteria listCriteria = IntentsActions.ListCriteria.builder()
                .intentTypes(Collections.singletonList(lastProposeIntent.getIntent().getPayload().asMap().get("type").toString()))
                .sortBy("details.metadata.createdAt")
                //       .entityId(lastProposeIntent.getIntent().getPayload().asMap().get("id").toString())
                .sortOrder(SortOrder.DESC)
                .limit(100)
                .build();

        return harmonize().intents(currentDomain())
                .list(listCriteria)
                .filter(intent -> intent.getId().equalsIgnoreCase(lastProposeIntent.getIntentId()))
                .findFirst().orElseThrow(() -> new Exception(String.format("Intent with id %s is not found in domain %s", lastProposeIntent.getIntentId(), currentDomain())));
    }

    public void setCurrentProgressPerPolicy(String domainId) throws Exception {
        Intents.WorkflowProgress progressPerPolicy = getLastProposeIntentPerCurrentDomain()
                .getState().getProgress()
                .stream()
                .filter(p -> p.getPolicyReference()
                        .getDomainId().equalsIgnoreCase(domainId))
                .findFirst().orElseThrow(() -> new Exception(String.format("Progress per policy for domain %s is not found", domainId)));
        memory().set("progress_per_policy_under_test", progressPerPolicy);
        memory().set("policy_under_test", PolicyHelper.getPolicyByDomainId(memory(), domainId));
    }

    @And("the progress per policy contains correct policy reference")
    public void theProgressPerPolicyContainsCorrectPolicyReference() {
        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> policy = memory().get("policy_under_test");

        Intents.WorkflowProgress progressPerPolicy = memory().get("progress_per_policy_under_test");

        String policyId = policy.getId();
        Assert.assertEquals(policyId, progressPerPolicy.getPolicyReference().getId());
    }

    @And("the progress per policy has {string} progress steps")
    public void theProgressPerPolicyHasProgressSteps(String arg0) {
        Intents.WorkflowProgress progressPerPolicy = memory().get("progress_per_policy_under_test");

        Assert.assertEquals(Integer.parseInt(arg0),progressPerPolicy.getStepProgress().size());
    }

    @And("the progress step {string} has state {string}")
    public void theProgressStepHasState(String arg0, String arg1) {
        Intents.WorkflowProgress progressPerPolicy = memory().get("progress_per_policy_under_test");

        Assert.assertEquals(arg1, progressPerPolicy.getStepProgress().get(Integer.parseInt(arg0)-1)
                .getState());
    }

    @And("intent submitter action is recorded in {string} decision of progress step {string}")
    public void intentSubmitterActionIsRecordedInDecisionOfProgressStep(String arg0, String arg1) throws Exception {
        BaseIntentData lastIntent = memory().get("last_intent");

        Intents.WorkflowProgress progressPerPolicy = memory().get("progress_per_policy_under_test");

        int decisionIndex = 0;
        List <Intents.DecisionPayload> decisions = progressPerPolicy.getStepProgress()
                .get(Integer.parseInt(arg1)-1).getDecisions();

        Assert.assertTrue(String.format("Number of decisions was %s", decisions.size()), decisions.size() > 0);

        if(decisions.size() > 1){
            decisionIndex = decisions.indexOf(decisions.stream().
                    filter(decisionPayload -> decisionPayload
                            .getAuthor()
                            .getId().equalsIgnoreCase(context().userCredentials.getId()))
                    .findFirst().orElseThrow(() -> new Exception(String.format("Decision with author id %s is not found", context().userCredentials.getId()))));
        }

        Assert.assertEquals(arg0, decisions.get(decisionIndex).getDecision());
        Assert.assertEquals(lastIntent.getIntent().getAuthor().asJson(), decisions.get(decisionIndex).getAuthor().asJson());
        Assert.assertEquals(Objects.requireNonNull(harmonize().users(currentDomain()).get(context().userCredentials.getId())
                .orElse(null)).getRoles(), decisions.get(decisionIndex).getUserRoles());
        Assert.assertEquals(lastIntent.getSignature(), decisions.get(decisionIndex).getSignature());
    }

    @And("intent details error {string} contains {string}")
    public void intentDetailsErrorContains(String errorProperty, String propertyValue) throws Exception {
        String errorCodeOrMsg = getLastProposeIntentPerCurrentDomain().getState().getError().asMap().get(errorProperty).toString();
        Assert.assertTrue(String.format("Expected %s, but actual value of %s was %s", propertyValue, errorProperty, errorCodeOrMsg), errorCodeOrMsg.toLowerCase().contains(propertyValue.toLowerCase()));
    }

    @Given("intent expiration time is set to {int} seconds from now")
    public void intentExpirationTimeIsSetToSecondsFromNow(int seconds) {
        memory().set("intent_expiration_time",Expire.seconds(seconds));
    }

    @Given("expiration timeout for intent is reached")
    public void intentExpirationTimeoutIsReached() throws InterruptedException {
        Expire expire = memory().get("intent_expiration_time");
        TemporalUnit tempUnit = expire.getChronoUnit();
        Thread.sleep(tempUnit.getDuration().toMillis());
    }

    @Given("expiration timeout for decision is reached")
    public void decisionExpirationTimeoutIsReached() throws InterruptedException {
        Expire expire = memory().get("decision_expiration_time");
        TemporalUnit tempUnit = expire.getChronoUnit();
        Thread.sleep(tempUnit.getDuration().toMillis());
        Thread.sleep(60000);
    }

    @Given("intent expiration time is set to {int} minutes from now")
    public void intentExpirationTimeIsSetToMinutesFromNow(int minutes) {
        memory().set("intent_expiration_time",Expire.minutes(minutes));
    }

    @Given("decision expiration time is set to {int} minute")
    public void decisionExpirationTimeIsSetToMinute(int minutes) {
        memory().set("decision_expiration_time",Expire.minutes(minutes));
    }
}
