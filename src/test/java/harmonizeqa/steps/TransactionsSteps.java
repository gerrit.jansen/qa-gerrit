package harmonizeqa.steps;

import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import harmonizeqa.TransactionsEnums;
import harmonizeqa.api.EntityWrapper;
import harmonizeqa.api.GlobalContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.math.BigInteger;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TransactionsSteps extends BaseSteps {
    private static final Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    @Given("{string} has token {string} in its balances")
    public void hasTokenInItsBalances(String accountPseudonym, String tickerName) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(accountPseudonym);
        String txOrderId = UUID.randomUUID().toString();
        String toAccountAddress = Objects.requireNonNull(harmonize().accounts(account.getDomainId()).getLatestAddress(account.getId()).orElse(null)).getAddress();

        memory().set("max_fee", "900000000000000000");
        memory().set("amount", "0");
        memory().set("tx_kind", TransactionsEnums.TxKinds.ERC20);

        //mint()
        String data = transactionsHelper.createData("40c10f19", toAccountAddress);
        String contractAddress = "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc";
        transactionsHelper.makeEthereumTxToAddressDest(account.getId(), account.getDomainId(), contractAddress, data, txOrderId);

        Transactions.Transaction transaction = transactionsHelper.getTransactionByOrderId(account.getDomainId(), txOrderId, Timeout.minutes(1));
        harmonize().transactions(account.getDomainId()).waitForTransactionProcessingStatus(transaction.getId(), Set.of(Accounts.ProcessingStatus.Completed.name()), Timeout.minutes(4));

        transactionsHelper.fetchTransferIdsByTxOrder(txOrderId, account.getDomainId());

        Assert.assertTrue(harmonize().accounts(account.getDomainId()).balances(account.getId())
                .anyMatch(balance -> Objects.requireNonNull(harmonize().tickers()
                        .get(balance.getTickerId()).orElse(null)).getName().equalsIgnoreCase(tickerName)));

        releaseQuarantinedTransfersToIntentIsExecuted(tickerName, accountPseudonym);
        memory().set("transfer_released", false);
    }

    @When("the bitcoin transaction intent from {string} to address of {string} is executed")
    public void theBitcoinTransactionIntentFromToAddressIsExecuted(String fromAccount, String toAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);

        prepareToInternalBtcTransaction(account1, account2);

        String txOrderId = UUID.randomUUID().toString();

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        transactionsHelper.createBitcoinTxToAddressDest(txOrderId, account1.getId(), account1.getDomainId(), account2Address);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the bitcoin dry run transaction intent from {string} to address of {string} is executed")
    public void theBitcoinDryRunTransactionIntentFromToAddressIsExecuted(String fromAccount, String toAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalBtcTransaction(account1, account2);

        String txOrderId = UUID.randomUUID().toString();

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        transactionsHelper.createDryRunBitcoinTxToAddressDest(txOrderId, account1.getId(), account1.getDomainId(), account2Address);
    }

    @Then("we have a valid dry run result")
    public void weHaveAValidBitcoinDryRunResult() {
        Transactions.TransactionDryRunResult result = memory().get("transaction_dry_run");

        Assert.assertNotNull(result);
        Assert.assertFalse(result.isFailure());
        Assert.assertFalse(result.isSuccess());
        Assert.assertTrue(result instanceof Transactions.TransactionDryRunResultSuccess);
    }

    @When("the bitcoin transaction intent from {string} to {string} is executed")
    public void theBitcoinTransactionIntentFromToIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalBtcTransaction(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.createBitcoinTxToAccountDest(txOrderId, account1.getId(), account2.getDomainId(), account2.getId());

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    private void prepareToInternalBtcTransaction(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> toAccount) {
        setBitcoinParameters(fromAccount);

        accountsHelper.storeCurrentBalances(toAccount, harmonize().accounts(toAccount.getDomainId()).balances(toAccount.getId()).collect(Collectors.toList()));
        transactionsHelper.setQuarantinedAmount(toAccount, new BigInteger("100000"), "Bitcoin testnet");
    }

    private void setBitcoinParameters(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount) {
        memory().set("amount", "100000");
        memory().set("max_fee", "20000");
        memory().set("tx_kind", TransactionsEnums.TxKinds.BTC);
        memory().set("transfer_released", false);

        accountsHelper.storeCurrentBalances(fromAccount, harmonize().accounts(fromAccount.getDomainId()).balances(fromAccount.getId()).collect(Collectors.toList()));
    }

    private void prepareToBtcRedirectTx(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount) {
        setBitcoinParameters(fromAccount);

        accountsHelper.storeCurrentBalances(fromAccount, harmonize().accounts(fromAccount.getDomainId()).balances(fromAccount.getId()).collect(Collectors.toList()));
    }

    private void prepareToInternalXRPLTransaction(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> toAccount) {
        setXrpParameters(fromAccount);
        accountsHelper.storeCurrentBalances(toAccount, harmonize().accounts(toAccount.getDomainId()).balances(toAccount.getId()).collect(Collectors.toList()));
        transactionsHelper.setQuarantinedAmount(toAccount, new BigInteger("1000000"), "XRPL testnet");
    }

    private void setXrpParameters(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount) {
        memory().set("amount", "1000000");
        memory().set("max_fee", "1000000");
        memory().set("tx_kind", TransactionsEnums.TxKinds.XRPL);
        memory().set("transfer_released", false);
        accountsHelper.storeCurrentBalances(fromAccount, harmonize().accounts(fromAccount.getDomainId()).balances(fromAccount.getId()).collect(Collectors.toList()));
    }

    @When("the transaction intent from {string} to {string} is executed using a call of smart contract transfer function")
    public void theTransactionIntentFromToIsExecutedUsingACallOfSmartContractFunction(String fromAccount, String toAccount, DataTable table) throws Exception {
        Map<String, String> map = table.asMap(String.class, String.class);

        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);

        prepareToInternalERC20Tx(map.get("amount"), map.get("amountInData"), account1, account2);

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                        .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        //transfer(address,uint256)
        assert account2Address != null;
        String data = transactionsHelper.createData("a9059cbb", account2Address);
        String txOrderId = UUID.randomUUID().toString();

        String contractAddress = "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc";
        transactionsHelper.makeEthereumTxToAddressDest(account1.getId(), account1.getDomainId(), contractAddress, data, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the transaction intent from {string} to external address is executed using a call of smart contract transfer function")
    public void theTransactionIntentFromToExternalAddressIsExecutedUsingACallOfSmartContractTransferFunction(String fromAccount, DataTable table) {
        Map<String, String> map = table.asMap(String.class, String.class);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(fromAccount);
        setERC20Parameters(map.get("amount"), map.get("amountInData"), account);

        //transfer(address,uint256)
        String data = transactionsHelper.createData("a9059cbb", "0x6bB1d0cd821F3F152478a498B146EEf220661DDF");
        String txOrderId = UUID.randomUUID().toString();

        String contractAddress = "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc";
        transactionsHelper.makeEthereumTxToAddressDest(account.getId(), account.getDomainId(), contractAddress, data, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the transaction intent from {string} to {string} is executed using transaction order to endpoint with ABI")
    public void theTransactionIntentFromToIsExecutedUsingTransactionOrderToEndpointWithABI(String fromAccount, String toAccount, DataTable dataTable) throws Exception {
        Map<String, String> map = dataTable.asMap(String.class, String.class);

        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);

        prepareToInternalERC20Tx(map.get("amount"), map.get("amountInData"), account1, account2);
        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        //transfer(address,uint256)
        assert account2Address != null;
        String data = transactionsHelper.createData("a9059cbb", account2Address);
        String txOrderId = UUID.randomUUID().toString();

        transactionsHelper.makeEthereumTxToEndpointDest(account1.getId(), account1.getDomainId(), memory().get("endpointId"), data, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    private void prepareToInternalERC20Tx(String amount, String amountInData, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> toAccount) {
        setERC20Parameters(amount, amountInData, fromAccount);

        accountsHelper.storeCurrentBalances(toAccount, harmonize().accounts(toAccount.getDomainId()).balances(toAccount.getId()).collect(Collectors.toList()));
        transactionsHelper.setQuarantinedAmount(toAccount, new BigInteger(amountInData), "METACO TEST TOKEN");
    }

    private void setERC20Parameters(String amount, String amountInData, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount) {
        memory().set("max_fee", "900000000000000000");
        memory().set("amount", amount);
        memory().set("amountInData", amountInData);
        memory().set("tx_kind", TransactionsEnums.TxKinds.ERC20);
        memory().set("transfer_released", false);

        accountsHelper.storeCurrentBalances(fromAccount, harmonize().accounts(fromAccount.getDomainId()).balances(fromAccount.getId()).collect(Collectors.toList()));
    }

    @When("ethereum transaction from {string} to address of {string} is executed")
    public void ethereumTransactionFromToAddressOfIsExecuted(String fromAccount, String toAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalEtherTx(account1, account2);

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeEthereumTxToAddressDest(account1.getId(), account1.getDomainId(), account2Address, null, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("ethereum transaction from {string} to address of {string} with encoded text in data field is executed")
    public void ethereumTransactionFromToAddressOfWithEncodedTextInDataFieldIsExecuted(String fromAccount, String toAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalEtherTx(account1, account2);

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                        .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        String txOrderId = UUID.randomUUID().toString();
        String data = "0x546869732069732051412074657374206f662064617461206669656c64";
        transactionsHelper.makeEthereumTxToAddressDest(account1.getId(), account1.getDomainId(), account2Address, data, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the ethereum transaction intent from {string} to external address is executed")
    public void theEthereumTransactionIntentFromToExternalAddressIsExecuted(String fromAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(fromAccount);
        setEthParameters(account);
        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeEthereumTxToAddressDest(account.getId(), account.getDomainId(), "0x6bB1d0cd821F3F152478a498B146EEf220661DDF", null, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("ethereum transaction from {string} to {string} is executed")
    public void ethereumTransactionFromToIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalEtherTx(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeEthereumTxToAccountDest(account1.getId(), account1.getDomainId(), account2.getId(), null, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("ethereum dry run transaction from {string} to {string} is executed")
    public void ethereumDryrunTransactionFromToIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalEtherTx(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeEthereumDryRunTxToAccountDest(account1.getId(), account1.getDomainId(), account2.getId(), null, txOrderId);
    }

    @When("the xrp transaction intent from {string} to address of {string} is executed")
    public void theXRPTransactionIntentFromToAddressIsExecuted(String fromAccount, String toAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalXRPLTransaction(account1, account2);
        String txOrderId = UUID.randomUUID().toString();

        String account2Address = Objects.requireNonNull(memory().getEntityDataById("account", account2.getDomainId(), account2.getId())
                .orElse(null)).getAddresses()
                .stream().findFirst()
                .orElseThrow(() -> new Exception(String.format("Address for account %s is not found", account2.getId())));

        transactionsHelper.makeXRPLTxToAddressDest(account1.getId(), account1.getDomainId(), account2Address, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the xrp transaction intent from {string} to external address is executed")
    public void theXrpTransactionIntentFromToExternalAddressIsExecuted(String fromAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(fromAccount);
        setXrpParameters(account);
        String txOrderId = UUID.randomUUID().toString();

        transactionsHelper.makeXRPLTxToAddressDest(account.getId(), account.getDomainId(),"rGHcRKASMaktfe3Ts7VQqwxjvrTT7gzD1X", txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the xrp transaction intent from {string} to {string} is executed")
    public void theXrpTransactionIntentFromToIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalXRPLTransaction(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeXRPLTxToAccountDest(account1.getId(), account1.getDomainId(), account2.getId(), txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the xrp transaction intent from {string} to endpoint with {string} address is executed")
    public void theXrpTransactionIntentFromToEndpointWithAddressIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalXRPLTransaction(account1, account2);
        EntityWrapper<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint> endpoint = endpointsHelper.getEndpointById(memory().get("endpointId").toString());

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeXRPLTxToEndpointDest(account1.getId(), account1.getDomainId(), endpoint.getId(), txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    private void prepareToInternalEtherTx(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount, EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> toAccount) {
        setEthParameters(fromAccount);

        accountsHelper.storeCurrentBalances(toAccount, harmonize().accounts(toAccount.getDomainId()).balances(toAccount.getId()).collect(Collectors.toList()));
        transactionsHelper.setQuarantinedAmount(toAccount, new BigInteger("100000000000000000"), "Ethereum testnet rinkeby");
    }

    private void setEthParameters(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> fromAccount) {
        memory().set("amount", "100000000000000000");
        memory().set("tx_kind", TransactionsEnums.TxKinds.ETHER);
        memory().set("max_fee", "200000000000000000");
        memory().set("transfer_released", false);

        accountsHelper.storeCurrentBalances(fromAccount, harmonize().accounts(fromAccount.getDomainId()).balances(fromAccount.getId()).collect(Collectors.toList()));
    }

    @Then("{string} has correct balances of {string} after {string} transaction")
    public void hasCorrectBalancesAfterTransaction(String accountNumber, String tickerName, String direction) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(accountNumber);
        List<Accounts.Balance> balances = accountsHelper.getAccountById1(account.getId(), account.getDomainId()).getBalances();

        TransactionsEnums.TxKinds txKind = memory().get("tx_kind");
        BigInteger amount = new BigInteger(memory().get(txKind.getAmountInPayload()).toString());

        transactionsHelper.verifyExpectedBalanceValue1(account, balances, TransactionsEnums.Tickers.byName(tickerName), amount, TransactionsEnums.TxDirections.fromString(direction));
    }

    @And("transaction processing status is {string}")
    public void transactionProcessingStatusIs(String txStatus) throws Exception {
        IntentData transactionOrder = memory().get("transaction_order");
        String targetDomain = transactionOrder.getIntent().getTargetDomainId();
        String orderId = transactionOrder.getIntent().getPayload().asMap().get("id").toString();

        Transactions.Transaction transaction = transactionsHelper.getTransactionByOrderId(targetDomain, orderId, Timeout.minutes(3));
        harmonize().transactions(targetDomain).waitForTransactionProcessingStatus(transaction.getId(), Set.of(txStatus), Timeout.minutes(3));
    }

    @And("transaction ledger status is {string}")
    public void transactionLedgerStatusIs(String ledgerStatus) throws Exception {
        IntentData transactionOrder = memory().get("transaction_order");
        String targetDomain = transactionOrder.getIntent().getTargetDomainId();
        String orderId = transactionOrder.getIntent().getPayload().asMap().get("id").toString();

        Transactions.Transaction transaction = transactionsHelper.getTransactionByOrderId(targetDomain, orderId, Timeout.minutes(3));
        harmonize().transactions(targetDomain).waitForTransactionLedgerStatus(transaction.getId(), Set.of(ledgerStatus), Timeout.minutes(4));
    }

    @And("transfers are quarantined")
    public void transfersAreQuarantined() throws Exception {
        IntentData transactionOrder = memory().get("transaction_order");
        String targetDomain = transactionOrder.getIntent().getTargetDomainId();
        String orderId = transactionOrder.getIntent().getPayload().asMap().get("id").toString();

        List<Transactions.Transfer> transfers = transactionsHelper.fetchTransferByTxOrder(orderId, targetDomain);
        transfers.forEach(transfer -> Assert.assertTrue(transfer.isQuarantined()));
    }

    @When("release {string} quarantined transfers to {string} intent is executed")
    public void releaseQuarantinedTransfersToIntentIsExecuted(String tickerName, String accountPseudonym) throws Exception {
        IntentData order = memory().get("transaction_order");
        String orderId = order.getIntent().getPayload().asMap().get("id").toString();
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(accountPseudonym);

        Accounts.ReleaseQuarantinedTransfers releaseQuarantinedTransfers = Accounts.ReleaseQuarantinedTransfers.builder()
                .accountId(account.getId())
                .transferIds(transactionsHelper.fetchTransferIdsByTxOrder(orderId, account.getDomainId()))
                .submit();

        harmonize().accounts(account.getDomainId()).releaseQuarantinedTransfers(releaseQuarantinedTransfers, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));

        memory().set("transfer_released", true);
    }

    public void releaseQuarantinedTransfersIsProposed(String accountPseudonym) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(accountPseudonym);

        Accounts.ReleaseQuarantinedTransfers release = getReleaseQuarantinedTransfersBuilder(accountPseudonym);

        try{
            IntentData<Accounts.ReleaseQuarantinedTransfers> intentData = harmonize().accounts(account.getDomainId())
                    .releaseQuarantinedTransfers(release, Expire.hours(1))
                    .waitForSucceed(Timeout.minutes(2));
            memory().set("last_propose_intent", intentData);
            memory().set("last_intent", intentData);
        }catch (RuntimeException ex){
            LOGGER.info(String.format("Intent could not be submitted %s", ex));
            memory().set("intent_error", ex);
        }
    }

    public Accounts.ReleaseQuarantinedTransfers getReleaseQuarantinedTransfersBuilder(String accountPseudonym) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(accountPseudonym);
        IntentData intentData = memory().get("transaction_order");
        String orderId = intentData.getIntent().getPayload().asMap().get("id").toString();

        return Accounts.ReleaseQuarantinedTransfers.builder()
                .accountId(account.getId())
                .transferIds(transactionsHelper.fetchTransferIdsByTxOrder(orderId, account.getDomainId()))
                .submit();
    }

    @When("release {string} quarantined transfers to {string} intent is proposed by {string} user")
    public void releaseQuarantinedTransfersToIntentIsProposedByUser(String tickerName, String accountPseudonym, String user) throws Exception {
        UserSteps.userIsLoggedIn(user);
        releaseQuarantinedTransfersIsProposed(accountPseudonym);
    }

    @And("{string} has {string} in its balances")
    public void hasInItsBalances(String account, String tickerName) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> accountWrapper = accountsHelper.getAccountByPseudonym(account);

        Assert.assertTrue(harmonize().accounts(accountWrapper.getDomainId()).balances(accountWrapper.getId())
                .anyMatch(balance -> Objects.requireNonNull(harmonize().tickers()
                        .get(balance.getTickerId()).orElse(null)).getName().equalsIgnoreCase(tickerName)));
    }

    @Then("{string} balances are not affected by redirect transaction")
    public void balancesAreNotAffectedByRedirectTransaction(String account) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> accountWrapper = accountsHelper.getAccountByPseudonym(account);

        List<Accounts.Balance> oldBalances = accountsHelper.getAccountById(accountWrapper.getId()).getBalances();
        List<Accounts.Balance> newBalances = harmonize().accounts(accountWrapper.getDomainId()).balances(accountWrapper.getId()).collect(Collectors.toList());

        Assert.assertEquals(oldBalances, newBalances);
    }

    @When("the bitcoin transaction intent from {string} to endpoint with {string} address is executed")
    public void theBitcoinTransactionIntentFromToEndpointWithAddressIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint> endpoint = endpointsHelper.getEndpointById(memory().get("endpointId").toString());
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalBtcTransaction(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.createBitcoinTxToEndpointDest(txOrderId, account1.getId(), account1.getDomainId(), endpoint.getId());

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the bitcoin transaction intent from {string} to external address is executed")
    public void theBitcoinTransactionIntentFromToExternalAddressIsExecuted(String fromAccount) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(fromAccount);
        String txOrderId = UUID.randomUUID().toString();
        setBitcoinParameters(account);
        transactionsHelper.createBitcoinTxToAddressDest(txOrderId, account.getId(), account.getDomainId(), "2NEt6Bdx8VDYkXXdRCFhbBHgt8fVULecFsd");

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("bitcoin redirect transaction order from {string} to an external address is executed")
    public void bitcoinRedirectTransactionOrderToAnExternalAddressIsExecuted(String fromAccount) throws Exception {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account = accountsHelper.getAccountByPseudonym(fromAccount);
        prepareToBtcRedirectTx(account);
        String txOrderId = UUID.randomUUID().toString();

        IntentData previousTxOrder = memory().get("transaction_order");
        String targetDomain = previousTxOrder.getIntent().getTargetDomainId();
        String orderId = previousTxOrder.getIntent().getPayload().asMap().get("id").toString();

        List<String> quarantinedTransfers = transactionsHelper.fetchTransferIdsByTxOrder(orderId, targetDomain);
        transactionsHelper.createBitcoinRedirectTxToAddressDest(txOrderId, quarantinedTransfers, account.getId(), account.getDomainId(),"2NEt6Bdx8VDYkXXdRCFhbBHgt8fVULecFsd");

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @When("the ethereum transaction intent from {string} to endpoint with {string} address is executed")
    public void theEthereumTransactionIntentFromToEndpointWithAddressIsExecuted(String fromAccount, String toAccount) {
        EntityWrapper<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint> endpoint = endpointsHelper.getEndpointById(memory().get("endpointId").toString());
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account1 = accountsHelper.getAccountByPseudonym(fromAccount);
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account2 = accountsHelper.getAccountByPseudonym(toAccount);
        prepareToInternalEtherTx(account1, account2);

        String txOrderId = UUID.randomUUID().toString();
        transactionsHelper.makeEthereumTxToEndpointDest(account1.getId(), account1.getDomainId(), endpoint.getId(), null, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize().transactions(account1.getDomainId()).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));
    }

    @And("the bitcoin funds are sent back to account1")
    public void theFundsAreSentBackTo() throws Exception {
        theBitcoinTransactionIntentFromToIsExecuted("account2", "account1");
        transactionLedgerStatusIs("Detected");
        releaseQuarantinedTransfersToIntentIsExecuted("Bitcoin testnet", "account1");
    }

    @And("the xrp funds are sent back to account1")
    public void xrpFundsAreSentBackTo() throws Exception {
        theXRPTransactionIntentFromToAddressIsExecuted("account2", "account1");
        transactionLedgerStatusIs("Confirmed");
        releaseQuarantinedTransfersToIntentIsExecuted("XRPL testnet", "account1");
    }

    @And("token funds are sent back to account1")
    public void tokenFundsAreSentBackToAccount(DataTable dataTable) throws Exception {
        theTransactionIntentFromToIsExecutedUsingACallOfSmartContractFunction("account2", "account1", dataTable);
        transactionLedgerStatusIs("Detected");
        releaseQuarantinedTransfersToIntentIsExecuted("METACO TEST TOKEN", "account1");
    }

    @And("ethereum funds are sent back to account1")
    public void ethereumFundsAreSentBackToAccount() throws Exception {
        ethereumTransactionFromToIsExecuted("account2", "account1");
        transactionLedgerStatusIs("Detected");
        releaseQuarantinedTransfersToIntentIsExecuted("Ethereum testnet rinkeby", "account1");
    }

    @And("all recovery transfers are released to accounts")
    public void allRecoveryQuarantinedTransfersAreReleased() {
        List<Transactions.Transfer> transfers = transactionsHelper.getRecoveryTransfers(currentDomain());

        List<Accounts.Account> accounts = harmonize().accounts(currentDomain()).list().collect(Collectors.toList());
        releaseQuarantinedTransfers(transfers, accounts);
    }

    @And("all ongoing quarantined transfers are released to accounts")
    public void quarantinedTransfersAreReleasedToAccounts() {
        List<Transactions.Transfer> transfers = transactionsHelper.getAllQuarantinedTransfers(currentDomain());

        List<Accounts.Account> accounts = harmonize().accounts(currentDomain()).list().collect(Collectors.toList());
        releaseQuarantinedTransfers(transfers, accounts);
    }

    public void releaseQuarantinedTransfers(List<Transactions.Transfer> transfers, List<Accounts.Account> accounts){
        accounts.forEach(account -> {
            List<Transactions.Transfer> recipientAccountTransfers = new ArrayList<>();

            transfers.forEach(transfer -> {
                if (transfer.getRecipient().asMap().get("accountId").toString().equalsIgnoreCase(account.getId())) {
                    recipientAccountTransfers.add(transfer);
                }
            });

            if (!recipientAccountTransfers.isEmpty()) {
                transactionsHelper.releaseQuarantinedTransfers(account.getId(), account.getDomainId(), recipientAccountTransfers.stream().map(Transactions.Transfer::getId).collect(Collectors.toList()));
            }
        });
    }

    @And("corresponding transfer and fee are present")
    public void correspondingTransferAndFeeArePresent() throws Exception {
        IntentData transactionOrder = memory().get("transaction_order");
        String targetDomain = transactionOrder.getIntent().getTargetDomainId();
        String orderId = transactionOrder.getIntent().getPayload().asMap().get("id").toString();

        Transactions.Transaction tx = transactionsHelper.getTransactionByOrderId(targetDomain, orderId, Timeout.minutes(1));

        List<Transactions.Transfer> transfers = transactionsHelper.waitForTransfersPerTransaction(targetDomain, orderId, tx.getId(), Timeout.minutes(2));
        Assert.assertTrue(transfers.stream().
                anyMatch(transfer -> transfer.getKind().equalsIgnoreCase("Transfer")));

        Assert.assertTrue(transfers.stream().
                anyMatch(transfer -> transfer.getKind().equalsIgnoreCase("Fee")));
    }
}
