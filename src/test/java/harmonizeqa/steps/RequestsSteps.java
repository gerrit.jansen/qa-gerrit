package harmonizeqa.steps;

import com.metaco.harmonize.api.EventsActions;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.utils.TypeUtils;
import harmonizeqa.api.EntityWrapper;
import harmonizeqa.api.GlobalContext;
import harmonizeqa.api.UserCredentials;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static harmonizeqa.steps.DomainSteps.theCurrentDomainIsARootDomain;

public class RequestsSteps extends BaseSteps {
    private static Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    @And("request to read users list is made")
    public void requestToGetUsersListIsMade() {
        memory().set("get_request_exception", "");

        try{
            List<Users.User> users = harmonize().users(currentDomain()).list().collect(Collectors.toList());
            harmonize().users(currentDomain()).get(users.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @Then("requests are succeeded")
    public void requestsSucceeded() {
        String exception = memory().get("get_request_exception").toString();
        Assert.assertEquals(String.format("Exception should not be thrown, but was %s", exception), "", exception);
    }
    
    @Then("get requests fail with error {string}")
    public void requestsFailWithError(String error) {
        String exception = memory().get("get_request_exception").toString();
        Assert.assertTrue(String.format("Request error should contain %s, but was %s", error, exception), exception.contains(error));
    }

    @And("request to read policies list is made")
    public void requestsToGetPoliciesListIsMade() {
        memory().set("get_request_exception", "");

        try{
            List<Policies.Policy> policies = harmonize().policies(currentDomain()).list().collect(Collectors.toList());
            harmonize().policies(currentDomain()).get(policies.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @And("request to read domains list is made")
    public void requestsToGetDomainsListIsMade() {
        memory().set("get_request_exception", "");

        try{
            harmonize().domains().list().collect(Collectors.toList());
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @When("request to read details of an item from domains list is made")
    public void requestToReadDetailsOfAnItemFromDomainsIsMade() {
        memory().set("get_request_exception", "");

        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> currentUser =  UserSteps.getUserById(context().userCredentials.getId(), currentDomain());
        String currentUserRole = currentUser.getRoles().stream().findFirst().get();

        // switch user to admin to query domains
        theCurrentDomainIsARootDomain();
        UserSteps.userIsLoggedIn("default");
        List<Domains.Domain> domains = harmonize().domains().list().collect(Collectors.toList());

        // switch user to the test user to query domain details
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> wrapper = UserSteps.getUserByPseudonym(currentUserRole, currentDomain());
        UserCredentials userCredentials = TypeUtils.assertNotNull(wrapper.getUserCredentials(), "UserCredentials");
        loginUser(currentUserRole, userCredentials);

        try{
            harmonize().domains().get(domains.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @And("request to read endpoints list is made")
    public void requestsToGetEndpointsListIsMade() {
        memory().set("get_request_exception", "");

        try{
            List<Endpoints.Endpoint> endpoints = harmonize().endpoints(currentDomain()).list().collect(Collectors.toList());
            if(endpoints.size() > 0)
                harmonize().endpoints(currentDomain()).get(endpoints.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @And("request to read accounts list is made")
    public void requestsToGetAccountsListIsMade() {
        memory().set("get_request_exception", "");

        try{
            List<Accounts.Account> accounts = harmonize().accounts(currentDomain()).list().collect(Collectors.toList());
            if(accounts.size() > 0)
                harmonize().accounts(currentDomain()).get(accounts.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @And("request to read transactions list is made")
    public void requestsToGetTransactionsListIsMade() {
        memory().set("get_request_exception", "");

        try{
            List<Transactions.Transaction> transactions = harmonize().transactions(currentDomain()).list().collect(Collectors.toList());
            if(transactions.size() > 0)
                harmonize().transactions(currentDomain()).getTransaction(transactions.get(0).getId()).get();
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @And("request to read events list is made")
    public void requestsToGetEventsListIsMade() {
        memory().set("get_request_exception", "");

        try{
            EventsActions.ListCriteria listCriteria = EventsActions.ListCriteria.builder()
                            .limit(100).build();
            harmonize().events(currentDomain()).list(listCriteria);
        }catch (RuntimeException ex){
            memory().set("get_request_exception", ex);
        }
    }

    @Then("request fails with error {string}")
    public void requestFailsWithError(String error) throws Exception {
        String requestId = memory().get("propose_intent_request_id");

        harmonize().requests(currentDomain()).waitForStatus(requestId, Set.of("Failed"), Timeout.seconds(20));
        Request request = harmonize().requests(currentDomain()).get(requestId)
                .orElseThrow(() -> new Exception(String.format("Request %s was not found", requestId)));

        List<Request.RequestStateHistory> historyList = request.getHistory();

        Request.RequestStateHistory item =  historyList.stream().filter(v ->
                        v.getSource().equalsIgnoreCase("Notary") &&
                                v.getStatus().equalsIgnoreCase("Failed"))
                .findFirst().orElseThrow(() -> new Exception(String.format("Request %s was not failed in the notary source", request.getId())));

        String hint = item.getHint();
        Assert.assertTrue("Request failure error hint is not as expected", hint.toLowerCase().contains(error.toLowerCase()));
    }
}
