package harmonizeqa.steps;

import com.metaco.harmonize.api.Lock;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Vaults;
import io.cucumber.java.en.And;

import java.util.Optional;
import java.util.Set;

public class VaultsSteps extends BaseSteps{

    @And("the vault is configured")
    public void theVaultIsConfigured() throws Exception {
        String vaultId = checkActiveVaultWithPubKey();

        if(vaultId.length() == 0){
            IntentData.VaultIntentData intentData = harmonize().vaults(currentDomain()).create()
                    .id("00000000-0000-0000-0000-000000000000")
                    .pubKey(context().config.getVaultPublicKey()
                            .orElseThrow(() -> new Exception("Vault public key was not set in configuration")))
                    .alias("AT test vault")
                    .submit();

            intentData.waitForSucceed(Timeout.minutes(3))
                    .waitForIntentExecuted(Timeout.minutes(5))
                    .waitForVaultProcessingStatus(Vaults.VaultProcessingStatus.Completed, Timeout.minutes(5));
        }
        memory().set("vault_id", "00000000-0000-0000-0000-000000000000");
    }

    public String checkActiveVaultWithPubKey() throws Exception {
        String configuredPubKey =  context().config.getVaultPublicKey().orElseThrow(() -> new Exception("Vault public key was not set in configuration"));
        Optional<Vaults.Vault> vault = harmonize().vaults(currentDomain())
                .list()
                .filter(v -> v.getPublicKey().equalsIgnoreCase(configuredPubKey)
                        && v.getLock().equalsIgnoreCase(String.valueOf(Lock.Unlocked)))
                .findAny();

        waitForVaultCompletedStatus(vault, Timeout.minutes(5));
        return vault.map(Vaults.Vault::getId).orElse("");
    }

    public void waitForVaultCompletedStatus(Optional <Vaults.Vault> vault, Timeout timeout) throws Exception {
        long start = System.currentTimeMillis();

        if(vault.isPresent()){
            while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
                Vaults.VaultProcessingDetails processingDetails = vault.get().getAdditionalDetails()
                        .orElseThrow(() -> new Exception("Vault processing details is null"));
                if (processingDetails.hasStatus(Set.of(Vaults.VaultProcessingStatus.Completed.name())))
                    return;
            }
            throw new RuntimeException("Timed out while waiting for Completed status of vault");
        }
    }
}
