package harmonizeqa.steps;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.HarmonizeContext;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.utils.TypeUtils;
import com.metaco.harmonizeqa.tests.epics.MemoizedData;
import harmonizeqa.api.*;
import harmonizeqa.api.GeneratedKeys;
import harmonizeqa.api.GlobalContext;
import harmonizeqa.api.conf.Memory;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseSteps {

    public static final String vaultId = "00000000-0000-0000-0000-000000000000";

    private static final GlobalContext context = GlobalContext.create();
    private static final AtomicReference<String> ROOT_DOMAIN_ID = new AtomicReference<>();
    private static final AtomicReference<String> CURRENT_DOMAIN = new AtomicReference<>();
    private static final AtomicReference<Users.User> defaultUser = new AtomicReference<>();

    /**
     * Logged in user is how in features we say operate with this user.
     * Default is null and is the user configured in the harmonize.json
     */
    private static final AtomicReference<String> loggedInUser = new AtomicReference<>(null);

    protected AccountsHelper accountsHelper;
    protected TransactionsHelper transactionsHelper;
    protected EndpointsHelper endpointsHelper;
    protected DomainHelper domainHelper;

    public static GlobalContext getContext() {
        return context;
    }

    public BaseSteps() {
        this.accountsHelper = new AccountsHelper(context.harmonize(), context.memory());
        this.transactionsHelper = new TransactionsHelper(context.harmonize(), context.memory(), accountsHelper);
        this.endpointsHelper = new EndpointsHelper(context.harmonize(), context.memory());
        this.domainHelper = new DomainHelper(context.harmonize(), context.memory());

        print("Using url: " + harmonize().getContext().getConfig().getGatewayURL());
    }

    public GeneratedKeys requestKeys() {
        return ApiHelpers.requestKeys(getContext().harmonize().getContext());
    }

    public String generateId() {
        return UUID.randomUUID().toString();
    }

    /**
     * Will set the current domain and login the admin user for that domain.
     * Gotcha: the harmonize.json user for the SDK is assumed to have an admin role.
     */
    public static String setCurrentDomain(String pseudonym) {

        final Domains.Domain domain;
        if (pseudonym == null || pseudonym == "root domain") {
            domain = harmonize(null).domains().getRootDomain().orElseThrow();
        }
        // possibility to set the domain created outside the test execution as current, for test exploration
        else if (pseudonym.matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}")) {
            String id = pseudonym;
            domain = harmonize(null).domains().find(id).orElseThrow(() -> new RuntimeException("No domain found for id " + id));

        } else {
            String id = memory().getEntityDataByPseudonym("domain", pseudonym).get().getId();
            domain = harmonize(null).domains().find(id).orElseThrow(() -> new RuntimeException("No domain found for " + pseudonym));
        }

        String domainId = domain.getId();
        String oldDomain = CURRENT_DOMAIN.updateAndGet(v -> domainId);

        return oldDomain;
    }

    public void setCurrentDomainFromUserContext() {
        String id = context().userCredentials.getDomainId();
//        Domains.Domain domain = harmonize(null).domains().find(id).orElseThrow(() -> new RuntimeException("No domain found with id " + id));
        CURRENT_DOMAIN.updateAndGet(v -> id);
    }

    /**
     * Helper function that will save the new CreateUser + its User representation if available to memory
     * as an EntityWrapper
     */
    public void storeCreatedUser(String domainId, Users.User user,
                                 Users.CreateUser createUser,
                                 IntentData<Users.CreateUser> createIntentData,
                                 String roles,
                                 UserCredentials userCredentials, String pseudonym) {
        userCredentials = TypeUtils.assertNotNull(userCredentials, "UserCredentials");

        final String userId;
        if (user != null)
            userId = user.getId();
        else if (createUser != null)
            userId = createUser.getId();
        else
            throw new RuntimeException("Either the user or createUser arguments must be set, both cannot be null");

        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> wrapper = new EntityWrapper<>(domainId, userId, "user", userCredentials);
        if (createIntentData != null) {
            wrapper.setCreateEntity(createUser, createIntentData);
            wrapper.setEntity(harmonize().users(domainId).get(createUser.getId()).orElseThrow());
        }

        if (user != null)
            wrapper.setEntity(user);

        if (roles != null)
            wrapper.addRoles(roles);

        wrapper.setPseudonym(pseudonym);
        memory().setEntityData(wrapper);
    }

    public static String getCurrentUser() {
        return loggedInUser.get();
    }

    /**
     * Helper function that loads the SDK configured user into the user EntityWrapper's in memory.
     */
    public static void _storeRootDomainAdminUser() {
        String rootDomain = rootDomainId();
        UserCredentials userCredentials = context.context(null).userCredentials;
        Optional<Users.User> userOpt = context.context(null).harmonize.users(rootDomain).list()
                .filter(u -> u.getPubKey().equals(userCredentials.getPublicKey().asBase64Str()) && u.getRoles().contains("admin"))
                .findFirst();

        if (userOpt.isEmpty()) {
            String publicKey = context.context(null).userCredentials.getPublicKey().asBase64Str();

            context.context(null).harmonize.users(rootDomain).list().forEach(System.out::println);
            System.out.println("SDK user: publicKey: " + publicKey);
            context.context(null).harmonize.users(rootDomain).list().forEach(u ->
                    System.out.println("Matches domain publicKey ?  domain.user: " + u.getPubKey() + " == " + publicKey + " => " + u.getPubKey().equals(publicKey))
            );

            throw new RuntimeException("For QA testing the SDK user configured must be a user in the root domain and have an admin role");
        }

        Users.User user = userOpt.get();
        EntityWrapper<Users.User, Users.CreateUser, Users.UpdateUser> wrapper = new EntityWrapper<>(rootDomain, user.getId(), "user", userCredentials);
        wrapper.setPseudonym("default");
        wrapper.setEntity(user);
        wrapper.addRoles(user.getRoles());

        context.memory().setEntityData(wrapper);
        defaultUser.set(user);
        // String the wrapper for root domain
        EntityWrapper<Domains.Domain, Domains.CreateDomain, UpdateActionWrapper<Domains.UpdateDomain, Domains.UpdateDomainPermissions>> rootDomainWrapper = new EntityWrapper<>(rootDomain, rootDomain, "domain", userCredentials);
        rootDomainWrapper.setPseudonym("root domain");
        context.memory().setEntityData(rootDomainWrapper);
    }

    public static String currentDomain() {
        String currentDomain = CURRENT_DOMAIN.get();
        if (currentDomain == null) {
            return CURRENT_DOMAIN.updateAndGet(v -> rootDomainId());
        }
        return currentDomain;
    }

    public static Memory memory() {
        return context.memory();
    }

    public static LocalHarmonizeContext context() {
        return context(loggedInUser.get());
    }

    public static LocalHarmonizeContext context(String alias) {
        return context.context(alias);
    }

    public static Harmonize harmonize() {
        return context.harmonize(loggedInUser.get());
    }

    /**
     * Changes the HarmonizeContext to that with the user credentials identified by "alias"
     *
     * @param alias
     * @return
     */
    public static LocalHarmonizeContext loginUser(String alias, UserCredentials userCredentials) {
        loggedInUser.set(alias);
        LocalHarmonizeContext ctx = context.load(alias, TypeUtils.assertNotNull(userCredentials, "UserCredentials"));
        assert ctx.userCredentials != null;
        assert ctx.harmonize != null;
        assert ctx.config != null;

        return ctx;
    }

    public static LocalHarmonizeContext loginCoerciveRootDomainUser() {
        loggedInUser.set("coercive root domain admin");
        LocalHarmonizeContext ctx = context.loadWithCoerciveRootDomainData();
        assert ctx.userCredentials != null;
        assert ctx.harmonize != null;
        assert ctx.config != null;

        return ctx;
    }

    /**
     * Logs out the current user and resets the HarmonizeContext to the default user
     * loaded via the harmonize.json config.
     *
     * @return
     */
    public HarmonizeContext logoutUser() {
        loggedInUser.set(null);
        return context();
    }

    /**
     * Prefer the logged in method
     *
     * @param alias
     * @return
     */
    public static Harmonize harmonize(String alias) {
        return context.harmonize(alias);
    }


    public static String rootDomainId() {
        String rootDomainId = ROOT_DOMAIN_ID.get();

        if (rootDomainId != null)
            return rootDomainId;

        return ROOT_DOMAIN_ID.updateAndGet(id -> id == null ? context.harmonize().domains().getRootDomain().get().getId() : id);
    }


    public static void print(Stream<?> st) {
        System.out.println(
                st.map(Object::toString).collect(Collectors.joining("\n"))
        );
    }

    public static void print(Collection<?> st) {
        System.out.println(
                st.stream().map(Object::toString).collect(Collectors.joining("\n"))
        );
    }

    public static void print(Object... obj) {
        System.out.println(
                Arrays.stream(obj).map(v -> v == null ? "null" : v.toString() ).collect(Collectors.joining(" "))
        );
    }
}
