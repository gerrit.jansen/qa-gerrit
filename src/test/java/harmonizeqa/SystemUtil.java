package harmonizeqa;

import java.lang.reflect.Field;
import java.util.Map;

public class SystemUtil {
    public static void setEnv(String key, String val) {
        try {
            getModifiableEnv().put(key, val);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Map<String, String> getModifiableEnv() throws Exception {
        Map<String, String> unmodifiableEnv = System.getenv();
        Field field = unmodifiableEnv.getClass().getDeclaredField("m");
        field.setAccessible(true);
        return (Map<String, String>) field.get(unmodifiableEnv);
    }
}
