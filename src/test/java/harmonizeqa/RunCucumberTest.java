package harmonizeqa;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources",

        plugin = {
                "pretty",
                "json:target/cucumber/cucumber.json",
                "junit:target/cucumber/cucumber.xml"
        },

        tags = "not @ignore and @smoke"
)



public class RunCucumberTest {

    @BeforeClass
    public static void startExecution() throws Exception {


    }

    @AfterClass
    public static void stopExecution() throws Exception {

    }

}


