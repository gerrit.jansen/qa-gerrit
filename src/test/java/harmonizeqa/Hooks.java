package harmonizeqa;

import harmonizeqa.api.conf.Memory;
import io.cucumber.java.Before;
import io.cucumber.java.After;

import static harmonizeqa.steps.BaseSteps._storeRootDomainAdminUser;

public class Hooks {
    @Before
    public void beforeScenario(){
        _storeRootDomainAdminUser();
    }

    @After
    public void afterScenario(){
        Memory.clearMemory();
    }
}
