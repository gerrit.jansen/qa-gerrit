package harmonizeqa.api.conf;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JsonHelper {

    public static ArrayList<JsonObject> getJsonObjects(String path) {
        assert path != null;

        Path filePath = Paths.get(getNormalizedPath(path));
        String jsonString;
        try {
            jsonString = Files.readString(filePath, StandardCharsets.UTF_8);

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(String.format("Could not read file ", filePath), e);
        }

        Gson gson = new Gson();
        Type object = new TypeToken<ArrayList<JsonObject>>(){}.getType();
        return gson.fromJson(jsonString, object);
    }

    public static Map<String, Object> getJsonObjectAsMap(JsonObject object) {
        return new Gson().fromJson(object.toString(), HashMap.class);
    }

    public static String getNormalizedPath(String path) {
        Path filePath = Paths.get(path);
        return filePath.normalize().toString() + "/";
    }
}
