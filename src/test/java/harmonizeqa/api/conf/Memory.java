package harmonizeqa.api.conf;

import com.metaco.harmonize.api.om.Users;
import harmonizeqa.api.EntityWrapper;
import harmonizeqa.api.UpdateActionWrapper;
import harmonizeqa.steps.DomainSteps;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Hold operations for a scenario
 */
public class Memory {

    private static final Map<String, Object> mem = new ConcurrentHashMap<>();

    private final Map<String, Object> cryptographicKeys = new ConcurrentHashMap<>();

    private final Map<String, Object> workflowConditionsMap = new ConcurrentHashMap<>();

    public <T> T get(String k) {
        return (T) mem.get(k);
    }

    public <T> T getCryptographicKeys(String k) {
        return (T) cryptographicKeys.get(k);
    }

    public <T> T get(String k, T def) {
        return (T) mem.getOrDefault(k, def);
    }

    public <T> void set(String k, T v) {
        mem.put(k, v);
    }

    public <T> void setCryptographicKeys(String k, T v) {
        cryptographicKeys.put(k, v);
    }

    public <E, C, U> Stream<EntityWrapper<E, C, U>> getEntityDataByType(String type) {
        Set<EntityWrapper<E, C, U>> wrappers = (Set<EntityWrapper<E, C, U>>) mem.getOrDefault("entity_wrappers", new HashSet<>());
        return wrappers.stream().filter(w -> w.getType().equalsIgnoreCase(type));
    }

    public <E, C, U, P> Stream<EntityWrapper<E, C, UpdateActionWrapper<U,P>>> getEntityDataWithActionWrapperByType(String type) {
        Set<EntityWrapper<E, C, UpdateActionWrapper<U,P>>> wrappers = (Set<EntityWrapper<E, C, UpdateActionWrapper<U,P>>>) mem.getOrDefault("entity_wrappers", new HashSet<>());
        return wrappers.stream().filter(w -> w.getType().equalsIgnoreCase(type));
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataByRole(String type, String domainId, String roles) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.hasAnyRole(roles)).findFirst();
    }

    public <E, C, U> List<EntityWrapper<E, C, U>> getEntityDataByDomain(String type, String domainId) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId)).collect(Collectors.toList());
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataByAddress(String type, String domainId, String address) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getAddresses().contains(address)).findFirst();
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityLedgerId(String type, String domainId, String ledgerId) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getLedgerId().equals(ledgerId)).findFirst();
    }

    public <E, C, U> Stream<EntityWrapper<E, C, U>> getEntitiesLedgerId(String type, String domainId, String ledgerId) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getLedgerId().equals(ledgerId));
    }

    public <E, C, U> Stream<EntityWrapper<E, C, U>> getEntitiesByDomainId(String type, String domainId) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId));
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityByDomainId(String type, String domainId) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId)).findFirst();
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataByAlias(String type, String domainId, String id) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.hasAlias(id)).findFirst();
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataByPseudonym(String type, String domainId, String pseudonym) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getPseudonym().equalsIgnoreCase(pseudonym)).findFirst();
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataByPseudonym(String type, String pseudonym) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getPseudonym()!=null && w.getPseudonym().equalsIgnoreCase(pseudonym)).findFirst();
    }

    public <E, C, U, P> Optional<EntityWrapper<E, C, UpdateActionWrapper<U, P>>> getEntityDataWithUpdateWrapperById(String type, String domainId, String id) {
        Stream<EntityWrapper<E, C, UpdateActionWrapper<U,P>>> wrappers = getEntityDataWithActionWrapperByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getId().equalsIgnoreCase(id)).findFirst();
    }

    public <E, C, U> Optional<EntityWrapper<E, C, U>> getEntityDataById(String type, String domainId, String id) {
        Stream<EntityWrapper<E, C, U>> wrappers = getEntityDataByType(type);
        return wrappers.filter(w -> w.getDomainId().equalsIgnoreCase(domainId) && w.getId().equalsIgnoreCase(id)).findFirst();
    }

    public <E, C, U> void setEntityData(EntityWrapper<E, C, U> wrapper) {
        assert wrapper.getDomainId() != null && wrapper.getId() != null && wrapper.getType() != null;

        String key = "entity_wrappers";
        Set<EntityWrapper<E, C, U>> wrappers = (Set<EntityWrapper<E, C, U>>) mem.getOrDefault(key, new HashSet<>());
        wrappers.add(wrapper);
        set(key,wrappers);
    }

    public static void clearMemory() {
        mem.clear();
    }
}
