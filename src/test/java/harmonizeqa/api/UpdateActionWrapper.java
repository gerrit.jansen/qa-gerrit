package harmonizeqa.api;

import java.util.Objects;

public class UpdateActionWrapper <U, P>{
    private U update;
    private P updateDomainPermissions;

    public U getUpdate() {
        return update;
    }

    public void setUpdate(U update) {
        this.update = update;
    }

    public P getUpdateDomainPermissions() {
        return updateDomainPermissions;
    }

    public void setUpdateDomainPermissions(P updateDomainPermissions) {
        this.updateDomainPermissions = updateDomainPermissions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(update, updateDomainPermissions);
    }

    @Override
    public String toString() {
        return "UpdateActionWrapper{" +
                ", update=" + update +
                ", updateDomainPermissions=" + updateDomainPermissions +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateActionWrapper that = (UpdateActionWrapper) o;
        return Objects.equals(update, that.update) && updateDomainPermissions.equals(that.updateDomainPermissions);
    }
}
