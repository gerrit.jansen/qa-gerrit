package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.HarmonizeContext;
import com.metaco.harmonize.conf.Config;
import com.metaco.harmonize.msg.SchemaValidations;
import com.metaco.harmonize.utils.CollectionUtils;
import com.metaco.harmonize.utils.GsonUtils;
import com.metaco.harmonize.utils.TypeUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static com.metaco.harmonize.utils.TypeUtils.assertNotNull;

/**
 *
 */
public class LocalHarmonizeContext extends HarmonizeContext {

    public final UserCredentials userCredentials;
    public final DefaultSigner signer;

    public LocalHarmonizeContext(Config config, Harmonize harmonize, UserCredentials userCredentials, DefaultSigner signer) {
        super(config, harmonize);
        this.userCredentials = assertNotNull(userCredentials, "userCredentials");
        this.signer = signer;
    }

    public LocalHarmonizeContext switchUser(UserCredentials userCredentials) {
        DefaultSigner signer2 = signer.switchUser(userCredentials);

        return new LocalHarmonizeContext(config,
                harmonize.switchAuthor(signer2), userCredentials, signer2);
    }


    /**
     * Load the configurations from <pre>"~/.harmonize.json", "./harmonize.json", userConfigFile, environmentVariables</pre>.<br/>
     * The configuration is merged together from left to right.<br/>
     * If a file is missing it is ignored, any syntax errors will cause a RuntimeException.
     *
     * @param userConfFiles if null then it is ignored
     */
    public static Map<String, Object> loadConfig(String... userConfFiles) {

        List<String> confFiles = new ArrayList<>();

        confFiles.add(System.getProperty("user.home") + "/.harmonize.json");
        confFiles.add(new File("").getAbsolutePath() + "/harmonize.json");

        String userOverrideConfig = System.getProperty("HARMONIZE_CONF");
        System.out.println("Local HARMONIZE_CONF: " + userOverrideConfig);

        if (userOverrideConfig == null || userOverrideConfig.trim().length() == 0)
            userOverrideConfig = System.getenv("HARMONIZE_CONF");

        if (userOverrideConfig != null) {
            confFiles.add(new File(userOverrideConfig).getAbsolutePath());
        }

        if (userConfFiles != null) {
            confFiles.addAll(Arrays.asList(userConfFiles));
        }


        // iterate over the possible config files
        Map<String, Object> configMap = readConfigFiles(confFiles.toArray(new String[0]));
        // read and merge in the environment variables
        Map<String, Object> envVars = readEnvVars();
        TypeUtils.merge(configMap, envVars);

        System.setProperty("accounts.recovery.config", configMap.getOrDefault("ACCOUNT_RECOVERY_JSON", "../qa-v4/src/test/resources/documents/qa-stable-accounts-recovery-data.json").toString());

        return configMap;
    }

    public static LocalHarmonizeContext loadHMZWithNonCoercivePrimaryDomainData(Map<String, Object> configMap){
        UserCredentials userCredentials = getRootDomainUserCredentials(configMap);

        Config config = Config.createConfig(configMap);

        DefaultSigner signer = new DefaultSigner(userCredentials, configMap.get("SIGNER_URL").toString());

        return new LocalHarmonizeContext(config,
                Harmonize.open(config, signer),
                userCredentials,
                signer);
    }

    public static LocalHarmonizeContext loadHMZCoercivePrimaryDomainData(Map<String, Object> configMap){
        UserCredentials userCredentials = getCoerciveRootDomainUserCredentials(configMap);

        Config config = Config.createConfig(configMap);

        DefaultSigner signer = new DefaultSigner(userCredentials, configMap.get("SIGNER_URL").toString());

        return new LocalHarmonizeContext(config,
                Harmonize.open(config, signer),
                userCredentials,
                signer);
    }

    public static UserCredentials getCoerciveRootDomainUserCredentials(Map<String, Object> configMap){
        SchemaValidations.validateSchema(configMap, Set.of("COERCIVE_USER_ID", "COERCIVE_USER_PUBLIC_KEY", "COERCIVE_USER_PRIVATE_KEY", "COERCIVE_USER_DOMAIN_ID"));

        UserCredentials userCredentials = UserCredentials.from(
                configMap.get("COERCIVE_USER_DOMAIN_ID").toString(),
                configMap.get("COERCIVE_USER_ID").toString(),
                configMap.get("COERCIVE_USER_PUBLIC_KEY").toString(),
                configMap.get("COERCIVE_USER_PRIVATE_KEY").toString());

        return userCredentials;
    }


    public static UserCredentials getRootDomainUserCredentials(Map<String, Object> configMap){
        SchemaValidations.validateSchema(configMap, Set.of("USER_ID", "USER_PUBLIC_KEY", "USER_PRIVATE_KEY", "USER_DOMAIN_ID"));

        UserCredentials userCredentials = UserCredentials.from(
                configMap.get("USER_DOMAIN_ID").toString(),
                configMap.get("USER_ID").toString(),
                configMap.get("USER_PUBLIC_KEY").toString(),
                configMap.get("USER_PRIVATE_KEY").toString());

        return userCredentials;
    }

    private static Map<String, Object> readEnvVars() {
        return CollectionUtils.mapEnvVars(
                Config.GATEWAY_URL, null,
                Config.KEY_REQUEST_URL, null,
                Config.OAUTH_URL, null,
                Config.OAUTH_CLIENT_ID, null,
                Config.OAUTH_CLIENT_SECRET, null,
                Config.HTTP_VERSION, "1"
        );
    }

    private static Map<String, Object> readConfigFiles(String[] confFiles) {
        Map<String, Object> configMap = new HashMap<>();

        for (String file : confFiles) {
            if (file != null && Files.exists(Path.of(file))) {
                try {
                    Map<String, Object> fileConfig = TypeUtils.mapKeysUpper(GsonUtils.getGson().fromJson(
                            Files.readString(Path.of(file)),
                            Map.class
                    ));
                    TypeUtils.merge(configMap, fileConfig);

                } catch (Exception e) {
                    throw new RuntimeException("Error reading config file: " + file + "; " + e, e);
                }
            }
        }
        return configMap;
    }
}
