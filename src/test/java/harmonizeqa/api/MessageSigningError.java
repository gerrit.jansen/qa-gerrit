package harmonizeqa.api;

class MessageSigningError extends RuntimeException {
    public MessageSigningError(String message) {
        super(message);
    }
}
