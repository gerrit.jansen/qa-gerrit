package harmonizeqa.api;

import com.metaco.harmonize.api.om.Author;
import com.metaco.harmonize.conf.Context;
import com.metaco.harmonize.msg.JSON;
import com.metaco.harmonize.net.Requests;
import com.metaco.harmonize.sig.Signature;
import com.metaco.harmonize.sig.Signer;
import com.metaco.harmonize.utils.URLUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class DefaultSigner implements Signer {
    protected final URL signerUrl;

    protected final UserCredentials userCredentials;

    public DefaultSigner(UserCredentials userCredentials, String signerUrl) {
        this.userCredentials = userCredentials;

        try {
            this.signerUrl = new URL(signerUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public Signature signPayload(Context context, JSON msg) {
        return doPayloadSign(context, msg);
    }

    protected Signature doPayloadSign(Context context, JSON msg) {
        JSON payload = JSON.of(Map.of(
                "payload", msg,
                "privateKey", this.userCredentials.getPrivateKey().asStr()
        ));

        Requests.Response resp = context.getRequests()
                .post(context.getConfig(),
                        URLUtils.join(signerUrl, "/key"),
                        payload.asJson(),
                        "Content-Type", "application/json"
                );

        if (!resp.isOk())
            throw new MessageSigningError("Sign service returned " + resp.status + ", " + resp.body);

        return Signature.of(resp.json());
    }

    @Override
    public Signature signChallenge(Context context, String challenge) {

        JSON payload = JSON.of(Map.of(
                "payload", challenge,
                "privateKey", userCredentials.getPrivateKey().asStr()
        ));

        Requests.Response resp = context.getRequests()
                .post(context.getConfig(),
                        URLUtils.join(signerUrl, "/string"),
                        payload.asJson(),
                        "Content-Type", "application/json"
                );

        if (!resp.isOk())
            throw new MessageSigningError("Sign service returned " + resp.status + ", " + resp.body);

        return Signature.of(resp.json());
    }

    @Override
    public String getPublicKey() {
        return userCredentials.getPublicKey().asBase64Str();
    }

    @Override
    public Author getAuthor() {
        return userCredentials.asAuthor();
    }

    public DefaultSigner switchUser(UserCredentials userCredentials) {
        return new DefaultSigner(userCredentials, signerUrl.toString());
    }
}
