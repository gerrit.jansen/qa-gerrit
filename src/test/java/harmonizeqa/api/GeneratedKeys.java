package harmonizeqa.api;


import com.metaco.harmonize.msg.JSON;

import java.util.Map;

public class GeneratedKeys extends JSON.MapJSON {

    public GeneratedKeys(Key.PrivateKey privateKey, Key.PublicKey publicKey) {
        super(Map.of(
                "privateKey", privateKey, "publicKey", publicKey
        ));
    }

    public Key.PrivateKey getPrivateKey() {
        return get(Key.PrivateKey.class, "privateKey");
    }

    public Key.PublicKey getPublicKey() {
        return get(Key.PublicKey.class, "publicKey");
    }
}
