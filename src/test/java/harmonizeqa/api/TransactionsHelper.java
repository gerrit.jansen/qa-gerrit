package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.Expire;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.TransactionActions;
import com.metaco.harmonize.api.criteria.SortOrder;
import com.metaco.harmonize.api.om.*;
import com.metaco.harmonize.api.om.ledgers.btc.BitcoinRedirectTransactionOrderParameters;
import com.metaco.harmonize.api.om.ledgers.btc.BitcoinTransactionOrderParameters;
import com.metaco.harmonize.api.om.ledgers.eth.EthereumTransactionOrderParameters;
import com.metaco.harmonize.api.om.ledgers.xrpl.XrplMemo;
import com.metaco.harmonize.api.om.ledgers.xrpl.XrplTransactionOrderParameters;
import harmonizeqa.TransactionsEnums;
import harmonizeqa.api.conf.Memory;
import org.junit.Assert;

import java.math.BigInteger;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TransactionsHelper {

    private final Harmonize harmonize;
    private final Memory memory;
    private final AccountsHelper accountsHelper;
    private static final Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    public TransactionsHelper(Harmonize harmonize, Memory memory, AccountsHelper accountsHelper) {
        this.harmonize = harmonize;
        this.memory = memory;
        this.accountsHelper = accountsHelper;
    }

    public List<Transactions.Transfer> waitForTransfersPerTransaction(String domainId, String txOrderId, String transactionId, Timeout timeout) throws Exception {

        long start = System.currentTimeMillis();

        List<Transactions.Transfer> transfersList = new ArrayList<>();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
            transfersList = harmonize.transactions(domainId).listTransfers()
                    .filter(tr -> !tr.getKind().equalsIgnoreCase("Recovery") && tr.getTransactionId().equalsIgnoreCase(transactionId)).collect(Collectors.toList());

            if (!transfersList.isEmpty()) {
                break;
            }
        }
        if (transfersList.isEmpty()) {
            Transactions.Transaction transaction = getTransactionByOrderId(domainId, txOrderId, Timeout.minutes(1));
            throw new RuntimeException("Timed out while waiting for transfers associated with transaction " + transactionId + ". Transaction processing hint: " +
                    transaction.getProcessingHint());
        }
        return transfersList;
    }

    public List<Transactions.Transfer> getAllQuarantinedTransfers(String domainId) {

        return harmonize.transactions(domainId).listTransfers()
                .filter(tr -> tr.getKind().equalsIgnoreCase("Transfer") &&
                        tr.isQuarantined()).collect(Collectors.toList());
    }

    public List<Transactions.Transfer> getRecoveryTransfers(String domainId) {

        return harmonize.transactions(domainId).listTransfers()
                .filter(tr -> tr.getKind().equalsIgnoreCase("Recovery") &&
                        tr.isQuarantined()).collect(Collectors.toList());
    }

    public Transactions.Transaction getTransactionByOrderId(String domainId, String txOrderId, Timeout timeout) throws Exception {
        TransactionActions.ListCriteria listCriteria = TransactionActions.ListCriteria.builder()
                .transactionOrderId(txOrderId)
                .sortBy("registeredAt")
                .sortOrder(SortOrder.DESC)
                .limit(100)
                .build();

        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {

            if (harmonize.transactions(domainId).list(listCriteria).count() != 0) {
                break;
            }
        }
        return harmonize.transactions(domainId).list(listCriteria)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Timed out while waiting for transactions associated with tx order " + txOrderId));
    }

    public void createBitcoinTxToAddressDest(String txOrderId, String fromAccountId, String accountDomain, String toAccountAddress) {
        TransactionDestination transactionDestination = TransactionDestination.AddressDestination
                .builder()
                .address(toAccountAddress).build();

        makeBitcoinTx(transactionDestination, fromAccountId, accountDomain, txOrderId);
    }

    public void createDryRunBitcoinTxToAddressDest(String txOrderId, String fromAccountId, String accountDomain, String toAccountAddress) {
        TransactionDestination transactionDestination = TransactionDestination.AddressDestination
                .builder()
                .address(toAccountAddress).build();

        makeDryRunBitcoinTx(transactionDestination, fromAccountId, accountDomain, txOrderId);
    }

    public void createBitcoinTxToAccountDest(String txOrderId, String fromAccountId, String accountDomain, String toAccountId) {
        TransactionDestination transactionDestination = TransactionDestination.AccountDestination
                .builder()
                .accountId(toAccountId).build();

        makeBitcoinTx(transactionDestination, fromAccountId, accountDomain, txOrderId);
    }

    public void createBitcoinTxToEndpointDest(String txOrderId, String fromAccountId, String accountDomain, String toEndpointId) {
        TransactionDestination transactionDestination = TransactionDestination.EndpointDestination
                .builder()
                .endpointId(toEndpointId).build();

        makeBitcoinTx(transactionDestination, fromAccountId, accountDomain, txOrderId);
    }

    public void makeBitcoinTx(TransactionDestination transactionDestination, String fromAccount, String accountDomain, String txOrderId) {
        Transactions.CreateTransactionOrder createTransactionOrder = Transactions.CreateTransactionOrder.builder()
                .id(txOrderId)
                .fromAccountId(fromAccount)

                .parameters(BitcoinTransactionOrderParameters.builder()
                        .toDestination(transactionDestination)
                        .maximumFee(memory.get("max_fee"))
                        .priorityHigh()
                        .amount(memory.get("amount"))
                        .build()
                )
                .expire(Expire.hours(1))
                .build();

        IntentData intentData = harmonize.transactions(accountDomain).create(createTransactionOrder, Expire.hours(24))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));
        memory.set("transaction_order", intentData);
    }

    public void makeDryRunBitcoinTx(TransactionDestination transactionDestination, String fromAccount, String domainId, String txOrderId) {

        Transactions.TransactionDryRun<Transactions.TransactionDryRunResult, Transactions.TransactionEstimate> result = harmonize.transactions(domainId).dryRun()
                .parameters(BitcoinTransactionOrderParameters.builder()
                        .toDestination(transactionDestination)
                        .maximumFee(memory.get("max_fee"))
                        .priorityHigh()
                        .amount(memory.get("amount"))
                        .build())
                .expire(Expire.hours(1))
                .submit();


        memory.set("transaction_dry_run", result);
    }

    public void createBitcoinRedirectTxToAddressDest(String txOrderId, List<String> transfers, String fromAccountId, String accountDomain, String toAccountAddress) {
        TransactionDestination transactionDestination = TransactionDestination.AddressDestination
                .builder()
                .address(toAccountAddress).build();

        makeBitcoinRedirectTx(transactionDestination, fromAccountId, accountDomain, transfers, txOrderId);
    }

    public void makeBitcoinRedirectTx(TransactionDestination transactionDestination, String fromAccount, String accountDomain, List<String> transfers, String txOrderId) {
        Transactions.CreateTransactionOrder createTransactionOrder = Transactions.CreateTransactionOrder.builder()
                .id(txOrderId)
                .fromAccountId(fromAccount)

                .parameters(BitcoinRedirectTransactionOrderParameters.builder()
                        .toDestination(transactionDestination)
                        .redirectedTransfers(transfers)
                        .maximumFee(memory.get("max_fee"))
                        .priorityHigh()
                        .amount(memory.get("amount"))
                        .build()
                )
                .expire(Expire.hours(1))
                .build();

        IntentData intentData = harmonize.transactions(accountDomain).create(createTransactionOrder, Expire.hours(24))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));

        memory.set("transaction_order", intentData);
    }

    public void makeEthereumTxToAddressDest(String fromAccount, String accountDomain, String toAddress, String data, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.AddressDestination
                .builder()
                .address(toAddress).build();

        makeEthereumTx(transactionDestination, fromAccount, accountDomain, data, txOrderId);
    }

    public void makeEthereumTxToAccountDest(String fromAccount, String accountDomain, String toAccount, String data, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.AccountDestination
                .builder()
                .accountId(toAccount).build();

        makeEthereumTx(transactionDestination, fromAccount, accountDomain, data, txOrderId);
    }

    public void makeEthereumDryRunTxToAccountDest(String fromAccount, String accountDomain, String toAccount, String data, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.AccountDestination
                .builder()
                .accountId(toAccount).build();

        makeEthereumDryRunTx(transactionDestination, fromAccount, accountDomain, data, txOrderId);
    }

    public void makeEthereumTxToEndpointDest(String fromAccount, String accountDomain, String endpointId, String data, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.EndpointDestination
                .builder()
                .endpointId(endpointId).build();

        makeEthereumTx(transactionDestination, fromAccount, accountDomain, data, txOrderId);
    }

    public void makeEthereumTx(TransactionDestination transactionDestination, String fromAccount, String accountDomain, String data, String txOrderId) {
        Transactions.CreateTransactionOrder createTransactionOrder = Transactions.CreateTransactionOrder.builder()
                .id(txOrderId)
                .fromAccountId(fromAccount)

                .parameters(EthereumTransactionOrderParameters.builder()
                        .toDestination(transactionDestination)
                        .maximumFee(memory.get("max_fee"))
                        .priorityLow()
                        .amount(memory.get("amount"))
                        .data(data)
                        .build()
                )
                .expire(Expire.hours(1))
                .build();

       IntentData intentData = harmonize.transactions(accountDomain).create(createTransactionOrder, Expire.hours(24))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));

        memory.set("transaction_order", intentData);
    }

    public void makeEthereumDryRunTx(TransactionDestination transactionDestination, String fromAccount, String accountDomain, String data, String txOrderId) {

        Transactions.TransactionDryRun<Transactions.TransactionDryRunResult, Transactions.TransactionEstimate> result = harmonize.transactions(accountDomain)
                .dryRun()
                .accountId(fromAccount)
                .parameters(EthereumTransactionOrderParameters.builder()
                        .toDestination(transactionDestination)
                        .maximumFee(memory.get("max_fee"))
                        .priorityLow()
                        .amount(memory.get("amount"))
                        .data(data)
                        .build())
                .submit();

        memory.set("transaction_dry_run", result);
        System.out.println(result);
    }

    public void makeXRPLTxToAccountDest(String fromAccount, String accountDomain, String toAccountId, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.AccountDestination
                .builder()
                .accountId(toAccountId).build();

        makeXRPLTx(transactionDestination, fromAccount, accountDomain, txOrderId);
    }

    public void makeXRPLTxToAddressDest(String fromAccount, String accountDomain, String toAddress, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.AddressDestination
                .builder()
                .address(toAddress).build();

        makeXRPLTx(transactionDestination, fromAccount, accountDomain, txOrderId);
    }

    public void makeXRPLTxToEndpointDest(String fromAccount, String accountDomain, String endpointId, String txOrderId) {
        TransactionDestination transactionDestination = TransactionDestination.EndpointDestination
                .builder()
                .endpointId(endpointId).build();

        makeXRPLTx(transactionDestination, fromAccount, accountDomain, txOrderId);
    }

    public void makeXRPLTx(TransactionDestination transactionDestination, String fromAccount, String accountDomain, String txOrderId) {
        XrplMemo.Builder memo = new XrplMemo.Builder();
        memo.memoData("74657374")
                .memoFormat("75726c")
                .memoType("68747470733a2f2f636f72652d71612d737461626c652d79796a3476652e6d33743463302e636c6f75642f76312f696e74656e7473");

        Transactions.CreateTransactionOrder createTransactionOrder = Transactions.CreateTransactionOrder.builder()
                .id(txOrderId)
                .fromAccountId(fromAccount)

                .parameters(XrplTransactionOrderParameters.builder()
                        .toEndpoint(transactionDestination)
                        .memos(Collections.singletonList(memo.build()))
                        .maximumFee(memory.get("max_fee"))
                        .sourceTag(234589L)
                        .priorityHigh()
                        .amount(memory.get("amount"))
                        .build()
                )
                .expire(Expire.hours(1))
                .build();

        IntentData intentData = harmonize.transactions(accountDomain).create(createTransactionOrder, Expire.hours(24))
                .waitForSucceed(Timeout.minutes(3))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(3));

        memory.set("transaction_order", intentData);
    }

    public List<String> fetchTransferIdsByTxOrder(String txOrderId, String domainId) throws Exception {
        return fetchTransferByTxOrder(txOrderId, domainId).stream().map(Transactions.Transfer::getId).collect(Collectors.toList());
    }

    public List<Transactions.Transfer> fetchTransferByTxOrder(String txOrderId, String domainId) throws Exception {
        Transactions.Transaction tx = getTransactionByOrderId(domainId, txOrderId, Timeout.minutes(1));

        return new ArrayList<>(waitForTransfersPerTransaction(domainId, txOrderId, tx.getId(), Timeout.minutes(2)))
                .stream().filter(tr -> tr.getKind().equalsIgnoreCase("Transfer"))
                .collect(Collectors.toList());
    }

    /**
     * Method calculates the approximate total amount of Reserved on the balance of the account.
     * Reserved amount consists of values of transfers per sender, for which the transaction ledger status is not confirmed, and fees.
     * The transactions are retrieved through transfers due to transfer containing information about the sender
     */
    public BigInteger getTotalReservedAmountPerSenderAccount(String accountId, String domainId) throws Exception {
        BigInteger reserved = BigInteger.ZERO;

        List<Transactions.Transfer> transfers = getTransfersBySenderAccount(accountId, domainId);
        List<Transactions.Transaction> transactions = getTransactionsByTransfers(transfers, domainId);

        for (Transactions.Transaction transaction : transactions) {
            if (!transaction.getLedgerStatus().equalsIgnoreCase("Confirmed")) {

                List<Transactions.Transfer> reservedTransfers = transfers.stream().filter(transfer -> transfer.getTransactionId().equalsIgnoreCase(transaction.getId())).collect(Collectors.toList());

                for (Transactions.Transfer transfer : reservedTransfers) {
                    reserved = reserved.add(new BigInteger(transfer.getValue()));
                }
            }
        }
        return reserved;
    }

    public List<Transactions.Transaction> getTransactionsByTransfers(List<Transactions.Transfer> transfers, String domain) throws Exception {
        List<Transactions.Transaction> transactions = new ArrayList<>();

        for (Transactions.Transfer transfer : transfers) {
            transactions.add(
                    harmonize.transactions(domain)
                            .getTransaction(transfer.getTransactionId())
                            .orElseThrow(() -> new Exception(String.format("Transaction for transfer %s not found", transfer.getId()))));
        }
        transactions = new ArrayList<>(new HashSet<>(transactions));
        return transactions;
    }

    public List<Transactions.Transfer> getTransfersBySenderAccount(String accountId, String domainId) {
        TransactionActions.ListCriteria listCriteria = TransactionActions.ListCriteria.builder()
                .accountId(accountId)
                .sortBy("registeredAt")
                .sortOrder(SortOrder.DESC)
                .build();

        List<Transactions.Transfer> transfers = harmonize.transactions(domainId).listTransfers(listCriteria).collect(Collectors.toList());

        transfers = transfers
                .stream()
                .filter(transfer -> !transfer.getKind().equalsIgnoreCase("Recovery")&&
                        transfer.getSender().getType().equals("Account") &&
                                transfer.getSender().asMap().get("accountId").toString().equalsIgnoreCase(accountId))
                .collect(Collectors.toList());

        // Temporary solution. Since currently transfer metadata contains "type": "Ethereum" for both ETH and ERC20 the value sent by
        // the test is the only way to differentiate between ETH and ERC20 transfers

        if (memory.get("tx_kind").equals(TransactionsEnums.TxKinds.ERC20)) {
            // the fee is paid in ETH
            transfers = transfers.stream().filter(transfer ->
                    transfer.getValue().equalsIgnoreCase("10000000000000000000")).collect(Collectors.toList());
        }
        if (memory.get("tx_kind").equals(TransactionsEnums.TxKinds.ETHER)) {
            transfers = transfers.stream().filter(transfer ->
                    transfer.getKind().equalsIgnoreCase("Fee") || transfer.getValue().equalsIgnoreCase("100000000000000000")
            ).collect(Collectors.toList());
        }
        return transfers;
    }

    public void verifyExpectedBalanceValue(String accountId, String accountDomain, List<Accounts.Balance> balancesBeforeTx, TransactionsEnums.Tickers ticker, BigInteger amount, TransactionsEnums.TxDirections direction) throws Exception {
        BigInteger total;
        BigInteger quarantined;

        BigInteger oldQuarantined;
        BigInteger oldTotal;

        Optional<Accounts.Balance> balanceBeforeTx = getBalanceBeforeTx(balancesBeforeTx, ticker.getName());
        Accounts.Balance newBalance = getNewBalance(ticker.getName(), accountId, accountDomain);

        if (balanceBeforeTx.isEmpty()) {
            oldQuarantined = new BigInteger("0");
            oldTotal = new BigInteger("0");
        } else {
            oldQuarantined = new BigInteger(balanceBeforeTx.get().getQuarantinedAmount());
            oldTotal = new BigInteger(balanceBeforeTx.get().getTotalAmount());
        }

        if (direction == TransactionsEnums.TxDirections.OUTGOING) {
            total = oldTotal.subtract(amount);
            quarantined = oldQuarantined;

            LOGGER.info(String.format("Verifying quarantined amount of %s on account %s to be the same as before tx after outgoing tx. Expected quarantined %s", ticker.getName(), accountId, quarantined));
            Assert.assertEquals(String.format("Expected quarantined amount of %s on account %s to be the same as before tx after outgoing tx. Expected quarantined %s, but was %s", ticker.getName(), accountId, quarantined, newBalance.getQuarantinedAmount()),
                    quarantined.toString(), newBalance.getQuarantinedAmount());
            verifyReservedAmount(ticker, accountId, accountDomain, Timeout.seconds(10));
            waitForTotalToBeAfterOutgoingTx(ticker, accountId, accountDomain, total, Timeout.seconds(2));
        } else {
            String quarantineVerificationMessage = "";
            String quarantineAssertionMessage = "";

            if(memory.get("transfer_released")){
                quarantined = oldQuarantined;
                quarantineVerificationMessage = String.format("Verifying quarantined amount of %s on account %s on incoming tx to be (quarantined before tx). Expected quarantined %s", ticker.getName(), accountId, quarantined);
                quarantineAssertionMessage = String.format("Expected quarantined amount of %s on account %s on incoming tx to be (quarantined before tx). Expected quarantined %s, but was %s", ticker.getName(), accountId, quarantined, newBalance.getQuarantinedAmount());
            }else {
                quarantined = oldQuarantined.add(amount);
                quarantineVerificationMessage = String.format("Verifying quarantined amount of %s on account %s on incoming tx to be (quarantined before tx + new quarantined). Expected quarantined %s", ticker.getName(), accountId, quarantined);
                quarantineAssertionMessage = String.format("Expected quarantined amount of %s on account %s on incoming tx to be (quarantined before tx + new quarantined). Expected quarantined %s, but was %s", ticker.getName(), accountId, quarantined, newBalance.getQuarantinedAmount());
            }

            total = oldTotal.add(amount);

            verifyReservedAmount(ticker, accountId, accountDomain, Timeout.seconds(10));
            LOGGER.info(quarantineVerificationMessage);
            Assert.assertEquals(quarantineAssertionMessage, quarantined.toString(), newBalance.getQuarantinedAmount());

            LOGGER.info(String.format("Verifying total amount of %s on account %s on incoming tx to be (total before tx + sent amount). Expected Total %s", ticker.getName(), accountId, total));
            Assert.assertEquals(String.format("Expected total amount of %s on account %s on incoming tx to be (total before tx + sent amount). Expected Total %s, but was %s", ticker.getName(), accountId, total, newBalance.getTotalAmount()),
                    total.toString(), newBalance.getTotalAmount());
        }
    }

    public void verifyExpectedBalanceValue1(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account, List<Accounts.Balance> balancesBeforeTx, TransactionsEnums.Tickers ticker, BigInteger amount, TransactionsEnums.TxDirections direction) throws Exception {
        BigInteger total;
        BigInteger quarantined;

        BigInteger oldQuarantined;
        BigInteger oldTotal;

        Optional<Accounts.Balance> balanceBeforeTx = getBalanceBeforeTx(balancesBeforeTx, ticker.getName());
        Accounts.Balance newBalance = getNewBalance(ticker.getName(), account.getId(), account.getDomainId());

        if (balanceBeforeTx.isEmpty()) {
            oldQuarantined = new BigInteger("0");
            oldTotal = new BigInteger("0");
        } else {
            oldQuarantined = new BigInteger(balanceBeforeTx.get().getQuarantinedAmount());
            oldTotal = new BigInteger(balanceBeforeTx.get().getTotalAmount());
        }

        if (direction == TransactionsEnums.TxDirections.OUTGOING) {
            total = oldTotal.subtract(amount);
            quarantined = oldQuarantined;

            LOGGER.info(String.format("Verifying quarantined amount of %s on account %s to be the same as before tx after outgoing tx. Expected quarantined %s", ticker.getName(), account.getId(), quarantined));
            Assert.assertEquals(String.format("Expected quarantined amount of %s on account %s to be the same as before tx after outgoing tx. Expected quarantined %s, but was %s", ticker.getName(), account.getId(), quarantined, newBalance.getQuarantinedAmount()),
                    quarantined.toString(), newBalance.getQuarantinedAmount());
            verifyReservedAmount(ticker, account.getId(), account.getDomainId(), Timeout.seconds(10));
            waitForTotalToBeAfterOutgoingTx(ticker, account.getId(), account.getDomainId(), total, Timeout.seconds(2));
        } else {
            String quarantineVerificationMessage = "";
            String quarantineAssertionMessage = "";

            if(memory.get("transfer_released")){
                quarantined = oldQuarantined;
                quarantineVerificationMessage = String.format("Verifying quarantined amount of %s on account %s on incoming tx to be (quarantined before tx). Expected quarantined %s", ticker.getName(), account.getId(), quarantined);
                quarantineAssertionMessage = String.format("Expected quarantined amount of %s on account %s on incoming tx to be (quarantined before tx). Expected quarantined %s, but was %s", ticker.getName(), account.getId(), quarantined, newBalance.getQuarantinedAmount());
            }else {
                quarantined = oldQuarantined.add(amount);
                quarantineVerificationMessage = String.format("Verifying quarantined amount of %s on account %s on incoming tx to be (quarantined before tx + new quarantined). Expected quarantined %s", ticker.getName(), account.getId(), quarantined);
                quarantineAssertionMessage = String.format("Expected quarantined amount of %s on account %s on incoming tx to be (quarantined before tx + new quarantined). Expected quarantined %s, but was %s", ticker.getName(), account.getId(), quarantined, newBalance.getQuarantinedAmount());
            }

            total = oldTotal.add(amount);

            verifyReservedAmount(ticker, account.getId(), account.getDomainId(), Timeout.seconds(10));
            LOGGER.info(quarantineVerificationMessage);
            Assert.assertEquals(quarantineAssertionMessage, quarantined.toString(), newBalance.getQuarantinedAmount());

            LOGGER.info(String.format("Verifying total amount of %s on account %s on incoming tx to be (total before tx + sent amount). Expected Total %s", ticker.getName(), account.getId(), total));
            Assert.assertEquals(String.format("Expected total amount of %s on account %s on incoming tx to be (total before tx + sent amount). Expected Total %s, but was %s", ticker.getName(), account.getId(), total, newBalance.getTotalAmount()),
                    total.toString(), newBalance.getTotalAmount());
        }
    }

    public Optional<Accounts.Balance> getBalanceBeforeTx(List<Accounts.Balance> balancesBeforeTx, String tickerName) {
        String tickerId = Objects.requireNonNull(harmonize.tickers()
                .list().filter(ticker -> ticker.getName().equalsIgnoreCase(tickerName)).findFirst().orElse(null)).getId();

        return Optional.of(balancesBeforeTx.stream()
                .filter(balance -> balance.asMap()
                        .get("tickerId").toString().equalsIgnoreCase(tickerId))
                .findFirst()).get();
    }

    public Accounts.Balance getNewBalance(String tickerName, String accountId, String accountDomain) throws Exception {
        String tickerId = Objects.requireNonNull(harmonize.tickers()
                .list().filter(ticker -> ticker.getName().equalsIgnoreCase(tickerName)).findFirst().orElse(null)).getId();

        return harmonize.accounts(accountDomain).balances(accountId)
                .filter(balance -> balance.getTickerId().equalsIgnoreCase(tickerId))
                .findFirst().orElseThrow(() -> new Exception(String.format("Balance for %s on account %s not found", tickerName, accountId)));
    }

    public void verifyReservedAmount(TransactionsEnums.Tickers ticker, String accountId, String accountDomain, Timeout timeout) throws Exception {
        boolean condition;
        String message = "";

        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
            BigInteger reserved = getTotalReservedAmountPerSenderAccount(accountId, accountDomain);
            Accounts.Balance newBalance = getNewBalance(ticker.getName(), accountId, accountDomain);

            condition = new BigInteger(newBalance.getReservedAmount()).compareTo(reserved) == 1 ||
                    new BigInteger(newBalance.getReservedAmount()).compareTo(reserved) == 0;
            message = String.format("Reserved amount on account %s. Expected actual reserved amount of %s to be equal or greater then sum of unconfirmed transactions and fees. Expected reserved %s >= But was %s", accountId, ticker.getName(), reserved, newBalance.getReservedAmount());

            LOGGER.info(String.format("Verifying reserved amount of %s to be equal or greater then sum of unconfirmed transactions and fees per sender account %s. Expected reserved %s", ticker.getName(), accountId, reserved));

            if (condition) {
                return;
            }
        }
        throw new RuntimeException(message);
    }

    public void waitForTotalToBeAfterOutgoingTx(TransactionsEnums.Tickers ticker, String accountId, String domainId, BigInteger total, Timeout timeout) throws Exception {
        boolean isTotalEqualOrLess;
        String message = "";

        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {
            Accounts.Balance newBalance = getNewBalance(ticker.getName(), accountId, domainId);

            isTotalEqualOrLess = new BigInteger(newBalance.getTotalAmount()).compareTo(total) == -1 ||
                    new BigInteger(newBalance.getTotalAmount()).compareTo(total) == 0;
            message = String.format("Expected actual total amount of %s on %s to be less then or equal to (old total - amount). Expected total = %s but was %s", ticker.getName(), accountId, total, newBalance.getTotalAmount());

            LOGGER.info(String.format("Verifying total amount of %s on %s after outgoing tx to be less then or equal to (old total - amount). Expected total = %s", ticker.getName(), accountId, total));
            if (isTotalEqualOrLess) {
                return;
            }
        }
        throw new RuntimeException(message);
    }

    public void setQuarantinedAmount(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account, BigInteger amount, String tickerName) {

        List<Accounts.Balance> balances = accountsHelper.getAccountById1(account.getId(), account.getDomainId()).getBalances();

        if (getBalanceBeforeTx(balances, tickerName).isPresent()) {
            memory.set("quarantined_amount", new BigInteger(getBalanceBeforeTx(balances, tickerName)
                    .get().getQuarantinedAmount()).add(amount));
        } else {
            memory.set("quarantined_amount", amount);
        }
    }

    public void releaseQuarantinedTransfers(String accountId, String domain, List<String> transferIds) {
        Accounts.ReleaseQuarantinedTransfers releaseQuarantinedTransfers = Accounts.ReleaseQuarantinedTransfers.builder()
                .accountId(accountId)
                .transferIds(transferIds)
                .submit();

        harmonize.accounts(domain).releaseQuarantinedTransfers(releaseQuarantinedTransfers, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    //todo: move data about available contract methods and their selector codes to json, improve contract methods calling
    // method should take function name as an argument and match it to method selector
    public String createData(String methodSelector, String address) {
        return "0x" + methodSelector + "000000000000000000000000" + address.replace("0x", "") + "0000000000000000000000000000000000000000000000008ac7230489e80000";
    }

    public void mintMetacoTestTokenToAccount(String accountId, String domainId) throws Exception {
        memory.set("max_fee", "900000000000000000");
        memory.set("amount", "0");
        memory.set("tx_kind", TransactionsEnums.TxKinds.ERC20);

        String txOrderId = UUID.randomUUID().toString();
        String toAccountAddress = Objects.requireNonNull(harmonize.accounts(domainId).getLatestAddress(accountId).orElse(null)).getAddress();

        //mint()
        String data = createData("40c10f19", toAccountAddress);
        String contractAddress = "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc";
        makeEthereumTxToAddressDest(accountId, domainId, contractAddress, data, txOrderId);

        Transactions.Transaction transaction = getTransactionByOrderId(domainId, txOrderId, Timeout.minutes(1));
        harmonize.transactions(domainId).waitForTransactionProcessingStatus(transaction.getId(), Set.of(Accounts.ProcessingStatus.Completed.name()), Timeout.minutes(4));

        fetchTransferIdsByTxOrder(txOrderId, domainId);

        Assert.assertTrue(harmonize.accounts(domainId).balances(accountId)
                .anyMatch(balance -> Objects.requireNonNull(harmonize.tickers()
                        .get(balance.getTickerId()).orElse(null)).getName().equalsIgnoreCase("METACO TEST TOKEN")));

        Accounts.ReleaseQuarantinedTransfers releaseQuarantinedTransfers = Accounts.ReleaseQuarantinedTransfers.builder()
                .accountId(accountId)
                .transferIds(fetchTransferIdsByTxOrder(txOrderId, domainId))
                .submit();

        harmonize.accounts(domainId).releaseQuarantinedTransfers(releaseQuarantinedTransfers, Expire.hours(1))
                .waitForSucceed(Timeout.minutes(2))
                .waitForIntentStatus(
                        Set.of(Intents.IntentStatus.Approved,
                                Intents.IntentStatus.Executed),
                        Timeout.minutes(2));
    }

    public String sendETHFromAccountToAddress(String accountId, String accountDomain, String address){
        memory.set("amount", "100000000000000000");
        memory.set("tx_kind", TransactionsEnums.TxKinds.ETHER);
        memory.set("max_fee", "200000000000000000");

        String txOrderId = UUID.randomUUID().toString();
        makeEthereumTxToAddressDest(accountId, accountDomain, address, null, txOrderId);

        Assert.assertTrue(String.format("Transaction Order %s was not in the list", txOrderId), harmonize.transactions(accountDomain).listOrders()
                .anyMatch(tx -> tx.getId().equalsIgnoreCase(txOrderId)));

        return txOrderId;
    }
}
