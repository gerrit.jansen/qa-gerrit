package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Policies;
import harmonizeqa.api.conf.Memory;

import java.util.*;
import java.util.stream.Collectors;

public class PolicyHelper {

    private static final String emptyIfNull(String v){
        return v == null ? "" : v;
    }

    public static Policies.CreatePolicy.Builder<Policies.CreatePolicy> setPropertiesToPolicyBuilder(Map<String, String> map, Policies.CreatePolicy.Builder<Policies.CreatePolicy> policy) {

        for (Map.Entry<String, String> entrySet : map.entrySet()) {
            switch (entrySet.getKey()) {
                case "intentTypes":
                    List<String> values = Arrays.stream(emptyIfNull(map.get("intentTypes"))
                            .replaceAll("\\s+", "")
                            .split(","))
                            .map(t -> {
                                if (t.startsWith("Create"))
                                    return "v0_" + t;
                                return t;
                            }).collect(Collectors.toList());
                    policy.intentTypes(values);
                    break;
                case "scope":
                    policy.scope(map.get("scope"));
                    break;
                case "description":
                    policy.description(map.get("description"));
                    break;
                case "locked":
                    policy.locked(Boolean.parseBoolean(map.getOrDefault("locked", "false")));
                    break;
                case "rank":
                    policy.rank(Integer.parseInt(map.get("rank")));
                    break;
//                case "workflow":
//                    policy.workflow(null);
//                break;
                case "conditionExpression":
                    policy.condition(
                            Policies.PolicyCondition.simpleBuilder()
                                    .expression(map.get("conditionExpression"))
                                    .build()
                    );
                    break;
            }
        }
        return policy;
    }


    public static Map<String, Policies.WorkflowCondition> createSimpleConditionsMap(Memory memory, Map<String, String> map) {

        Map<String, List<Map.Entry<String, String>>> conditionGroups = map.entrySet().stream().collect(Collectors.groupingBy(
                e -> e.getKey().strip().split(" ")[0]
        ));

        Map<String, Policies.WorkflowCondition> workflowConditionMap = new HashMap<>();

        conditionGroups.forEach((conditionName, entries) -> {
            Policies.WorkflowSimpleCondition.Builder builder = Policies.WorkflowCondition.simpleBuilder();

            entries.forEach(e -> {
                if (e.getKey().endsWith("role"))
                    builder.role(e.getValue());
                else if (e.getKey().endsWith("quorum"))
                    builder.quorum(Integer.parseInt(e.getValue()));
            });

            workflowConditionMap.put(conditionName, builder.build());
        });


        memory.set("workflow_conditions", workflowConditionMap);
        return workflowConditionMap;
    }

    public static Map<String, Policies.WorkflowCondition> createConditionsMap(Memory memory, Map<String, String> map, String operator) {

        Map<String, Policies.WorkflowCondition> workflowConditionMap = memory.get("workflow_conditions");

        if (workflowConditionMap == null)
            throw new RuntimeException("Please specify simple workflow conditions first");


        Policies.WorkflowCondition workflowCondition = null;

        if (operator.toLowerCase().contains("or")) {
            workflowCondition = Policies.WorkflowCondition.orBuilder().left(
                    lookupWorkflowCondition(map.get("left"), workflowConditionMap)
            ).right(
                    lookupWorkflowCondition(map.get("right"), workflowConditionMap)
            ).build();
        } else {
            workflowCondition = Policies.WorkflowCondition.andBuilder().left(
                    lookupWorkflowCondition(map.get("left"), workflowConditionMap)
            ).right(
                    lookupWorkflowCondition(map.get("right"), workflowConditionMap)
            ).build();
        }

        workflowConditionMap.put(operator, workflowCondition);
        memory.set("workflow_conditions", workflowConditionMap);

        return workflowConditionMap;
    }

    private static Policies.WorkflowCondition lookupWorkflowCondition(String name, Map<String, Policies.WorkflowCondition> workflowConditionMap) {
        if (name == null)
            return null;

        Policies.WorkflowCondition condition = workflowConditionMap.get(name);
        if (condition == null) {
            throw new RuntimeException("The workflow name " + name + " does not exist");
        }
        return condition;
    }

    public static Policies.CreatePolicy.Builder buildPolicyWorkflow(Memory memory, Map<String, String> map, Policies.CreatePolicy.Builder<Policies.CreatePolicy> policyBuilder) {
        List<String> keys = new ArrayList<>(map.keySet());
        Collections.sort(keys);

        Map<String, Policies.WorkflowCondition> workflowConditionMap = memory.get("workflow_conditions");

        if (workflowConditionMap == null)
            throw new RuntimeException("Please specify simple workflow conditions first");

        List<Policies.WorkflowCondition> workflowConditions = map.entrySet().stream()
                .map(e -> lookupWorkflowCondition(e.getValue(), workflowConditionMap))
                .collect(Collectors.toList());

        policyBuilder.workflow(workflowConditions);
        
        return policyBuilder;
    }

    public static void storeCreatedPolicy(Memory memory, Harmonize harmonize, Policies.CreatePolicy createPolicy, String domainId, IntentData createIntentData) {

        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> wrapper = new EntityWrapper<>(domainId, createPolicy.getId(), "policy", null);
        wrapper.setCreateEntity(createPolicy, createIntentData);
        wrapper.setEntity(harmonize.policies(domainId).get(createPolicy.getId()).orElseThrow());
        memory.setEntityData(wrapper);
    }

    public static void storeUpdatedPolicy(Memory memory, Harmonize harmonize, Policies.UpdatePolicy updatePolicy, String policyId, String domainId, IntentData createIntentData) {

        EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> wrapper = new EntityWrapper<>(domainId, policyId, "policy", null);
        wrapper.setUpdateEntity(updatePolicy, createIntentData);
        wrapper.setEntity(harmonize.policies(domainId).get(policyId).orElseThrow());
        memory.setEntityData(wrapper);
    }

    public static EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> getPolicyByDomainId(Memory memory, String domainId) {
        return memory.<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy>getEntityByDomainId("policy", domainId)
                .orElseThrow(() -> new RuntimeException("The policy from domain " + domainId + " was not set to memory"));
    }

    public static EntityWrapper<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy> getPolicyById(Memory memory, String domainId, String id) {
        return memory.<Policies.Policy, Policies.CreatePolicy, Policies.UpdatePolicy>getEntityDataById("policy", domainId, id)
                .orElseThrow(() -> new RuntimeException("The policy from domain " + domainId + " was not set to memory"));
    }
}
