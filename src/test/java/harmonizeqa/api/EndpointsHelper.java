package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.om.Accounts;
import com.metaco.harmonize.api.om.Endpoints;
import com.metaco.harmonize.api.om.IntentData;
import harmonizeqa.api.conf.Memory;

import java.util.List;
import java.util.stream.Collectors;

import static harmonizeqa.steps.BaseSteps.currentDomain;

public class EndpointsHelper {
    private Harmonize harmonize;
    private Memory memory;

    public EndpointsHelper(Harmonize harmonize, Memory memory) {
        this.harmonize = harmonize;
        this.memory = memory;
    }
    public void storeCreatedEndpoint(String domainId, Endpoints.CreateEndpoint createEndpoint, IntentData createIntentData) {

        EntityWrapper<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint> wrapper = new EntityWrapper<>(domainId, createEndpoint.getId(), "endpoint", null);
        wrapper.setCreateEntity(createEndpoint, createIntentData);
        wrapper.setEntity(harmonize.endpoints(domainId).get(createEndpoint.getId()).orElseThrow());
        wrapper.setLedgerId(createEndpoint.getLedgerId());
        memory.setEntityData(wrapper);
    }

    public EntityWrapper<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint> getEndpointById(String id) {
        return memory.<Endpoints.Endpoint, Endpoints.CreateEndpoint, Endpoints.UpdateEndpoint>getEntityDataById("endpoint", currentDomain(), id).get();
    }
}
