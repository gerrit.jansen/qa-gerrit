package harmonizeqa.api;
import com.metaco.harmonize.api.om.Author;
import com.metaco.harmonize.utils.TypeUtils;

import java.util.Objects;

public interface UserCredentials {

    String getDomainId();

    String getId();

    Key.PrivateKey getPrivateKey();

    Key.PublicKey getPublicKey();

    default Author asAuthor() {
        return Author.builder()
                .id(getId())
                .domainId(getDomainId()).build();
    }

    static UserCredentials from(String domainId, String id, String publicKeyBase64, String privateKey) {
        return new DefaultUserCredentials(
                TypeUtils.assertNotNull(domainId, "domain id"),
                TypeUtils.assertNotNull(id, "user id"),
                Key.fromBase64Str(publicKeyBase64),
                Key.fromPemString(privateKey)
        );
    }

    static UserCredentials from(String domainId, String id, GeneratedKeys user1Keys) {
        return UserCredentials.from(domainId, id, user1Keys.getPublicKey().asBase64Str(), user1Keys.getPrivateKey().asStr());
    }

    class DefaultUserCredentials implements UserCredentials {
        final String domainId;
        final String id;
        final Key.PrivateKey privateKey;
        final Key.PublicKey publicKey;

        public DefaultUserCredentials(String domainId, String id, Key.PublicKey publicKey, Key.PrivateKey privateKey) {
            this.domainId = domainId;
            this.id = id;
            this.privateKey = privateKey;
            this.publicKey = publicKey;
        }

        @Override
        public String getDomainId() {
            return domainId;
        }

        @Override
        public String getId() {
            return id;
        }

        public Key.PrivateKey getPrivateKey() {
            return privateKey;
        }

        public Key.PublicKey getPublicKey() {
            return publicKey;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DefaultUserCredentials that = (DefaultUserCredentials) o;
            return Objects.equals(domainId, that.domainId) && Objects.equals(id, that.id) && Objects.equals(privateKey, that.privateKey) && Objects.equals(publicKey, that.publicKey);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainId, id, privateKey, publicKey);
        }
    }
}
