package harmonizeqa.api;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@SuppressWarnings({"unused"})
public interface Key {


    String asStr();

    String asBase64Str();

    static PublicKey fromBase64File(Path path) throws IOException {
        return fromBase64Str(Files.readString(path));
    }

    static PrivateKey fromPemFile(Path path) throws IOException {
        return fromPemString(Files.readString(path));
    }

    static PublicKey publicKey(String key, String keyBase64) {
        return new DefaultPublicKey(key, keyBase64);
    }

    static PrivateKey privateKey(String key, String keyBase64) {
        return new DefaultPrivateKey(key, keyBase64);
    }

    static PrivateKey fromPemString(String str) {
        return new DefaultPrivateKey(str);
    }

    static PublicKey fromBase64Str(String str) {
        return new DefaultPublicKey(null, str);
    }

    interface PrivateKey extends Key {
    }

    interface PublicKey extends Key {
    }

    class DefaultPublicKey extends DefaultKey implements PublicKey, Serializable {

        public DefaultPublicKey(String content) {
            super(content);
        }

        public DefaultPublicKey(String content, String base64) {
            super(content, base64);
        }
    }

    class DefaultPrivateKey extends DefaultKey implements PrivateKey, Serializable {

        public DefaultPrivateKey(String content) {
            super(content);
        }

        public DefaultPrivateKey(String content, String base64) {
            super(content, base64);
        }
    }

    class DefaultKey implements Key, Serializable {

        String content;
        String base64;

        private DefaultKey() {
        }

        public DefaultKey(String content) {
            this(content, Base64.getEncoder().encodeToString(content.getBytes(StandardCharsets.UTF_8)));
        }

        public DefaultKey(String content, String base64) {
            this.content = content;
            this.base64 = base64;
        }


        private void writeObject(ObjectOutputStream oos)
                throws IOException {
            oos.writeUTF(content);
            oos.writeUTF(base64);

        }

        private void readObject(ObjectInputStream ois)
                throws ClassNotFoundException, IOException {
            this.content = ois.readUTF();
            this.base64 = ois.readUTF();
        }

        @Override
        public String asStr() {
            return content;
        }

        public String asBase64Str() {
            return base64;
        }
    }
}
