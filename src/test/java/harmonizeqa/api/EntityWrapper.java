package harmonizeqa.api;

import com.metaco.harmonize.api.om.Accounts;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.msg.JSON;

import java.util.*;

/**
 * Wraps the actions for a single User/Domain/Policy etc.
 * All must have an id and are keyed on domainId+id
 */
public class EntityWrapper<E, C, U> {

    final UserCredentials userCredentials;

    final String domainId;
    final String id;

    /**
     * a logical type like user, domain, policy
     */
    final String type;
    /**
     *
     */
    Set<String> roles = new HashSet<>();
    Set<String> addresses = new HashSet<>();
    private String ledgerId;
    private String pseudonym;
    private List<Accounts.Balance> balances;

    private E entity; // User
    private C createEntity; // CreateUser
    private IntentData createIntentData;

    private U updateEntity; // UpdateUser
    private IntentData updateIntentData;
    private UpdateActionWrapper updateActionWrapper;

    /**
     * Type user is not the creatuser, updateuser type but a logical type like "user", "domain" given to the wrapper
     */
    public EntityWrapper(String domainId, String id, String type, UserCredentials userCredentials) {
        this.domainId = domainId;
        this.id = id;
        this.type = type;
        this.userCredentials = userCredentials;
        updateActionWrapper = new UpdateActionWrapper<>();

        if (type.equalsIgnoreCase("user") && userCredentials == null)
            throw new RuntimeException("UserCredentials cannot be null here");
    }

    public UserCredentials getUserCredentials() {
        return userCredentials;
    }

    public void setEntity(E entity) {
        this.entity = entity;
    }

    public void addRoles(List<String> roles) {
        this.roles.addAll(roles);
    }

    public void addRoles(String roles) {
        if (roles != null)
            this.roles.addAll(Arrays.asList(roles.split(",")));
    }

    public void addAddress(String address) {
        this.addresses.add(address);
    }

    public void setLedgerId(String ledgerId){
        this.ledgerId = ledgerId;
    }

    public void setBalances(List<Accounts.Balance> balances){
        this.balances = balances;
    }

    public boolean hasAnyRole(String roles) {
        if (roles != null)
            return Arrays.stream(roles.split(","))
                    .anyMatch(this.roles::contains);
        return false;
    }

    public void setCreateEntity(C createEntity, IntentData intentData) {
        this.createEntity = createEntity;
        this.createIntentData = intentData;
    }

    public void setUpdateEntity(U updateEntity, IntentData intentData) {
        this.updateEntity = updateEntity;
        this.updateIntentData = intentData;
    }

    public UpdateActionWrapper getUpdateActionWrapper() {
        return updateActionWrapper;
    }

    public void setUpdateActionWrapper(UpdateActionWrapper updateActionWrapper, IntentData intentData) {
        this.updateActionWrapper = updateActionWrapper;
        this.updateIntentData = intentData;
    }

    public void setUpdateActionWrapper(UpdateActionWrapper updateActionWrapper) {
        this.updateActionWrapper = updateActionWrapper;
    }

    public String getDomainId() {
        return domainId;
    }

    public String getId() {
        return id;
    }

    public E getEntity() {
        return entity;
    }

    public C getCreateEntity() {
        return createEntity;
    }

    public IntentData getCreateIntentData() {
        return createIntentData;
    }

    public U getUpdateEntity() {
        return updateEntity;
    }

    public IntentData getUpdateIntentData() {
        return updateIntentData;
    }

    public String getType() {
        return type;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public Set<String> getAddresses() {
        return addresses;
    }

    public List<Accounts.Balance> getBalances(){
        return this.balances;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityWrapper that = (EntityWrapper) o;
        return Objects.equals(domainId, that.domainId) && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainId, id);
    }

    public boolean hasAlias(String id) {
        if (entity != null)
            return ((JSON) entity).asMap().get("alias").equals(id);

        if (createEntity != null)
            return ((JSON) createEntity).asMap().get("alias").equals(id);


        if (createIntentData != null)
            return ((JSON) createIntentData).asMap().get("alias").equals(id);

        return false;
    }

    @Override
    public String toString() {
        return "EntityWrapper{" +
                "userCredentials=" + userCredentials +
                ", domainId='" + domainId + '\'' +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", roles=" + roles +
                ", entity=" + entity +
                ", createEntity=" + createEntity +
                ", createIntentData=" + createIntentData +
                ", updateEntity=" + updateEntity +
                ", updateIntentData=" + updateIntentData +
                ", updateActionWrapper=" + updateActionWrapper +
                '}';
    }
}
