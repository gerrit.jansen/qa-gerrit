package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.HarmonizeContext;
import harmonizeqa.api.conf.Memory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class GlobalContext {
    private static Logger LOGGER = Logger.getLogger(GlobalContext.class.getName());

    private final Memory mem = new Memory();

    private final Map<String, LocalHarmonizeContext> userContexts = new ConcurrentHashMap<>();

    public GlobalContext() {
        Map<String, Object> configMap = LocalHarmonizeContext.loadConfig();
        userContexts.put("default", LocalHarmonizeContext.loadHMZWithNonCoercivePrimaryDomainData(configMap));
    }

    public Harmonize harmonize() {
        return harmonize(null);
    }

    public LocalHarmonizeContext context() {
        return userContexts.get("default");
    }

    public LocalHarmonizeContext context(String alias) {
        return userContexts.get(alias == null ? "default" : alias);
    }

    public Harmonize harmonize(String alias) {
        if (alias == null)
            alias = "default";

        HarmonizeContext ctx = userContexts.get(alias);
        if (ctx == null) {
            throw new RuntimeException("No context created for " + alias + " please use load(alias, configFile) first");
        }
        return ctx.harmonize;
    }

    /**
     *
     */
    public LocalHarmonizeContext load(String alias, UserCredentials userCredentials) {

        if (alias == null)
            return context();


        LocalHarmonizeContext context = context().switchUser(userCredentials);

        LocalHarmonizeContext prevCtx = userContexts.put(alias, context);

        if (prevCtx != null) {
            LOGGER.warning("A previous context was already loaded for " + alias + ", overwriting old context");
        }
        return context;
    }

    public LocalHarmonizeContext loadWithCoerciveRootDomainData(){
        Map<String, Object> configMap = LocalHarmonizeContext.loadConfig();
        return load("coercive root domain admin", LocalHarmonizeContext.getCoerciveRootDomainUserCredentials(configMap));
    }


    public Memory memory() {
        return mem;
    }


    public static GlobalContext create() {
        return new GlobalContext();
    }
}
