package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.om.Domains;
import com.metaco.harmonize.api.om.Policies;
import harmonizeqa.api.conf.Memory;
import io.cucumber.java.eo.Do;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DomainHelper {
    private Harmonize harmonize;
    private Memory memory;

    public DomainHelper(Harmonize harmonize, Memory memory) {
        this.harmonize = harmonize;
        this.memory = memory;
    }

    /**
     * Helper function that returns the EntityWrapper for a domain by its pseudonym
     */
    public static EntityWrapper<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain> getDomainByPseudonym(Memory memory, String pseudonym) {
        return memory.<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain>getEntityDataByType("domain")
                .filter(d -> d.getPseudonym().equalsIgnoreCase(pseudonym))
                .findFirst().orElseThrow(() -> new RuntimeException("The domain " + pseudonym + " was not set to memory"));
    }

    public Domains.UpdateDomain setUpdateDomainProperties(Map<String, String> map, EntityWrapper<Domains.Domain, Domains.CreateDomain, Domains.UpdateDomain> domain){
        Domains.UpdateDomain updateDomain = harmonize.domains().get(domain.getId()).get().asUpdate();

        for (Map.Entry<String, String> entrySet : map.entrySet()) {
            switch (entrySet.getKey()) {
                case "alias":
                    updateDomain.setAlias(map.get("alias"));
                    break;

                case "description":
                    updateDomain.setDescription(map.get("description"));
                    break;
            }
        }
        return updateDomain;
    }

    public Domains.UpdateDomainPermissions setUpdatePermissions(Map<String, List<String>> map, Domains.UpdateDomainPermissions updateDomainPermissions){

        Domains.ReadAccess readAccess = updateDomainPermissions.getPermissions().getReadAccess();
        Domains.ReadAccess.Builder readAccessBuilder = setCurrentReadAccess(readAccess);

        for (Map.Entry<String, List<String>> entrySet : map.entrySet()) {
            switch (entrySet.getKey()) {
                case "domains":
                    readAccessBuilder.domains(map.get("domains"));
                    break;
                case "users":
                    readAccessBuilder.users(map.get("users"));
                    break;
                case "endpoints":
                    readAccessBuilder.endpoints(map.get("endpoints"));
                    break;
                case "accounts":
                    readAccessBuilder.accounts(map.get("accounts"));
                    break;
                case "transactions":
                    readAccessBuilder.transactions(map.get("transactions"));
                    break;
                case "events":
                    readAccessBuilder.events(map.get("events"));
                    break;
                case "requests":
                    readAccessBuilder.requests(map.get("requests"));
                    break;
                case "policies":
                    readAccessBuilder.policies(map.get("policies"));
                    break;
            }
        }

        Domains.Permissions permissions = Domains.Permissions.builder().readAccess(readAccessBuilder.build()).build();
        updateDomainPermissions.setPermissions(permissions);
        memory.set("update_permissions", updateDomainPermissions);
        return  updateDomainPermissions;
    }

    private Domains.ReadAccess.Builder setCurrentReadAccess(Domains.ReadAccess readAccess){
        return Domains.ReadAccess.builder()
                .accounts(readAccess.getAccounts())
                .domains(readAccess.getDomains())
                .endpoints(readAccess.getEndpoints())
                .events(readAccess.getEvents())
                .policies(readAccess.getPolicies())
                .requests(readAccess.getRequests())
                .transactions(readAccess.getTransactions())
                .users(readAccess.getUsers());
    }

}
