package harmonizeqa.api;

import com.metaco.harmonize.Harmonize;
import com.metaco.harmonize.api.Timeout;
import com.metaco.harmonize.api.om.Accounts;
import com.metaco.harmonize.api.om.IntentData;
import com.metaco.harmonize.api.om.Users;
import harmonizeqa.api.conf.Memory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static harmonizeqa.steps.BaseSteps.context;
import static harmonizeqa.steps.BaseSteps.currentDomain;

public class AccountsHelper {
    private Harmonize harmonize;
    private Memory memory;

    public AccountsHelper(Harmonize harmonize, Memory memory) {
        this.harmonize = harmonize;
        this.memory = memory;
    }

    public static void waitForAccountAddress(Harmonize harmonize, String domainId, String accountId, Timeout timeout) {

        long start = System.currentTimeMillis();

        while ((System.currentTimeMillis() - start) < timeout.toMillis()) {

            Optional<Accounts.Account> accountOpt = harmonize.accounts(domainId).get(accountId);
            if (accountOpt.isPresent()) {

                if (harmonize.accounts(domainId).getLatestAddress(accountId).isPresent())
                    return;
            }
        }

        throw new RuntimeException("Timed out while waiting for " + accountId + " account address " + harmonize.accounts(domainId).get(accountId).get());
    }

    /**
     * Helper function that will save the new CreateAccount + its Account representation if available to memory
     * as an EntityWrapper
     */
    public void storeCreatedAccount(String domainId, Accounts.CreateAccount createAccount, IntentData createIntentData, String accountPseudonym) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> wrapper = new EntityWrapper<>(domainId, createAccount.getId(), "account", null);
        wrapper.setPseudonym(accountPseudonym);
        wrapper.setCreateEntity(createAccount, createIntentData);
        wrapper.setEntity(harmonize.accounts(domainId).get(createAccount.getId()).orElseThrow());
        wrapper.setLedgerId(createAccount.getLedgerId());
        memory.setEntityData(wrapper);
    }

    public void addAccount(String domainId, Accounts.Account account, String accountPseudonym) {
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> wrapper = new EntityWrapper<>(domainId, account.getId(), "account", null);
        Accounts.Account accountEntity = harmonize.accounts(domainId).get(account.getId()).orElseThrow();
        wrapper.setPseudonym(accountPseudonym);
        wrapper.setEntity(accountEntity);
        wrapper.setLedgerId(accountEntity.getLedgerId());
        memory.setEntityData(wrapper);
    }

    public void addPseudonymToAccount(String id, String accountPseudonym){
        EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> wrapper = getAccountById(id);
        wrapper.setPseudonym(accountPseudonym);
        memory.setEntityData(wrapper);
    }

    public void storeCurrentBalances(EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> account, List<Accounts.Balance> balances) {
        memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataById("account", account.getDomainId(), account.getId())
                .orElseThrow(() -> new RuntimeException("The account " + account.getId() + " was not set to memory")).setBalances(balances);
    }

    public void setAccountAddresses(String accountId, String addresses) {
        memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataById("account", currentDomain(), accountId)
                .orElseThrow(() -> new RuntimeException("The account " + accountId + " was not set to memory")).addAddress(addresses);
    }

    public Accounts.Account getAccountByAddress(String addresses) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataByAddress("account", currentDomain(), addresses)
                .orElseThrow(() -> new RuntimeException("The account with address " + addresses + " was not set to memory")).getEntity();
    }

    public  EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> getAccountById(String id) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataById("account", currentDomain(), id)
                .orElseThrow(() -> new RuntimeException("The account with id " + id + " was not set to memory"));
    }

    public  EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> getAccountById1(String id, String domainId) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataById("account", domainId, id)
                .orElseThrow(() -> new RuntimeException("The account with id " + id + " was not set to memory"));
    }

    public EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount> getAccountByPseudonym(String pseudonym) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataByPseudonym("account", pseudonym)
                .orElseThrow(() -> new RuntimeException(String.format("The account %s was not set to memory", pseudonym)));
    }

    public  boolean isAccountPresent(String id) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntityDataById("account", currentDomain(), id)
                .isPresent();
    }

    public List <EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>> getAccountsByLedgerId(String ledgerId) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntitiesLedgerId("account", currentDomain(), ledgerId)
                .collect(Collectors.toList());
    }

    public List <EntityWrapper<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>> getAccountsByDomainId(String ledgerId) {
        return memory.<Accounts.Account, Accounts.CreateAccount, Accounts.UpdateAccount>getEntitiesByDomainId("account", currentDomain())
                .collect(Collectors.toList());
    }
}
