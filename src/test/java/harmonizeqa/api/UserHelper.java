package harmonizeqa.api;

import com.metaco.harmonize.api.om.Users;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class UserHelper {

    public static Users.UpdateUser setUserProperties(Users.UpdateUser user, Map<String, String> map) {

        for (Map.Entry<String, String> entrySet : map.entrySet()) {
            switch (entrySet.getKey()) {
                case "roles":
                    List<String> values = Arrays.asList(map.get("roles").replaceAll("\\s+", "").split(","));
                    user.setRoles(values);
                    break;
                case "alias":
                    user.setAlias(map.get("alias"));
                    break;
                case "description":
                    user.setDescription(map.get("description"));
                    break;
            }
        }
        return user;
    }

    public static Users.CreateUser.Builder<Users.CreateUser> setUserProperties(Map<String, String> map) {
        Users.CreateUser.Builder<Users.CreateUser> user = Users.CreateUser.builder();

        for (Map.Entry<String, String> entrySet : map.entrySet()) {
            switch (entrySet.getKey()) {
                case "roles":
                    List<String> values = Arrays.asList(map.get("roles").replaceAll("\\s+", "").split(","));
                    user.roles(values);
                    break;
                case "alias":
                    user.alias(map.get("alias"));
                    break;
                case "description":
                    user.description(map.get("description"));
                    break;
                case "locked":
                    user.locked(Boolean.parseBoolean(map.getOrDefault("locked", "false")));
                    break;
            }
        }
        return user;
    }
}

