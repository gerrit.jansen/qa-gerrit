package harmonizeqa;

import com.metaco.harmonize.api.om.Tickers;

public class TransactionsEnums {
    public enum Tickers {

        BITCOIN("Bitcoin testnet", "bitcoin-testnet", "Native"),
        ETHER("Ethereum testnet rinkeby", "ethereum-testnet-rinkeby", "Native"),
        XRP("XRPL testnet", "xrpl-testnet", "Native"),
        MTT("METACO TEST TOKEN", "ethereum-testnet-rinkeby", com.metaco.harmonize.api.om.Tickers.TickerKind.Contract.name(), "0xb2f701a94d864b131fd47c9cf4563e6298afdbfc"),
        MTT1("METACO TEST TOKEN 1", "ethereum-testnet-rinkeby", com.metaco.harmonize.api.om.Tickers.TickerKind.Contract.name(), "0x8a75f985d5316b1a98bc11fe5364abbad55e1c7a");

        private String name;
        private String ledgerId;
        private String kind;
        private String address;

        Tickers(String name, String ledgerId, String kind) {
            this(name, ledgerId, kind, null);
        }

        Tickers(String name, String ledgerId, String kind, String address) {
            this.name = name;
            this.ledgerId = ledgerId;
            this.kind = kind;
            this.address = address;
        }

        public String getAddress() {
            return address;
        }

        public String getName() {
            return name;
        }

        public String getLedgerId() {
            return ledgerId;
        }

        public String getKind() {
            return kind;
        }

        public static Tickers byName(String name) {
            for (Tickers b : Tickers.values()) {
                if (b.name.equalsIgnoreCase(name.trim())) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + name + "'");
        }
    }

    public enum TxDirections {
        INCOMING("incoming"),
        OUTGOING("outgoing");

        String description;

        public String getDescription() {
            return description;
        }

        TxDirections(String description) {
            this.description = description;
        }

        public static TxDirections fromString(String description) {
            for (TxDirections b : TxDirections.values()) {
                if (b.description.equalsIgnoreCase(description.trim())) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + description + "'");
        }
    }

    public enum TxKinds {
        BTC("BTC", "amount"),
        ETHER("ETH", "amount"),
        ERC20("ERC20", "amountInData"),
        ERC20_ETHER("ERC20 mixed", "amount"),
        XRPL("XRPL", "amount");

        String description;
        String amountInPayload;

        public String getDescription() {
            return description;
        }

        public String getAmountInPayload() {
            return amountInPayload;
        }

        TxKinds(String description, String amountInPayload) {
            this.description = description;
            this.amountInPayload = amountInPayload;
        }

        public TxKinds fromString(String description) {
            for (TxKinds b : TxKinds.values()) {
                if (b.description.equalsIgnoreCase(description.trim())) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + description + "'");
        }
    }
}