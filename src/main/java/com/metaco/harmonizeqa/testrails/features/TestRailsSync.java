package com.metaco.harmonizeqa.testrails.features;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.*;

import java.io.File;
import java.nio.file.Path;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <pre>
 * When run: this class will:
 *   1 ) go through each feature file in src/test/resources/hamornizeq
 *   2 ) for each scenario in each feature file, it creates a TestCase
 *   3 ) after that it searches for the TEST-harmonizeqa.RunCucumberTest.xml file
 *   4 ) creates a test run
 *   5 ) and match the results for the output with each scenario in the feature files.
 * </pre>
 * <br/>
 * <p>
 * Configuration:
 * <p>
 * We expect the following environment variables
 * <table>
 *     <tr><td>TEST_RAIL_URL</td></tr>
 *     <tr><td>TEST_RAIL_USER</td></tr>
 *     <tr><td>TEST_RAIL_PASSWORD</td></tr>
 *    <tr><td>TEST_RAIL_PROJECT_ID</td></tr>
 * </table>
 * <p>
 * Usage:
 * <pre>
 *     mvn compile exec:java -Dexec.mainClass=com.metaco.harmonizeqa.testrails.features.TestRailsSync
 * </pre>
 */
public class TestRailsSync {

    public static final Config config = Config.createFromEnv();

    public static void main(String[] arg) {
        System.out.println("Sync features");
        List<Scenario.ScenarioEntity> scenarios = syncFeatures(Path.of("src/test/resources/features"));

        System.out.println("Synced " + scenarios.size() + " scenarios with test cases");
        System.out.println("Sync test results");
        syncTestResults(Path.of("target/surefire-reports/TEST-harmonizeqa.RunCucumberTest.xml"), scenarios);
        System.out.println("Done");
    }

    private static void syncTestResults(Path resultXmlFile, List<Scenario.ScenarioEntity> scenarios) {

        TestRail testRail = createTestRail();

        try {
            List<TestResult> results = TestResultsParser.parseResults(resultXmlFile.toString(), scenarios)
                    .stream().filter(r -> r.getScenario() != null).collect(Collectors.toList());

            int projectId = Integer.parseInt(config.getProjectId());

            Suite suite = getMasterSuite(testRail, projectId);

            Run run = new Run();
            run.setSuiteId(suite.getId())
                    .setName("[" + GitUtil.getCurrentGitBranch() + "] Harmonize QA Test Run - " + Instant.now().toString())
                    .setCaseIds(
                            results.stream()
                                    .map(r -> ((Scenario.ScenarioEntity) r.getScenario()).getId())
                                    .map(Integer::valueOf).collect(Collectors.toList()));

            run = testRail.runs().add(projectId, run).execute();

            List<ResultField> customResultFields = testRail.resultFields().list().execute();

            for (TestResult result : results) {
                Scenario.ScenarioEntity scenarioEntity = (Scenario.ScenarioEntity) result.getScenario();

                int statusId = result.isOk() ? 1 : 5; // 5 is Failed

                System.out.println("Sync Result: " + result.getScenario().getFeatureName() + ", isOk: " + result.isOk());
                testRail.results().addForCase(
                        run.getId(),
                        Integer.parseInt(scenarioEntity.getId()), // test case id, each scenario represents a test case
                        new Result().setStatusId(statusId).setComment(
                                "```\n" +
                                        "JOB_URL: "
                                        + System.getenv("CI_JOB_URL") +
                                        "\n" + messageToMarkDown(result.getMessage()) +
                                        "\n```"
                        )
                        , customResultFields).execute();
            }

            testRail.runs().close(run.getId()).execute();

        } catch (Throwable e) {
            throw new RuntimeException(e);
        }


    }

    private static String messageToMarkDown(String message) {
        return message.replace("&amp#27;", "\n")
                .replaceAll("\\[(\\d+)m", " ");
    }

    public static List<Scenario.ScenarioEntity> syncFeatures(Path dir) {

        List<Scenario> scenarios = ScenarioParser.parseFeatureFiles(dir).collect(Collectors.toList());

        int projectId = Integer.parseInt(config.getProjectId());

        TestRail testRail = createTestRail();

        Suite suite = getMasterSuite(testRail, projectId);
        List<CaseField> customCaseFields = testRail.caseFields().list().execute();

        List<Priority> priorities = testRail.priorities().list().execute();
        List<CaseType> types = testRail.caseTypes().list().execute();

        List<Case> cases = testRail.cases().list(projectId, suite.getId(), customCaseFields).execute();
        Map<String, Case> caseTitleLookup = new HashMap<>();
        cases.forEach(c -> caseTitleLookup.put(c.getTitle(), c));

        List<Section> sections = testRail.sections().list(projectId, suite.getId()).execute();
        Map<String, Section> sectionsMap = new HashMap<>();

        if (sections.size() == 0) {
            testRail.sections().add(projectId, new Section().setSuiteId(suite.getId()).setName("harmonize-qa")).execute();
        }

        sections = testRail.sections().list(projectId, suite.getId()).execute();
        sections.forEach(s -> sectionsMap.put(sectionKey(s), s));

        List<Scenario.ScenarioEntity> scenariosEntities = new ArrayList<>();

        for (Scenario scenario : scenarios) {

            if (scenario.getName().isBlank())
                continue;

            Case c = caseTitleLookup.get(scenario.getName());


            Section section = findSection(sections, sectionsMap, scenario);

            scenariosEntities.add(
                    c == null ? createNewScenario(testRail, types, priorities, customCaseFields, section, scenario, customCaseFields)
                            : updateScenario(testRail, types, priorities, customCaseFields, c, scenario, customCaseFields)
            );
        }

        return scenariosEntities;
    }

    /**
     * Returns the first Section in sectionsMap that has a key equal to a tag in the Scenario,
     * otherwise the first section is returned.
     */
    private static Section findSection(List<Section> sections, Map<String, Section> sectionsMap, Scenario scenario) {

        Set<String> keyedTags = scenario.getFeatureTags().stream().map(TestRailsSync::sectionKey).collect(Collectors.toSet());

        for (Map.Entry<String, Section> entry : sectionsMap.entrySet()) {
            if (keyedTags.contains(entry.getKey()))
                return entry.getValue();
        }

        return sections.get(0);
    }

    private static String sectionKey(Section s) {
        return sectionKey(s.getName());
    }

    private static String sectionKey(String s) {
        return s.toLowerCase().replaceAll("(\\s)+|([.,:\\-_\\[\\]()\"';])+", " ").replace("  ", " ");
    }

    private static Suite getMasterSuite(TestRail testRail, int projectId) {
        List<Suite> suites = testRail.suites().list(projectId).execute().stream().filter(s -> s.getName().equalsIgnoreCase("Master")).collect(Collectors.toList());

        if (suites.size() == 0)
            throw new RuntimeException("No test suite with name Master was found");

        return suites.get(0);
    }

    private static TestRail createTestRail() {

        return TestRail.builder(
                config.getTestRailUrl(),
                config.getTestRailUser(),
                config.getTestRailPassword())
                .applicationName("Harmonize").build();
    }

    private static Scenario.ScenarioEntity createNewScenario(TestRail testRail, List<CaseType> types, List<Priority> priorities, List<CaseField> caseFields, Section section, Scenario scenario, List<CaseField> customCaseFields) {

        Case c = new Case()
                .setTitle(scenario.getName());

        setTypeInformation(types, priorities, caseFields, c, scenario);

        addSteps(c, scenario);
        addCustomFields(c, scenario);

        Case testCase = testRail.cases().add(
                section.getId(),
                c,
                customCaseFields).execute();

        return new Scenario.ScenarioEntity(String.valueOf(testCase.getId()), scenario);
    }

    private static void addCustomFields(Case c, Scenario scenario) {

        c.addCustomField("feature_tags", scenario.getFeatureTags().stream()
                .map(t -> "@" + t)
                .collect(Collectors.joining("  \n")));

        c.addCustomField("feature_file", removeTestFilePrefix(scenario.getFile()));
    }

    private static Scenario.ScenarioEntity updateScenario(TestRail testRail, List<CaseType> types, List<Priority> priorities, List<CaseField> caseFields, Case c, Scenario scenario, List<CaseField> customCaseFields) {

        System.out.println("Update Scenario: " + c.getTitle());

        setTypeInformation(types, priorities, caseFields, c, scenario);
        addSteps(c, scenario);
        addCustomFields(c, scenario);

        c = testRail.cases().update(c, customCaseFields).execute();

        return new Scenario.ScenarioEntity(String.valueOf(c.getId()), scenario);
    }

    private static void addSteps(Case c, Scenario scenario) {
        List<Field.Step> caseSteps = scenario.getSteps().stream()
                .map(s -> {
                    Field.Step step = new Field.Step();
                    step.setContent("**" + s.getKey().strip() + "**: " + s.getDescription());
                    step.setExpected(null);
                    return step;
                }).collect(Collectors.toList());

        c.addCustomField("steps_separated", caseSteps);
        c.addCustomField("steps", "");
    }

    private static void setTypeInformation(List<CaseType> types, List<Priority> priorities, List<CaseField> caseFields, Case c, Scenario scenario) {
        if (scenario.tagsLike("smoke")) {
            c.setTypeId(findTypeIdFor("Smoke & Sanity", types));
            c.setPriorityId(findPriorityIdFor("High", priorities));
        } else {
            c.setTypeId(findTypeIdFor("Functional", types));
            c.setPriorityId(findPriorityIdFor("Medium", priorities));
        }

        // 1 Automated, 2 Manual
        c.addCustomField("execution_type", 1);
    }

    private static int findCustomCaseFieldIdFor(List<CaseField> caseFields, String name) {
        for (CaseField field : caseFields) {
            if (field.getName().equalsIgnoreCase(name))
                return field.getId();

        }

        return 0;
    }

    private static String safeString(String key) {
        return key == null ? "" : key;
    }

    private static String removeTestFilePrefix(String file) {
        return new File(file).getName();
    }

    private static Integer findPriorityIdFor(String field, List<Priority> priorities) {
        for (Priority p : priorities) {
            if (p.getName().equalsIgnoreCase(field))
                return p.getId();
        }

        return 0;
    }

    private static Integer findTypeIdFor(String field, List<CaseType> types) {
        for (CaseType type : types) {
            if (type.getName().equalsIgnoreCase(field))
                return type.getId();
        }

        return 0;
    }
}
