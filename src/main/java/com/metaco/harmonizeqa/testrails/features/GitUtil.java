package com.metaco.harmonizeqa.testrails.features;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GitUtil {

    public static String getCurrentGitBranch() {
        try {
            Process process = Runtime.getRuntime().exec("git rev-parse --abbrev-ref HEAD");
            process.waitFor();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            return reader.readLine();
        } catch (Exception exc) {
            return System.getenv("CI_COMMIT_BRANCH");
        }
    }
}
