package com.metaco.harmonizeqa.testrails.features;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TestResultsParser {

    private static final Pattern FIND_HASH_NUMBER = Pattern.compile("#\\s*\\d*$");

    public static List<TestResult> parseResults(String xmlFile, List<? extends Scenario> scenarios) throws ParserConfigurationException, IOException, SAXException {
        try (FileInputStream input = new FileInputStream(xmlFile)) {
            return parseResults(input, scenarios);
        }

    }

    public static List<TestResult> parseResults(InputStream xmlFile, List<? extends Scenario> scenarios) throws ParserConfigurationException, IOException, SAXException {

        Map<String, Scenario> scenarioNameMap = new HashMap<>();
        scenarios.forEach(s -> scenarioNameMap.put(s.getName(), s));

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.parse(xmlFile);

        Node root = doc.getFirstChild();
        if (!root.getNodeName().equalsIgnoreCase("testsuite")) {
            throw new RuntimeException("Test results file does not contain a root testsuite tag");
        }

        List<Node> children = getTestResults(root);
        return children.stream().map(node -> createTestResultFromNode(scenarioNameMap, node)).collect(Collectors.toList());

    }

    private static TestResult createTestResultFromNode(Map<String, Scenario> scenarioNameMap, Node node) {

        NamedNodeMap attributes = node.getAttributes();
        String testCaseName = attributes.getNamedItem("name").getTextContent();

        testCaseName = FIND_HASH_NUMBER.matcher(testCaseName).replaceAll("").strip();

        boolean ok = !hasErrorChild(node);

        return new TestResult(
                scenarioNameMap.get(testCaseName),
                ok,
                node.getTextContent()
        );

    }

    private static boolean hasErrorChild(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equalsIgnoreCase("error"))
                return true;
        }

        return false;
    }

    private static List<Node> getTestResults(Node doc) {
        List<Node> nodes = new ArrayList<>();

        NodeList children = doc.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String nodeName = child.getNodeName();
            if (nodeName.equalsIgnoreCase("testcase")) {
                nodes.add(child);
            }
        }
        return nodes;
    }
}
