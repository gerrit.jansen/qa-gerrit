package com.metaco.harmonizeqa.testrails.features;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Scenario {
    final Set<String> featureTags;
    final Set<String> tags;
    final String file;
    final String name;
    final String featureName;
    final List<Step> steps;

    public Scenario(String file, String name, String featureName, Collection<String> featureTags, Collection<String> tags, List<Step> steps) {
        this.featureTags = featureTags instanceof Set ? (Set<String>) featureTags : new HashSet<>(featureTags);
        this.tags = tags instanceof Set ? (Set<String>) tags : new HashSet<>(tags);
        this.file = file;
        this.name = name;
        this.featureName = featureName;
        this.steps = steps;
    }

    public String readContent() {
        try {
            return Files.readString(Path.of(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getFile() {
        return file;
    }

    public String getName() {
        return name;
    }

    public String getFeatureName() {
        return featureName;
    }

    public Set<String> getFeatureTags() {
        return featureTags;
    }

    public Set<String> getTags() {
        return tags;
    }

    public List<Step> getSteps() {
        return steps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scenario scenario = (Scenario) o;
        return Objects.equals(file, scenario.file) && Objects.equals(name, scenario.name) && Objects.equals(featureName, scenario.featureName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(file, name, featureName);
    }

    public boolean tagsLike(String likeFragment) {
        for (String tag : tags) {
            if (tag.contains(likeFragment))
                return true;
        }

        return false;
    }

    public boolean featureTagsLike(String likeFragment) {
        for (String tag : featureTags) {
            if (tag.contains(likeFragment))
                return true;
        }

        return false;
    }


    public static class Step {

        final String key;
        final String description;
        final String expectedResult;
        final String docString;

        public Step(String key, String description, String expectedResult, String docString) {
            this.key = key;
            this.description = description;
            this.expectedResult = expectedResult;
            this.docString = docString;
        }

        public String getKey() {
            return key;
        }

        public String getDocString() {
            return docString;
        }

        public String getDescription() {
            return description;
        }

        public String getExpectedResult() {
            return expectedResult;
        }

        public String toString() {
            return key + " " + description + " \n" + docString;
        }
    }

    public static class ScenarioEntity extends Scenario {

        final String id;

        public ScenarioEntity(String id, Scenario scenario) {
            super(scenario.getFile(), scenario.getName(), scenario.getFeatureName(), scenario.getFeatureTags(), scenario.getTags(), scenario.getSteps());
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }
}
