package com.metaco.harmonizeqa.testrails.features;

public class TestResult {

    final Scenario scenario;
    final boolean ok;
    final String message;

    public TestResult(Scenario scenario, boolean ok, String message) {
        this.scenario = scenario;
        this.ok = ok;
        this.message = message;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public boolean isOk() {
        return ok;
    }

    public String getMessage() {
        return message;
    }
}
