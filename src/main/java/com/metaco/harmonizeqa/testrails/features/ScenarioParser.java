package com.metaco.harmonizeqa.testrails.features;

import io.cucumber.gherkin.Gherkin;
import io.cucumber.messages.IdGenerator;
import io.cucumber.messages.Messages;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScenarioParser {
    private final static IdGenerator idGenerator = new IdGenerator.Incrementing();

    public static boolean isFeatureFile(Path p) {
        return !p.toFile().isDirectory() && p.toString().endsWith(".feature");
    }

    public static Stream<Scenario> parseFeatureFiles(Path dir) {
        try {
            return Files.walk(dir, FileVisitOption.FOLLOW_LINKS)
                    .filter(ScenarioParser::isFeatureFile)
                    .flatMap(ScenarioParser::parseFeature);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Stream<Scenario> parseFeature(Path file) {

        List<Messages.Envelope> envelopes = Gherkin.fromPaths(
                List.of(file.toString()),
                false, true, false, idGenerator)
                .collect(Collectors.toList());

        if (envelopes.size() == 0)
            return Stream.empty();


        Messages.GherkinDocument doc = envelopes.get(0).getGherkinDocument();
        Messages.GherkinDocument.Feature feature = doc.getFeature();

        Set<String> featureTags = new HashSet<>();

        List<Messages.GherkinDocument.Feature.Tag> featureGerkinTags = feature.getTagsList();
        if (featureGerkinTags != null && featureGerkinTags.size() > 0) {
            featureTags.addAll(featureGerkinTags.stream().map(t -> t.getName().toLowerCase().replace("@", "")).collect(Collectors.toList()));
        }

        List<Messages.GherkinDocument.Feature.Scenario> scenarios = feature.getChildrenList().stream().map(Messages.GherkinDocument.Feature.FeatureChild::getScenario).collect(Collectors.toList());

        List<Scenario> resultScenarios = new ArrayList<>();

        for (Messages.GherkinDocument.Feature.Scenario scenario : scenarios) {
            if (scenario.getName() == "")
                continue;

            Set<String> tags = new HashSet<>();
            List<Messages.GherkinDocument.Feature.Tag> scenarioTags = scenario.getTagsList();
            if (scenarioTags != null && scenarioTags.size() > 0) {
                tags.addAll(scenarioTags.stream().map(t -> t.getName().toLowerCase().replace("@", "")).collect(Collectors.toList()));
            }

            List<Scenario.Step> steps = new ArrayList<>();

            for (Messages.GherkinDocument.Feature.Step step : scenario.getStepsList()) {
                steps.add(new Scenario.Step(
                        step.getKeyword(), step.getText(), "", step.getDocString().getContent()
                ));
            }

            resultScenarios.add(
                    new Scenario(
                            file.toAbsolutePath().toString(),
                            scenario.getName(),
                            feature.getName(),
                            featureTags,
                            tags,
                            steps
                    ));
        }

        return resultScenarios.stream();
    }

}
