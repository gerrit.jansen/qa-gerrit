package com.metaco.harmonizeqa.testrails.features;

public class Config {

    final String testRailUrl;
    final String testRailUser;
    final String testRailPassword;
    final String projectId;

    public Config(String testRailUrl, String testRailUser, String testRailPassword, String projectId) {
        this.testRailUrl = testRailUrl;
        this.testRailUser = testRailUser;
        this.testRailPassword = testRailPassword;
        this.projectId = projectId;
    }

    public String getTestRailUrl() {
        return testRailUrl;
    }

    public String getTestRailUser() {
        return testRailUser;
    }

    public String getTestRailPassword() {
        return testRailPassword;
    }

    public String getProjectId() {
        return projectId;
    }

    public static Config createFromEnv() {
        return new Config(
                env("TEST_RAIL_URL"),
                env("TEST_RAIL_USER"),
                env("TEST_RAIL_PASSWORD"),
                env("TEST_RAIL_PROJECT_ID"));
    }

    private static String env(String name) {
        String v = System.getenv(name);

        v = v == null ? "" : v.strip();

        if (v.length() == 0) {
            throw new RuntimeException("Required environment variable " + name + " was not defined");
        }

        return v;
    }
}
