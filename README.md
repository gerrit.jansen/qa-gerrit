# Harmonize QA

This project uses Cucumber to run "scenarios" which test the Harmonize system.

## Pre-requisites

A valid maven repository configuration is required.

Please copy the `.m2/.settings.xml` file to your local `~/.m2/settings.xml`


## Configuration

Use the `HARMONIZE_CONF` environment variable to set the configuration file to use.

For information on configuration
see https://gitlab.internal.m3t4c0.com/silo/platform/sdk-java/-/blob/master/sdk/docs/Getting-Started.md

### Test Rail integration

The test rail sync will look for each scenario in each feature file and create
a corresponding Test Case in the master Test Suite.

|Name       |Description        |
|-----------|-------------------|
|TEST_RAIL_URL| The test rail project url |
|TEST_RAIL_USER| The user for login |
|TEST_RAIL_PASSWORD| The login password |
|TEST_RAIL_PROJECT_ID| If only one project use 1|


