#!/usr/bin/env bash

CMD="$1"
shift

export GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

case "$CMD" in
test)

  confg=${HARMONIZE_CONF:-harmonize-qa-stable.json}
  tags=${CUCUMBER_FILTER_TAGS:-"@smoke"}

  echo "Running tests with $conf and $tags"
  mvn $MAVEN_CLI_OPTS clean test -DHARMONIZE_CONF=${conf} -Dcucumber.filter.tags="${tags}"

  # mvn $MAVEN_CLI_OPTS compile exec:java -Dexec.mainClass=com.metaco.harmonizeqa.testrails.features.TestRailsSync
  ;;
*)
  echo "$0 test"
  exit 1
  ;;
esac
